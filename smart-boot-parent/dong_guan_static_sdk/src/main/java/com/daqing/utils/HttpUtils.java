package com.daqing.utils;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.log.StaticLog;
import com.alibaba.fastjson.JSON;
import com.daqing.config.DongGuanStaticConfig;

import java.util.HashMap;
import java.util.Map;

public class HttpUtils {

    static Map<String, String> mapHead = null;

    static {
        mapHead = new HashMap<>();
        mapHead.put("Content-Type", "application/json; charset=UTF-8");
        mapHead.put("Accept", "application/json");
    }


    public static String post(String url , Map<String,Object> map){
        //获取当前环境
        if("1".equals(DongGuanStaticConfig.dongGuanIsEnvironment)){
            StaticLog.info("当前环境为开发环境");
        }else if("2".equals(DongGuanStaticConfig.dongGuanIsEnvironment)){
            StaticLog.info("当前环境为生产环境");
        }

        //构建签名
        if(StringUtils.isEmpty(DongGuanStaticConfig.dongGuanRequestKey)){
            StaticLog.error("东莞静态交通requestKey为空");
            return null;
        }

        //公共参数：comId，appId，sign
        map.put("comId", DongGuanStaticConfig.dongGuanComId);
        map.put("appId",DongGuanStaticConfig.dongGuanAppId);
        String sign = StringUtils.sign(map, DongGuanStaticConfig.dongGuanRequestKey);
        map.put("sign",sign);
        String paramStr = JSON.toJSONString(map);
        StaticLog.info("东莞静态交通请求url:{},请求参数:{}",url,paramStr);

        String result = null;
        //开始请求
        try {
            HttpRequest httpRequest = HttpUtil.createPost(url).timeout(8000);
            HttpResponse httpResponse = httpRequest.addHeaders(mapHead).body(paramStr).execute();
            result = httpResponse.body();
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }


}

package com.daqing.utils;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.digest.MD5;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class StringUtils {


    /**
     * 判断为空
     * @param str
     * @return
     */
    public static boolean isEmpty(String str){
        if(str == null){
            return true;
        }
        if(str.length() == 0){
            return true;
        }
        if("".equals(str)){
            return true;
        }
        if("null".equals(str)){
            return true;
        }
        return false;
    }

    /**
     * 判断不为空
     * @param str
     * @return
     */
    public static boolean isNotEmpty(String str){
        return !isEmpty(str);
    }


    /**
     * 构建url
     * @param host
     * @param url
     * @return
     */
    public static String buildUrl(String host , String url){
        return new StringBuilder(host).append(url).toString();
    }



    /**
     * Map转String
     *
     * @param map
     * @return
     */
    public static String getMapToString(Map<String, Object> map) {
        Set<String> keySet = map.keySet();
        //将set集合转换为数组
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        //给数组排序(升序)
        Arrays.sort(keyArray);
        //因为String拼接效率会很低的，所以转用StringBuilder
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keyArray.length; i++) {
            // 参数值为空，则不参与签名 这个方法trim()是去空格
            sb.append(keyArray[i]).append("=").append(String.valueOf(map.get(keyArray[i])+"&".trim()));
        }
        return sb.toString();
    }


    /**
     * 构建签名
     * @param map
     * @param requestKey
     * @return
     */
    public static String sign(Map<String,Object> map , String requestKey){
        return SecureUtil.md5(getMapToString(map) + "requestKey=" + requestKey);
    }


    public static void main(String[] args) {
        Map<String,Object> map = new HashMap<>();
        map.put("appId","jskj");
        map.put("sign","8cac55a78af3f159122b7fafbbfe7454");
        map.put("updateTime","1635306585000");
        map.put("comId","2020jskj");
        String mapToString = sign(map,"123");
        System.out.println(mapToString);
    }


}

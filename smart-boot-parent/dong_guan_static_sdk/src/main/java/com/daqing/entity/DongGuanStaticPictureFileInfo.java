package com.daqing.entity;

/**
 * 创建图片文件
 */
public class DongGuanStaticPictureFileInfo {


    public DongGuanStaticPictureFileInfo() {
    }

    public DongGuanStaticPictureFileInfo(String comId, String parkingOrSectionId, String orderId, Integer picture, Integer pictureType, Long size, Long createTime, Long updateTime, String remark) {
        this.comId = comId;
        this.parkingOrSectionId = parkingOrSectionId;
        this.orderId = orderId;
        this.picture = picture;
        this.pictureType = pictureType;
        this.size = size;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.remark = remark;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;


    /**
     * 停车场或者路段ID
     * 字符长度     64
     * 是否必填     是
     * 取值范围     comId+停车场服务商自主停车场编号，唯一性，如果是停车场的话，此处为停车场的ID；如果是路段的话，此处是路段的ID
     */
    private String parkingOrSectionId;


    /**
     * 订单ID
     * 字符长度     64
     * 是否必填     是
     * 取值范围     唯一值即可（推荐comId+服务商订单id，唯一性）与进出场orderId一致
     */
    private String orderId;


    /**
     * 图片类型
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1-进场 2-出场
     */
    private Integer picture;


    /**
     * 图片场景类型
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1 道路停车  2停车场
     */
    private Integer pictureType;


    /**
     * 文件大小
     * 字符长度     24
     * 是否必填     是
     * 取值范围     1 道路停车  2停车场
     */
    private Long size;


    /**
     * 创建时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的创建时间
     */
    private Long createTime;


    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    /**
     * 备注
     * 字符长度     512
     * 是否必填     否
     * 取值范围
     */
    private String remark;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getParkingOrSectionId() {
        return parkingOrSectionId;
    }

    public void setParkingOrSectionId(String parkingOrSectionId) {
        this.parkingOrSectionId = parkingOrSectionId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getPicture() {
        return picture;
    }

    public void setPicture(Integer picture) {
        this.picture = picture;
    }

    public Integer getPictureType() {
        return pictureType;
    }

    public void setPictureType(Integer pictureType) {
        this.pictureType = pictureType;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "DongGuanStaticPictureFileInfo{" +
                "comId='" + comId + '\'' +
                ", parkingOrSectionId='" + parkingOrSectionId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", picture=" + picture +
                ", pictureType=" + pictureType +
                ", size=" + size +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}

package com.daqing.entity;

/**
 * 剩余泊位同步接口(必须传)
 */
public class DongGuanStaticRestBerthInfo{


    public DongGuanStaticRestBerthInfo() {
    }

    public DongGuanStaticRestBerthInfo(String comId, Integer uploadType, String parkingLotId, String sectionId, Integer total, Integer rest, Long updateTime, String remark) {
        this.comId = comId;
        this.uploadType = uploadType;
        this.parkingLotId = parkingLotId;
        this.sectionId = sectionId;
        this.total = total;
        this.rest = rest;
        this.updateTime = updateTime;
        this.remark = remark;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;


    /**
     * 上传类型
     * 字符长度     64
     * 是否必填     是
     * 取值范围     1-路外停车场 2-道路泊位
     */
    private Integer uploadType;


    /**
     * 车场ID
     * 字符长度     64
     * 是否必填     否
     * 取值范围     comId+停车场服务商自主停车场编号，唯一性，如果是停车
     */
    private String parkingLotId;


    /**
     * 路段id
     * 字符长度     64
     * 是否必填     否
     * 取值范围     comId+服务商自主路段编号，唯一性，如果是路段的话必传
     */
    private String sectionId;

    /**
     * 总泊位数
     * 字符长度     12
     * 是否必填     是
     * 取值范围     总泊位数
     */
    private Integer total;

    /**
     * 空余泊位数
     * 字符长度     12
     * 是否必填     是
     * 取值范围     空余泊位数
     */
    private Integer rest;


    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    /**
     * 备注
     * 字符长度 512
     * 是否必填 否
     */
    private String remark;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public Integer getUploadType() {
        return uploadType;
    }

    public void setUploadType(Integer uploadType) {
        this.uploadType = uploadType;
    }

    public String getParkingLotId() {
        return parkingLotId;
    }

    public void setParkingLotId(String parkingLotId) {
        this.parkingLotId = parkingLotId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getRest() {
        return rest;
    }

    public void setRest(Integer rest) {
        this.rest = rest;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String toString() {
        return "DongGuanStaticRestBerthInfo{" +
                "comId='" + comId + '\'' +
                ", uploadType=" + uploadType +
                ", parkingLotId='" + parkingLotId + '\'' +
                ", sectionId='" + sectionId + '\'' +
                ", total=" + total +
                ", rest=" + rest +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}

package com.daqing.entity;

/**
 * 路段基础信息
 */
public class DongGuanStaticSysSection {


    public DongGuanStaticSysSection() {
    }


    public DongGuanStaticSysSection(String comId, String sectionName, String areaId, String cantonId, Integer sectionDirection, Integer berthForm, Integer distribution, String berthCount, String minBerthCode, String maxBerthCode, Integer isPoint, Integer isValid, String longitude, String latitude, String sectionAddress, Long createTime, Long updateTime, String remark) {
        this.comId = comId;
        this.sectionName = sectionName;
        this.areaId = areaId;
        this.cantonId = cantonId;
        this.sectionDirection = sectionDirection;
        this.berthForm = berthForm;
        this.distribution = distribution;
        this.berthCount = berthCount;
        this.minBerthCode = minBerthCode;
        this.maxBerthCode = maxBerthCode;
        this.isPoint = isPoint;
        this.isValid = isValid;
        this.longitude = longitude;
        this.latitude = latitude;
        this.sectionAddress = sectionAddress;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.remark = remark;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;


    /**
     * 路段名称
     * 字符长度     64
     * 是否必填     是
     * 取值范围     路段的名称
     */
    private String sectionName;


    /**
     * 所属镇街id
     * 字符长度     64
     * 是否必填     是
     * 取值范围     参考 附录 C：片区和镇街
     */
    private String areaId;


    /**
     * 片区id
     * 字符长度     64
     * 是否必填     是
     * 取值范围     参考 附录 C：片区和镇街
     */
    private String cantonId;


    /**
     * 路段朝向
     * 字符长度     6
     * 是否必填     否
     * 取值范围     1 东 2南 3西 4 北
     */
    private Integer sectionDirection ;


    /**
     * 泊位排列顺序
     * 字符长度     6
     * 是否必填     否
     * 取值范围     1 平行 2 垂直 3 斜交
     */
    private Integer berthForm ;


    /**
     * 泊位分布
     * 字符长度     6
     * 是否必填     否
     * 取值范围     1 单侧   2 双侧
     */
    private Integer distribution ;


    /**
     * 泊位数量
     * 字符长度     6
     * 是否必填     是
     * 取值范围     泊位数量
     */
    private String berthCount;


    /**
     * 起始泊位号
     * 字符长度     64
     * 是否必填     是
     */
    private String minBerthCode;


    /**
     * 终止泊位号
     * 字符长度     64
     * 是否必填     是
     */
    private String maxBerthCode;


    /**
     * 是否为重点路段
     * 字符长度     6
     * 是否必填     是
     * 取值范围     0 不是   1 是
     */
    private Integer isPoint;


    /**
     * 是否启用
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1 已启用  0 未启用
     */
    private Integer isValid;


    /**
     * 路段经度
     * 字符长度     50
     * 是否必填     是
     */
    private String longitude;


    /**
     * 路段纬度
     * 字符长度     50
     * 是否必填     是
     */
    private String latitude;


    /**
     * 地址
     * 字符长度     256
     * 是否必填     是
     */
    private String sectionAddress;


    /**
     * 创建时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的创建时间
     */
    private Long createTime;


    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    /**
     * 备注
     * 字符长度 512
     * 是否必填 否
     */
    private String remark;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getCantonId() {
        return cantonId;
    }

    public void setCantonId(String cantonId) {
        this.cantonId = cantonId;
    }

    public Integer getSectionDirection() {
        return sectionDirection;
    }

    public void setSectionDirection(Integer sectionDirection) {
        this.sectionDirection = sectionDirection;
    }

    public Integer getBerthForm() {
        return berthForm;
    }

    public void setBerthForm(Integer berthForm) {
        this.berthForm = berthForm;
    }

    public Integer getDistribution() {
        return distribution;
    }

    public void setDistribution(Integer distribution) {
        this.distribution = distribution;
    }

    public String getBerthCount() {
        return berthCount;
    }

    public void setBerthCount(String berthCount) {
        this.berthCount = berthCount;
    }

    public String getMinBerthCode() {
        return minBerthCode;
    }

    public void setMinBerthCode(String minBerthCode) {
        this.minBerthCode = minBerthCode;
    }

    public String getMaxBerthCode() {
        return maxBerthCode;
    }

    public void setMaxBerthCode(String maxBerthCode) {
        this.maxBerthCode = maxBerthCode;
    }

    public Integer getIsPoint() {
        return isPoint;
    }

    public void setIsPoint(Integer isPoint) {
        this.isPoint = isPoint;
    }

    public Integer getIsValid() {
        return isValid;
    }

    public void setIsValid(Integer isValid) {
        this.isValid = isValid;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getSectionAddress() {
        return sectionAddress;
    }

    public void setSectionAddress(String sectionAddress) {
        this.sectionAddress = sectionAddress;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String toString() {
        return "DongGuanStaticSysSection{" +
                "comId='" + comId + '\'' +
                ", sectionName='" + sectionName + '\'' +
                ", areaId='" + areaId + '\'' +
                ", cantonId='" + cantonId + '\'' +
                ", sectionDirection=" + sectionDirection +
                ", berthForm=" + berthForm +
                ", distribution=" + distribution +
                ", berthCount=" + berthCount +
                ", minBerthCode='" + minBerthCode + '\'' +
                ", maxBerthCode='" + maxBerthCode + '\'' +
                ", isPoint=" + isPoint +
                ", isValid=" + isValid +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", sectionAddress=" + sectionAddress +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}

package com.daqing.entity;

/**
 * 道路泊位车辆进出信息
 */
public class DongGuanStaticRoadBerthBusInfo{


    public DongGuanStaticRoadBerthBusInfo() {
    }

    public DongGuanStaticRoadBerthBusInfo(String comId, String orderId, String berthCode, String areaId, String sectionId, String mebTel, Integer passType, String carPlate, Integer carType, Long intime, Long outTime, Long parkTime, Integer payMethod, Integer payType, Integer carProperty, Long createTime, Long updateTime, String remark) {
        this.comId = comId;
        this.orderId = orderId;
        this.berthCode = berthCode;
        this.areaId = areaId;
        this.sectionId = sectionId;
        this.mebTel = mebTel;
        this.passType = passType;
        this.carPlate = carPlate;
        this.carType = carType;
        this.intime = intime;
        this.outTime = outTime;
        this.parkTime = parkTime;
        this.payMethod = payMethod;
        this.payType = payType;
        this.carProperty = carProperty;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.remark = remark;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;


    /**
     * 订单id
     * 字符长度     64
     * 是否必填     否
     * 取值范围     comId+服务商订单id，唯一性
     */
    private String orderId;


    /**
     * 泊位编号
     * 字符长度     64
     * 是否必填     是
     */
    private String berthCode;


    /**
     * 街镇id
     * 字符长度     64
     * 是否必填     是
     * 取值范围     参考 附录 C：片区和镇街
     */
    private String areaId;


    /**
     * comId+服务商自主路段编号，唯一性
     * 字符长度     64
     * 是否必填     是
     * 取值范围     路段id
     */
    private String sectionId;



    /**
     * 用户手机号
     * 字符长度     24
     * 是否必填     是
     * 取值范围     默认99999999999
     */
    private String mebTel;



    /**
     * 进出类型
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1-车辆驶入 2-车辆驶出
     */
    private Integer passType;


    /**
     * 车牌号码
     * 字符长度     12
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String carPlate;



    /**
     * 车辆类型
     * 字符长度     6
     * 是否必填     是
     * 取值范围     传值1、2……等，具体参考附录B中的表格《机动车号牌种类字典》
     */
    private Integer carType;



    /**
     * 车辆驶入时间
     * 字符长度     24
     * 是否必填     是
     * 取值范围     车辆驶入数据上报必传(时间戳格式)
     */
    private Long intime;


    /**
     * 车辆驶出时间
     * 字符长度     24
     * 是否必填     是
     * 取值范围     车辆驶出数据上报必传(时间戳格式)
     */
    private Long outTime;


    /**
     * 停车时长（分钟）
     * 字符长度     24
     * 是否必填     是
     * 取值范围     驶入传0，驶出传车辆的停车时长，单位默认是分钟
     */
    private Long parkTime;



    /**
     * 付费类型
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1-后付费 2-预付费
     */
    private Integer payMethod;



    /**
     * 缴费方式
     * 字符长度     6
     * 是否必填     否
     * 取值范围     0-免费 1-钱包余额 2-现金 3-月卡 4-微信扫码 5-支付宝扫码 6-白名单 7-ETC 8-银联 9-其他
     */
    private Integer payType;



    /**
     * 车辆属性
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1-临停车 2-包月车 3-特殊车辆（执法、施工等） 4-其他
     */
    private Integer carProperty;

    /**
     * 创建时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的创建时间
     */
    private Long createTime;


    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    /**
     * 备注
     * 字符长度 512
     * 是否必填 否
     */
    private String remark;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getBerthCode() {
        return berthCode;
    }

    public void setBerthCode(String berthCode) {
        this.berthCode = berthCode;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getMebTel() {
        return mebTel;
    }

    public void setMebTel(String mebTel) {
        this.mebTel = mebTel;
    }

    public Integer getPassType() {
        return passType;
    }

    public void setPassType(Integer passType) {
        this.passType = passType;
    }

    public String getCarPlate() {
        return carPlate;
    }

    public void setCarPlate(String carPlate) {
        this.carPlate = carPlate;
    }

    public Integer getCarType() {
        return carType;
    }

    public void setCarType(Integer carType) {
        this.carType = carType;
    }

    public Long getIntime() {
        return intime;
    }

    public void setIntime(Long intime) {
        this.intime = intime;
    }

    public Long getOutTime() {
        return outTime;
    }

    public void setOutTime(Long outTime) {
        this.outTime = outTime;
    }

    public Long getParkTime() {
        return parkTime;
    }

    public void setParkTime(Long parkTime) {
        this.parkTime = parkTime;
    }

    public Integer getPayMethod() {
        return payMethod;
    }

    public void setPayMethod(Integer payMethod) {
        this.payMethod = payMethod;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public Integer getCarProperty() {
        return carProperty;
    }

    public void setCarProperty(Integer carProperty) {
        this.carProperty = carProperty;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String toString() {
        return "DongGuanStaticRoadBerthBusInfo{" +
                "comId='" + comId + '\'' +
                ", orderId='" + orderId + '\'' +
                ", berthCode='" + berthCode + '\'' +
                ", areaId='" + areaId + '\'' +
                ", sectionId='" + sectionId + '\'' +
                ", mebTel='" + mebTel + '\'' +
                ", passType=" + passType +
                ", carPlate='" + carPlate + '\'' +
                ", carType=" + carType +
                ", intime=" + intime +
                ", outTime=" + outTime +
                ", parkTime=" + parkTime +
                ", payMethod=" + payMethod +
                ", payType=" + payType +
                ", carProperty=" + carProperty +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}

package com.daqing.entity;

/**
 * 东莞停车场基础信息
 */
public class DongGuanStaticParkingLogBaseData {

    public DongGuanStaticParkingLogBaseData() {
    }

    public DongGuanStaticParkingLogBaseData(String comId, String parkingLotId, String parkingLotName, Integer parkingStatus, Integer carCount, Integer inEntryCount, Integer outEntryCount, Long insideFreeTime, String billRulesRemark, String areaId, String areaName, Integer parkingProperty, Integer parkingType, String detailAddress, String longitude, String latitude, Integer isStallReserve, Integer isCharingPile, Integer isIndoorNavigation, Integer isOnlinePay, Long ms, Integer ve, Long createTime, Long updateTime, String remark) {
        this.comId = comId;
        this.parkingLotId = parkingLotId;
        this.parkingLotName = parkingLotName;
        this.parkingStatus = parkingStatus;
        this.carCount = carCount;
        this.inEntryCount = inEntryCount;
        this.outEntryCount = outEntryCount;
        this.insideFreeTime = insideFreeTime;
        this.billRulesRemark = billRulesRemark;
        this.areaId = areaId;
        this.areaName = areaName;
        this.parkingProperty = parkingProperty;
        this.parkingType = parkingType;
        this.detailAddress = detailAddress;
        this.longitude = longitude;
        this.latitude = latitude;
        this.isStallReserve = isStallReserve;
        this.isCharingPile = isCharingPile;
        this.isIndoorNavigation = isIndoorNavigation;
        this.isOnlinePay = isOnlinePay;
        this.ms = ms;
        this.ve = ve;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.remark = remark;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;

    /**
     * 停车场ID
     * 字符长度     64
     * 是否必填     是
     * 取值范围     comId+停车场服务商自主停车场编号，唯一性
     */
    private String parkingLotId;

    /**
     * 停车场名称
     * 字符长度 64
     * 是否必填 是
     * 取值范围     建议使用中文名称
     */
    private String parkingLotName;

    /**
     * 停车场状态
     * 字符长度 6
     * 是否必填 是
     * 取值范围     (1 已启用，2 未启用)
     */
    private Integer parkingStatus;


    /**
     * 车位数
     * 字符长度 6
     * 是否必填 是
     * 取值范围     必须大于0
     */
    private Integer carCount;


    /**
     * 入口通道数
     * 字符长度 6
     * 是否必填 是
     * 取值范围     必须大于0
     */
    private Integer inEntryCount;


    /**
     * 出口通道数
     * 字符长度 6
     * 是否必填 是
     * 取值范围     必须大于0
     */
    private Integer outEntryCount;


    /**
     * 场内免费时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     单位：分钟，例如: 30表示免费30分钟，若没有免费时间就传0
     */
    private Long insideFreeTime;


    /**
     * 计费规则描述
     * 字符长度 512
     * 是否必填 是
     */
    private String billRulesRemark;


    /**
     * 所属镇街id
     * 字符长度 64
     * 是否必填 是
     * 取值范围     参考 附录 C：片区和镇街
     */
    private String areaId;


    /**
     * 所属镇街名字
     * 字符长度 64
     * 是否必填 是
     * 取值范围     参考 附录 C：片区和镇街
     */
    private String areaName;


    /**
     * 车场性质
     * 字符长度 6
     * 是否必填 是
     * 取值范围     1商超停车场 2写字楼停车场 3小区停车场 4公园广场停车场 5政府公共停车场 6村社区停车场 7其他
     */
    private Integer parkingProperty;


    /**
     * 车场类型
     * 字符长度 6
     * 是否必填 是
     * 取值范围     1室内停车场 2室外停车场 3立体停车库 4其他
     */
    private Integer parkingType;


    /**
     * 详细地址
     * 字符长度 256
     * 是否必填 是
     */
    private String  detailAddress;


    /**
     * 经度
     * 字符长度 50
     * 是否必填 是
     */
    private String  longitude;


    /**
     * 纬度
     * 字符长度 50
     * 是否必填 是
     */
    private String  latitude;


    /**
     * 是否支持车位预定
     * 字符长度 6
     * 是否必填 是
     * 取值范围     (1 支持   0 不支持)
     */
    private Integer isStallReserve;


    /**
     * 是否支持有充电桩
     * 字符长度 6
     * 是否必填 是
     * 取值范围     (1 支持   0 不支持)
     */
    private Integer isCharingPile;


    /**
     * 是否支持室内导航
     * 字符长度 6
     * 是否必填 是
     * 取值范围     (1 支持   0 不支持)
     */
    private Integer isIndoorNavigation;


    /**
     * 是否支持在线支付
     * 字符长度 6
     * 是否必填 是
     * 取值范围     (1 支持   0 不支持)
     */
    private Integer isOnlinePay;


    /**
     * 时间戳 毫秒
     * 字符长度 24
     * 是否必填 是
     * 取值范围     调用接口时，服务器的系统时间
     */
    private Long ms;


    /**
     * 加密是否编码
     * 字符长度 6
     * 是否必填 是
     * 取值范围     防止乱码，1是编码，2不编码（建议传1）。若传1：生成签名前拼接的字符串中的中文需要进行编码
     */
    private Integer ve;


    /**
     * 创建时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的创建时间
     */
    private Long createTime;


    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    /**
     * 备注
     * 字符长度 512
     * 是否必填 否
     * 取值范围
     */
    private String remark;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getParkingLotId() {
        return parkingLotId;
    }

    public void setParkingLotId(String parkingLotId) {
        this.parkingLotId = parkingLotId;
    }

    public String getParkingLotName() {
        return parkingLotName;
    }

    public void setParkingLotName(String parkingLotName) {
        this.parkingLotName = parkingLotName;
    }

    public Integer getParkingStatus() {
        return parkingStatus;
    }

    public void setParkingStatus(Integer parkingStatus) {
        this.parkingStatus = parkingStatus;
    }

    public Integer getCarCount() {
        return carCount;
    }

    public void setCarCount(Integer carCount) {
        this.carCount = carCount;
    }

    public Integer getInEntryCount() {
        return inEntryCount;
    }

    public void setInEntryCount(Integer inEntryCount) {
        this.inEntryCount = inEntryCount;
    }

    public Integer getOutEntryCount() {
        return outEntryCount;
    }

    public void setOutEntryCount(Integer outEntryCount) {
        this.outEntryCount = outEntryCount;
    }

    public Long getInsideFreeTime() {
        return insideFreeTime;
    }

    public void setInsideFreeTime(Long insideFreeTime) {
        this.insideFreeTime = insideFreeTime;
    }

    public String getBillRulesRemark() {
        return billRulesRemark;
    }

    public void setBillRulesRemark(String billRulesRemark) {
        this.billRulesRemark = billRulesRemark;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Integer getParkingProperty() {
        return parkingProperty;
    }

    public void setParkingProperty(Integer parkingProperty) {
        this.parkingProperty = parkingProperty;
    }

    public Integer getParkingType() {
        return parkingType;
    }

    public void setParkingType(Integer parkingType) {
        this.parkingType = parkingType;
    }

    public String getDetailAddress() {
        return detailAddress;
    }

    public void setDetailAddress(String detailAddress) {
        this.detailAddress = detailAddress;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public Integer getIsStallReserve() {
        return isStallReserve;
    }

    public void setIsStallReserve(Integer isStallReserve) {
        this.isStallReserve = isStallReserve;
    }

    public Integer getIsCharingPile() {
        return isCharingPile;
    }

    public void setIsCharingPile(Integer isCharingPile) {
        this.isCharingPile = isCharingPile;
    }

    public Integer getIsIndoorNavigation() {
        return isIndoorNavigation;
    }

    public void setIsIndoorNavigation(Integer isIndoorNavigation) {
        this.isIndoorNavigation = isIndoorNavigation;
    }

    public Integer getIsOnlinePay() {
        return isOnlinePay;
    }

    public void setIsOnlinePay(Integer isOnlinePay) {
        this.isOnlinePay = isOnlinePay;
    }

    public Long getMs() {
        return ms;
    }

    public void setMs(Long ms) {
        this.ms = ms;
    }

    public Integer getVe() {
        return ve;
    }

    public void setVe(Integer ve) {
        this.ve = ve;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String toString() {
        return "DongGuanStaticParkingLogBaseData{" +
                "comId='" + comId + '\'' +
                ", parkingLotId='" + parkingLotId + '\'' +
                ", parkingLotName='" + parkingLotName + '\'' +
                ", parkingStatus=" + parkingStatus +
                ", carCount=" + carCount +
                ", inEntryCount=" + inEntryCount +
                ", outEntryCount=" + outEntryCount +
                ", insideFreeTime=" + insideFreeTime +
                ", billRulesRemark='" + billRulesRemark + '\'' +
                ", areaId='" + areaId + '\'' +
                ", areaName='" + areaName + '\'' +
                ", parkingProperty=" + parkingProperty +
                ", parkingType=" + parkingType +
                ", detailAddress='" + detailAddress + '\'' +
                ", longitude='" + longitude + '\'' +
                ", latitude='" + latitude + '\'' +
                ", isStallReserve=" + isStallReserve +
                ", isCharingPile=" + isCharingPile +
                ", isIndoorNavigation=" + isIndoorNavigation +
                ", isOnlinePay=" + isOnlinePay +
                ", ms=" + ms +
                ", ve=" + ve +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}

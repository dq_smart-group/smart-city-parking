package com.daqing.entity;

/**
 * 东莞静态交通响应
 */
public class DongGuanStaticResponse {


    public DongGuanStaticResponse() {
    }

    public DongGuanStaticResponse(String message, Object obj, Integer state, Long size, Long startPosition) {
        this.message = message;
        this.obj = obj;
        this.state = state;
        this.size = size;
        this.startPosition = startPosition;
    }

    private String message;

    private Object obj;

    /**
     * 操作状态（0-系统错误，1-成功，2-失败）
     */
    private Integer state;

    /**
     * 文件大小
     */
    private Long size;

    /**
     * 文件起始位置
     */
    private Long startPosition;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(Long startPosition) {
        this.startPosition = startPosition;
    }

    @Override
    public String toString() {
        return "DongGuanStaticResponse{" +
                "message='" + message + '\'' +
                ", obj=" + obj +
                ", state=" + state +
                ", size=" + size +
                ", startPosition=" + startPosition +
                '}';
    }
}

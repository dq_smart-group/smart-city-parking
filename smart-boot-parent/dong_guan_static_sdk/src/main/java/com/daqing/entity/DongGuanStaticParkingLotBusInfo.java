package com.daqing.entity;

/**
 * 车辆进出场时，数据上传调用接口
 */
public class DongGuanStaticParkingLotBusInfo {


    public DongGuanStaticParkingLotBusInfo() {
    }


    public DongGuanStaticParkingLotBusInfo(String comId, String parkingLotId, String entryId, Integer passType, Long passTime, Integer carType, String plateNumber, Integer plateColor, String orderId, Long parkTime, Integer payType, String remark, Long ms, Integer ve, Long createTime, Long updateTime) {
        this.comId = comId;
        this.parkingLotId = parkingLotId;
        this.entryId = entryId;
        this.passType = passType;
        this.passTime = passTime;
        this.carType = carType;
        this.plateNumber = plateNumber;
        this.plateColor = plateColor;
        this.orderId = orderId;
        this.parkTime = parkTime;
        this.payType = payType;
        this.remark = remark;
        this.ms = ms;
        this.ve = ve;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;

    /**
     * 停车场ID
     * 字符长度     64
     * 是否必填     是
     * 取值范围     comId+停车场服务商自主停车场编号，唯一性
     */
    private String parkingLotId;


    /**
     * 通道ID
     * 字符长度     64
     * 是否必填     是
     * 取值范围     comId+停车场服务商通道ID，唯一性
     */
    private String entryId;


    /**
     * 进出类型
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1-车辆入场 2-车辆出场
     */
    private Integer passType;


    /**
     * 停车场进出时间 （毫秒)
     * 字符长度     24
     * 是否必填     是
     * 取值范围     进场和出场时间都用这个字段
     */
    private Long passTime;


    /**
     * 车辆类型
     * 字符长度     24
     * 是否必填     是
     * 取值范围     传值1、2……等，具体参考附录B中的表格《机动车号牌种类字典》
     */
    private Integer carType;


    /**
     * 车牌号
     * 字符长度     12
     * 是否必填     是
     */
    private String plateNumber;


    /**
     * 车牌颜色
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1-蓝色 2-黄色 3-绿色 4-白色 5-黑色
     */
    private Integer plateColor;


    /**
     * 订单id
     * 字符长度     64
     * 是否必填     是
     * 取值范围     唯一值即可（推荐comId+服务商订单id，唯一性）
     */
    private String orderId;


    /**
     * 停车时长，精确到毫秒值
     * 字符长度     6
     * 是否必填     是
     * 取值范围     进场传0，出场时传车辆的停车时长
     */
    private Long parkTime;



    /**
     * 缴费方式
     * 字符长度     6
     * 是否必填     是
     * 取值范围     0-免费 1-钱包 2-现金 3-月卡 4-微信扫码 5-支付宝扫码 6-白名单 7-ETC 8-银联 9-其他
     */
    private Integer payType;


    /**
     * 备注
     * 字符长度     512
     * 是否必填     否
     * 取值范围
     */
    private String remark;


    /**
     * 时间戳 毫秒
     * 字符长度 24
     * 是否必填 是
     * 取值范围     调用接口时，服务器的系统时间
     */
    private Long ms;


    /**
     * 加密是否编码
     * 字符长度 6
     * 是否必填 是
     * 取值范围     防止乱码，1是编码，2不编码（建议传1）。若传1：生成签名前拼接的字符串中的中文需要进行编码
     */
    private Integer ve;


    /**
     * 创建时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的创建时间
     */
    private Long createTime;


    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getParkingLotId() {
        return parkingLotId;
    }

    public void setParkingLotId(String parkingLotId) {
        this.parkingLotId = parkingLotId;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public Integer getPassType() {
        return passType;
    }

    public void setPassType(Integer passType) {
        this.passType = passType;
    }

    public Long getPassTime() {
        return passTime;
    }

    public void setPassTime(Long passTime) {
        this.passTime = passTime;
    }

    public Integer getCarType() {
        return carType;
    }

    public void setCarType(Integer carType) {
        this.carType = carType;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Integer getPlateColor() {
        return plateColor;
    }

    public void setPlateColor(Integer plateColor) {
        this.plateColor = plateColor;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getParkTime() {
        return parkTime;
    }

    public void setParkTime(Long parkTime) {
        this.parkTime = parkTime;
    }

    public Integer getPayType() {
        return payType;
    }

    public void setPayType(Integer payType) {
        this.payType = payType;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getMs() {
        return ms;
    }

    public void setMs(Long ms) {
        this.ms = ms;
    }

    public Integer getVe() {
        return ve;
    }

    public void setVe(Integer ve) {
        this.ve = ve;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        return "DongGuanStaticParkingLotBusInfo{" +
                "comId='" + comId + '\'' +
                ", parkingLotId='" + parkingLotId + '\'' +
                ", entryId='" + entryId + '\'' +
                ", passType=" + passType +
                ", passTime=" + passTime +
                ", carType=" + carType +
                ", plateNumber='" + plateNumber + '\'' +
                ", plateColor=" + plateColor +
                ", orderId=" + orderId +
                ", parkTime=" + parkTime +
                ", payType=" + payType +
                ", remark='" + remark + '\'' +
                ", ms=" + ms +
                ", ve=" + ve +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}

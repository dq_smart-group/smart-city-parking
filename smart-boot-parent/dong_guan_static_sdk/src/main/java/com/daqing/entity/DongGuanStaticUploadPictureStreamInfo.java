package com.daqing.entity;

/**
 * 上传进出场车牌图片流
 */
public class DongGuanStaticUploadPictureStreamInfo {


    public DongGuanStaticUploadPictureStreamInfo() {
    }

    public DongGuanStaticUploadPictureStreamInfo(String comId, String fileId, String pictureType, String urlAddress, Long startPosition, byte[] content, Long createTime, Long updateTime, String remark) {
        this.comId = comId;
        this.fileId = fileId;
        this.pictureType = pictureType;
        this.urlAddress = urlAddress;
        this.startPosition = startPosition;
        this.content = content;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.remark = remark;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;


    /**
     * 文件id（创建文件接口返回）
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String fileId;


    /**
     * 图片场景类型
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1道路停车  2停车场
     */
    private String pictureType;


    /**
     * 保存图片的url地址
     * 字符长度     256
     * 是否必填     是
     * 取值范围     全路径地址
     */
    private String urlAddress;


    /**
     * 文件起始位置
     * 字符长度     24
     * 是否必填     是
     * 取值范围     默认为0
     */
    private Long startPosition;


    /**
     * 文件字节流为byte数组
     * 字符长度
     * 是否必填     否
     */
    private byte[] content;


    /**
     * 创建时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的创建时间
     */
    private Long createTime;


    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    /**
     * 备注
     * 字符长度     512
     * 是否必填     否
     * 取值范围
     */
    private String remark;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getPictureType() {
        return pictureType;
    }

    public void setPictureType(String pictureType) {
        this.pictureType = pictureType;
    }

    public String getUrlAddress() {
        return urlAddress;
    }

    public void setUrlAddress(String urlAddress) {
        this.urlAddress = urlAddress;
    }

    public Long getStartPosition() {
        return startPosition;
    }

    public void setStartPosition(Long startPosition) {
        this.startPosition = startPosition;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }


    @Override
    public String toString() {
        return "DongGuanStaticUploadPictureStreamInfo{" +
                "comId='" + comId + '\'' +
                ", fileId='" + fileId + '\'' +
                ", pictureType='" + pictureType + '\'' +
                ", urlAddress='" + urlAddress + '\'' +
                ", startPosition=" + startPosition +
                ", content=" + content +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}

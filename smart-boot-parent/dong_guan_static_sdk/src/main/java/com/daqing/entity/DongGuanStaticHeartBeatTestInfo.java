package com.daqing.entity;

/**
 * 系统心跳检测
 */
public class DongGuanStaticHeartBeatTestInfo {


    public DongGuanStaticHeartBeatTestInfo() {

    }


    public DongGuanStaticHeartBeatTestInfo(String comId, Long updateTime, String remark) {
        this.comId = comId;
        this.updateTime = updateTime;
        this.remark = remark;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;

    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    /**
     * 备注
     * 字符长度 512
     * 是否必填 否
     */
    private String remark;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "DongGuanStaticHeartBeatTestInfo{" +
                "comId='" + comId + '\'' +
                ", updateTime=" + updateTime +
                ", remark='" + remark + '\'' +
                '}';
    }
}

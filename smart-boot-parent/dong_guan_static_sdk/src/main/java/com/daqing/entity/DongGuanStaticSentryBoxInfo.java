package com.daqing.entity;

/**
 * 上传停车场通道（岗亭）基础数据
 */
public class DongGuanStaticSentryBoxInfo {


    public DongGuanStaticSentryBoxInfo() {
    }

    public DongGuanStaticSentryBoxInfo(String comId, String parkingLotId, String entryId, Integer sentryBoxType, String sentryBoxName, String remark, Long ms, Integer ve, Long createTime, Long updateTime) {
        this.comId = comId;
        this.parkingLotId = parkingLotId;
        this.entryId = entryId;
        this.sentryBoxType = sentryBoxType;
        this.sentryBoxName = sentryBoxName;
        this.remark = remark;
        this.ms = ms;
        this.ve = ve;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    /**
     * 业主标识
     * 字符长度     64
     * 是否必填     是
     * 取值范围     云平台提供
     */
    private String comId;


    /**
     * 停车场ID
     * 字符长度     64
     * 是否必填     是
     * 取值范围     comId+停车场服务商自主停车场编号，唯一性
     */
    private String parkingLotId;


    /**
     * 通道ID
     * 字符长度     64
     * 是否必填     是
     * 取值范围     comId+停车场服务商通道ID，唯一性
     */
    private String entryId;


    /**
     * 岗亭类型
     * 字符长度     6
     * 是否必填     是
     * 取值范围     1- 单进单出 2- 单进 3- 单出 4- 同进同出
     */
    private Integer sentryBoxType;


    /**
     * 岗亭名称
     * 字符长度     64
     * 是否必填     是
     * 取值范围
     */
    private String sentryBoxName;



    /**
     * 备注
     * 字符长度     512
     * 是否必填     否
     * 取值范围
     */
    private String remark;


    /**
     * 时间戳 毫秒
     * 字符长度 24
     * 是否必填 是
     * 取值范围     调用接口时，服务器的系统时间
     */
    private Long ms;


    /**
     * 加密是否编码
     * 字符长度 6
     * 是否必填 是
     * 取值范围     防止乱码，1是编码，2不编码（建议传1）。若传1：生成签名前拼接的字符串中的中文需要进行编码
     */
    private Integer ve;


    /**
     * 创建时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的创建时间
     */
    private Long createTime;


    /**
     * 修改时间
     * 字符长度 24
     * 是否必填 是
     * 取值范围     服务商停车场的修改时间
     */
    private Long updateTime;


    public String getComId() {
        return comId;
    }

    public void setComId(String comId) {
        this.comId = comId;
    }

    public String getParkingLotId() {
        return parkingLotId;
    }

    public void setParkingLotId(String parkingLotId) {
        this.parkingLotId = parkingLotId;
    }

    public String getEntryId() {
        return entryId;
    }

    public void setEntryId(String entryId) {
        this.entryId = entryId;
    }

    public Integer getSentryBoxType() {
        return sentryBoxType;
    }

    public void setSentryBoxType(Integer sentryBoxType) {
        this.sentryBoxType = sentryBoxType;
    }

    public String getSentryBoxName() {
        return sentryBoxName;
    }

    public void setSentryBoxName(String sentryBoxName) {
        this.sentryBoxName = sentryBoxName;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getMs() {
        return ms;
    }

    public void setMs(Long ms) {
        this.ms = ms;
    }

    public Integer getVe() {
        return ve;
    }

    public void setVe(Integer ve) {
        this.ve = ve;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }


    @Override
    public String toString() {
        return "DongGuanStaticSentryBoxInfo{" +
                "comId='" + comId + '\'' +
                ", parkingLotId='" + parkingLotId + '\'' +
                ", entryId='" + entryId + '\'' +
                ", sentryBoxType=" + sentryBoxType +
                ", sentryBoxName=" + sentryBoxName +
                ", remark='" + remark + '\'' +
                ", ms=" + ms +
                ", ve=" + ve +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                '}';
    }
}

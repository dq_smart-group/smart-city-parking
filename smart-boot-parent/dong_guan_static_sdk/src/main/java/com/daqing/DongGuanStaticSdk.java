package com.daqing;


import com.alibaba.fastjson.JSON;
import com.daqing.config.DongGuanStaticConfig;
import com.daqing.entity.DongGuanStaticResponse;
import com.daqing.utils.HttpUtils;
import com.daqing.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 东莞静态交通SDK
 */
public class DongGuanStaticSdk {


    /**
     * 停车场基础信息
     * 上传停车场的基础数据
     * @param map
     * @return
     */
    public static DongGuanStaticResponse parkingLotBaseData(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.dongGuanParkingBasisInfo, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }


    /**
     * 停车场通道（岗亭）基础信息接口
     * 上传停车场通道（岗亭）基础数据
     * @param map
     * @return
     */
    public static DongGuanStaticResponse sentryBoxInfo(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.dongGuanParkingChannelBasisInfo, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }


    /**
     * 停车场车辆进出场信息
     * 车辆进出场时，数据上传调用接口
     * @param map
     * @return
     */
    public static DongGuanStaticResponse parkingLotBusInfo(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.dongGuanParkingInOutBasisInfo, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }


    /**
     * 创建图片文件
     * 在上传进出场图片的时候需要先在服务器上创建对应的图片文件
     * @param map
     * @return
     */
    public static DongGuanStaticResponse pictureFileInfo(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.dongGuanCreateImageFile, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }



    /**
     * 上传进出场车牌图片流
     * 用于上传车辆进出场图片流，可分段上传
     * @param map
     * @return
     */
    public static DongGuanStaticResponse uploadPictureStreamInfo(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.dongGuanInOutPlateNumberImageStream, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }


    /**
     * 路段基础信息
     * 上传基础的路段泊位数据
     * @param map
     * @return
     */
    public static DongGuanStaticResponse sysSection(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.dongGuanRoadBasisInfo, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }


    /**
     * 道路泊位车辆进出信息
     * 车辆进出道路泊位时，数据上传调用接口
     * @param map
     * @return
     */
    public static DongGuanStaticResponse roadBerthBusInfo(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.dongGuanRoadInOutInfo, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }




    /**
     * 剩余泊位同步接口(必须传)
     * 剩余泊位同步接口
     * @param map
     * @return
     */
    public static DongGuanStaticResponse restBerthInfo(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.dongGuanSurplusSync, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }


    /**
     * 系统心跳检测
     * 心跳同步，检测停车运营服务商系统是否正常运行
     * @param map
     * @return
     */
    public static DongGuanStaticResponse heartBeatTestInfo(Map<String ,Object> map){
        String result = HttpUtils.post(DongGuanStaticConfig.domgGuanSystemHeartDance, map);
        if(StringUtils.isEmpty(result)){
            return null;
        }
        return JSON.parseObject(result,DongGuanStaticResponse.class);
    }


    public static void main(String[] args) {
        DongGuanStaticConfig.setConfig("2021dqwl",
                "dqwl",
                "c0dd0beb15490b0e59cd15bb306de905",
                "1",
                "http://bigdata.dgparking.cn:9005",
                "http://bigdata.dgparking.cn:9002");

        DongGuanStaticConfig.setRequestUrl("/parkingLotBaseData/operate",
                "/sentryBoxInfo/operate",
                "/parkingLotBusInfo/operate",
                "/pictureFileInfo/upload",
                "/uploadPictureStreamInfo/upload",
                "/sysSection/operate",
                "/roadBerthBusInfo/operate",
                "/restBerthInfo/operate",
                "/heartBeatTestInfo/operate");
        DongGuanStaticSdk dongGuanStaticSdk = new DongGuanStaticSdk();
        //----------------------------------停车场基础接口----------------------------------
//        Map<String,Object> map = new HashMap<>();
//        map.put("parkingLotId","2021dqwlnulSDSADADAl");
//        map.put("areaId","735722");
//        map.put("parkingLotName","测试场地1");
//        map.put("insideFreeTime",30L);
//        map.put("billRulesRemark","30分钟内免费");
//        map.put("areaName","城区片区");
//        map.put("detailAddress","北京市海淀区复兴路69号院");
//        map.put("longitude",113.360036);
//        map.put("latitude",22.538988);
//        map.put("updateTime",1605593697011L);
//        map.put("createTime",1605593697024L);
//        map.put("ms",1605593697007L);
//        map.put("parkingType",1);
//        map.put("parkingStatus",1);
//        map.put("carCount",477);
//        map.put("inEntryCount",1);
//        map.put("outEntryCount",1);
//        map.put("parkingProperty",6);
//        map.put("isStallReserve",1);
//        map.put("isCharingPile",1);
//        map.put("isIndoorNavigation",1);
//        map.put("isOnlinePay",1);
//        map.put("ve",2);
//        DongGuanStaticResponse dongGuanStaticResponse = dongGuanStaticSdk.parkingLotBaseData(map);
//        System.out.println(dongGuanStaticResponse.toString());

        //----------------------------------停车场通道(岗亭)基础接口----------------------------------
//        Map<String,Object> map = new HashMap<>();
//        map.put("parkingLotId","2020fsznB5D018E5-4AB3-46AF-BD15-7563C07C2724");
//        map.put("entryId","2020fsznD8D0E48A-9339-46CD-B0A3-4FCD94B655ED");
//        map.put("sentryBoxType",1);
//        map.put("sentryBoxName","岗亭一");
//        map.put("ms",1604739414115L);
//        map.put("ve",2);
//        map.put("createTime",1604739414115L);
//        map.put("updateTime",1604739414115L);

        //----------------------------------停车场车辆进出接口----------------------------------
//        Map<String,Object> map = new HashMap<>();
//        map.put("parkingLotId","2021dqwlnulSDSADADAl");
//        map.put("entryId","jlzn000000005130");
//        map.put("passType",1);
//        map.put("passTime",1605080998000L);
//        map.put("carType",1);
//        map.put("plateNumber","粤888888");
//        map.put("plateColor",0);
//        map.put("parkTime",3);
//        map.put("ms",1605081003789L);
//        map.put("ve",2);
//        map.put("payType",1);
//        map.put("orderId","2020100081605080787356d64af26b7334f568e6e");

        //----------------------------------创建图片文件接口----------------------------------
        Map<String,Object> map = new HashMap<>();
        map.put("parkingOrSectionId","2020100080000000067");
        map.put("orderId","202010008160507724b447a9a647");
        map.put("picture",1);
        map.put("pictureType",1);
        map.put("size",179734);
        DongGuanStaticResponse dongGuanStaticResponse = dongGuanStaticSdk.pictureFileInfo(map);
        System.out.println(dongGuanStaticResponse.toString());


        //----------------------------------上传进出场车牌图片流----------------------------------
//        Map<String,Object> map = new HashMap<>();
//        map.put("fileId","1519957049176645634");
//        map.put("pictureType",2);
//        map.put("urlAddress","https://www.2008php.com/09_Website_appreciate/10-07-11/1278862200_222.jpg");
//        map.put("startPosition",0L);
//        map.put("createTime",1603256910000000L);
//        map.put("updateTime",1603256910000000L);


        //----------------------------------路段基础信息----------------------------------
//        Map<String,Object> map = new HashMap<>();
//        map.put("sectionName","荣昌街");
//        map.put("areaId","73572201");
//        map.put("cantonId","735722");
//        map.put("minBerthCode","1350");
//        map.put("maxBerthCode","1700");
//        map.put("sectionDirection",1);
//        map.put("berthForm",1);
//        map.put("distribution",1);
//        map.put("isPoint",1);
//        map.put("isValid",1);
//        map.put("longitude","113.800047");
//        map.put("latitude","22.809383");
//        map.put("sectionAddress","东莞市");
//        map.put("berthCount",350);
//        map.put("createTime",1605668504343L);


        //----------------------------------道路泊位车辆进出接口----------------------------------
//        Map<String,Object> map = new HashMap<>();
//        map.put("cantonId","735722");
//        map.put("parkTime",584517336L);
//        map.put("orderId","20201029163118758127163958641694");
//        map.put("updateTime",1604544799018L);
//        map.put("berthCode","202003");
//        map.put("sectionId","20200604151610063407847643818000");
//        map.put("mebTel","99999999999");
//        map.put("carProperty",1);
//        map.put("ve",2);
//        map.put("inTime",1603960278743L);
//        map.put("carPlate","贵S1029V");
//        map.put("passType",1);
//        map.put("areaId","73572801");
//        map.put("carType",1);
//        map.put("payType",0);
//        map.put("createTime",1604544799018L);
//        map.put("payMethod",2);
//        map.put("outTime",1604544796079L);

        //----------------------------------剩余泊位同步接口----------------------------------
//        Map<String,Object> map = new HashMap<>();
//        map.put("restId","1323452589450301442");
//        map.put("uploadType","1");
//        map.put("parkingLotId","2020100080000000052");
//        map.put("sectionId","null");
//        map.put("total",230);
//        map.put("rest",60);
//        map.put("remark","植物园停车场剩余泊位");
//        map.put("ve",2);

        //----------------------------------系统心跳检测----------------------------------
//        Map<String,Object> map = new HashMap<>();
//        map.put("updateTime",new Date().getTime());
//
//        DongGuanStaticResponse dongGuanStaticResponse = dongGuanStaticSdk.sysSection(map);
//        System.out.println(dongGuanStaticResponse.toString());






    }


}

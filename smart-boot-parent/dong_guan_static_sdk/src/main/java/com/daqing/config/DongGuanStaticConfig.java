package com.daqing.config;

import cn.hutool.log.StaticLog;
import com.daqing.utils.StringUtils;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;

/**
 * 东莞静态交通配置
 */
public class DongGuanStaticConfig {

    /**
     * 停车场基础信息接口
     */
    public static String dongGuanParkingBasisInfo = "";

    /**
     * 停车场通道（岗亭）基础信息接口
     */
    public static String dongGuanParkingChannelBasisInfo = "";

    /**
     * 停车场车辆进出场信息接口
     */
    public static String dongGuanParkingInOutBasisInfo = "";

    /**
     * 创建图片文件接口
     */
    public static String dongGuanCreateImageFile = "";;

    /**
     * 上传进出场车牌图片流接口
     */
    public static String dongGuanInOutPlateNumberImageStream = "";

    /**
     * 路段基础信息接口
     */
    public static String dongGuanRoadBasisInfo = "";

    /**
     * 道路泊位车辆进出信息接口
     */
    public static String dongGuanRoadInOutInfo = "";

    /**
     * 剩余泊位同步接口(必须传)
     */
    public static String dongGuanSurplusSync = "";

    /**
     * 系统心跳检测接口
     */
    public static String domgGuanSystemHeartDance = "";

    /**
     * 测试环境请求域名
     */
    public static String dongGuanDevRequestHost = "";

    /**
     * 生产环境请求域名
     */
    public static String dongGuanProdRequestHost = "";

    /**
     * 当前环境(1开发环境,2生产环境)
     */
    public static String dongGuanIsEnvironment = "";

    /**
     * 业主id
     */
    public static String dongGuanComId = "";

    /**
     * 调用者id
     */
    public static String dongGuanAppId = "";

    /**
     * requestKey
     */
    public static String dongGuanRequestKey = "";


    /**
     * 设置请求配置信息
     * @param dongGuanComId
     * @param dongGuanAppId
     * @param dongGuanRequestKey
     * @param dongGuanIsEnvironment
     * @param dongGuanDevRequestHost
     * @param dongGuanProdRequestHost
     */
    public static void setConfig(String dongGuanComId ,
                                 String dongGuanAppId ,
                                 String dongGuanRequestKey ,
                                 String dongGuanIsEnvironment ,
                                 String dongGuanDevRequestHost ,
                                 String dongGuanProdRequestHost){
        DongGuanStaticConfig.dongGuanComId = dongGuanComId;
        DongGuanStaticConfig.dongGuanAppId = dongGuanAppId;
        DongGuanStaticConfig.dongGuanRequestKey = dongGuanRequestKey;
        DongGuanStaticConfig.dongGuanIsEnvironment = dongGuanIsEnvironment;
        DongGuanStaticConfig.dongGuanDevRequestHost = dongGuanDevRequestHost;
        DongGuanStaticConfig.dongGuanProdRequestHost = dongGuanProdRequestHost;
    }


    /**
     * 设置请求url
     * @param dongGuanParkingBasisInfo
     * @param dongGuanParkingChannelBasisInfo
     * @param dongGuanParkingInOutBasisInfo
     * @param dongGuanCreateImageFile
     * @param dongGuanInOutPlateNumberImageStream
     * @param dongGuanRoadBasisInfo
     * @param dongGuanRoadInOutInfo
     * @param dongGuanSurplusSync
     * @param domgGuanSystemHeartDance
     */
    public static void setRequestUrl(String dongGuanParkingBasisInfo ,
                                     String dongGuanParkingChannelBasisInfo ,
                                     String dongGuanParkingInOutBasisInfo ,
                                     String dongGuanCreateImageFile ,
                                     String dongGuanInOutPlateNumberImageStream,
                                     String dongGuanRoadBasisInfo ,
                                     String dongGuanRoadInOutInfo,
                                     String dongGuanSurplusSync,
                                     String domgGuanSystemHeartDance){

        //判断当前环境是否为空
        if(StringUtils.isEmpty(DongGuanStaticConfig.dongGuanIsEnvironment)){
            DongGuanStaticConfig.dongGuanIsEnvironment = "1";
        }
        if("1".equals(DongGuanStaticConfig.dongGuanIsEnvironment)){

            //判断测试请求域名是否为空
            if(StringUtils.isEmpty(DongGuanStaticConfig.dongGuanDevRequestHost)){
                StaticLog.error("东莞静态交通测试环境请求域名为空");
            }

            DongGuanStaticConfig.dongGuanParkingBasisInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,dongGuanParkingBasisInfo);
            DongGuanStaticConfig.dongGuanParkingChannelBasisInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,dongGuanParkingChannelBasisInfo);
            DongGuanStaticConfig.dongGuanParkingInOutBasisInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,dongGuanParkingInOutBasisInfo);
            DongGuanStaticConfig.dongGuanCreateImageFile = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,dongGuanCreateImageFile);
            DongGuanStaticConfig.dongGuanInOutPlateNumberImageStream = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,dongGuanInOutPlateNumberImageStream);
            DongGuanStaticConfig.dongGuanRoadBasisInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,dongGuanRoadBasisInfo);
            DongGuanStaticConfig.dongGuanRoadInOutInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,dongGuanRoadInOutInfo);
            DongGuanStaticConfig.dongGuanSurplusSync = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,dongGuanSurplusSync);
            DongGuanStaticConfig.domgGuanSystemHeartDance = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanDevRequestHost,domgGuanSystemHeartDance);
        }else if("2".equals(DongGuanStaticConfig.dongGuanIsEnvironment)){

            //判断生产请求域名是否为空
            if(StringUtils.isEmpty(DongGuanStaticConfig.dongGuanProdRequestHost)){
                StaticLog.error("东莞静态交通生产环境请求域名为空");
            }

            DongGuanStaticConfig.dongGuanParkingBasisInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,dongGuanParkingBasisInfo);
            DongGuanStaticConfig.dongGuanParkingChannelBasisInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,dongGuanParkingChannelBasisInfo);
            DongGuanStaticConfig.dongGuanParkingInOutBasisInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,dongGuanParkingInOutBasisInfo);
            DongGuanStaticConfig.dongGuanCreateImageFile = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,dongGuanCreateImageFile);
            DongGuanStaticConfig.dongGuanInOutPlateNumberImageStream = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,dongGuanInOutPlateNumberImageStream);
            DongGuanStaticConfig.dongGuanRoadBasisInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,dongGuanRoadBasisInfo);
            DongGuanStaticConfig.dongGuanRoadInOutInfo = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,dongGuanRoadInOutInfo);
            DongGuanStaticConfig.dongGuanSurplusSync = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,dongGuanSurplusSync);
            DongGuanStaticConfig.domgGuanSystemHeartDance = StringUtils.buildUrl(DongGuanStaticConfig.dongGuanProdRequestHost,domgGuanSystemHeartDance);
        }
    }



}

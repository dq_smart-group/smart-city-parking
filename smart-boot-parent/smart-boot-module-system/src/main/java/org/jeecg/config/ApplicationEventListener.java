package org.jeecg.config;

import com.daqing.config.DongGuanStaticConfig;
import lombok.SneakyThrows;
import org.checkerframework.checker.units.qual.C;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.api.CarApi;
import org.jeecg.modules.parking.api.DgStaticParkingApi;
import org.jeecg.modules.parking.service.ICarDeviceGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class ApplicationEventListener implements ApplicationListener {
    @Autowired
    private ICameraDeviceService cameraDeviceService;
    @Autowired
    private ICarDeviceGroupService carDeviceGroupService;

    @Autowired
    private DgStaticParkingApi dgStaticParkingApi;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        // 应用已启动完成
        if (event instanceof ApplicationReadyEvent) {
            applicationReadyEvent();
        }
    }
    /**
     * 项目启动完成时触发
     */
    private void applicationReadyEvent() {
        try{
            carDeviceGroupService.saveDeviceGroupInRedis();//加载级联设备组数据到redis中
            cameraDeviceService.saveDeviceGroupInRedis();//添加设备信息到redis中
            dgStaticParkingApi.setDgStaticConfig();//加载东莞静态交通配置
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

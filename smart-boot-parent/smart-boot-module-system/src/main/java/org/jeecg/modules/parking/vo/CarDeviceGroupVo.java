package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarDeviceGroup;

@Data
public class CarDeviceGroupVo extends CarDeviceGroup {
    private String companyName;
    private String parkingName;
    private String sentryBoxName;
}

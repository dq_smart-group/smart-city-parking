package org.jeecg.modules.parking.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daqing.entity.DongGuanStaticParkingLogBaseData;
import com.daqing.entity.DongGuanStaticSysSection;
import com.daqing.utils.StringUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.query.ParkingLotQuery;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.parking.vo.ParkingLotVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

 /**
 * @Description: 车场表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
@Api(tags="车场表")
@RestController
@RequestMapping("/parking/parkingLot")
@Slf4j
public class ParkingLotController extends JeecgController<ParkingLot, IParkingLotService> {
	@Autowired
	private IParkingLotService parkingLotService;
	@Autowired
	private ISysDepartService sysDepartService;
	@Autowired
	private RedisUtil redisUtil;
	
	/**
	 * 分页列表查询
	 *
	 * @param parkingLotQuery
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "车场表-分页列表查询")
	@ApiOperation(value="车场表-分页列表查询", notes="车场表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(ParkingLotQuery parkingLotQuery,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			parkingLotQuery.setCompanyId(sysDepart.getId());
		}
		Page<ParkingLotVo> page = new Page<ParkingLotVo>(pageNo, pageSize);
		IPage<ParkingLotVo> pageList = parkingLotService.pageList(page, parkingLotQuery);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param parkingLot
	 * @return
	 */
	@AutoLog(value = "车场表-添加")
	@ApiOperation(value="车场表-添加", notes="车场表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody ParkingLot parkingLot) {
		ParkingLot parkingLotByIdentifier = null;
		if(StringUtils.isNotEmpty(parkingLot.getParentYard())){
			parkingLotByIdentifier = parkingLotService.getParkingLotByIdentifier(parkingLot.getParentYard());
		}
		if(parkingLotByIdentifier!=null){
			parkingLot.setParentYard(parkingLotByIdentifier.getId());
		}
		parkingLotService.save(parkingLot);
		parkingLotService.toUpdate();
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param parkingLot
	 * @return
	 */
	@AutoLog(value = "车场表-编辑")
	@ApiOperation(value="车场表-编辑", notes="车场表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody ParkingLot parkingLot) {
		ParkingLot parkingLotByIdentifier = null;
		if(StringUtils.isNotEmpty(parkingLot.getParentYard())){
			parkingLotByIdentifier = parkingLotService.getParkingLotByIdentifier(parkingLot.getParentYard());
			if(parkingLotByIdentifier==null){
				return Result.error("请检查父车场的标识是否正确");
			}
			parkingLot.setParentYard(parkingLotByIdentifier.getId());
		}

		parkingLotService.updateById(parkingLot);
		parkingLotService.toUpdate();
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "车场表-通过id删除")
	@ApiOperation(value="车场表-通过id删除", notes="车场表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		parkingLotService.removeById(id);
		redisUtil.del(ParkingRedisConstant.PARKING_LOT+id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "车场表-批量删除")
	@ApiOperation(value="车场表-批量删除", notes="车场表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.parkingLotService.removeByIds(Arrays.asList(ids.split(",")));
		String[] split = ids.split(",");
		for (String s : split) {
			redisUtil.del(ParkingRedisConstant.PARKING_LOT+s );
		}
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "车场表-通过id查询")
	@ApiOperation(value="车场表-通过id查询", notes="车场表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		ParkingLot parkingLot = parkingLotService.getById(id);
		if(parkingLot==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(parkingLot);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param parkingLot
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, ParkingLot parkingLot) {
        return super.exportXls(request, parkingLot, ParkingLot.class, "车场表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, ParkingLot.class);
    }

	 /**
	  * 是管理员就查询全部车场，不是管理员就获取登陆人公司编码对应的所有的车场
	  */
	 @GetMapping("/getParkingLotByUserId")
	 public List<ParkingLotAndCamVo> getParkingLotByUserId(){
		 List<ParkingLotAndCamVo> list = null;
		 String companyId = null;
		 if(!LoginUserUtil.isAdmin()){
			 SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			 if(sysDepart!=null){
				 companyId = sysDepart.getId();
			 }else {
				 companyId="0";
			 }
		 }
		 list = parkingLotService.getParkingLotByCompanyId(companyId);
		 return list;
	 }

	 /**
	  * 是管理员就查询全部岗亭，不是管理员就获取登陆人公司编码对应的所有的岗亭
	  */
	 @GetMapping("/getSentryBoxLotByUserId")
	 public List<ParkingLotAndCamVo> getSentryBoxLotByUserId(){
		 List<ParkingLotAndCamVo> list = null;
		 String companyId = null;
		 if(!LoginUserUtil.isAdmin()){
			 SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			 if(sysDepart!=null){
				 companyId = sysDepart.getId();
			 }else {
				 companyId="0";
			 }
		 }
		 list = parkingLotService.getSentryBoxLotByUserId(companyId);
		 return list;
	 }


	 /**
	  * 停车场基础数据上传
	  * @param data
	  * @return
	  */
	 @PostMapping("/parkingLotInformationUpload")
	 public Result<?> parkingLotInformationUpload(@RequestBody DongGuanStaticParkingLogBaseData data){
	 	return parkingLotService.parkingLotInformationUpload(data);
	 }

	 /**
	  * 道路基础数据上传
	  * @param data
	  * @return
	  */
	 @PostMapping("/reloadInformationUpload")
	 public Result<?> reloadInformationUpload(@RequestBody DongGuanStaticSysSection data){
		 return parkingLotService.reloadInformationUpload(data);
	 }


 }

package org.jeecg.modules.parking.util;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.entity.CarStandard;
import org.jeecg.modules.parking.query.ShouFei;
import org.jeecg.modules.parking.query.TwentyFourShouFei;
import org.jeecg.modules.parking.service.ICarStandardService;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GetMoney {
    @Autowired
    private  ICarStandardService carStandardService;
    @Autowired
    private  ICameraDeviceService cameraDeviceService;

    /**
     *
     * @param number 设备号
     * @param carCardTypeId 车牌类型id
     * @param rt 入场时间
     * @param ct 出场时间
     * @throws Exception
     */
    public  BigDecimal getMoney(String carStandardId,String number,String carCardTypeId,Date rt,Date ct)throws Exception{
//        //获取设备
//         CameraDevice cameraDevice = cameraDeviceService.selectByDevice(number);
//        //获取车场
//        String parkingLotId = cameraDevice.getParkingLotId();
        //根据车场和车牌类型查出收费规则
        //CarStandard carStandard = carStandardService.getCarStandard(parkingLotId, carCardTypeId);
        CarStandard carStandard = carStandardService.getById(carStandardId);
        //小时收费类型（1：24小时收费  0：分段收费）
        String hourlyChargeType = carStandard.getHourlyChargeType();
        //全天最高收费
        BigDecimal highestMoney = carStandard.getHighestMoney();
        //免费分钟
        Integer freeMinutes = carStandard.getFreeMinutes();
        //免费分钟是否参与收费
        Boolean chargingFlag = carStandard.getChargingFlag();
        //收费规则
        String hourlyCharge = carStandard.getHourlyCharge();
        //未知
        Boolean enablesFlag = carStandard.getEnablesFlag();

        //入场时间
        Date date = new Date();
        Date date1 = rt;
        Date date2 = ct;
//        Date date1 = DateUtils.parseDate("2021-12-10 7:20", "yyyy-MM-dd HH:mm");
//        Date date2 = DateUtils.parseDate("2021-12-10 9:20", "yyyy-MM-dd HH:mm");
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        //String s = DateUtils.date2Str(date, simpleDateFormat);
        //总时长

        //要交费的金额
        BigDecimal money = new BigDecimal(0);
        //计算出进场时间和出场时间相差多少天
        int i = DateUtils.daysBetween(date1, date2);
        //入场时
        Integer rshour = Integer.parseInt(DateUtils.formatShorthour(date1));
        //入场分
        Integer rsmiu = Integer.parseInt(DateUtils.formatShortmiu(date1));
        //入场时间
        int rtimes = rshour * 60 + rsmiu;
        //出场时
        Integer cshour = Integer.parseInt(DateUtils.formatShorthour(date2));
        //出场分
        Integer csmiu = Integer.parseInt(DateUtils.formatShortmiu(date2));
        //出场时间
        int ctimes = cshour * 60 + csmiu+24*i*60;
        //总时长
        int times = ctimes - rtimes;
        //分段收费
        if(hourlyChargeType.equals("0")){
            JSONArray jsonArray = JSONArray.fromObject(hourlyCharge);
            if (jsonArray != null) {
                //转换为实体类后的收费规则
                List<ShouFei> shouFeiList = new ArrayList<>();
                for (Object o : jsonArray) {
                    JSONObject jsonObject = JSONObject.fromObject(o);
                    ShouFei shouFei = (ShouFei) JSONObject.toBean(jsonObject, ShouFei.class);
                    shouFeiList.add(shouFei);
                }
                //是否减去了免费时间
                boolean flag = false;
                //是否已经添加了起步价
                boolean qflag = false;
                for (int m = 0; m < i+2; m++) {
                    for (ShouFei shouFei : shouFeiList) {
                        //开始时
                        Integer shour = shouFei.getShour();
                        //开始分
                        Integer smiu = shouFei.getSmiu();
                        //计费开始时间
                        int stimes = shour * 60 + smiu+24*(m-1)*60;
                        //结束时
                        Integer ohour = shouFei.getOhour();
                        //结束分
                        Integer omiu = shouFei.getOmiu();
                        //计费结束时间
                        int otimes = 0;
                        if(shour>ohour){
                            otimes = ohour * 60 + omiu+24*(m-1)*60+24*60;
                        }else{
                            otimes = ohour * 60 + omiu+24*(m-1)*60;
                        }
                        //需要算费的时间
                        int i2 = otimes - stimes;

                        //免费分钟
                        String sf = shouFei.getSf();
                        //起步价
                        String sfs = shouFei.getSfs();
                        //本段最高收费
                        String zg = shouFei.getZg();
                        //价格
                        String jy = shouFei.getJy();
                        //分钟
                        String fz = shouFei.getFz();
                        //减去不算费的时间
                        int i1 = 0;
                        if(stimes<rtimes){
                            i1 = ctimes - rtimes;
                        }else{
                            i1 = ctimes - stimes;
                        }
                        //判断入场时间是否在这段收费时间里
                        if(rtimes>otimes){
                            continue;
                        }
                        //如果入场时间小于开始时间
                        if(rtimes<stimes){
                            //减去免费分钟
                            int i3 = 0;
                            if(i1>i2){
                                i3 = i2;
                            }else{
                                i3 = i1;
                            }
                            if(!flag){
                                flag = true;
                                i3 = i3 - Integer.parseInt(sf);
                            }
                            if(i3<0){
                                if(i1>i2){
                                    rtimes = otimes-i3;
                                    continue;
                                }else{
                                    return money;
                                }

                            }
                            //times = times - stimes;
                            //单价次数
                            int c = 0;
                            if(i3%Integer.parseInt(fz)!=0){
                                c = i3/Integer.parseInt(fz)+1;
                                //更新入场时间为收费的结束时间
                                if(enablesFlag){
                                    rtimes = otimes+Integer.parseInt(fz)-i3%Integer.parseInt(fz);
                                }
                            }else{
                                c = i3/Integer.parseInt(fz);
                                //更新入场时间为收费的结束时间
                                rtimes = otimes;
                            }
                            BigDecimal dmoney = new BigDecimal(0);
                            BigDecimal bigJy = new BigDecimal(jy);
                            if(!qflag){
                                qflag = true;
                                BigDecimal bigSfs = new BigDecimal(sfs);

                                dmoney = bigSfs.add(BigDecimal.valueOf(c).multiply(bigJy));
                            }else{
                                dmoney = BigDecimal.valueOf(c).multiply(bigJy);
                            }
                            //是否超出本段收费的最高收费
                            BigDecimal bigZg = new BigDecimal(zg);
                            if(dmoney.compareTo(bigZg)==1){
                                money = money.add(bigZg);
                            }else{
                                money = money.add(dmoney);
                            }
                            if(!(i1>i2)){
                                //结束
                                return money;
                            }

                        }else if(rtimes>=stimes){//入场时间大于计费开始时间
                            int i4 = i2 - (rtimes - stimes);
                            //减去免费分钟
                            int i3 = 0;
                            if(i1>i4){
                                i3 = i4;
                            }else{
                                i3 = i1;
                            }
                            if(!flag){
                                flag = true;
                                i3 = i3 - Integer.parseInt(sf);
                            }
                            if(i3<0){
                                if(i1>i4){
                                    rtimes = otimes-i3;
                                    continue;
                                }else{
                                    return money;
                                }
                            }
                            //单价次数
                            int c = 0;
                            if(i3%Integer.parseInt(fz)!=0){
                                c = i3/Integer.parseInt(fz)+1;
                                //更新入场时间为收费的结束时间
                                if(enablesFlag){
                                    rtimes = otimes+Integer.parseInt(fz)-i3%Integer.parseInt(fz);
                                }
                            }else{
                                c = i3/Integer.parseInt(fz);
                                //更新入场时间为收费的结束时间
                                rtimes = otimes;

                            }
                            BigDecimal dmoney = new BigDecimal(0);
                            BigDecimal bigSfs = new BigDecimal(sfs);
                            BigDecimal bigJy = new BigDecimal(jy);
                            if(!qflag){
                                qflag = true;
                                dmoney = bigSfs.add(BigDecimal.valueOf(c).multiply(bigJy));
                            }else{
                                dmoney = BigDecimal.valueOf(c).multiply(bigJy);
                            }
                            //是否超出本段收费的最高收费
                            BigDecimal bigZg = new BigDecimal(zg);
                            if(dmoney.compareTo(bigZg)==1){
                                money = money.add(bigZg);
                            }else{
                                money = money.add(dmoney);
                            }
                            //结束
                            if(!(i1>i4)){
                                //return
                                return money;
                            }
                        }
                    }
                }
                //System.out.println("总金额a"+money);
            }
        }else{//24小时收费
            JSONArray jsonArray = JSONArray.fromObject(hourlyCharge);
            if (jsonArray != null) {
                List<TwentyFourShouFei> list = new ArrayList<>();
                for (Object o : jsonArray) {
                    JSONObject jsonObject = JSONObject.fromObject(o);
                    TwentyFourShouFei twentyFourShouFei = (TwentyFourShouFei) JSONObject.toBean(jsonObject, TwentyFourShouFei.class);
                    list.add(twentyFourShouFei);
                }
                for (int m = 0; m < i+1; m++) {
                    int i1 = 1440;
                    if(ctimes<=1440){
                        i1=ctimes;
                    }
                    //待的时间
                    int i2 = i1 - rtimes;
                    if(ctimes>1440){
                        rtimes = 0;
                        ctimes = ctimes - 1440;
                    }
                    //几个小时
                    int c = 0;
                    if(i2%60!=0){
                        c = i2/60+1;
                    }else{
                        c = i2/60;
                    }
                    for (TwentyFourShouFei twentyFourShouFei : list) {
                        if (Integer.parseInt(twentyFourShouFei.getTime())==c){
                            BigDecimal bigDecimal = new BigDecimal(twentyFourShouFei.getMoney());
                            if(bigDecimal.compareTo(bigDecimal)==1){
                                money = money.add(highestMoney);
                            }else{
                                money = money.add(bigDecimal);
                            }
                        }
                    }
                }
            }
        }
        return money;
    }
}

package org.jeecg.modules.parking.service.impl;

import cn.hutool.core.lang.Assert;
import com.alipay.api.domain.Car;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.entity.CarCharge;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import org.jeecg.modules.parking.entity.CarShiftForm;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.mapper.CarChargeMapper;
import org.jeecg.modules.parking.mapper.CarInfoCollectMapper;
import org.jeecg.modules.parking.service.ICarChargeService;
import org.jeecg.modules.parking.service.ICarInfoCollectService;
import org.jeecg.modules.parking.service.ICarShiftFormService;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.vo.*;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Description: 收费员表
 * @Author: jeecg-boot
 * @Date:   2022-03-25
 * @Version: V1.0
 */
@Service
public class CarChargeServiceImpl extends ServiceImpl<CarChargeMapper, CarCharge> implements ICarChargeService {

    @Autowired
    private ICameraDeviceService cameraDeviceService;
    @Autowired
    private ICarInfoCollectService carInfoCollectService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private IParkingLotService parkingLotService;
    @Autowired
    private ISysDepartService sysDepartService;
    @Autowired
    private ICarShiftFormService carShiftFormService;

    @Override
    public IPage<CarChargeVo> pageList(Page<CarChargeVo> page, CarCharge carCharge) {
        List<CarChargeVo> list = this.baseMapper.pageList(page,carCharge);
        if(!list.isEmpty()){
            for (CarChargeVo carChargeVo : list) {
                String groupIds = carChargeVo.getGroupIds();
                if(StringUtils.isNotEmpty(groupIds)){
                    String[] split = groupIds.split(",");
                    List<String> list1 = new ArrayList<>();
                    for (String s : split) {
                        list1.add(s);
                    }
                    carChargeVo.setDeviceGroupIdS(list1);
                }

            }
        }
        return page.setRecords(list);
    }

    /**
     * 将数据存入到redis中
     * @param redisUtil
     * @return
     */
    @Override
    public List<CarChargeVo> setListToRedis(RedisUtil redisUtil) {
        List<CarChargeVo> list = baseMapper.setListToRedis();
        if(!list.isEmpty()){
            for (CarChargeVo carChargeVo : list) {
                String groupIds = carChargeVo.getGroupIds();
                if(StringUtils.isNotEmpty(groupIds)){
                    String[] split = groupIds.split(",");
                    List<String> list1 = new ArrayList<>();
                    for (String s : split) {
                        list1.add(s);
                    }
                    carChargeVo.setDeviceGroupIdS(list1);
                }
                redisUtil.set(ParkingRedisConstant.CAR_CHARGE + carChargeVo.getId(),carChargeVo);
            }
        }
        return list;
    }

    /**
     * 查询出登录的收费员对应管理车场的所有设备
     *
     * @return
     */
    @Override
    public List<CameraDeviceChVo> getSheBeiByUserId(String loginUserId,String deviceName) {
        LambdaQueryWrapper<CarCharge> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CarCharge::getUserId,loginUserId);
        wrapper.eq(CarCharge::getFlag,true);
        CarCharge carCharge = this.baseMapper.selectOne(wrapper);
        Assert.isTrue(carCharge!=null,"找不到收费员");
        String groupIds = carCharge.getGroupIds();
        List<CameraDevice> cameraDevices = new ArrayList<>();
        List<CameraDevice> cameraDevicess = new ArrayList<>();
        if(StringUtils.isNotEmpty(groupIds)){
            String[] split = groupIds.split(",");
            for (String s : split) {
                CameraDevice mainDevice = carInfoCollectService.getMainDevice(s);
                if(mainDevice!=null){
                    cameraDevicess.add(mainDevice);
                }

            }
        }

        if(cameraDevicess.isEmpty()){
            if(StringUtils.isNotEmpty(groupIds)){
                String[] split = groupIds.split(",");
                List<String> collect = Arrays.stream(split).collect(Collectors.toList());
                cameraDevicess = cameraDeviceService.getMainDeviceByGroupIds(collect);
                if(cameraDevicess.isEmpty()){
                    return null;
                }
            }
        }

        if(StringUtils.isNotEmpty(deviceName)){
            if(cameraDevicess.size()>0){
                for (CameraDevice devicess : cameraDevicess) {
                    if(devicess.getDeviceName().contains(deviceName)){
                        cameraDevices.add(devicess);
                    }
                }
            }
        }else{
            cameraDevices = cameraDevicess;
        }
        List<CameraDeviceChVo> list= new ArrayList<>();
        if(cameraDevices.size()>0){
            LambdaQueryWrapper<CarShiftForm> wrapper1 = new LambdaQueryWrapper<>();
            wrapper1.isNull(CarShiftForm::getShiftTime);
            List<CarShiftForm> list1 = carShiftFormService.list(wrapper1);
            for (int i = 0; i < cameraDevices.size(); i++) {
                CameraDeviceChVo cameraDeviceChVo = new CameraDeviceChVo();
                CarInfoCollectVo carInfoCollectvo = new CarInfoCollectVo();
                CameraDevice cameraDevice = cameraDevices.get(i);
                cameraDeviceChVo.setDevicename(cameraDevice.getDeviceName());
                cameraDeviceChVo.setRtsp("rtsp://"+cameraDevice.getIpaddr()+":8557/h264");
                cameraDeviceChVo.setSerialno(cameraDevice.getSerialno());
                cameraDeviceChVo.setDeviceUse(cameraDevice.getDeviceUse());
                cameraDeviceChVo.setId(i+1+"");
                CarShiftForm carShiftForm = null;
                if(list1.size()>0){
                    for (CarShiftForm shiftForm : list1) {
                        if(carCharge.getUserId().equals(shiftForm.getChargeId())){
                            carShiftForm = shiftForm;
                        }
                    }
                }
                if(carShiftForm==null){
                    return null;
                }
                if(cameraDevice.getDeviceUse().equals("0")){
                    //入口
                    carInfoCollectvo.setComeSerialno(cameraDevice.getSerialno());
                    List<String> lists = new ArrayList<>();
                    lists.add(CommonConstant.SX_CAR_STATUS_0);
                    lists.add(CommonConstant.SX_CAR_STATUS_1);
                    lists.add(CommonConstant.SX_CAR_STATUS_4);
                    lists.add(CommonConstant.SX_CAR_STATUS_5);
                    lists.add(CommonConstant.SX_CAR_STATUS_6);
                    carInfoCollectvo.setCarStatuslist(lists);
                    carInfoCollectvo.setCarStatuslists("0,1,4,5,6");
                    carInfoCollectvo.setStartComeTime(carShiftForm.getLoginTime());
                    carInfoCollectvo.setEndComeTime(new Date());
                }else if(cameraDevice.getDeviceUse().equals("1")){
                    //出口
                    carInfoCollectvo.setOutSerialno(cameraDevice.getSerialno());
                    List<String> lists = new ArrayList<>();
                    lists.add(CommonConstant.SX_CAR_STATUS_1);
                    carInfoCollectvo.setCarStatuslist(lists);
                    carInfoCollectvo.setCarStatuslists("1");
                    carInfoCollectvo.setStartOutTime(carShiftForm.getLoginTime());
                    carInfoCollectvo.setEndOutTime(new Date());
                }

                Page<CarInfoCollectVo> page = new Page<CarInfoCollectVo>(1, 10);
                Page<CarInfoCollectVo> carInfoCollectVoPage = carInfoCollectService.selectData(page, carInfoCollectvo);
                cameraDeviceChVo.setDataVoList(carInfoCollectVoPage.getRecords());
                list.add(cameraDeviceChVo);
            }
        }
        return list;
    }

    @Override
    public List<CarInfoCollectVo> getCarInfoDataBySerialno(String serialno) {
        CarInfoCollectVo carInfoCollectvo = new CarInfoCollectVo();
        CameraDevice cameraDevice = (CameraDevice)redisUtil.get(ParkingRedisConstant.PARKING_DEVICE + serialno);
        if(cameraDevice==null){
            cameraDevice = cameraDeviceService.selectByDevice(serialno);
        }
        CarShiftForm carShiftForm = carShiftFormService.getDataByUserId(LoginUserUtil.getLoginUserId());
        if(carShiftForm==null){
            return null;
        }
        if(cameraDevice.getDeviceUse().equals("0")){
            carInfoCollectvo.setComeSerialno(serialno);
            List<String> lists = new ArrayList<>();
            lists.add(CommonConstant.SX_CAR_STATUS_0);
            lists.add(CommonConstant.SX_CAR_STATUS_1);
            lists.add(CommonConstant.SX_CAR_STATUS_4);
            lists.add(CommonConstant.SX_CAR_STATUS_5);
            lists.add(CommonConstant.SX_CAR_STATUS_6);
            carInfoCollectvo.setCarStatuslist(lists);
            carInfoCollectvo.setCarStatuslists("0,1,4,5,6,7");
            carInfoCollectvo.setStartComeTime(carShiftForm.getLoginTime());
            carInfoCollectvo.setEndComeTime(new Date());
        }else{
            carInfoCollectvo.setOutSerialno(serialno);
            List<String> lists = new ArrayList<>();
            lists.add(CommonConstant.SX_CAR_STATUS_1);
            carInfoCollectvo.setCarStatuslist(lists);
            carInfoCollectvo.setCarStatuslists("1");
            carInfoCollectvo.setStartOutTime(carShiftForm.getLoginTime());
            carInfoCollectvo.setEndOutTime(new Date());
        }
        Page<CarInfoCollectVo> page = new Page<CarInfoCollectVo>(1, 10);
        Page<CarInfoCollectVo> carInfoCollectVoPage = carInfoCollectService.selectData(page, carInfoCollectvo);
        return carInfoCollectVoPage.getRecords();
    }

    /**
     * 根据登录的收费员返回车场信息
     * @return
     */
    @Override
    public ChargeParkingVo getDataInfoByCharge(String loginUserId) {
        ChargeParkingVo chargeParkingVo = new ChargeParkingVo();
        LambdaQueryWrapper<CarCharge> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CarCharge::getUserId,loginUserId);
        wrapper.eq(CarCharge::getFlag,true);
        CarCharge carCharge = this.baseMapper.selectOne(wrapper);
        Assert.isTrue(carCharge!=null,"找不到收费员");
        ParkingLot parkingLot = (ParkingLot)redisUtil.get(ParkingRedisConstant.PARKING_LOT + carCharge.getParkingLotId());
        if(parkingLot==null){
            parkingLot =  parkingLotService.getById(carCharge.getParkingLotId());
            Assert.isTrue(parkingLot!=null,"找不到该车场");

        }
        chargeParkingVo.setParkingLotId(parkingLot.getId());
        chargeParkingVo.setParkingName(parkingLot.getName());
        CarShiftForm carShiftForm = carShiftFormService.getDataByUserId(carCharge.getUserId());
        if(carShiftForm==null){
            return null;
        }
        //获取到收费员管理的设备组
        String groupIds = carCharge.getGroupIds();
        //通过设备组获取到各个设备组的主设备
        List<CameraDevice> list = new ArrayList<>();
        if(StringUtils.isNotEmpty(groupIds)){
            String[] split = groupIds.split(",");

            for (String s : split) {
                CameraDevice mainDevice = carInfoCollectService.getMainDevice(s);
                if(mainDevice!=null){
                    list.add(mainDevice);
                }
            }
        }

        if(list.isEmpty()){
            if(StringUtils.isNotEmpty(groupIds)){
                String[] split = groupIds.split(",");
                List<String> collect = Arrays.stream(split).collect(Collectors.toList());
                list = cameraDeviceService.getMainDeviceByGroupIds(collect);

            }
        }

        //入场设备序列号
        List<String> collect = list.stream().map(item -> {
            return item.getSerialno();
        }).collect(Collectors.toList());


        if(collect.isEmpty()){
            log.error("getDataInfoByCharge未查询到主设备");
            return null;
        }


        //本班入车，本班出车，已收合计，减免合计
        Integer comeSum = carInfoCollectService.getCountByloginTime(carShiftForm.getLoginTime(),collect);
        List<CarInfoCollect> carInfoCollects= carInfoCollectService.getoutCountByloginTime(carShiftForm.getLoginTime(),collect);
        chargeParkingVo.setComeCount(comeSum);
        BigDecimal totalReceived = new BigDecimal("0");
        BigDecimal totalRelief = new BigDecimal("0");
        if(carInfoCollects!=null){
            chargeParkingVo.setOutCount(carInfoCollects.size());
            for (CarInfoCollect carInfoCollect : carInfoCollects) {
                if(CommonConstant.PAYMENT_METHOD_1.equals(carInfoCollect.getChargeType())){
                    totalReceived = totalReceived.add(carInfoCollect.getCurrentAmount());
                    totalRelief = totalRelief.add(carInfoCollect.getDeductionAmount());
                }
            }
        }else{
            chargeParkingVo.setOutCount(0);
        }
        chargeParkingVo.setTotalReceived(totalReceived);
        chargeParkingVo.setTotalRelief(totalRelief);
        return chargeParkingVo;
    }

    /**
     *  编辑
     *
     * @param carCharge
     * @return
     */
    @Override
    public Result<?> updateCarChargeById(CarCharge carCharge) {
        //TODO 修改收费员的管理设备组的时候需要判断目前是否处于一个未交班状态，未交班状态不能修改
        //查重
        List<CarCharge> list = this.baseMapper.getCarChargeByUserId(carCharge.getUserId(),carCharge.getId());
        if(list.size()>0){
            return Result.error("已添加有该用户为收费员，请检查");
        }
        this.baseMapper.updateById(carCharge);
        return Result.OK("修改成功");
    }

    @Override
    public Result<?> saveCarCharge(CarCharge carCharge) {
        //查重
        List<CarCharge> list = this.baseMapper.getCarChargeByUserId(carCharge.getUserId(),carCharge.getId());
        if(list.size()>0){
            return Result.error("已添加有该用户为收费员，请检查");
        }
        this.baseMapper.insert(carCharge);
        return Result.OK("添加成功");
    }

    /**
     * 判断登录的是不是收费员
     * @param id
     * @return
     */
    @Override
    public Boolean isCharge(String id) {
        List<CarCharge> list = this.baseMapper.isCharge(id);
        if(list.size()>0){
            return true;
        }
        return false;
    }

    /**
     * 结算交班
     * @return
     */
    @Override
    public Result<?> shiftHandover(CarShiftForm carShiftForm) {
        Boolean charge = isCharge(carShiftForm.getChargeId());
        if(!charge){
            return Result.error("不是收费员");
        }
        CarShiftForm dataByUserId = carShiftFormService.getDataByUserId(carShiftForm.getChargeId());
        dataByUserId.setShiftTime(new Date());
        //统计一下这段时间内现金收款：已收金额和减免金额
        MoneyStatisticsVo moneyStatisticsVo = carInfoCollectService.moneyStatisticss(dataByUserId);
        SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
        dataByUserId.setCompanyId(sysDepart.getId());
        dataByUserId.setMoneys(moneyStatisticsVo.getPayMoneyTotal());
        dataByUserId.setTotalReceived(moneyStatisticsVo.getNetReceiptsMoneyTotal());
        dataByUserId.setTotalRelief(moneyStatisticsVo.getReductionMoneyTotal());
        carShiftFormService.updateById(dataByUserId);
        return Result.OK("交班成功");
    }

    /**
     * 查询出自己公司下所有的收费员
     * @return
     */
    @Override
    public List<SysUser> getSysUserByChange() {
        String companyId = null;
        if(!LoginUserUtil.isAdmin()){
            SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
            if(sysDepart!=null){
                companyId = sysDepart.getId();
            }else{
                companyId = "0";
            }
        }
        return this.baseMapper.getSysUserByChange(companyId);
    }
}

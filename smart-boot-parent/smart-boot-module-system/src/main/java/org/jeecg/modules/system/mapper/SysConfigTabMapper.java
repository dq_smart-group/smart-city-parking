package org.jeecg.modules.system.mapper;

import org.jeecg.modules.system.entity.SysConfigTab;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 配置分类表
 * @Author: jeecg-boot
 * @Date:   2021-12-03
 * @Version: V1.0
 */
public interface SysConfigTabMapper extends BaseMapper<SysConfigTab> {

}

package org.jeecg.modules.system.service;

import org.jeecg.modules.system.entity.SysConfigTab;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 配置分类表
 * @Author: jeecg-boot
 * @Date:   2021-12-03
 * @Version: V1.0
 */
public interface ISysConfigTabService extends IService<SysConfigTab> {

}

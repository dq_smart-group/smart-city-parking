package org.jeecg.modules.parking.api;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import net.sf.json.JSONObject;
import org.eclipse.paho.client.mqttv3.*;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.parking.service.ICarInfoCollectService;
import org.jeecg.modules.parking.util.GetCrc16;
import org.jeecg.modules.parking.util.TestQRcode;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;

/**
 *
 * 功能描述：MQTT测试
 * 创建人： mao2080@sina.com
 * 创建时间：2017年7月4日 下午5:08:59
 * 修改人： mao2080@sina.com
 * 修改时间：2017年7月4日 下午5:08:59
 */
public class MQTTTest_tcp {

    @Autowired
    private ICarInfoCollectService carInfoCollectService;

    /**MQTT服务端ip及端口*/
    private static String host = "tcp://124.71.57.221:1883";

    /**账号*/
    private static String username = "pan";

    /**密码*/
    private static String password = "123456";

    /**订阅的主题*/
    private static String subTopic = "/device/push/result";

    /**clientID*/
    private static String clientId = "123456";

    /**发布的主题*/
    private static String pubTopic = "/device/f5afb6f9-818f82e5/command";
    //private static String pubTopic = "/gpxj279J7Kd/android1/user/get";

    /**MQTT-Client*/
    private static MqttClient client;

    /**MQTT-Client*/
    private static int uuid = 0;

    /**
     * @throws InterruptedException
     * @throws MqttException */
    public static void main(String[] args) throws Exception{

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm");
        String format = simpleDateFormat.format(new DateTime());
        boolean b = DateUtils.timeCompare("8:00", "16:00");
        System.out.println(b);

        //String s = GetCrc16.get("00 64 FF FF 62 23 02 15 05 00 09 15 01 03 00 FF 00 00 00 00 00 00 00 10 00 CB C0 D7 D0 C5 CC A3 AC C5 DD E6 A4 C8 A5 C0 B2");
       // String sum = "有效期2022年01月14日22时21分";
       // String s = GetCrc16.getVoice(sum,"01");
       // String s3 = GetCrc16.getCrc16(s);
       //// System.out.println(s3);
       // byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
       // String encode3 = Base64.encode(aa2);
//        String s = GetCrc16.getQrCode("https://www.baidu.com/");
//        String s3 = GetCrc16.getCrc16(s);
//        System.out.println(s3);
//        byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
//        String encode3 = Base64.encode(aa2);
//        publish(encode3);
        // 订阅消息的方法
        //subscribe();
    //    TestQRcode.shenPic("https://tool.oschina.net/hexconvert","皖K76932","/device/f5afb6f9-818f82e5/command","皖K76932,停车0分钟,请缴费5元", false);
//        String qrCode = "00 C8 FF FF E5 D1 01 01 00 01 1E 00 80 24 B2 E2 CB E3 B2 E2 CB E3 B2 E2 CB E3 B2 E2 CB E3 B2 E2 CB E3 B2 E2 CB E3 B2 E2 CB E3 B2 E2 CB E3 B2 E2 CB E3 42 4D A6 01 00 00 00 00 00 00 3E 00 00 00 28 00 00 00 2D 00 00 00 D3 FF FF FF 01 00 01 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 FF FF FF 00 00 00 00 00 FE B7 F9 8E 4B F8 00 00 82 AE CC F9 92 08 00 00 BA DC 66 6C 12 E8 00 00 BA 89 B3 39 9A E8 00 00 BA 0B BF 93 3A E8 00 00 82 96 D8 E6 02 08 00 00 FE AA AA AA AB F8 00 00 00 14 78 8A 88 00 00 00 CE 6F 5F A4 A1 78 00 00 0D 92 3E F7 DB A0 00 00 6A BE C4 DE C2 10 00 00 98 03 99 9B 79 C0 00 00 AA E1 31 39 DB E8 00 00 84 1E 66 EC 8E B8 00 00 A6 8E C4 C4 24 58 00 00 A8 5B 99 FB F7 08 00 00 3A 98 31 58 C7 40 00 00 D8 4F E1 8E 9F C0 00 00 12 59 33 03 52 70 00 00 D4 C4 66 6C 1E A0 00 00 3F DE 4F C6 2F A8 00 00 E8 E3 B8 93 68 A8 00 00 AA D1 2A 99 EA A0 00 00 88 94 68 88 88 D0 00 00 4F CF 4F A0 AF D8 00 00 38 92 3C F7 D4 20 00 00 9E 3E CE 5E C6 90 00 00 68 8B 83 1B 69 C0 00 00 DE 09 39 B9 C0 F0 00 00 B5 76 74 EC 9D 18 00 00 62 7E D1 44 3D F8 00 00 E4 4B 9C F8 92 C8 00 00 EE 98 2E 5E A6 00 00 00 D4 CF F1 0E 4B D0 00 00 0B E1 31 03 D1 70 00 00 78 74 7C EC 0C 38 00 00 9B DE 4F C6 2F B8 00 00 00 AB A8 93 78 C8 00 00 FE 21 2A 99 EA A0 00 00 82 B4 68 9C B8 D0 00 00 BA EF 5F A1 CF 98 00 00 BA 52 2E 67 C9 B0 00 00 BA 7E C6 CB 0E 80 00 00 82 EB 96 1B 7C 40 00 00 FE C9 3C B9 D1 E8 00 00";

//        String r = GetCrc16.bytesToHexFun1("欢迎光临,请入场停车", "01", "R");
//        String s3 = GetCrc16.getCrc16(r);
//        byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
//        String encode3 = Base64.encode(aa2);
//        System.out.println(encode3);
//        //MqttPushClient.pushlishOrder(topic,encode3);
//        MQTTTest_tcp.publish(encode3);
//
//        String r1 = GetCrc16.getVoice("欢迎光临,请入场停车", "02");
//        String s31 = GetCrc16.getCrc16(r1);
//        byte[] aa21 = classHexstringToByteUtil.hexString2Bytes(s31);
//        String encode31 = Base64.encode(aa21);
//        System.out.println(encode3);
//        //MqttPushClient.pushlishOrder(topic,encode3);
//        MQTTTest_tcp.publish(encode31);

        //GetCrc16.get();
    }

    /**
     *
     * 描述：订阅信息
     * @author mao2080@sina.com
     * @created 2017年7月4日 下午4:53:47
     * @since
     * @return
     */
    public static void subscribe() {
        try {
            // 创建MqttClient
            MQTTTest_tcp.getClient().setCallback(new MqttCallback() {

                public void connectionLost(Throwable arg0) {

                }

                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    String str=message.toString();
                    JSONObject carInfo = JSONObject.fromObject(str);
                    System.out.println("MQTT Rece:" + message.toString());
                    //保存车牌信息
                    //生成订单
//                    CarApi carApi=new CarApi();
//                    carApi.ShibiePost(carInfo);
                }

                public void deliveryComplete(IMqttDeliveryToken token) {

                }

            });
            MQTTTest_tcp.getClient().subscribe(subTopic, 0);
            System.out.println("连接状态:" + client.isConnected());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * 描述：获取MqttClient
     * @author mao2080@sina.com
     * @created 2017年7月6日 上午9:56:37
     * @since
     * @return
     * @throws MqttException
     */
    public static MqttClient getClient() throws MqttException{
        try {
            if(client == null){
                client = new MqttClient(host, clientId);
                MqttConnectOptions conOptions = new MqttConnectOptions();
                conOptions.setUserName(username);
                conOptions.setPassword(password.toCharArray());
                conOptions.setCleanSession(true);
                client.connect(conOptions);
            }
            if(!client.isConnected()){
                client.reconnect();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return client;
    }

    /**
     *
     * 描述：发布信息
     * @author mao2080@sina.com
     * @throws MqttException
     * @created 2017年7月4日 下午4:53:32
     * @since
     */
    public static void publish(String str) throws MqttException {
        //信息
        String sendMsge = "{\n" +
                "\"Response_AlarmInfoPlate\": \n" +
                "    {\n" +
                "\"info\":\"ok\",\n" +
                "\"msgId\":\"2020-4-10 16:23:24 454\",\n" +
                "\"delay\":1000,\n" +
                "\"channelNum\":0\n" +
                "    }\n" +
                "}\n";
        String sendMsg = " {\n" +
                "\"Response_AlarmInfoPlate\": {\n" +
                "\"white_list_operate\":{\n" +
                "\"operate_type\" : 0,\n" +
                "\"white_list_data\":\n" +
                "                [\n" +
                "                    {\n" +
                "\"plate\": \"皖K76932\",\n" +
                "\"enable\": 1,\n" +
                "\"need_alarm\": 0,\n" +
                "\"enable_time\": \"2022-01-010 11:11:11\",\n" +
                "\"overdue_time\": \"2023-01-01 11:11:11\"\n" +
                "                    },\n" +
                "                    {\n" +
                "\"plate\": \"川A12345\",\n" +
                "\"enable\": 1,\n" +
                "\"need_alarm\": 1,\n" +
                "\"enable_time\": \"2018-01-01 11:11:11\",\n" +
                "\"overdue_time\": \"2018-01-01 11:11:11\"\n" +
                "                    }\n" +
                "                ]\n" +
                "            }\n" +
                "        }\n" +
                "    }\n";
        String sendMsg1 = "{\n" +
                "\"Response_AlarmInfoPlate\": {\n" +
                "\"white_list_operate\":{\n" +
                "\"operate_type\" : 1,\n" +
                "        }\n" +
                "    }\n";
        String sendMsg2 = "{\n" +
                "\"Response_AlarmInfoPlate\": \n" +
                "    {\n" +
                "\"serialData\" :[\n" +
                "            {\n" +
                "\"serialChannel\":0,\n" +
                "\"data\" : \"" +str+
                "\",\n" +
                "\"dataLen\" : 123\n" +
                "            } \n" +

                "        ]\n" +
                "    }\n" +
                "}";

        try {
            MqttTopic topic = MQTTTest_tcp.getClient().getTopic(pubTopic);
            MqttMessage message = new MqttMessage(sendMsg2.getBytes());
            message.setQos(0);
            topic.publish(message);
            System.out.println("MQTT Send:" + sendMsg2);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }





}
package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daqing.entity.DongGuanStaticSentryBoxInfo;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarSentryBox;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: car_sentry_box
 * @Author: jeecg-boot
 * @Date:   2022-05-05
 * @Version: V1.0
 */
public interface ICarSentryBoxService extends IService<CarSentryBox> {

    IPage<CarSentryBox> pageList(CarSentryBox carSentryBox, Page<CarSentryBox> page, LambdaQueryWrapper<CarSentryBox> queryWrapper);

    void add(CarSentryBox carSentryBox);

    Result<?> dgSentryBoxInfo(DongGuanStaticSentryBoxInfo data);
}

package org.jeecg.modules.parking.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.parking.entity.CarShiftForm;
import org.jeecg.modules.parking.query.CarShiftFormQuery;
import org.jeecg.modules.parking.service.ICarShiftFormService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.parking.vo.CarDeviceGroupVo;
import org.jeecg.modules.parking.vo.CarShiftFormVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 结算交班表
 * @Author: jeecg-boot
 * @Date:   2022-04-15
 * @Version: V1.0
 */
@Api(tags="结算交班表")
@RestController
@RequestMapping("/parking/carShiftForm")
@Slf4j
public class CarShiftFormController extends JeecgController<CarShiftForm, ICarShiftFormService> {
	@Autowired
	private ICarShiftFormService carShiftFormService;
	 @Autowired
	 private ISysDepartService sysDepartService;
	/**
	 * 分页列表查询
	 *
	 * @param carShiftFormQuery
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "结算交班表-分页列表查询")
	@ApiOperation(value="结算交班表-分页列表查询", notes="结算交班表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarShiftFormQuery carShiftFormQuery,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carShiftFormQuery.setCompanyId(sysDepart.getId());
			}else{
				carShiftFormQuery.setCompanyId("0");
			}
		}
		Page<CarShiftFormVo> page = new Page<CarShiftFormVo>(pageNo, pageSize);
		IPage<CarShiftFormVo> pageList = carShiftFormService.pageList(page, carShiftFormQuery);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carShiftForm
	 * @return
	 */
	@AutoLog(value = "结算交班表-添加")
	@ApiOperation(value="结算交班表-添加", notes="结算交班表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarShiftForm carShiftForm) {
		carShiftFormService.save(carShiftForm);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param carShiftForm
	 * @return
	 */
	@AutoLog(value = "结算交班表-编辑")
	@ApiOperation(value="结算交班表-编辑", notes="结算交班表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarShiftForm carShiftForm) {
		carShiftFormService.updateById(carShiftForm);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "结算交班表-通过id删除")
	@ApiOperation(value="结算交班表-通过id删除", notes="结算交班表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carShiftFormService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "结算交班表-批量删除")
	@ApiOperation(value="结算交班表-批量删除", notes="结算交班表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carShiftFormService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "结算交班表-通过id查询")
	@ApiOperation(value="结算交班表-通过id查询", notes="结算交班表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarShiftForm carShiftForm = carShiftFormService.getById(id);
		if(carShiftForm==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carShiftForm);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carShiftForm
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarShiftForm carShiftForm) {
        return super.exportXls(request, carShiftForm, CarShiftForm.class, "结算交班表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarShiftForm.class);
    }



}

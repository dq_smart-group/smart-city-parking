package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarVehicle;
@Data
public class CarVehicleVo extends CarVehicle {
 //车牌类型
private String name;

//公司名称
private String departName;

 //车场名称
 private String parkingLotName;
}

package org.jeecg.modules.parking.mapper;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.LedDisplay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.query.LedDisplayQuery;
import org.jeecg.modules.parking.vo.LedDisplayVo;

/**
 * @Description: led显示操作表
 * @Author: jeecg-boot
 * @Date:   2022-01-05
 * @Version: V1.0
 */
public interface LedDisplayMapper extends BaseMapper<LedDisplay> {

    List<LedDisplayVo> pageList(Page<LedDisplayVo> page,@Param("q") LedDisplayQuery q);
}

package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarStandard;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class CarStandardQuery extends CarStandard {
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;
    //登陆人id
    private String userId;
    private List<ShouFei> ShouFeiList;
    private List<String> licensePlateTypes;
    private List<TwentyFourShouFei> TwentyFourShouFeiList;
}

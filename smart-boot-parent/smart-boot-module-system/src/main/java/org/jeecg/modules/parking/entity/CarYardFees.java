package org.jeecg.modules.parking.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @Description: car_yard_fees
 * @Author: jeecg-boot
 * @Date:   2022-03-29
 * @Version: V1.0
 */
@Data
@TableName("car_yard_fees")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_yard_fees对象", description="car_yard_fees")
public class CarYardFees implements Serializable {
    private static final long serialVersionUID = 1L;

	/**车场商户收费记录*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "车场商户收费记录")
    private java.lang.String id;
	/**车场id*/
	@Excel(name = "车场id", width = 15)
    @ApiModelProperty(value = "车场id")
    private java.lang.String parkingLotId;
	/**信息收集id*/
	@Excel(name = "信息收集id", width = 15)
    @ApiModelProperty(value = "信息收集id")
    private java.lang.String infoCollectId;
	/**累计收费分钟*/
	@Excel(name = "累计收费分钟", width = 15)
    @ApiModelProperty(value = "累计收费分钟")
    private java.lang.Integer cumulativeMinute;
	/**累计收费分钟*/
	@Excel(name = "累计收费余额", width = 15)
    @ApiModelProperty(value = "累计收费余额")
    private BigDecimal cumulativeAmount;
}

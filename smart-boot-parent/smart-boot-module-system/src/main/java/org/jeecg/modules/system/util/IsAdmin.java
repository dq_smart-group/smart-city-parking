package org.jeecg.modules.system.util;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.modules.system.entity.SysRole;
import org.jeecg.modules.system.entity.SysUserRole;
import org.jeecg.modules.system.service.ISysRoleService;
import org.jeecg.modules.system.service.ISysUserRoleService;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class IsAdmin {
    @Autowired
    private ISysUserRoleService sysUserRoleService;
    @Autowired
    private ISysRoleService sysRoleService;
    public  Boolean getIsAdmin(String loginId){
        SysRole sysRole = null;
        LambdaQueryWrapper<SysRole> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysRole::getRoleCode,"admin");
        wrapper.last("limit 1");
        List<SysRole> list = sysRoleService.list(wrapper);
        if(list.size()>0){
            sysRole = list.get(0);
        }
        String ruleId = null;
        if(sysRole!=null){
            ruleId = sysRole.getId();
        }
        SysUserRole sysUserRole = sysUserRoleService.getSysUserRole(loginId, ruleId);
        if(sysUserRole!=null){
            return true;
        }else{
            return false;
        }
    }
}

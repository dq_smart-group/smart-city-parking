package org.jeecg.modules.parking.mqtt;


import cn.hutool.core.codec.Base64;
import cn.hutool.json.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.jeecg.modules.parking.api.classHexstringToByteUtil;
import org.jeecg.modules.parking.util.GetCrc16;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.Date;

@Slf4j
@Component
public class MqttPushClient {

    @Autowired
    private PushCallback pushCallback;


    private static MqttClient client;

    public static MqttClient getClient() {
        return client;
    }

    public static void setClient(MqttClient client) {
        MqttPushClient.client = client;
    }

    /**
     * 客户端连接
     *
     * @param host     ip+端口
     * @param clientID 客户端Id
     * @param username 用户名
     * @param password 密码
     * @param timeout  超时时间
     * @param keeplive 保留数
     */
    public void connect(String host, String clientID, String username, String password, int timeout, int keeplive) {

        MqttClient client;

        try {

            // host为主机名，clientid即连接MQTT的客户端ID，一般以唯一标识符表示，MemoryPersistence设置clientid的保存形式，默认为以内存保存
            client = new MqttClient(host, clientID, new MemoryPersistence());
            // MQTT的连接设置
            MqttConnectOptions options = new MqttConnectOptions();
            // 设置是否清空session,这里如果设置为false表示服务器会保留客户端的连接记录，设置为true表示每次连接到服务器都以新的身份连接
            options.setCleanSession(true);
            // 设置连接的用户名
            options.setUserName(username);
            // 设置连接的密码
            options.setPassword(password.toCharArray());
            // 设置超时时间 单位为秒
            options.setConnectionTimeout(timeout);
            // 设置会话心跳时间 单位为秒 服务器会每隔1.5*20秒的时间向客户端发送个消息判断客户端是否在线，但这个方法并没有重连的机制
            options.setKeepAliveInterval(keeplive);
            //设置断开后重新连接
            options.setAutomaticReconnect(false);

            MqttPushClient.setClient(client);
            try {
                //监听
                client.setCallback(pushCallback);
                client.connect(options);
                log.info("连接mqtt服务成功");
            }catch (Exception e){
                System.out.println("重连失败");
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * 发布，默认qos为0，非持久化
     * @param topic
     * @param pushMessage
     */

    public static  void pushlish(String topic,String pushMessage){
        pushlish(2,false,topic,pushMessage);
    }

    /**
     * 发布，默认qos为0,保存最后一条
     * @param topic
     * @param pushMessage
     */

    public static  void pushlishRetained(String topic,String pushMessage){
        pushlish(2,true,topic,pushMessage);
    }


    /**
     * 发布
     *
     * @param qos         连接方式
     * @param retained    是否保留
     * @param topic       主题
     * @param pushMessage 消息体
     */

    public static  void pushlish(int qos,boolean retained,String topic,String pushMessage){
        MqttMessage message=new MqttMessage();
        message.setQos(qos);
        message.setRetained(retained);
        message.setPayload(pushMessage.getBytes());
        MqttTopic mqttTopic=MqttPushClient.getClient().getTopic(topic);
        if(null== mqttTopic){
            log.error("topic not exist");
        }
        MqttDeliveryToken token;
        try {
            token=mqttTopic.publish(message);
            token.waitForCompletion();
        }catch (MqttPersistenceException e){
            e.printStackTrace();
        }catch (MqttException e){
            e.printStackTrace();
        }

    }


    /**
     *
     * 描述：开闸
     * topic 发布主题
     * @since
     */
    public static void publishOpen(String topic) throws MqttException {
        Map map = new HashMap();
        Map mapdata = new HashMap();
        mapdata.put("info","ok");
        mapdata.put("delay","1000");
        map.put("Response_AlarmInfoPlate",mapdata);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        MqttPushClient.pushlish(topic,cmd);
    }


    /**
     *
     * 描述：关闸
     * topic 发布主题
     * @since
     */
    public static void publishClose(String topic) throws MqttException {
        String s = "A6 01 01 80 80";
        String s3 = GetCrc16.getCrc16(s);
        byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
        String encode3 = Base64.encode(aa2);
        MqttPushClient.pushlish(topic,getsendMsg(encode3));
    }


    /**
     * 读取道闸状态
     */
    public static void readState(String topic){
        String str = "A6 01 01 3C 3C";
        byte[] aa21 = classHexstringToByteUtil.hexString2Bytes(str);
        String encode31 = Base64.encode(aa21);
        MqttPushClient.pushlish(topic,getsendMsg(encode31));
    }



    /**
     * 手动抓拍
     * serialno序列号
     */
    public static void manualCapture(String serialno){
        Map map = new HashMap();
        Map mapdata = new HashMap();
        mapdata.put("manualTrigger","ok");
        map.put("Response_AlarmInfoPlate",mapdata);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        String topic = getTopic(serialno);
        MqttPushClient.pushlish(topic,cmd);
    }

    /**
     * 订阅某个主题，qos默认为0
     * @param topic
     */
    public void subscribe(String topic){
        log.error("开始订阅主题" + topic);
        subscribe(topic,0);
    }


    public void subscribe(String topic,int qos){
        try {
            MqttPushClient.getClient().subscribe(topic,qos);
        }catch (MqttException e){
            e.printStackTrace();
        }
    }
    /**
     * 生成二维码
     * @param topic
     * @param pushMessage
     */

    public static  void pushlishOrder(String topic,String pushMessage){
        pushlish(topic,getsendMsg(pushMessage));
    }

    /**
     * 下载临时广告语
     * @param topic 发布主题
     * @param cont 广告语内容
     * @param hang 显示第几行
     * @param color 字体颜色
     */
    public static void pushBytesToHexFun1(String topic,String cont,String hang,String color)throws Exception{
        String s = GetCrc16.bytesToHexFun1(cont,hang,color);
        String s3 = GetCrc16.getCrc16(s);
        byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
        String encode3 = Base64.encode(aa2);
        pushlish(topic,getsendMsg(encode3));
    }


    /**
     * 进场时下载临时广告语
     * @param onetext 第一行显示内容
     * @param topic 发布主题
     * @param chepai 车牌
     * @param type 车牌类型
     * @param yu 欢迎语
     */
    public static void pushBytesToHexFuns(String topic,String chepai,String type,String yu,String onetext,String color1,String color2,String color3,String color4)throws Exception{
        //第一行显示
        pushBytesToHexFun1(topic,onetext,"00",color1);
        //第二行显示
        pushBytesToHexFun1(topic,chepai,"01",color2);
        //第三行显示
        pushBytesToHexFun1(topic,type,"02",color3);
        //第四行显示
        pushBytesToHexFun1(topic,yu,"03",color4);
    }

    /**
     * 下载广告语
     * @param topic 发布主题
     * @param cont 广告语内容
     * @param hang 显示第几行
     * @param color 字体颜色
     * @param type 文字显示方式
     */
    public static void pushBytesToHexFun(String topic,String cont,String hang,String color,String type)throws Exception{
        String s = GetCrc16.downloadLanguage(cont,hang,color,type);
        String s3 = GetCrc16.getCrc16(s);
        byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
        String encode3 = Base64.encode(aa2);
        pushlish(topic,getsendMsg(encode3));
    }
    /**
     * 获取播报语音的串码
     * @param topic 主题
     * @param str 播报内容
     * @param type OTP:为操作选项字。
     * @return
     */
    public static void pushVoice(String topic,String str,String type)throws Exception{
        String s = GetCrc16.getVoice(str,type);
        String s3 = GetCrc16.getCrc16(s);
        byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
        String encode3 = Base64.encode(aa2);
        pushlish(topic,getsendMsg(encode3));
    }

    /**
     * 调整音量大小
     * @param sum
     */
    public static void pushVolume(String topic,Integer sum){
        String s = GetCrc16.getVolume(sum);
        String s3 = GetCrc16.getCrc16(s);
        byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
        String encode3 = Base64.encode(aa2);
        pushlish(topic,getsendMsg(encode3));
    }



    /**
     * 获取拼接好后的内容
     * @param str
     * @return
     */
    public static String getsendMsg(String str){
        Map map = new HashMap();
        Map mapdata = new HashMap();
        Map mapdata1 = new HashMap();
        mapdata1.put("serialChannel","0");
        mapdata1.put("data",str);
        mapdata1.put("dataLen",123);
        List<Map> list = new ArrayList<>();
        list.add(mapdata1);
        mapdata.put("serialData",list);
        map.put("Response_AlarmInfoPlate",mapdata);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return cmd;
    }

    /**
     * 调整屏幕亮度
     */
    public static void adjustBrightness(String topic,Integer sum){
        String s = GetCrc16.adjustBrightness(sum);
        pushlish(topic,getsendMsg(s));
    }

    /**
     * 同步当前时间到设备
     */
    public static void synchronizationTime(String serialno){
        String topic = getTopic(serialno);
        String s = GetCrc16.synchronizationTime();
        byte[] bytes = classHexstringToByteUtil.hexString2Bytes(s);
        String encode31 = Base64.encode(bytes);
        pushlish(topic,getsendMsg(encode31));
    }

    /**
     *
     * @param deviceSn  设备号
     * @param operateType  操作类型(0:增加，1：删除)
     * @param plate      车牌
     * @param enable     是否有效
     * @param need_alarm  黑白名单
     * @param enable_time  生效时间
     * @param overdue_time 过期时间
     */
    public static void pushWhiteorBlack(String deviceSn,int operateType,String plate, int enable, int need_alarm, String enable_time, String overdue_time) {
        String topic = getTopic(deviceSn);
        String white = getcarWhiteorBlack(operateType,plate, enable, need_alarm, enable_time, overdue_time);
        pushlish(topic, white);
    }

    public static String getcarWhiteorBlack(int operateType,String plate, int enable, int need_alarm, String enableTime, String overdueTime) {
        String sendMsg = " {\n" +
                "\"Response_AlarmInfoPlate\": {\n" +
                "\"white_list_operate\":{\n" +
                "\"operate_type\" : "+operateType+",\n" +
                "\"white_list_data\":\n" +
                "                [\n" +
                "                    {\n" +
                "\"plate\": \"" + plate + "\",\n" +
                "\"enable\": " + enable + ",\n" +
                "\"need_alarm\": " + need_alarm + ",\n" +
                "\"enable_time\": \"" + enableTime + "\",\n" +
                "\"overdue_time\": \"" + overdueTime + "\"\n" +
                "                    }\n" +
                "                ]\n" +
                "            }\n" +
                "        }\n" +
                "    }\n";
        System.out.println(sendMsg);
        return sendMsg;
    }

    /**
     * 获取发布主题
     * @param str 设备序列号
     * @return
     */
    public static String getTopic(String str){
        return "/device/" + str + "/command";
    }
}
package org.jeecg.modules.device.mapper;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.device.entity.CameraDevice;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.device.query.CameraDeviceQuery;
import org.jeecg.modules.device.vo.CameraDeviceVo;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;

/**
 * @Description: 停车场摄像机设备信息表
 * @Author: jeecg-boot
 * @Date:   2021-11-25
 * @Version: V1.0
 */
public interface CameraDeviceMapper extends BaseMapper<CameraDevice> {

    //
    List<CameraDeviceVo> listPage(Page<CameraDeviceVo> page, @Param("q") CameraDeviceQuery q);


    //根据公司id查询出公司下的设备
    List<CameraDevice> selectByOrgCode(@Param("companyId") String companyId);

    /**
     * 根据设备组id查询车场内所有的设备
     * @param id
     * @return
     */
    List<ParkingLotAndCamVo> getByParkingLotId(@Param("id") String id);

    /**
     * 根据设备组id和设备类型查找数据
     * @param deviceGroupId
     * @param parkingDeviceType0
     * @return
     */
    List<CameraDevice> getByGroupIdAndRepairFlag(@Param("id") String id,@Param("deviceGroupId") String deviceGroupId, @Param("parkingDeviceType0") String parkingDeviceType0);

    /**
     * 根据设备组id查找该设备组的主设备
     * @param collect
     * @return
     */
    List<CameraDevice> getMainDeviceByGroupIds(@Param("list") List<String> collect);


    /**
     * 根据序列号查出设备信息
     * @param serlino  设备序列号
     * @return
     */
    CameraDeviceVo selectByDevice(@Param("q") String serlino);

    /**
     * 根据园区id查找设备
     * @param parkId
     * @return
     */
    List<CameraDevice> getDeviceByParkId(@Param("parkId") String parkId);

    /**
     * 查询启用车场下面的所有设备
     * @return
     */
    List<CameraDevice> selectByParkingStateYard();

    /**
     *  根据园区id查询此园区下的所有设备
     * @param screenCode
     * @return
     */
    List<String> screenDeviceList(@Param("screenCode")String screenCode);

}

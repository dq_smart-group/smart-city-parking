package org.jeecg.modules.authentication.utils;

import cn.hutool.crypto.SecureUtil;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class SignUtil {

    /**
     * HashMap转TreeMap
     * @param map
     * @return
     */
    public static TreeMap<String, Object> mapToTreeMap(Map<String,Object> map){
        TreeMap<String, Object> mapBuild = MapBuild.treeMapBuild();
        for (String key : map.keySet()) {
            mapBuild.put(key,map.get(key));
        }
        return mapBuild;
    }


    /**
     * HashMap转TreeMap
     * @param treeMap map
     * @return
     */
    public static Map<String ,Object> threeMapToMap(TreeMap<String, Object> treeMap){
        Map<String, Object> mapBuild = MapBuild.hashMapBuild();
        for (String key : treeMap.keySet()) {
            mapBuild.put(key,treeMap.get(key));
        }
        return mapBuild;
    }


    /**
     * 签名
     * @param mapParam
     * @return
     */
    public static String sign(TreeMap<String, Object> mapParam , String accessKey , String accessSecret) {
        String paramStr = getMapToString(mapParam);
        String signParam = paramStr + accessKey + accessSecret + paramStr;
        return SecureUtil.sha1(signParam).toUpperCase();
    }

    /**
     * Map转String
     *
     * @param map
     * @return
     */
    public static String getMapToString(Map<String, Object> map) {
        Set<String> keySet = map.keySet();
        //将set集合转换为数组
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        //给数组排序(升序)
        Arrays.sort(keyArray);
        //因为String拼接效率会很低的，所以转用StringBuilder
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keyArray.length; i++) {
            // 参数值为空，则不参与签名 这个方法trim()是去空格
                sb.append(keyArray[i]).append("=").append(String.valueOf(map.get(keyArray[i])).trim());
        }
        return sb.toString();
    }

}
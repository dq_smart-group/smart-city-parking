package org.jeecg.modules.system.mapper;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.system.query.SysConfigQuery;
import org.jeecg.modules.system.entity.SysConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * @Description: 系统分类配置表
 * @Author: jeecg-boot
 * @Date:   2021-12-03
 * @Version: V1.0
 */
public interface SysConfigMapper extends BaseMapper<SysConfig> {
    /**
     * 通过标识查询数据
     * @param query
     * @return
     */
    List<SysConfig> queryByFlag(@Param("q") SysConfigQuery query);
}

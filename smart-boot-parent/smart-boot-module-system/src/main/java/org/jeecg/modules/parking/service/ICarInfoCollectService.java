package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.entity.CarShiftForm;
import org.jeecg.modules.parking.query.CarInfoQuery;
import org.jeecg.modules.parking.vo.CarInfoCollectVo;
import org.jeecg.modules.parking.vo.MoneyStatisticsVo;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 停车车辆信息收集表
 * @Author: jeecg-boot
 * @Date:   2021-11-24
 * @Version: V1.0
 */
public interface ICarInfoCollectService extends IService<CarInfoCollect> {

    /**
     * 对摄像机传送的数据进行处理
     * @param carInfo
     * @return
     */
    //void addCarInfo(String carInfo) throws  Exception;

    /*
    * 条件查询
    * */
    Page<CarInfoCollectVo> selectData(Page page, CarInfoCollectVo carInfoCollectvo);


    Result<?> calculatedAmount(String id, String license, Date outTime)throws Exception;

    /**
     * 根据车牌查出最新的进场记录删除
     * @param chepai
     */
    void deleteByChePai(String chepai);


    /**
     * 获取小于strDate的数据的数量
     * @param strDate
     * @return
     */
    int getListCountBeforeDay(String strDate);

    /**
     * 判断是否存在该表
     * @param tableName
     * @return
     */
    List<String> hasTable(String tableName);

    /**
     * 查询小于strDate时间的数据
     * @param strDate
     * @return
     */
    List<CarInfoCollect> getListBeforeDay(String strDate);


    /**
     * 根据时间删除表数据
     * @param strDate
     * @return
     */
    boolean deleteToBeforeDay(String strDate);

    /**
     * 根据车牌号查找进场信息
     */
    Result<?> getInfoByPlate(String plate);

    /**
     * 根据出场时间生成订单
     * @return
     */
    Result<?> generateOrder(String id,String openid);

    /**
     *订单支付成功后修改记录状态
     */
//    void paySuccess(CarInfoCollect carInfoCollect);

    /**
     * 免费放行和结算放行
     * @return
     */
    Result<?> settlementRelease(CarInfoQuery carInfoQuery);

    /**
     * 从缓存获取该设备组的主设备
     * groupId 设备组id
     */
    CameraDevice getMainDevice(String groupId);

    Integer getCountByloginTime(Date loginTime,List<String> list);

    List<CarInfoCollect>  getoutCountByloginTime(Date loginTime,List<String> list);

    /**
     * 发送设备的实时状态给3D
     * cameraDeviceId 设备id
     * status 状态
     */
    void send3DWebsocket(String cameraDeviceId,String status);

    /**
     * http 上传图片
     * @param obj
     */
    void uploadPictures(Object obj);

    /**
     * 根据园区Id查询此园区最近一天的出入场记录
     * @param screenCode
     * @return
     */
    Result<?> screenCarInfo(String screenCode);

    //统计金额
    Result<?> moneyStatistics(CarInfoCollectVo carInfoCollectvo);

    //统计一下这段时间内现金收款：已收金额和减免金额
    MoneyStatisticsVo moneyStatisticss(CarShiftForm dataByUserId);
}

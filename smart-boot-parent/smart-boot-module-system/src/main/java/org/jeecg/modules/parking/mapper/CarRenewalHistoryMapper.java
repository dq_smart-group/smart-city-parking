package org.jeecg.modules.parking.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarRenewalHistory;
import org.jeecg.modules.parking.vo.CarRenewalHistoryVo;

import java.util.List;

/**
 * @Description: car_renewal_history
 * @Author: jeecg-boot
 * @Date:   2022-04-16
 * @Version: V1.0
 */
public interface CarRenewalHistoryMapper extends BaseMapper<CarRenewalHistory> {

    List<CarRenewalHistoryVo> pageList(Page<CarRenewalHistoryVo> page, @Param("rh") CarRenewalHistoryVo carRenewalHistoryVo);
}

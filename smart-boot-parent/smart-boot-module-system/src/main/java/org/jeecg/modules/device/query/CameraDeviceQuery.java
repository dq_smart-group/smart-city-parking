package org.jeecg.modules.device.query;

import lombok.Data;
import org.jeecg.modules.device.entity.CameraDevice;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class CameraDeviceQuery extends CameraDevice {
    /**创建开始时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    /**创建结束时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;
    //登陆人id
    private String userId;


}

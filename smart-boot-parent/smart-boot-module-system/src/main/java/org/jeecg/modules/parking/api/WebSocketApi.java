package org.jeecg.modules.parking.api;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.constant.WebsocketConst;
import org.jeecg.modules.parking.entity.CarCommercial;
import org.jeecg.modules.parking.service.impl.CarCommercialServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;

@ServerEndpoint("/parking/websocket/ListeningQR/{id}")
@Component
@Slf4j
public class WebSocketApi {

    private static CarCommercialServiceImpl commercialService;

    private Session session;

    private static String id;

    @Autowired
    public void setCommercialService(CarCommercialServiceImpl commercialService){
        WebSocketApi.commercialService = commercialService;
    }

    @OnMessage
//    @PostMapping("/ListeningQR")
    public void ListeningQR(String message, Session session){
        log.debug("【webSocketApi消息】收到客户端消息:" + message);
        CarCommercial serviceOne = commercialService.getOne(Wrappers.<CarCommercial>lambdaQuery()
                .eq(CarCommercial::getId, this.id)
                .eq(CarCommercial::getQuickMark, message));
        if (null==serviceOne){
            serviceOne = commercialService.getById(this.id);
        }
        JSONObject obj = new JSONObject();
        //业务类型
        obj.put(WebsocketConst.MSG_CMD, WebsocketConst.CMD_CHECK);
        //消息内容
        obj.put(WebsocketConst.MSG_TXT, "心跳响应");
        for (WebSocketApi webSocket : webSockets) {
            webSocket.pushMessage(serviceOne.getQuickMark());
        }
    }
    /**
     * 缓存 WebSocketApi连接到单机服务class中（整体方案支持集群）
     */
    private static CopyOnWriteArraySet<WebSocketApi> webSockets = new CopyOnWriteArraySet<>();
    private static Map<String, Session> sessionPool = new HashMap<String, Session>();


    @OnOpen
    public void onOpen(Session session, @PathParam(value = "id") String id) {
        try {
            this.session = session;
            this.id = id;
            webSockets.add(this);
            sessionPool.put(id, session);
            log.info("【webSocketApi消息】有新的连接，总数为:" + webSockets.size());
        } catch (Exception e) {
        }
    }

    @OnClose
    public void onClose() {
        try {
            webSockets.remove(this);
            sessionPool.remove(this.id);
            log.info("【webSocketApi消息】连接断开，总数为:" + webSockets.size());
        } catch (Exception e) {
        }
    }


    /**
     * 服务端推送消息
     *
     * @param id
     * @param message
     */
    public void pushMessage(String id, String message) {
        Session session = sessionPool.get(id);
        if (session != null && session.isOpen()) {
            try {
                //update-begin-author:taoyan date:20211012 for: websocket报错 https://gitee.com/jeecg/jeecg-boot/issues/I4C0MU
                synchronized (session){
                    log.info("【webSocketApi消息】 单点消息:" + message);
                    session.getBasicRemote().sendText(message);
                }
                //update-end-author:taoyan date:20211012 for: websocket报错 https://gitee.com/jeecg/jeecg-boot/issues/I4C0MU
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 服务器端推送消息
     */
    public void pushMessage(String message) {
        try {
            webSockets.forEach(ws -> ws.session.getAsyncRemote().sendText(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

package org.jeecg.modules.parking.api;


import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.oss.service.IOSSFileService;
import org.jeecg.modules.parking.service.ICarBannerService;
import org.jeecg.modules.parking.service.ICarInfoCollectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping("/api/carApi")
@Slf4j
public class CarApi {
    @Autowired
    private ICarInfoCollectService carInfoCollectService;


    @Autowired
    private ICarBannerService carBannerService;


    @Autowired
    private IOSSFileService iossFileService;

    @Resource(name = "ParkingImgExecutor")
    private ThreadPoolTaskExecutor ParkingImgExecutor;

    @Autowired
    private RedisUtil redisUtil;



    /**
     * HTTP 推送接受数据
     * @return
     * @throws IOException
     */
    @PostMapping(value = "/shibiePost")
    public String ShibiePost(@RequestBody Object obj) throws Exception {
        String s = JSON.toJSONString(obj);
        JSONObject jsonObject = JSONObject.fromObject(s);
        JSONObject alarmInfoPlate= JSONObject.fromObject(jsonObject.getString("AlarmInfoPlate"));
        if(alarmInfoPlate.has("result")){
            ParkingImgExecutor.execute(()->{
                carInfoCollectService.uploadPictures(obj);
            });
        }
        //接口带自定义
        return null;
    }


    /**
     * 心跳返回开闸信号
     * @param request
     * @param response
     * @return
     * @throws IOException
     */
    @PostMapping(value = "/xintiao")
    protected String lunXun(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String sendBlack="{\"Response_AlarmInfoPlate\":{\"info\":\"ok\",\"content\":\"...\",\"is_pay\":\"true\"}}";
        return sendBlack;
    }


    /**
     * 支付系统根传递车牌号，查询信息
     * @param obj
     * @return
     */
//    @PostMapping(value = "/paySelect")
//    public Result<?> paySelectCarInfo(@RequestBody Object obj){
//        //取值转json
//        JSONObject jsonObject=JSONObject.fromObject(obj);
//        if(jsonObject.has("license")){
//            //获取车牌号
//            String license=jsonObject.getString("license");
//            return carAccessRecordService.selectPayInfo(license);
//        }
//        return Result.error("车牌号不能为空");
//    }

    /**
     * 根据传递的预计的出库时间，计算出库金额
     * @return
     */
    @PostMapping(value = "carPayAmount")
    public Result<?> calculatedAmount(@RequestBody Object obj) throws Exception{
        //取值转json
        JSONObject jsonObject=JSONObject.fromObject(obj);
        if(!jsonObject.has("id")){
            return Result.error("记录id不能为空");
        }
        if(!jsonObject.has("license")){
            return Result.error("车牌号不能为空");
        }
        if(!jsonObject.has("outTime")){
            return Result.error("预计出库时间不能为空");
        }
        //获取id,车牌号,出库时间
        String id=jsonObject.getString("id");
        String license=jsonObject.getString("license");
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date outTime=DateUtils.str2Date(jsonObject.getString("outTime"),ft);
        return carInfoCollectService.calculatedAmount(id,license,outTime);
    }


    /**
     * 根据组来获取banner
     * @param map
     * @return
     */
    @PostMapping("/parking/getCarBanner")
    public Result<?> getCarBanner(@RequestBody Map<String,String> map){
        String group = map.get("group");
        Assert.isTrue(!StringUtils.isEmpty(group),"参数异常");
        return carBannerService.getCarBanner(group);
    }


//
//    @GetMapping("/test")
//    public Result test()throws Exception{
//        File file = new File("E:/home/img/pathCarImg_1650246247337.jpg");
//        InputStream inputStream = new FileInputStream(file);
//        MultipartFile multipartFile = new MockMultipartFile(file.getName(),file.getName(), ContentType.APPLICATION_OCTET_STREAM.toString(), inputStream);
//        iossFileService.upload(multipartFile,"upload/packing");
//        return null;
//    }


}

package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarShiftForm;

@Data
public class CarShiftFormQuery extends CarShiftForm {

}

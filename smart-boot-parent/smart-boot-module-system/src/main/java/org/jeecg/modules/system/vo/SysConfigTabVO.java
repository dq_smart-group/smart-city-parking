package org.jeecg.modules.system.vo;

import org.jeecg.modules.system.entity.SysConfigTab;

public class SysConfigTabVO extends SysConfigTab {

    /**
     * 状态名称
     */
    private String statusName;

    /**
     * 类型名称
     */
    private String typeName;

}
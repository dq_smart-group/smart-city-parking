package org.jeecg.modules.parking.mqtt;


import org.jeecg.modules.system.util.SysConfigUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@Configuration
@ConfigurationProperties(MqttConfiguration.PREFIX)
public class MqttConfiguration {

    @Autowired
    private MqttPushClient mqttPushClient;


    public static final  String PREFIX="mqtt";
    private String host;
    private String clientid;
    private String username;
    private String password;
    private String topic;
    private int timeout;
    private int keepalive;

    /**
     * 项目启动的时候，自动连接mqtt服务器
     * @return
     */
    @Bean
    public MqttPushClient getMqttPushClient() {
        mqttPushClient.connect("tcp://"+SysConfigUtil.getVal("mqtt_ip")+":"+SysConfigUtil.getVal("mqtt_port"), UUID.randomUUID().toString(), SysConfigUtil.getVal("mqtt_number"), SysConfigUtil.getVal("mqtt_password"), 1000,100);
        // 以/#结尾表示订阅所有以test开头的主题
        mqttPushClient.subscribe("/device/push/result", 2);
        return mqttPushClient;
    }
    /**
     * mqtt断开时调用，在PushCallback监听类调用重连的方法
     * @return
     */
    public MqttPushClient getMqttPushClients() {
        mqttPushClient.connect("tcp://"+SysConfigUtil.getVal("mqtt_ip")+":"+SysConfigUtil.getVal("mqtt_port"), UUID.randomUUID().toString(), SysConfigUtil.getVal("mqtt_number"), SysConfigUtil.getVal("mqtt_password"), 1000,100);
        // 以/#结尾表示订阅所有以test开头的主题，
        // qos : 0：最多一次 、1：最少一次 、2：只有一次
        mqttPushClient.subscribe("/device/push/result", 2);
        return mqttPushClient;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getClientid() {
        return clientid;
    }

    public void setClientid(String clientid) {
        this.clientid = clientid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public int getKeepalive() {
        return keepalive;
    }

    public void setKeepalive(int keepalive) {
        this.keepalive = keepalive;
    }
}
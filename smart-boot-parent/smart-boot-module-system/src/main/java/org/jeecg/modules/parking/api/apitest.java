package org.jeecg.modules.parking.api;

import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.formula.functions.T;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/api/testApi")
@Slf4j
public class apitest {

    public static void main(String[] args) throws Exception{
        CarInfoCollect carInfoCollect = new CarInfoCollect();
        //可以单独传入http参数，这样参数会自动做URL编码，拼接在URL中
        JSONObject query = JSONUtil.createObj();
        //HashMap<String, Object> query = new HashMap<>();
        query.put("license", "3112222222");
        query.put("carType", "临时车");
        query.put("parkCost", "10");
        query.put("costStartTime", "2021-12-17 16:18");
        query.put("costEndTime", "2021-12-17 16:18");
        query.put("depId", "133131");
        query.put("depName", "qrfrefe");
        query.put("parkingLotId", "42423");
        query.put("parkingLotName", "fafasfsaf");
        query.put("carTypeId", "53242424");
        query.put("cheLianId", "53242424434");
        query.put("sub_mchid", "1617522518");
        String s1 = query.toString();
        String result = HttpUtil.post("http://192.168.3.248:9980/smart-boot/parking/api/order",s1);
        Result result1 = com.alibaba.fastjson.JSONObject.parseObject(result, Result.class);
        String result2 = JSON.toJSONString(result1.getResult());
        HashMap hashMa = JSON.parseObject(result2,HashMap.class);
        Object code_url = hashMa.get("code_url");
        System.out.println(hashMa.get("code_url"));
        //String s1 = abv.mapPost("http://pay.daqinkc.com:8081/smart-boot/parking/api/order", query);
       // System.out.println(result);
    }

    @PostMapping(value = "smoke")
    public Result<?> smoke(@RequestBody Object obj){
        System.out.println(obj);

        return Result.ok();
    }

}

package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.LedDisplay;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class LedDisplayQuery extends LedDisplay {
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;
    //车场id
    private String parkingLotId;
    //设备序列号
    private String serialno;
}

package org.jeecg.modules.parking.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.config.authentication.AccessLimit;
import org.jeecg.modules.parking.entity.CarCommercial;
import org.jeecg.modules.parking.service.ICarCommercialService;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: car_commercial
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
@Api(tags="car_commercial")
@RestController
@RequestMapping("/parking/carCommercial")
@Slf4j
public class CarCommercialController extends JeecgController<CarCommercial, ICarCommercialService> {
	@Autowired
	private ICarCommercialService carCommercialService;
	 @Autowired
	 private ISysDepartService sysDepartService;
	
	/**
	 * 分页列表查询
	 *
	 * @param carCommercial
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AccessLimit(seconds = 15,maxCount = 5)
	@AutoLog(value = "car_commercial-分页列表查询")
	@ApiOperation(value="car_commercial-分页列表查询", notes="car_commercial-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarCommercial carCommercial,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carCommercial.setCompanyId(sysDepart.getId());
			}else{
				carCommercial.setCompanyId("0");
			}
		}
		LambdaQueryWrapper<CarCommercial> wrapper = Wrappers.lambdaQuery();
		if(StringUtils.isNotBlank(carCommercial.getName())) {
			wrapper.and(c->c.like(CarCommercial::getName, carCommercial.getName()));
		}
		if(StringUtils.isNotBlank(carCommercial.getPhone())) {
			wrapper.and(c->c.like(CarCommercial::getPhone, carCommercial.getPhone()));
		}
		if(StringUtils.isNotBlank(carCommercial.getAddress())) {
			wrapper.and(c->c.like(CarCommercial::getAddress, carCommercial.getAddress()));
		}
		Page<CarCommercial> page = new Page<CarCommercial>(pageNo, pageSize);
		IPage<CarCommercial> pageList = carCommercialService.page(page, wrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carCommercial
	 * @return
	 */
	@AccessLimit(seconds = 15,maxCount = 5)
	@AutoLog(value = "car_commercial-添加")
	@ApiOperation(value="car_commercial-添加", notes="car_commercial-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarCommercial carCommercial) {

		carCommercialService.saveCarCommercial(carCommercial);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param carCommercial
	 * @return
	 */
	@AccessLimit(seconds = 15,maxCount = 5)
	@AutoLog(value = "car_commercial-编辑")
	@ApiOperation(value="car_commercial-编辑", notes="car_commercial-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarCommercial carCommercial) {
		carCommercialService.edit(carCommercial);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_commercial-通过id删除")
	@ApiOperation(value="car_commercial-通过id删除", notes="car_commercial-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carCommercialService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "car_commercial-批量删除")
	@ApiOperation(value="car_commercial-批量删除", notes="car_commercial-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carCommercialService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AccessLimit(seconds = 15,maxCount = 5)
	@AutoLog(value = "car_commercial-通过id查询")
	@ApiOperation(value="car_commercial-通过id查询", notes="car_commercial-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarCommercial carCommercial = carCommercialService.getById(id);
		if(carCommercial==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carCommercial);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carCommercial
    */
	@AccessLimit(seconds = 15,maxCount = 5)
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarCommercial carCommercial) {
        return super.exportXls(request, carCommercial, CarCommercial.class, "car_commercial");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
	@AccessLimit(seconds = 15,maxCount = 5)
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarCommercial.class);
    }


}

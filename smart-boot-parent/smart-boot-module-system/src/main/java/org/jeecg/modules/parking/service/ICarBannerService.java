package org.jeecg.modules.parking.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarBanner;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 停车场banner广告
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
public interface ICarBannerService extends IService<CarBanner> {

    /**
     * 是否点击跳转
     * @param ids    id值
     * @param value    value值 0否 1是
     * @return
     */
    Result<?> onSwitchChangeClick(String ids, String value);

    /**
     * 是否显示
     * @param ids
     * @param value
     * @return
     */
    Result<?> onSwitchChangeShow(String ids, String value);

    /**
     * 获取banner中的group
     * @return
     */
    Result<?> getBannerGroup();

    /**
     * 根据组来获取banner
     * @param group
     * @return
     */
    Result<?> getCarBanner(String group);
}

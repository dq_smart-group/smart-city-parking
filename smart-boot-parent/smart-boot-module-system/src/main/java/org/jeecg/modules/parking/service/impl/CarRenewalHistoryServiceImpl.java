package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.modules.parking.entity.CarRenewalHistory;
import org.jeecg.modules.parking.mapper.CarRenewalHistoryMapper;
import org.jeecg.modules.parking.service.ICarRenewalHistoryService;
import org.jeecg.modules.parking.vo.CarRenewalHistoryVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: car_renewal_history
 * @Author: jeecg-boot
 * @Date:   2022-04-16
 * @Version: V1.0
 */
@Service
public class CarRenewalHistoryServiceImpl extends ServiceImpl<CarRenewalHistoryMapper, CarRenewalHistory> implements ICarRenewalHistoryService {

    @Autowired
    private ISysDepartService sysDepartService;
    @Override
    public IPage<CarRenewalHistoryVo> pageList(Page<CarRenewalHistoryVo> page, CarRenewalHistoryVo carRenewalHistoryVo) {
        if(!LoginUserUtil.isAdmin()){
            SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
            if(sysDepart!=null){
                carRenewalHistoryVo.setCompanyId(sysDepart.getId());
            }else{
                carRenewalHistoryVo.setCompanyId("0");
            }
        }
        List<CarRenewalHistoryVo> carRenewalHistoryVoIPage = this.baseMapper.pageList(page, carRenewalHistoryVo);
        return page.setRecords(carRenewalHistoryVoIPage);
    }
}

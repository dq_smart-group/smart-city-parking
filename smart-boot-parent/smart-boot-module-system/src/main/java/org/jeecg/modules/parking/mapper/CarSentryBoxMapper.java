package org.jeecg.modules.parking.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarSentryBox;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: car_sentry_box
 * @Author: jeecg-boot
 * @Date:   2022-05-05
 * @Version: V1.0
 */
public interface CarSentryBoxMapper extends BaseMapper<CarSentryBox> {

}

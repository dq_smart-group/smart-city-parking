package org.jeecg.modules.parking.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 收费标准表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
@Data
@TableName("car_standard")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_standard对象", description="收费标准表")
public class CarStandard implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**车场id*/
	@Excel(name = "车场id", width = 15)
    @ApiModelProperty(value = "车场id")
    private java.lang.String parkingLotId;
	/**收费名称*/
	@Excel(name = "收费名称", width = 15)
    @ApiModelProperty(value = "收费名称")
    private java.lang.String name;
	/**收费类型(小时，日租，月租，年租)*/
	@Excel(name = "收费类型(小时，日租，月租，年租)", width = 15)
    @ApiModelProperty(value = "收费类型(小时，日租，月租，年租)")
    private java.lang.String chargeType;
	/**车牌类型（黄牌车，蓝牌车）*/
	@Excel(name = "车牌类型（黄牌车，蓝牌车）", width = 15)
    @ApiModelProperty(value = "车牌类型（黄牌车，蓝牌车）")
    private java.lang.String licensePlateType;
	/**计费单价*/
	@Excel(name = "计费单价", width = 15)
    @ApiModelProperty(value = "计费单价")
    private java.math.BigDecimal money;
	/**小时收费标准*/
	@Excel(name = "小时收费标准", width = 15)
    @ApiModelProperty(value = "小时收费标准")
    private String hourlyCharge;
	/**是否续费标准*/
	@Excel(name = "是否续费标准", width = 15)
    @ApiModelProperty(value = "是否续费标准")
    private java.lang.Integer renewFlag;
    /**全天最高收费*/
    @Excel(name = "全天最高收费", width = 15)
    @ApiModelProperty(value = "全天最高收费")
    private java.math.BigDecimal highestMoney;
    /**免费分钟*/
    @Excel(name = "免费分钟", width = 15)
    @ApiModelProperty(value = "免费分钟")
    private java.lang.Integer freeMinutes;
    /**免费分钟是否参与计费*/
    @Excel(name = "免费分钟是否参与计费", width = 15)
    @ApiModelProperty(value = "免费分钟是否参与计费")
    private Boolean chargingFlag;
    /**免费分钟是否参与计费*/
    @Excel(name = "小时收费类型（按24小时，分段收费）", width = 15)
    @ApiModelProperty(value = "小时收费类型（按24小时，分段收费）")
    private String hourlyChargeType;
    /**免费分钟是否参与计费*/
    @Excel(name = "计费规则", width = 15)
    @ApiModelProperty(value = "计费规则")
    private String billingRules;
    /**是否启用*/
    @Excel(name = "是否启用", width = 15)
    @ApiModelProperty(value = "是否启用")
    private Boolean enableFlag;
    /**未知*/
    @Excel(name = "未知", width = 15)
    @ApiModelProperty(value = "未知")
    private Boolean enablesFlag;

    private String companyId;
}

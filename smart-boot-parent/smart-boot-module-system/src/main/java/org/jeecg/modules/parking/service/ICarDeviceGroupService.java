package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.parking.entity.CarDeviceGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.query.CarDeviceGroupQuery;
import org.jeecg.modules.parking.vo.CarDeviceGroupVo;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;

import java.util.List;

/**
 * @Description: 设备组表
 * @Author: jeecg-boot
 * @Date:   2022-04-02
 * @Version: V1.0
 */
public interface ICarDeviceGroupService extends IService<CarDeviceGroup> {

    IPage<CarDeviceGroupVo> pageList(Page<CarDeviceGroupVo> page, CarDeviceGroupQuery carDeviceGroup);

    /**
     * 获取登录人下的设备组 级联
     * @return
     */
    List<ParkingLotAndCamVo> selectByCompanyId();

    /**
     * 获取登录人下的设备组 级联
     * @param companyId
     * @return
     */
    List<ParkingLotAndCamVo> selectDeviceGroupByCompanyId(String companyId);

    /**
     * 加载级联设备组的信息到redis中
     */
    void saveDeviceGroupInRedis();

    /**
     * 根据车场id获取设备组
     * @return
     */
    List<CarDeviceGroup> getCameraDanDeviceGroup(String companyId);

    /**
     * 更新redis数据
     */
    void updateRedisDeviceGroup();
}

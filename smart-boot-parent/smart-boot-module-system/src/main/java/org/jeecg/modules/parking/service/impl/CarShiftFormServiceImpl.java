package org.jeecg.modules.parking.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.modules.parking.entity.CarShiftForm;
import org.jeecg.modules.parking.mapper.CarShiftFormMapper;
import org.jeecg.modules.parking.query.CarShiftFormQuery;
import org.jeecg.modules.parking.service.ICarChargeService;
import org.jeecg.modules.parking.service.ICarShiftFormService;
import org.jeecg.modules.parking.vo.CarShiftFormVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Description: 结算交班表
 * @Author: jeecg-boot
 * @Date:   2022-04-15
 * @Version: V1.0
 */
@Service
public class CarShiftFormServiceImpl extends ServiceImpl<CarShiftFormMapper, CarShiftForm> implements ICarShiftFormService {

    @Autowired
    private ICarChargeService carChargeService;
    @Autowired
    private ISysDepartService sysDepartService;
    /**
     * 添加交班记录
     * @param data
     * @return
     */
    @Override
    public Result addCarShiftForm(Map<String, String> data) {
        String id = data.get("id");
        if(StringUtils.isEmpty(id)){
            return Result.error("参数异常");
        }
        Boolean flag = carChargeService.isCharge(id);
        if(!flag){
            return Result.error("登录账户不是收费员，请检查");
        }
        List<CarShiftForm> list= this.baseMapper.selectByIdAndTime(id);
        if(list.size()>0){
            return Result.OK();
        }
        CarShiftForm carShiftForm = new CarShiftForm();
        carShiftForm.setChargeId(id);
        carShiftForm.setLoginTime(new Date());
        this.baseMapper.insert(carShiftForm);
        return Result.OK();
    }

    @Override
    public CarShiftForm getDataByUserId(String userId) {
        List<CarShiftForm> list= this.baseMapper.selectByIdAndTime(userId);
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }

    @Override
    public IPage<CarShiftFormVo> pageList(Page<CarShiftFormVo> page, CarShiftFormQuery carShiftFormQuery) {
        return page.setRecords(this.baseMapper.pageList(page,carShiftFormQuery));
    }
}

package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarVehicle;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class CarVehicleQuery extends CarVehicle {
    /**创建开始时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    /**创建结束时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;

    private List<String> ids;
}

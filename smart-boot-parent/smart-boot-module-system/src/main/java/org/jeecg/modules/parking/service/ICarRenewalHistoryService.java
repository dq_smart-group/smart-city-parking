package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.entity.CarRenewalHistory;
import org.jeecg.modules.parking.vo.CarRenewalHistoryVo;

/**
 * @Description: car_renewal_history
 * @Author: jeecg-boot
 * @Date:   2022-04-16
 * @Version: V1.0
 */
public interface ICarRenewalHistoryService extends IService<CarRenewalHistory> {

    IPage<CarRenewalHistoryVo> pageList(Page<CarRenewalHistoryVo> page, CarRenewalHistoryVo carRenewalHistoryVo);
}

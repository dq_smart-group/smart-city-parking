package org.jeecg.modules.parking.vo;

import lombok.Data;

import java.util.List;

@Data
public class ParkingLotAndCamVo {
    private String value;
    private String label;
    private List<ParkingLotAndCamVo> children;
}

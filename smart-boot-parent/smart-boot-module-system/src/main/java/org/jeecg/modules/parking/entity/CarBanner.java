package org.jeecg.modules.parking.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 停车场banner广告
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
@Data
@TableName("car_banner")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_banner对象", description="停车场banner广告")
public class CarBanner implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**图片路由*/
	@Excel(name = "图片路由", width = 15)
    @ApiModelProperty(value = "图片路由")
    private java.lang.String imageUrl;
	/**是否点击跳转*/
	@Excel(name = "是否点击跳转", width = 15)
    @ApiModelProperty(value = "是否点击跳转")
    private String isClick;
	/**banner组*/
	@Excel(name = "banner组", width = 15)
    @ApiModelProperty(value = "banner组")
    private java.lang.String bannerGroup;
	/**跳转路由*/
	@Excel(name = "跳转路由", width = 15)
    @ApiModelProperty(value = "跳转路由")
    private java.lang.String routing;

    /*是否显示 0 不显示  1显示*/
    @Excel(name = "是否显示", width = 15)
    @ApiModelProperty(value = "是否显示")
    private String isShow;
}

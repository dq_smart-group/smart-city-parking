package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class CarMonthlyRentQuery {
    /**
     * 车牌类型
     */
    private String plateType;
    /**
     * 车场id
     */
    private String parkingLotId;
    /**
     * 续费时长
     */
    private Integer renewalDuration;
    /**
     * 1续费，2充值
     */
    private String czorxf;
}

package org.jeecg.modules.system.service.impl;

import org.jeecg.modules.system.entity.SysConfigTab;
import org.jeecg.modules.system.mapper.SysConfigTabMapper;
import org.jeecg.modules.system.service.ISysConfigTabService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 配置分类表
 * @Author: jeecg-boot
 * @Date:   2021-12-03
 * @Version: V1.0
 */
@Service
public class SysConfigTabServiceImpl extends ServiceImpl<SysConfigTabMapper, SysConfigTab> implements ISysConfigTabService {

}

package org.jeecg.modules.authentication.utils;

import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * 请求工具类
 */
@Slf4j
public class HttpRequestUtil {


    static Map<String,String> mapHead = null;

    //请求数据类型
    static {
        mapHead = new HashMap<>();
        mapHead.put("Content-Type","application/json; charset=UTF-8");
        mapHead.put("Accept","application/json");
        mapHead.put("Accept-Encoding","");
    }

    public static Result<?> getRequest(String url , Map<String,Object> paramMap){
        try {
            String json = JSON.toJSONString(paramMap);
            log.info("get请求url:[{}],get请求参数:[{}]" , url,json);
            HttpRequest httpRequest = HttpUtil.createGet(url);
            HttpResponse httpResponse = httpRequest.addHeaders(mapHead).body(json).execute();
            String result = httpResponse.body();
            if(!StringUtils.isEmpty(result)){
                return com.alibaba.fastjson.JSONObject.parseObject(result, Result.class);
            }
            return null;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


    /**
     * post请求方式
     * @param url   请求url
     * @param paramMap  请求参数
     * @return
     */
    public static Result<?> postRequest(String url , Map<String,Object> paramMap){

        try {
            String json = JSON.toJSONString(paramMap);
            log.info("post请求url:[{}],post请求参数:[{}]" , url,json);
            HttpRequest httpRequest = HttpUtil.createPost(url);
            HttpResponse httpResponse = httpRequest.addHeaders(mapHead).body(json).execute();
            String result = httpResponse.body();
            if(!StringUtils.isEmpty(result)){
                return com.alibaba.fastjson.JSONObject.parseObject(result, Result.class);
            }
            return Result.error();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    /**
     * post请求方式
     * @param url   请求url
     * @param paramMap  请求参数
     * @return
     */
    public static Result<?> postRequests(String url , Map<String,Object> paramMap){
        try {
            String json = JSON.toJSONString(paramMap);
            log.info("post请求url:[{}],post请求参数:[{}]" , url,json);
            HttpRequest httpRequest = HttpUtil.createPost(url);
            HttpResponse httpResponse = httpRequest.addHeaders(mapHead).body(json).execute();
            String result = httpResponse.body();
            if(!StringUtils.isEmpty(result)){
                return Result.OK(result);
            }
            return Result.error();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }


}

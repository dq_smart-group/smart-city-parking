package org.jeecg.modules.parking.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: car_renewal_history
 * @Author: jeecg-boot
 * @Date:   2022-04-16
 * @Version: V1.0
 */
@Data
@TableName("car_renewal_history")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_renewal_history对象", description="car_renewal_history")
public class CarRenewalHistory implements Serializable {
    private static final long serialVersionUID = 1L;

	/**续费历史id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "续费历史id")
    private java.lang.String id;
    /**公司id*/
    @Excel(name = "公司id", width = 15)
    @ApiModelProperty(value = "公司id")
    private java.lang.String companyId;
	/**白名单车id*/
	@Excel(name = "白名单车id", width = 15)
    @ApiModelProperty(value = "白名单车id")
    private java.lang.String carWhiteId;
	/**续费时长*/
	@Excel(name = "续费时长", width = 15)
    @ApiModelProperty(value = "续费时长")
    private java.lang.Integer renewalDuration;
	/**金额*/
	@Excel(name = "金额", width = 15)
    @ApiModelProperty(value = "金额")
    private java.lang.String amount;
	/**金额*/
	@Excel(name = "过期时间", width = 15)
    @ApiModelProperty(value = "过期时间")
    private java.util.Date pastTime;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
}

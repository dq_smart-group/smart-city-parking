package org.jeecg.modules.parking.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.parking.entity.CarRechargeRecord;
import org.jeecg.modules.parking.service.ICarRechargeRecordService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.parking.vo.CarRechargeRecordVo;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: car_recharge_record
 * @Author: jeecg-boot
 * @Date:   2022-03-23
 * @Version: V1.0
 */
@Api(tags="car_recharge_record")
@RestController
@RequestMapping("/parking/carRechargeRecord")
@Slf4j
public class CarRechargeRecordController extends JeecgController<CarRechargeRecord, ICarRechargeRecordService> {
	@Autowired
	private ICarRechargeRecordService carRechargeRecordService;
	
	/**
	 * 分页列表查询
	 *
	 * @param carRechargeRecord
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "car_recharge_record-分页列表查询")
	@ApiOperation(value="car_recharge_record-分页列表查询", notes="car_recharge_record-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarRechargeRecordVo carRechargeRecord,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CarRechargeRecordVo> queryWrapper = QueryGenerator.initQueryWrapper(carRechargeRecord, req.getParameterMap());
		Page<CarRechargeRecord> page = new Page<CarRechargeRecord>(pageNo, pageSize);
		IPage<CarRechargeRecordVo> pageList = carRechargeRecordService.pageList(page, queryWrapper,carRechargeRecord);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carRechargeRecord
	 * @return
	 */
	@AutoLog(value = "car_recharge_record-添加")
	@ApiOperation(value="car_recharge_record-添加", notes="car_recharge_record-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarRechargeRecord carRechargeRecord) {
		carRechargeRecordService.save(carRechargeRecord);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param carRechargeRecord
	 * @return
	 */
	@AutoLog(value = "car_recharge_record-编辑")
	@ApiOperation(value="car_recharge_record-编辑", notes="car_recharge_record-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarRechargeRecord carRechargeRecord) {
		carRechargeRecordService.updateById(carRechargeRecord);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_recharge_record-通过id删除")
	@ApiOperation(value="car_recharge_record-通过id删除", notes="car_recharge_record-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carRechargeRecordService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "car_recharge_record-批量删除")
	@ApiOperation(value="car_recharge_record-批量删除", notes="car_recharge_record-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carRechargeRecordService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_recharge_record-通过id查询")
	@ApiOperation(value="car_recharge_record-通过id查询", notes="car_recharge_record-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarRechargeRecord carRechargeRecord = carRechargeRecordService.getById(id);
		if(carRechargeRecord==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carRechargeRecord);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carRechargeRecord
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarRechargeRecord carRechargeRecord) {
        return super.exportXls(request, carRechargeRecord, CarRechargeRecord.class, "car_recharge_record");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarRechargeRecord.class);
    }

}

package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class CameraListQuery {
    private String host;
    private Integer port;
    private String number;
}

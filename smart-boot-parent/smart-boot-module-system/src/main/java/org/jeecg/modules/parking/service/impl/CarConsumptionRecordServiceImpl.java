package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.modules.parking.entity.CarConsumptionRecord;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import org.jeecg.modules.parking.mapper.CarConsumptionRecordMapper;
import org.jeecg.modules.parking.service.ICarConsumptionRecordService;
import org.jeecg.modules.parking.vo.CarConsumptionRecordVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: car_consumption_record
 * @Author: jeecg-boot
 * @Date:   2022-03-23
 * @Version: V1.0
 */
@Service
public class CarConsumptionRecordServiceImpl extends ServiceImpl<CarConsumptionRecordMapper, CarConsumptionRecord> implements ICarConsumptionRecordService {

    @Autowired
    CarInfoCollectServiceImpl carInfoCollectService;
    @Autowired
    private ISysDepartService sysDepartService;

    @Override
    public IPage<CarConsumptionRecordVo> pageList(Page<CarConsumptionRecord> page, QueryWrapper<CarConsumptionRecordVo> queryWrapper, CarConsumptionRecordVo carConsumptionRecord) {
        QueryWrapper<CarConsumptionRecord> recordQueryWrapper = new QueryWrapper<>();
        BeanUtils.copyProperties(queryWrapper,recordQueryWrapper);
        if(null!=carConsumptionRecord.getPayTime()){
            //日期查询
            String paytime = carConsumptionRecord.getPayTime();
            String[] split = paytime.split("#");
            Assert.notNull(split[0],"请输入开始日期");
            Assert.notNull(split[1],"请输入结束日期");
            recordQueryWrapper = recordQueryWrapper.gt("payment_time",split[0]).and(i->{
                i.lt("payment_time",split[1]);
            });
        }
        Page<CarConsumptionRecord> recordPage = this.page(page, recordQueryWrapper);
        List<CarConsumptionRecord> records = recordPage.getRecords();
        IPage<CarConsumptionRecordVo> carTopUpVoPage = new Page<>();
        BeanUtils.copyProperties(recordPage,carTopUpVoPage);
        if(records.size()>0) {
            //避免重复请求数据库，一次性查询出信息收集
            List<String> ids = records.stream().map(CarConsumptionRecord::getInfoCollectId).collect(Collectors.toList());
            List<String> idDis = ids.stream().distinct().collect(Collectors.toList());
            List<CarInfoCollect> carInfoCollects;
            if(!LoginUserUtil.isAdmin()){
                SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
                if(sysDepart!=null){
                    carInfoCollects = carInfoCollectService.listByIds(idDis).stream().filter(i->i.getCompanyId().equals(sysDepart.getId())).collect(Collectors.toList());
                }else{
                    carInfoCollects = carInfoCollectService.listByIds(idDis).stream().filter(i->i.getCompanyId().equals("0")).collect(Collectors.toList());
                }
            }else {
                carInfoCollects = carInfoCollectService.listByIds(idDis);
            }
            //筛选数据
            List<CarInfoCollect> commercials = carInfoCollects.stream().filter(i -> null != carConsumptionRecord.getLicense() ? carConsumptionRecord.getLicense().equals(i.getLicense()) : true).collect(Collectors.toList());
            //转换为map，便于匹配数据
            Map<String, CarInfoCollect> collectMap = commercials.stream().collect(Collectors.toMap(CarInfoCollect::getId, i -> i));
            if(collectMap.size()>0) {
                //聚合数据
                List<CarConsumptionRecordVo> collect = records.stream().map(i -> {
                    CarConsumptionRecordVo recordVo = new CarConsumptionRecordVo();
                    BeanUtils.copyProperties(i, recordVo);
                    BeanUtils.copyProperties(collectMap.get(i.getInfoCollectId()), recordVo);
                    return recordVo;
                }).collect(Collectors.toList());
                carTopUpVoPage.setRecords(collect);
                carTopUpVoPage.setTotal(collect.size());
            }else {
                carTopUpVoPage = new Page<>();
            }
        }
        return carTopUpVoPage;
    }
}

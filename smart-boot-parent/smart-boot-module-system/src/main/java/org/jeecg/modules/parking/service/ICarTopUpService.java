package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.parking.entity.CarTopUp;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.vo.CarTopUpVo;
import org.jeecg.modules.parking.vo.TopUpVo;

/**
 * @Description: car_top_up
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
public interface ICarTopUpService extends IService<CarTopUp> {

    IPage<CarTopUpVo> pageVo(Page<CarTopUp> page, QueryWrapper<CarTopUpVo> queryWrapper, CarTopUpVo carTopUp);

    void edit(TopUpVo topUpVo);
}

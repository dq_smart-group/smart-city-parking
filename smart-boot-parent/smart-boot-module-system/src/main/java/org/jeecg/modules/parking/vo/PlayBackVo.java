package org.jeecg.modules.parking.vo;

import lombok.Data;

@Data
public class PlayBackVo {
    private String name;
    private String startTime;
    private String endTime;
    private String filePath;
}

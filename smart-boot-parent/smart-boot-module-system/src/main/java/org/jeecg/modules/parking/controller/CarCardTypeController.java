package org.jeecg.modules.parking.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.parking.entity.CarCardType;
import org.jeecg.modules.parking.entity.Depart;
import org.jeecg.modules.parking.query.CarCardTypeQuery;
import org.jeecg.modules.parking.service.ICarCardTypeService;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.List;

 /**
 * @Description: 车牌类型
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Api(tags="车牌类型")
@RestController
@RequestMapping("/parking/carCardType")
@Slf4j
public class CarCardTypeController extends JeecgController<CarCardType, ICarCardTypeService> {
	@Autowired
	private ICarCardTypeService carCardTypeService;
	 @Autowired
	 private ISysDepartService sysDepartService;
	 @Autowired
	 private RedisUtil redisUtil;


	/**
	 * 分页列表查询
	 *
	 * @param carCardTypeQuery
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "车牌类型-分页列表查询")
	@ApiOperation(value="车牌类型-分页列表查询", notes="车牌类型-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarCardTypeQuery carCardTypeQuery,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carCardTypeQuery.setDepId(sysDepart.getId());
			}else{
				carCardTypeQuery.setDepId("0");
			}
		}
		Page<CarCardType> page = new Page<CarCardType>(pageNo, pageSize);
		IPage<CarCardType> pageList = carCardTypeService.pageList(page,carCardTypeQuery);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carCardType
	 * @return
	 */
	@AutoLog(value = "车牌类型-添加")
	@ApiOperation(value="车牌类型-添加", notes="车牌类型-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarCardType carCardType) {
		QueryWrapper<CarCardType>Wrapper=new QueryWrapper<>();
		Wrapper.eq("dep_id",carCardType.getDepId());
		Wrapper.eq("name",carCardType.getName());

		List<CarCardType>dd=carCardTypeService.list(Wrapper);

		if (dd.size()!=0){
			return Result.error("已存在该车牌类型！");
		}else {
			carCardTypeService.save(carCardType);
			redisUtil.set(ParkingRedisConstant.PARKING_TYPE+carCardType.getId(),carCardType);
			return Result.OK("添加成功！");
		}
	}
	
	/**
	 *  编辑
	 *
	 * @param carCardType
	 * @return
	 */
	@AutoLog(value = "车牌类型-编辑")
	@ApiOperation(value="车牌类型-编辑", notes="车牌类型-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarCardType carCardType) {



		QueryWrapper<CarCardType>Wrapper=new QueryWrapper<>();
		Wrapper.ne("id",carCardType.getId());
		Wrapper.eq("dep_id",carCardType.getDepId());
		Wrapper.eq("name",carCardType.getName());

		List<CarCardType>dd=carCardTypeService.list(Wrapper);

		if (dd.size()!=0){
			return Result.error("已存在该车牌类型！");
		}else {
			carCardTypeService.updateById(carCardType);
			redisUtil.set(ParkingRedisConstant.PARKING_TYPE+carCardType.getId(),carCardType);
			return Result.OK("编辑成功!");
		}
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "车牌类型-通过id删除")
	@ApiOperation(value="车牌类型-通过id删除", notes="车牌类型-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		redisUtil.del(ParkingRedisConstant.PARKING_TYPE+id);
		carCardTypeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "车牌类型-批量删除")
	@ApiOperation(value="车牌类型-批量删除", notes="车牌类型-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		String[] split = ids.split(",");
		for (String s : split) {
			redisUtil.del(ParkingRedisConstant.PARKING_TYPE+s);
		}
		this.carCardTypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "车牌类型-通过id查询")
	@ApiOperation(value="车牌类型-通过id查询", notes="车牌类型-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarCardType carCardType = carCardTypeService.getById(id);
		if(carCardType==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carCardType);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carCardType
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarCardType carCardType) {
        return super.exportXls(request, carCardType, CarCardType.class, "车牌类型");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarCardType.class);
    }


	 /**
	  * 获取登陆人对应的所有公司
	  * @return
	  */
	 @GetMapping("/getDepartByUserId")
	 public List<Depart> getParkingLotByUserId(){
		 //登录人id
		 String loginUserId = LoginUserUtil.getLoginUserId();
		 List<Depart> list = null;
		 if(LoginUserUtil.isAdmin()){
			 //管理员查全部
			 list = carCardTypeService.getDepartlist();
		 }else{
			 list = carCardTypeService.getDepartByUserId(loginUserId);
		 }
		 return list;
	 }

	 /**
	  * 获取登陆人对应的公司下的车牌类型,是管理员就获取全部
	  * @return
	  */
	 @GetMapping("/getCardTypeByparkingLotId")
	 public List<ParkingLotAndCamVo> getCardTypeByCompanyId(){
		 //登录人id
		 String companyId = null;
		 List<ParkingLotAndCamVo> list = null;
		 if(!LoginUserUtil.isAdmin()){
			 SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			 if(sysDepart!=null){
				 companyId = sysDepart.getId();
			 }else{
				 companyId = "0";
			 }
		 }
		 list = carCardTypeService.getCardTypeByparkingLotId(companyId);

		 return list;
	 }

	 /**
	  * 根据车场id查询出车场下的车牌类型
	  * @return
	  */
	 @GetMapping("/getCardTypeByParkingId")
	 public List<CarCardType> getCardTypeByParkingId(String parkingId){
	 	if(StringUtils.isEmpty(parkingId)){
	 		return null;
		}
		 return carCardTypeService.getCardTypeByParkingId(parkingId);
	 }


}

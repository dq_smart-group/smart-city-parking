package org.jeecg.modules.parking.mqtt;



import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.util.DySmsEnum;
import org.jeecg.common.util.DySmsHelper;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.authentication.utils.HttpRequestUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.service.ICarInfoCollectService;
import org.jeecg.modules.parking.util.GetCrc16;
import org.jeecg.modules.parking.util.MobileUtil;
import org.jeecg.modules.system.util.SysConfigUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
/**
 * 发布消息的回调类
 *
 * 必须实现MqttCallback的接口并实现对应的相关接口方法CallBack 类将实现 MqttCallBack。
 * 每个客户机标识都需要一个回调实例。在此示例中，构造函数传递客户机标识以另存为实例数据。
 * 在回调中，将它用来标识已经启动了该回调的哪个实例。
 * 必须在回调类中实现三个方法：
 *
 *  public void messageArrived(MqttTopic topic, MqttMessage message)接收已经预订的发布。
 *
 *  public void connectionLost(Throwable cause)在断开连接时调用。
 *
 *  public void deliveryComplete(MqttDeliveryToken token))
 *  接收到已经发布的 QoS 1 或 QoS 2 消息的传递令牌时调用。
 *  由 MqttClient.connect 激活此回调。
 *
 */

/**
 * @Classname PushCallback
 * @Description 消费监听类
 */
@Slf4j
@Component
public class PushCallback implements MqttCallback {
    @Autowired
    private MqttConfiguration mqttConfiguration;
    @Autowired
    private RedisUtil redisUtil;

    private static MqttClient client;
    @Autowired
    private ICameraDeviceService cameraDeviceService;


    @Override
    public void connectionLost(Throwable throwable) {
        int times = 1;
        client = MqttPushClient.getClient();
        while (!client.isConnected()) {
            try {
                log.error("连接断开，正在重连...., 重新连接, 第" + (times++) + "次");
                log.error("断开了MQTT连接 ：{},"+throwable.getMessage());
                // 执行到这里，说明连接成功，重新订阅主题
                mqttConfiguration.getMqttPushClients();
                client = MqttPushClient.getClient();
                client.disconnect();
                if(times > 5){
                    //TODO 通知
                    break;
                }
            } catch (Exception e) {
                e.printStackTrace();
                log.error("MQTT重连失败, msg:" + e.getMessage());
            }
            // 每隔10秒重试一次
            try {
                TimeUnit.SECONDS.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if(client.isConnected()){
            log.info("MQTT重连成功");
        }else{
            //消息通知
            try {
                String numbers = SysConfigUtil.getVal("service_no_response_numbers");
                String[] split = numbers.split(",");
                for (String s : split) {
                    if(MobileUtil.checkPhone(s)){
                        com.alibaba.fastjson.JSONObject obj = new com.alibaba.fastjson.JSONObject();
                        obj.put("servert", "mq");
                        DySmsHelper.sendSms(s, obj, DySmsEnum.SERVICE_TEMPLATE_CODE);
                    }
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }


    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        JSONObject jsonObject = JSONObject.fromObject(message.toString());

        log.info("接收消息主题 : " + topic);
        log.info("接收消息Qos : " + message.getQos());
        log.info("接收消息内容 : " + new String(message.getPayload()));
        if(jsonObject.has("AlarmInfoPlate")){
            //设备心跳信息保存
            JSONObject alarmInfoPlate=JSONObject.fromObject(jsonObject.getString("AlarmInfoPlate"));
            if(alarmInfoPlate.has("heartbeat")){
               redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_HEARTBEAT+alarmInfoPlate.getString("serialno"),40,40);
            }
        }
        if(jsonObject.has("SerialData")){
        JSONObject alarmInfoPlate=JSONObject.fromObject(jsonObject.getString("SerialData"));
            if(alarmInfoPlate.has("data")){
                if(Integer.parseInt(alarmInfoPlate.get("dataLen").toString())==8){
                    String s = GetCrc16.base64To16(alarmInfoPlate.get("data").toString());
                    String serialno = alarmInfoPlate.get("serialno").toString();
                    CameraDevice cameraDevice = cameraDeviceService.selectByDevice(serialno);
                    if(null == cameraDevice){
                        log.info("没有该设备");
                        return;
                    }
                    String[] s1 = s.split(" ");
                    Map<String,Object> map = new HashMap<>();
                    map.put("serialno",cameraDevice.getSerialno());
                    map.put("largeScreenId",cameraDevice.getScreenCode());
                    map.put("openOrClose",Integer.parseInt(s1[4]));
                    HttpRequestUtil.postRequest(SysConfigUtil.getVal("3d_sendWebsocket"), map);
                }
            }
        }
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        System.out.println("deliveryComplete---------" + token.isComplete());
    }
}

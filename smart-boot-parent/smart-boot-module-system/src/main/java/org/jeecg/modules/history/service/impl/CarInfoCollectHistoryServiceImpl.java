package org.jeecg.modules.history.service.impl;

import org.jeecg.modules.history.entity.CarInfoCollectHistory;
import org.jeecg.modules.history.mapper.CarInfoCollectHistoryMapper;
import org.jeecg.modules.history.service.ICarInfoCollectHistoryService;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: car_info_collect_history_1
 * @Author: jeecg-boot
 * @Date:   2022-03-02
 * @Version: V1.0
 */
@Service
public class CarInfoCollectHistoryServiceImpl extends ServiceImpl<CarInfoCollectHistoryMapper, CarInfoCollectHistory> implements ICarInfoCollectHistoryService {

    /**
     * 获取表格数据数量
     * @param tableName
     * @return
     */
    @Override
    public int getListCount(String tableName) {
        return baseMapper.getListCount(tableName);
    }

    /**
     * 创建表
     * @param tableName
     */
    @Override
    public void createHistoryTable(String tableName) {
        baseMapper.createHistoryTable(tableName);
    }

    /**
     * 保存数据到历史
     * @param list
     * @return
     */
    @Override
    @Transactional
    public int saveDataToHistory(List<CarInfoCollect> list, String tableName) {
        return baseMapper.saveDataToHistory(list,tableName);
    }
}

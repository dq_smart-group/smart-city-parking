package org.jeecg.modules.parking.api;

import com.daqing.DongGuanStaticSdk;
import com.daqing.config.DongGuanStaticConfig;
import com.daqing.entity.*;
import lombok.AllArgsConstructor;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Map;

@RestController
@AllArgsConstructor
public class DgParkingApi {

    /**
     * 停车场基础接口
     */
    public DongGuanStaticResponse dgParkingLotBaseData(DongGuanStaticParkingLogBaseData data, ParkingLot parkingLot){
        Map<String, Object> map = EntityUtils.entityToMap(data);
        setTime(parkingLot.getCreateTime(),parkingLot.getUpdateTime(), map);
        return DongGuanStaticSdk.parkingLotBaseData(map);
    }

    /**
     * 停车场通道(岗亭)基础接口
     */
    public DongGuanStaticResponse dgSentryBoxInfo(DongGuanStaticSentryBoxInfo data, Date ct,Date ut){
        Map<String, Object> map = EntityUtils.entityToMap(data);
        setTime(ct,ut, map);
        return DongGuanStaticSdk.sentryBoxInfo(map);
    }

    /**
     * 停车场车辆进出接口
     */
    public DongGuanStaticResponse dgParkingLotBusInfo(DongGuanStaticParkingLotBusInfo data){
        Map<String, Object> map = EntityUtils.entityToMap(data);
        setTime(null,null, map);
        return DongGuanStaticSdk.parkingLotBusInfo(map);
    }

    /**
     * 上传进出场车牌图片
     */
    public DongGuanStaticResponse dgUploadPicture(DongGuanStaticPictureFileInfo pictureFileInfo,DongGuanStaticUploadPictureStreamInfo streamInfo){
        //1、创建图片文件
        Map<String, Object> map = EntityUtils.entityToMap(pictureFileInfo);
        setTime(null,null, map);
        DongGuanStaticResponse fileInfo = DongGuanStaticSdk.pictureFileInfo(map);
        if(1==fileInfo.getState()){
            //2、获取返回的fileId
            Object fileId = fileInfo.getObj();
            //3、上传进出场车牌图片流
            Map<String, Object> objectMap = EntityUtils.entityToMap(streamInfo);
            setTime(null,null, map);
            objectMap.put("fileId",fileId);
            return DongGuanStaticSdk.uploadPictureStreamInfo(objectMap);
        }else {
            return fileInfo;
        }
    }

    /**
     * 路段基础信息
     */
    public DongGuanStaticResponse dgSysSection(DongGuanStaticSysSection data, ParkingLot parkingLot){
        Map<String, Object> map = EntityUtils.entityToMap(data);
        setTime(parkingLot.getCreateTime(),parkingLot.getUpdateTime(), map);
        return DongGuanStaticSdk.sysSection(map);
    }

    /**
     * 道路泊位车辆进出接口
     */
    public DongGuanStaticResponse dgRoadBerthBusInfo(DongGuanStaticRoadBerthBusInfo data, ParkingLot parkingLot){
        Map<String, Object> map = EntityUtils.entityToMap(data);
        setTime(parkingLot.getCreateTime(),parkingLot.getUpdateTime(), map);
        return DongGuanStaticSdk.roadBerthBusInfo(map);
    }

    /**
     * 剩余泊位同步接口
     */
    public DongGuanStaticResponse dgRestBerthInfo(DongGuanStaticRestBerthInfo data, ParkingLot parkingLot){
        Map<String, Object> map = EntityUtils.entityToMap(data);
        setTime(parkingLot.getCreateTime(),parkingLot.getUpdateTime(), map);
        return DongGuanStaticSdk.restBerthInfo(map);
    }

    /**
     * 系统心跳检测（定时任务10分钟检测一次）
     */
    public DongGuanStaticResponse dgHeartBeatTestInfo(DongGuanStaticHeartBeatTestInfo data){
        Map<String, Object> map = EntityUtils.entityToMap(data);
        return DongGuanStaticSdk.heartBeatTestInfo(map);
    }


    /**
     * 设置时间
     * @param map
     * @return
     */
    private void setTime(Date ct,Date ut, Map<String, Object> map) {
        if(ut!=null){
            map.put("updateTime", ut.getTime());
        }else{
            map.put("updateTime", System.currentTimeMillis());
        }
        if(ut!=null){
            map.put("createTime", ct.getTime());
        }

        map.put("ve", 2);
        map.put("ms", System.currentTimeMillis());
    }

}

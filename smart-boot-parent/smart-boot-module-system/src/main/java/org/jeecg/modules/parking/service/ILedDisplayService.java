package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.LedDisplay;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.query.LedDisplayQuery;
import org.jeecg.modules.parking.vo.LedDisplayVo;

import java.util.List;

/**
 * @Description: led显示操作表
 * @Author: jeecg-boot
 * @Date:   2022-01-05
 * @Version: V1.0
 */
public interface ILedDisplayService extends IService<LedDisplay> {

    IPage<LedDisplayVo> pageList(Page<LedDisplayVo> page, LedDisplayQuery ledDisplayQuery);

    /**
     * 添加设备
     * @param ledDisplay
     */
    Result<?> saveLedDisplay(LedDisplay ledDisplay);

    Result<?> updateLedDisplayById(LedDisplay ledDisplay);

    List<LedDisplay> getList();

    /**
     * 晚上定时修改led屏幕的声音和亮度
     */
    void updateVoiceAndBrightness();

    /**
     * 早上定时修改led屏幕的声音和亮度
     */
    void updateVoiceAndBrightnessMorning();
}

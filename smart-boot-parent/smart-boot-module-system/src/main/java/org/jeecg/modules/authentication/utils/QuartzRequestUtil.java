package org.jeecg.modules.authentication.utils;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.TreeMap;

/**
 * 定时任务请求
 */
@Slf4j
public class QuartzRequestUtil {


    private static String quartzAuthentication = "smart-city-authentication-bHai";





    /**
     * 定时任务post请求
     * @param url      请求url
     * @param paramMap  请求参数
     * @return
     */
    public static Result<?> quartzPostRequest(String url , Map<String,Object> paramMap){
        TreeMap<String, Object> treeMap = SignUtil.mapToTreeMap(paramMap);
        String sign = SignUtil.sign(treeMap, quartzAuthentication, quartzAuthentication);
        Map<String, Object> map = SignUtil.threeMapToMap(treeMap);
        map.put("sign",sign);
        return HttpRequestUtil.postRequest(url, map);
    }


    /**
     * 获取视频播放路径 post请求
     * @param url      请求url
     * @param paramMap  请求参数
     * @return
     */
    public static Result<?> quartzPostRequests(String url , Map<String,Object> paramMap){
        TreeMap<String, Object> treeMap = SignUtil.mapToTreeMap(paramMap);
        String sign = SignUtil.sign(treeMap, quartzAuthentication, quartzAuthentication);
        Map<String, Object> map = SignUtil.threeMapToMap(treeMap);
        map.put("sign",sign);
        return HttpRequestUtil.postRequests(url, map);
    }



}

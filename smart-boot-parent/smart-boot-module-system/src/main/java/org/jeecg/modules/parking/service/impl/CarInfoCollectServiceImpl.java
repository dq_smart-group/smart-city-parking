package org.jeecg.modules.parking.service.impl;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daqing.config.DongGuanStaticConfig;
import com.daqing.entity.DongGuanStaticPictureFileInfo;
import com.daqing.entity.DongGuanStaticResponse;
import com.daqing.entity.DongGuanStaticUploadPictureStreamInfo;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.entity.ContentType;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.authentication.utils.HttpRequestUtil;
import org.jeecg.modules.authentication.utils.MapBuild;
import org.jeecg.modules.authentication.utils.QuartzRequestUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.device.vo.CameraDeviceVo;
import org.jeecg.modules.oss.entity.OSSFile;
import org.jeecg.modules.oss.service.IOSSFileService;
import org.jeecg.modules.parking.api.DgParkingApi;
import org.jeecg.modules.parking.entity.*;
import org.jeecg.modules.parking.mapper.CarInfoCollectMapper;
import org.jeecg.modules.parking.mqtt.MqttPushClient;
import org.jeecg.modules.parking.query.*;
import org.jeecg.modules.parking.service.*;
import org.jeecg.modules.parking.tcp.SocketClient;
import org.jeecg.modules.parking.tcp.SocketSendParameter;
import org.jeecg.modules.parking.tcp.SocketWriter;
import org.jeecg.modules.parking.util.PicUtils;
import org.jeecg.modules.parking.util.TestQRcode;
import org.jeecg.modules.parking.vo.CarInfoCollectVo;
import org.jeecg.modules.parking.vo.MoneyStatisticsVo;
import org.jeecg.modules.parking.vo.PayInfoVo;
import org.jeecg.modules.system.util.SysConfigUtil;
import org.jeecg.modules.utils.RedisGetUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @Description: 停车车辆信息收集表
 * @Author: jeecg-boot
 * @Date: 2021-11-24
 * @Version: V1.0
 */
@Service
@Slf4j
public class CarInfoCollectServiceImpl extends ServiceImpl<CarInfoCollectMapper, CarInfoCollect> implements ICarInfoCollectService {

    @Autowired
    private ICameraDeviceService deviceService;


    @Autowired
    private ICarVehicleService carVehicleService;

    @Autowired
    private ICameraDeviceService cameraDeviceService;

    @Autowired
    private ICarStandardService carStandardService;

    @Autowired
    private ICarCardTypeService carCardTypeService;

    @Autowired
    private ILedDisplayService ledDisplayService;

    @Autowired
    private ICarWhiteListService carWhiteListService;
    @Autowired
    private ICarYardFeesService carYardFeesService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private ICarTimeSlotService carTimeSlotService;


    @Autowired
    private IParkingLotService parkingLotService;

    @Autowired
    private IOSSFileService iossFileService;



    /**
     * 对摄像机传送的数据进行处理
     *
     * @param carInfo
     * @return
     */
   // @Override
//    public void addCarInfo(String carInfo) throws Exception{
//        try {
//            long start = System.currentTimeMillis();
//            CarInfoCollect carInfoCollect = new CarInfoCollect();
//            //String转JSONObject
//            JSONObject jsonObject =JSONUtil.parseObj(carInfo);
//            //根据key获取值，把值转为JSONObject
//            JSONObject alarmInfoPlate=jsonObject.getJSONObject("AlarmInfoPlate");
//            //摄像机基本信息
//            AlarmInfoPlateQuery alarmInfoPlateQuery = JSONUtil.toBean(alarmInfoPlate, AlarmInfoPlateQuery.class);
//            JSONObject resultInfo=alarmInfoPlate.getJSONObject("result");
//            if(resultInfo != null){
//                //获取车牌基本信息
//                JSONObject plateResult=resultInfo.getJSONObject("PlateResult");
//                //车辆详细信息转实体接受
//                PlateResultQuery plateResultQuery=JSONUtil.toBean(plateResult,PlateResultQuery.class);
//                //根据设备序列号获取设备使用情况：入库设备，出库设备
//                CameraDeviceVo deviceVo = cameraDeviceService.selectDevice(alarmInfoPlateQuery.getSerialno());
//                if (deviceVo != null) {
//                    //公司名称
//                    carInfoCollect.setCompanyName(deviceVo.getCompanyName());
//                    //公司id
//                    carInfoCollect.setCompanyId(deviceVo.getCompanyId());
//                    //车库名称
//                    carInfoCollect.setParkingName(deviceVo.getParkingName());
//
//
//
//                    //设备基本信息
//                    //设备存在
//                    if (CommonConstant.SX_DEVICE_USE_0.equals(deviceVo.getDeviceUse())) {
//                        //入场设备
//                        //设备名字
//                        carInfoCollect.setComeDevice(alarmInfoPlateQuery.getDeviceName());
//                        //设备ip
//                        carInfoCollect.setComeIpaddr(alarmInfoPlateQuery.getIpaddr());
//                        //设备序列号
//                        carInfoCollect.setComeSerialno(alarmInfoPlateQuery.getSerialno());
//                        //车辆基本信息
//                        carInfoCollect.setCarStatus(CommonConstant.SX_CAR_STATUS_0);
//                        //入库车牌截图路径
//                        carInfoCollect.setComeImagePath("https://binhai-city.oss-cn-guangzhou.aliyuncs.com/"+plateResultQuery.getImagePath());
//                        //入场报文
//                        carInfoCollect.setComeInfoText(alarmInfoPlate.toString());
//                    }
//                    if (CommonConstant.SX_DEVICE_USE_1.equals(deviceVo.getDeviceUse())) {
//                        //出场设备
//                        //出口设备名字
//                        carInfoCollect.setOutDevice(alarmInfoPlateQuery.getDeviceName());
//                        //出口设备ip
//                        carInfoCollect.setOutIpaddr(alarmInfoPlateQuery.getIpaddr());
//                        //出口设备序列号
//                        carInfoCollect.setOutSerialno(alarmInfoPlateQuery.getSerialno());
//                        //出库车牌号截图
//                        carInfoCollect.setOutImagePath("https://binhai-city.oss-cn-guangzhou.aliyuncs.com/"+plateResultQuery.getImagePath());
//                        //已出库状态
//                        carInfoCollect.setCarStatus(CommonConstant.SX_CAR_STATUS_4);
//                        //出场报文
//                        carInfoCollect.setOutInfoText(alarmInfoPlate.toString());
//                    }
//                    //设备存在的情况下，才保存
//                    String carTypeId=carVehicleService.carTypeId(plateResultQuery.getLicense(),deviceVo.getParkingLotId());
//                    if(carTypeId!=null){
//                        carInfoCollect.setCardTypeId(carTypeId);
//                    }else {
//                        //车牌类型
//                        carInfoCollect.setCardTypeId(plateResultQuery.getType());
//                    }
//                    //车牌号
//                    carInfoCollect.setLicense(plateResultQuery.getLicense());
//                    //车身颜色
//                    carInfoCollect.setCarColor(plateResultQuery.getCarColor());
//                    //车牌颜色
//                    carInfoCollect.setCarCardColor(plateResultQuery.getColorType());
//                    //识别可信度
//                    carInfoCollect.setConfidence(plateResultQuery.getConfidence());
//                    //车牌真伪
//                    carInfoCollect.setIsFakePlate(plateResultQuery.getIs_fake_plate());
//                    //触发类型
//                    carInfoCollect.setTriggerType(plateResultQuery.getTriggerType());
//                    //车辆特征码
//                    carInfoCollect.setFeatureCode(plateResultQuery.getFeature_code());
//
//                    //识别所用时间
//                    carInfoCollect.setTimeUsed(plateResultQuery.getTimeUsed());
//                    //识别结果车牌 ID-
//                   // carInfoCollect.setPlateid(plateResultQuery.getPlateid());
//                    //车牌信息是否存在
//                    JSONObject carBrand=plateResult.getJSONObject("car_brand");
//                    if (carBrand != null){
//                        CarBrandQuery carBrandQuery=JSONUtil.toBean(carBrand,CarBrandQuery.class);
//                        //车辆品牌
//                        carInfoCollect.setBrand(carBrandQuery.getBrand());
//                        //车辆年份
//                        carInfoCollect.setCarYear(carBrandQuery.getYear());
//                        //车辆类型
//                        carInfoCollect.setCarType(carBrandQuery.getType());
//                    }
//                    Result<?> result = selectInfoId(carInfoCollect, deviceVo.getDeviceUse());
//
//
//                    //发布主题
//
//                    //查找是否在黑白名单里面
//                    CarWhiteList byChePai = (CarWhiteList)redisUtil.get(ParkingRedisConstant.PARKING_WHITE_LIST + carInfoCollect.getLicense());
//                    //CarWhiteList byChePai = carWhiteListService.getByChePai(carInfoCollect.getLicense());
//                    // 支付
//                    if(CommonConstant.SX_DEVICE_USE_0.equals(deviceVo.getDeviceUse())){
//                        //发布主题
//                        String topic = MqttPushClient.getTopic(carInfoCollect.getComeSerialno());
//                        String text = "欢迎入场";
//                        String text1 = "禁止入场";
//                        outLedDisplay(carInfoCollect,byChePai,topic,text,text1,true);//led显示操作
//
//                    }else{
//                        //出场
//                        String topic = MqttPushClient.getTopic(carInfoCollect.getOutSerialno());//出场主题
//                        String text = "一路顺风";
//                        String text1 = "禁止出场";
//                        if(result.isSuccess()){
//                            if(result.getCode()==400){
//                                //已支付，不需要生成二维码，可以直接出场
//                                outLedDisplay(carInfoCollect,byChePai,topic,text,text1,false);//led显示操作
//                            }else {
//                                String result2 = JSON.toJSONString(result.getResult());
//                                HashMap hashMa = JSON.parseObject(result2, HashMap.class);
//
//                                //判断是否在黑白名单内
//                                if (byChePai != null) {
//                                    //白名单放行,不需要显示二维码
//                                    if (!byChePai.getNeedAlarm().equals("1")) {
//                                        //到期时间
//                                        Date overdueTime = byChePai.getOverdueTime();
//                                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                                        //白名单到期则需要显示二维码
//                                        if (DateUtils.dateCompare(sdf.format(overdueTime))) {
//                                            //如果停车费用是0元则不需要显示二维码
//                                            if (!hashMa.get("money").toString().equals("0")) {
//                                                generateOrderCode(result, carInfoCollect);
//                                            } else {//放行通过，显示放行语
//                                                outLedDisplay(carInfoCollect, byChePai, topic, text, text1, false);//led显示操作
//                                            }
//                                        } else {
//                                            //白名单车辆
//                                            //放行通过，显示放行语
//                                            outLedDisplay(carInfoCollect, byChePai, topic, text, text1, false);//led显示操作
//                                        }
//                                    } else {//黑名单
//
//                                    }
//                                } else {
//                                    //如果停车费用是0元则不需要显示二维码
//                                    if (!hashMa.get("money").toString().equals("0")) {
//                                        generateOrderCode(result, carInfoCollect);
//                                    } else {//放行通过，显示放行语
//                                        outLedDisplay(carInfoCollect, byChePai, topic, text, text1, false);//led显示操作
//                                    }
//                                }
//                            }
//                        }else{
//                            //支付服务挂了后：月租车可以出去，临时车人工
//
//                        }
//                    }
//                }else{
//                    //设备不存在
//
//                }
//            }
//            long end = System.currentTimeMillis();
//            log.info("异步方法，结束：" + (end - start) + "毫秒");
//        }catch (Exception e){
//            e.printStackTrace();
//        }finally {
//            redisUtil.del("pan");
//        }
//
//    }
    /**
     * 根据设备序列号去缓存里查找设备信息
     * @param Serialno
     * @return
     */
    public CameraDevice getCameraDevice(String Serialno){
        return (CameraDevice)redisUtil.get(ParkingRedisConstant.PARKING_DEVICE + Serialno);
    }

    /**
     * 出场时不需要支付的led显示操作
     * @param carInfoCollect
     * @param byChePai
     * @param topic
     */
    public void outLedDisplay(CarInfoCollect carInfoCollect,CarWhiteList byChePai,String topic,String text,String text1,Boolean flag ){
        try {
            String cont = carInfoCollect.getLicense()+text;
            CameraDevice cameraDevice = null;
            if(flag){
                cameraDevice = getCameraDevice(carInfoCollect.getComeSerialno());
                if(cameraDevice==null){
                    cameraDevice = cameraDeviceService.selectByDevice(carInfoCollect.getComeSerialno());
                }
            }else{
                cameraDevice = getCameraDevice(carInfoCollect.getOutSerialno());
                if(cameraDevice==null){
                    cameraDevice = cameraDeviceService.selectByDevice(carInfoCollect.getOutSerialno());
                }
            }

            LedDisplay byId1 = (LedDisplay)redisUtil.get(ParkingRedisConstant.PARKING_LED+cameraDevice.getId());
            if(byId1==null){
                LambdaQueryWrapper<LedDisplay> wrapper = new LambdaQueryWrapper<LedDisplay>();
                wrapper.eq(LedDisplay::getEquipmentId,cameraDevice.getId());
                wrapper.last("limit 1");
                List<LedDisplay> list = ledDisplayService.list(wrapper);
                if(list.size()>0){
                    byId1 = list.get(0);
                }
            }
            //led播报内容
            String bobao = text;

            String yu = text;
            String onecolor = "R";
            String twocolor = "R";
            String threecolor = "R";
            String fourcolor = "R";
            if(byId1!=null){
                if(!StringUtils.isEmpty(byId1.getFourText())){
                    yu =byId1.getFourText();
                }
                if(!StringUtils.isEmpty(byId1.getOneText())){
                    cont =byId1.getOneText();
                }
                onecolor = byId1.getOneColor();
                twocolor = byId1.getTwoColor();
                threecolor = byId1.getThreeColor();
                fourcolor = byId1.getFourColor();
                if(byId1.getEnableFlag()){
                    if(!StringUtils.isEmpty(byId1.getBroadcastText())){
                        bobao = byId1.getBroadcastText();
                    }
                }
            }
            //获取到当前日期星期几
            int day = DateUtil.thisDayOfWeek();

            String type = "临时车";


            //白名单是否有效和在有效期内
            Boolean aBoolean = carWhiteListService.ifFag(byChePai);
            if(aBoolean){//白名单车辆
                type = "月租车";
                Date overdueTime = byChePai.getOverdueTime();
                //到期时间
                int i = DateUtils.daysBetween(new Date(), overdueTime);
                yu = "到期时间剩余"+i +"天";
                bobao = "剩余天数为 "+i+" 天";
                boolean carTimeSlotSByCameraIdAndTypeFlag = getCarTimeSlotSByCameraIdAndTypeFlag(cameraDevice.getId(), "1", day, topic,byChePai.getUploadDevice());
                if(carTimeSlotSByCameraIdAndTypeFlag){
                    cont = "暂时禁止入场";
                    yu = "禁止入场";
                    bobao = "该时段不允许进入";
                    deleteByChePai(carInfoCollect.getLicense());
                }
            }else{
                //判断是否是黑名单
                Boolean aBoolean1 = carWhiteListService.ifBlack(byChePai);
                if(aBoolean1){//黑名单车辆
                    type = "黑名单";
                    yu = text1;
                    bobao = text1;
                    deleteByChePai(carInfoCollect.getLicense());
                }else{//临时车辆
                    if(cameraDevice.getVehicleEntry().equals("1")){//临时车禁止入场
                        cont = "临时车禁止入场";
                        yu = "禁止入场";
                        bobao = "临时车无权限";
                        deleteByChePai(carInfoCollect.getLicense());
                    }else{
                        //查看该卡口是否有时间段管理
                        boolean carTimeSlotSByCameraIdAndTypeFlag = getCarTimeSlotSByCameraIdAndTypeFlag(cameraDevice.getId(), "2", day,topic, null);
                        if(carTimeSlotSByCameraIdAndTypeFlag){
                            cont = "暂时禁止入场";
                            yu = "禁止入场";
                            bobao = "该时段不允许进入";
                            deleteByChePai(carInfoCollect.getLicense());
                        }
                    }
                }
            }

            //播报语音
            if(byId1!=null){
                if(byId1.getEnableFlag()){
                    MqttPushClient.pushVoice(topic,bobao,"01");
                }
            }
            //led显示文字
            MqttPushClient.pushBytesToHexFuns(topic,carInfoCollect.getLicense(),type,yu,cont,onecolor,twocolor,threecolor,fourcolor);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    /**
     * 根据车牌查出最新的进场记录删除
     * @param chepai
     */
    @Override
    public void deleteByChePai(String chepai) {
        LambdaQueryWrapper<CarInfoCollect> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CarInfoCollect::getLicense,chepai);
        wrapper.eq(CarInfoCollect::getCarStatus,"0");
        wrapper.orderByDesc(CarInfoCollect::getCreateTime);
        wrapper.last("limit 1");
        List<CarInfoCollect> carInfoCollects = this.baseMapper.selectList(wrapper);
        if(carInfoCollects.size()>0){
            this.baseMapper.deleteById(carInfoCollects.get(0).getId());
        }
    }

    /**
     * 查询是否有时间段管理并进行进入判断
     * cameraDeviceId 设备id
     * type 车辆类型
     * day 星期几
     * topic 主题
     * uploadDevice 白名单是否上传设备
     * @return
     */
    public boolean getCarTimeSlotSByCameraIdAndTypeFlag(String cameraDeviceId,String type,Integer day,String topic,Boolean uploadDevice){
        try {
            //查出时间段管理
            List<CarTimeSlot> carTimeSlotSByCameraIdAndType = new ArrayList<>();
            CarTimeSlot c1 = (CarTimeSlot)redisUtil.get(ParkingRedisConstant.PARKING_TIME_SLOT + cameraDeviceId + ":" + "0" + ":" + day);
            if(c1!=null){
                carTimeSlotSByCameraIdAndType.add(c1);
            }
            CarTimeSlot c2 = (CarTimeSlot)redisUtil.get(ParkingRedisConstant.PARKING_TIME_SLOT + cameraDeviceId + ":" + type + ":" + day);
            if(c2!=null){
                carTimeSlotSByCameraIdAndType.add(c2);
            }
            //List<CarTimeSlot> carTimeSlotSByCameraIdAndType = carTimeSlotService.getCarTimeSlotSByCameraIdAndType(cameraDeviceId, type,day);
            //有时间段管理
            if(carTimeSlotSByCameraIdAndType.size()>0){
                boolean publishFlag = true;
                for (CarTimeSlot carTimeSlot : carTimeSlotSByCameraIdAndType) {
                    if(DateUtils.timeCompare(carTimeSlot.getStarTime(),carTimeSlot.getEndTime())){//禁止进场
                        publishFlag = false;
                        break;
                    }
                }
                if(publishFlag){
                    if(uploadDevice!=null){
                        if(!uploadDevice){
                            MqttPushClient.publishOpen(topic);
                        }
                    }else{
                        MqttPushClient.publishOpen(topic);
                    }
                }else{
                    return true;
                }
            }else{
                if(uploadDevice!=null){
                    if(!uploadDevice){
                        MqttPushClient.publishOpen(topic);
                    }
                }else{
                    MqttPushClient.publishOpen(topic);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }




    /**
     * 解码
     *
     * @param str
     * @return string
     */
    public byte[] decode(String str) {
        return Base64.getDecoder().decode(str);
    }


    @Override
    public Page<CarInfoCollectVo> selectData(Page page, CarInfoCollectVo carInfoCollectvo) {
        List<CarInfoCollectVo> list = this.baseMapper.selectData(page, carInfoCollectvo);
        return page.setRecords(list);
    }



    /**
     * 判断是否需要生成支付订单
     * @param carInfoCollect 订单信息
     * @param byChePai 白名单信息
     * @param flag true为入场设备，false为出场设备
     * @return
     */
    private Result setPayment(CarInfoCollect carInfoCollect,CarWhiteList byChePai,boolean flag){
        Result result = new Result();
        try{
            //临时车生成订单，白名单车不需要生成订单
            if(byChePai!=null){
                if(!byChePai.getNeedAlarm().equals("1")){//白名单
                    //到期时间
                    Date overdueTime = byChePai.getOverdueTime();
                    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    if(DateUtils.dateCompare(sdf.format(overdueTime))){//判断到期时间是否大于当前时间
                        if(!flag){
                            if(carInfoCollect.getComeInTime().compareTo(overdueTime)==-1){//入场时间小于白名单到期时间
                                carInfoCollect.setComeInTime(overdueTime);
                            }
                        }
                        //到期
                        Result payment = payment(carInfoCollect, flag);
                        result = payment;
                    }else{
                        result.setSuccess(true);
                    }
                }else{
                    result.setSuccess(false);
                }
            }else{
                Result payment = payment(carInfoCollect, flag);
                result = payment;
            }
        }catch (Exception e){
            result.setSuccess(true);
            e.printStackTrace();
        }
        return result;
    }

    private Result payment(CarInfoCollect carInfoCollect,boolean flag) {

        try {
            //可以单独传入http参数，这样参数会自动做URL编码，拼接在URL中
            JSONObject query = JSONUtil.createObj();
            query.put("license", carInfoCollect.getLicense());
            String cardTypeId = carInfoCollect.getCardTypeId();
            CarCardType byId = (CarCardType)redisUtil.get(ParkingRedisConstant.PARKING_TYPE + cardTypeId);
            //CarCardType byId = carCardTypeService.getById(cardTypeId);
            String carType = null;
            if(byId!=null){
                carType = byId.getName();
            }else{
                carType = cardTypeId;
            }
            query.put("carType", carType);
            BigDecimal money = new BigDecimal(0);
            CameraDevice cameraDevice = null;
            if(flag){
                //获取设备
                cameraDevice = getCameraDevice(carInfoCollect.getComeSerialno());
                //cameraDevice = cameraDeviceService.selectByDevice(carInfoCollect.getComeSerialno());
            }else{
                //获取设备
                cameraDevice = getCameraDevice(carInfoCollect.getOutSerialno());
                //cameraDevice = cameraDeviceService.selectByDevice(carInfoCollect.getOutSerialno());
                money = getMoney(carInfoCollect.getOutSerialno(), cardTypeId, carInfoCollect.getComeInTime(), carInfoCollect.getOutTime());
            }

            query.put("parkCost", money);
            query.put("costStartTime", carInfoCollect.getComeInTime());
            query.put("costEndTime", carInfoCollect.getOutTime());
            query.put("depId", carInfoCollect.getCompanyId());
            query.put("depName", carInfoCollect.getCompanyName());

            query.put("parkingLotId", cameraDevice.getParkingLotId());
            query.put("parkingLotName", carInfoCollect.getParkingName());
            query.put("carTypeId", cardTypeId);
            query.put("cheLianId", carInfoCollect.getId());
            query.put("sub_mchid", "1617522518");
            String result = HttpUtil.post("http://127.0.0.1:8085/smart-boot/parking/api/order",query.toString());
            Result result1 = com.alibaba.fastjson.JSONObject.parseObject(result, Result.class);
            return result1;
        }catch (Exception e){
            e.printStackTrace();
            return Result.error("");
        }
    }


    /**
     *
     * @param number 设备号
     * @param carCardTypeId 车牌类型id
     * @param rt 入场时间
     * @param ct 出场时间
     * @throws Exception
     */
    public  BigDecimal getMoney(String number,String carCardTypeId,Date rt,Date ct)throws Exception{
        //获取设备
        CameraDevice cameraDevice = getCameraDevice(number);
        if(cameraDevice==null){
            cameraDevice = cameraDeviceService.selectByDevice(number);
        }
        //获取车场
        String parkingLotId = cameraDevice.getParkingLotId();
        //根据车场和车牌类型查出收费规则
        CarStandard carStandard  = (CarStandard)redisUtil.get(ParkingRedisConstant.PARKING_STANDARD+parkingLotId+":"+carCardTypeId);
        if(carStandard==null){
            carStandard = carStandardService.getCarStandard(parkingLotId, carCardTypeId);
        }
        if(carStandard!=null){
            //小时收费类型（1：24小时收费  0：分段收费）
            String hourlyChargeType = carStandard.getHourlyChargeType();
            //全天最高收费
            BigDecimal highestMoney = carStandard.getHighestMoney();
            //免费分钟
            Integer freeMinutes = carStandard.getFreeMinutes();
            //免费分钟是否参与收费
            Boolean chargingFlag = carStandard.getChargingFlag();
            //收费规则
            String hourlyCharge = carStandard.getHourlyCharge();
            //未知
            Boolean enablesFlag = carStandard.getEnablesFlag();

            //入场时间
            Date date = new Date();
            Date date1 = rt;
            Date date2 = ct;
//        Date date1 = DateUtils.parseDate("2021-12-10 7:20", "yyyy-MM-dd HH:mm");
//        Date date2 = DateUtils.parseDate("2021-12-10 9:20", "yyyy-MM-dd HH:mm");
//        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            //String s = DateUtils.date2Str(date, simpleDateFormat);
            //总时长

            //要交费的金额
            BigDecimal money = new BigDecimal(0);
            //计算出进场时间和出场时间相差多少天
            int i = DateUtils.daysBetween(date1, date2);
            //入场时
            Integer rshour = Integer.parseInt(DateUtils.formatShorthour(date1));
            //入场分
            Integer rsmiu = Integer.parseInt(DateUtils.formatShortmiu(date1));
            //入场时间
            int rtimes = rshour * 60 + rsmiu;
            //出场时
            Integer cshour = Integer.parseInt(DateUtils.formatShorthour(date2));
            //出场分
            Integer csmiu = Integer.parseInt(DateUtils.formatShortmiu(date2));
            //出场时间
            int ctimes = cshour * 60 + csmiu+24*i*60;
            //总时长
            int times = ctimes - rtimes;
            //分段收费
            if(hourlyChargeType.equals("0")){
                JSONArray jsonArray = JSONArray.fromObject(hourlyCharge);
                if (jsonArray != null) {
                    //转换为实体类后的收费规则
                    List<ShouFei> shouFeiList = new ArrayList<>();
                    for (Object o : jsonArray) {
                        net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(o);
                        ShouFei shouFei = (ShouFei) net.sf.json.JSONObject.toBean(jsonObject, ShouFei.class);
                        shouFeiList.add(shouFei);
                    }
                    //是否减去了免费时间
                    boolean flag = false;
                    //是否已经添加了起步价
                    boolean qflag = false;
                    for (int m = 0; m < i+2; m++) {
                        for (ShouFei shouFei : shouFeiList) {
                            //开始时
                            Integer shour = shouFei.getShour();
                            //开始分
                            Integer smiu = shouFei.getSmiu();
                            //计费开始时间
                            int stimes = shour * 60 + smiu+24*(m-1)*60;
                            //结束时
                            Integer ohour = shouFei.getOhour();
                            //结束分
                            Integer omiu = shouFei.getOmiu();
                            //计费结束时间
                            int otimes = 0;
                            if(shour>ohour){
                                otimes = ohour * 60 + omiu+24*(m-1)*60+24*60;
                            }else{
                                otimes = ohour * 60 + omiu+24*(m-1)*60;
                            }
                            //需要算费的时间
                            int i2 = otimes - stimes;

                            //免费分钟
                            String sf = shouFei.getSf();
                            //起步价
                            String sfs = shouFei.getSfs();
                            //本段最高收费
                            String zg = shouFei.getZg();
                            //价格
                            String jy = shouFei.getJy();
                            //分钟
                            String fz = shouFei.getFz();
                            //减去不算费的时间
                            int i1 = 0;
                            if(stimes<rtimes){
                                i1 = ctimes - rtimes;
                            }else{
                                i1 = ctimes - stimes;
                            }
                            //判断入场时间是否在这段收费时间里
                            if(rtimes>otimes){
                                continue;
                            }
                            //如果入场时间小于开始时间
                            if(rtimes<stimes){
                                //减去免费分钟
                                int i3 = 0;
                                if(i1>i2){
                                    i3 = i2;
                                }else{
                                    i3 = i1;
                                }
                                if(!flag){
                                    flag = true;
                                    i3 = i3 - Integer.parseInt(sf);
                                }
                                if(i3<0){
                                    if(i1>i2){
                                        rtimes = otimes-i3;
                                        continue;
                                    }else{
                                        return money;
                                    }

                                }
                                //times = times - stimes;
                                //单价次数
                                int c = 0;
                                if(i3%Integer.parseInt(fz)!=0){
                                    c = i3/Integer.parseInt(fz)+1;
                                    //更新入场时间为收费的结束时间
                                    if(enablesFlag){
                                        rtimes = otimes+Integer.parseInt(fz)-i3%Integer.parseInt(fz);
                                    }
                                }else{
                                    c = i3/Integer.parseInt(fz);
                                    //更新入场时间为收费的结束时间
                                    rtimes = otimes;
                                }
                                BigDecimal dmoney = new BigDecimal(0);
                                BigDecimal bigJy = new BigDecimal(jy);
                                if(!qflag){
                                    qflag = true;
                                    BigDecimal bigSfs = new BigDecimal(sfs);

                                    dmoney = bigSfs.add(BigDecimal.valueOf(c).multiply(bigJy));
                                }else{
                                    dmoney = BigDecimal.valueOf(c).multiply(bigJy);
                                }
                                //是否超出本段收费的最高收费
                                BigDecimal bigZg = new BigDecimal(zg);
                                if(dmoney.compareTo(bigZg)==1){
                                    money = money.add(bigZg);
                                }else{
                                    money = money.add(dmoney);
                                }
                                if(!(i1>i2)){
                                    //结束
                                    return money;
                                }

                            }else if(rtimes>=stimes){//入场时间大于计费开始时间
                                int i4 = i2 - (rtimes - stimes);
                                //减去免费分钟
                                int i3 = 0;
                                if(i1>i4){
                                    i3 = i4;
                                }else{
                                    i3 = i1;
                                }
                                if(!flag){
                                    flag = true;
                                    i3 = i3 - Integer.parseInt(sf);
                                }
                                if(i3<0){
                                    if(i1>i4){
                                        rtimes = otimes-i3;
                                        continue;
                                    }else{
                                        return money;
                                    }
                                }
                                //单价次数
                                int c = 0;
                                if(i3%Integer.parseInt(fz)!=0){
                                    c = i3/Integer.parseInt(fz)+1;
                                    //更新入场时间为收费的结束时间
                                    if(enablesFlag){
                                        rtimes = otimes+Integer.parseInt(fz)-i3%Integer.parseInt(fz);
                                    }
                                }else{
                                    c = i3/Integer.parseInt(fz);
                                    //更新入场时间为收费的结束时间
                                    rtimes = otimes;

                                }
                                BigDecimal dmoney = new BigDecimal(0);
                                BigDecimal bigSfs = new BigDecimal(sfs);
                                BigDecimal bigJy = new BigDecimal(jy);
                                if(!qflag){
                                    qflag = true;
                                    dmoney = bigSfs.add(BigDecimal.valueOf(c).multiply(bigJy));
                                }else{
                                    dmoney = BigDecimal.valueOf(c).multiply(bigJy);
                                }
                                //是否超出本段收费的最高收费
                                BigDecimal bigZg = new BigDecimal(zg);
                                if(dmoney.compareTo(bigZg)==1){
                                    money = money.add(bigZg);
                                }else{
                                    money = money.add(dmoney);
                                }
                                //结束
                                if(!(i1>i4)){
                                    //return
                                    return money;
                                }
                            }
                        }
                    }
                    //log.info("总金额a"+money);
                }
            }else{//24小时收费
                JSONArray jsonArray = JSONArray.fromObject(hourlyCharge);
                if (jsonArray != null) {
                    List<TwentyFourShouFei> list = new ArrayList<>();
                    for (Object o : jsonArray) {
                        net.sf.json.JSONObject jsonObject = net.sf.json.JSONObject.fromObject(o);
                        TwentyFourShouFei twentyFourShouFei = (TwentyFourShouFei) net.sf.json.JSONObject.toBean(jsonObject, TwentyFourShouFei.class);
                        list.add(twentyFourShouFei);
                    }
                    for (int m = 0; m < i+1; m++) {
                        int i1 = 1440;
                        if(ctimes<=1440){
                            i1=ctimes;
                        }
                        //待的时间
                        int i2 = i1 - rtimes;
                        if(ctimes>1440){
                            rtimes = 0;
                            ctimes = ctimes - 1440;
                        }
                        if(m==0){
                            if(freeMinutes!=null&& freeMinutes!=0){//减去免费分钟
                                i2 = i2 - freeMinutes;
                            }
                        }
                        //几个小时
                        int c = 0;
                        if(i2%60!=0){
                            c = i2/60+1;
                        }else{
                            c = i2/60;
                        }
                        for (TwentyFourShouFei twentyFourShouFei : list) {
                            if (Integer.parseInt(twentyFourShouFei.getTime())==c){
                                BigDecimal bigDecimal = new BigDecimal(twentyFourShouFei.getMoney());
                                if(bigDecimal.compareTo(highestMoney)==1){
                                    money = money.add(highestMoney);
                                }else{
                                    money = money.add(bigDecimal);
                                }
                            }
                        }
                    }
                }
            }
            return money;
        }else{
            return new BigDecimal(0);
        }



    }

    @Override
    public Result<?> calculatedAmount(String id, String license, Date outTime)throws Exception {
        CarInfoCollect carInfoCollect=this.getById(id);
        if(carInfoCollect!=null){
            BigDecimal money = new BigDecimal(0);
            money=this.getMoney(carInfoCollect.getComeSerialno(), carInfoCollect.getCardTypeId(), carInfoCollect.getComeInTime(),outTime);

            Calendar cal = Calendar.getInstance();
            cal.setTime(outTime);
            cal.add(Calendar.MINUTE, 30);
            Date waitOutTime = cal.getTime();
            PayInfoVo payInfoVo = new PayInfoVo();
            payInfoVo.setLicense(license);
            payInfoVo.setCarStatus(CommonConstant.SX_CAR_STATUS_0);
            payInfoVo.setCost(money.toString());
            payInfoVo.setWaitOutTime(waitOutTime);
            payInfoVo.setId(carInfoCollect.getId());
            payInfoVo.setOutTime(outTime);
            return Result.ok(payInfoVo);
        }

        return Result.error("数据异常");
    }



    /**
     * 获取小于strDate的数据的数量
     * @param strDate
     * @return
     */
    @Override
    public int getListCountBeforeDay(String strDate) {
        return baseMapper.getListCountBeforeDay(strDate);
    }


    /**
     * 判断是否存在该表
     * @param tableName
     * @return
     */
    @Override
    public List<String> hasTable(String tableName) {
        return baseMapper.hasTable(tableName);
    }

    /**
     * 查询小于strDate时间的数据
     * @param strDate
     * @return
     */
    @Override
    public List<CarInfoCollect> getListBeforeDay(String strDate) {
        return baseMapper.getListBeforeDay(strDate);
    }

    /**
     * 根据时间删除表数据
     * @param strDate
     * @return
     */
    @Override
    public boolean deleteToBeforeDay(String strDate) {
        return baseMapper.deleteToBeforeDay(strDate);
    }

    @Override
    public Result<?> getInfoByPlate(String plate) {
        if(plate.length() < 10){
            String pattern = "([京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼]{1}(([A-HJ-Z]{1}[A-HJ-NP-Z0-9]{5})|([A-HJ-Z]{1}(([DF]{1}[A-HJ-NP-Z0-9]{1}[0-9]{4})|([0-9]{5}[DF]{1})))|([A-HJ-Z]{1}[A-D0-9]{1}[0-9]{3}警)))|([0-9]{6}使)|((([沪粤川云桂鄂陕蒙藏黑辽渝]{1}A)|鲁B|闽D|蒙E|蒙H)[0-9]{4}领)|(WJ[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼·•]{1}[0-9]{4}[TDSHBXJ0-9]{1})|([VKHBSLJNGCE]{1}[A-DJ-PR-TVY]{1}[0-9]{5})";
            boolean matches = Pattern.matches(pattern, plate);
            if(!matches){
                return Result.error(5001, "请输入正确的车牌号码");
            }
        }
        LambdaQueryWrapper<CarInfoCollect> wrapper = new LambdaQueryWrapper<>();
        wrapper.select(
                CarInfoCollect::getId,
                CarInfoCollect::getLicense,
                CarInfoCollect::getParkingName,
                CarInfoCollect::getCardTypeId,
                CarInfoCollect::getComeInTime,
                CarInfoCollect::getComeImagePath
        );
        wrapper.eq(CarInfoCollect::getLicense,plate);
        wrapper.in(CarInfoCollect::getCarStatus,CommonConstant.SX_CAR_STATUS_0,CommonConstant.SX_CAR_STATUS_4,CommonConstant.SX_CAR_STATUS_5,CommonConstant.SX_CAR_STATUS_6);
        wrapper.orderByDesc(CarInfoCollect::getComeInTime);
        wrapper.last("limit 1");
        List<CarInfoCollect> carInfoCollects = this.baseMapper.selectList(wrapper);
        if(carInfoCollects.size()>0){
            //沒有出场时间的直接返回，
            return Result.ok(carInfoCollects.get(0));
        }else{
            return Result.error(5002,"没进场记录，请检查车牌是否输入正确");
        }
    }

    /**
     * 根据出场时间生成订单
     * @return
     */
    @Override
    public Result<?> generateOrder(String id,String openid) {
        try {
            //判断该id是否存在
            CarInfoCollect carInfoCollect = this.baseMapper.selectById(id);
            if(carInfoCollect == null){
                return Result.error("该车牌没有入场记录");
            }
            carInfoCollect.setOpenid(openid);
            //获取车场id
            String parkingLotId  = carInfoCollect.getParkingLotId();
            ParkingLot parkingLot = (ParkingLot) redisUtil.get(ParkingRedisConstant.PARKING_LOT+parkingLotId);
            if(parkingLot == null){
                parkingLot = parkingLotService.getById(parkingLotId);
            }
            if(parkingLot == null){
                Assert.isTrue(false,"该车场不存在");
            }

            Date date = new Date();
            //生成订单
            Result<?> result = paymentWxJsApi(carInfoCollect,parkingLot.getSubMchid(),date);
            if(!result.isSuccess()){
                return Result.error(result.getMessage(),result.getCode());
            }

//            CarInfoCollect entity = new CarInfoCollect();
//            entity.setId(id);
//            entity.setOutTime(dateTime);
//            entity.setCarStatus(CommonConstant.SX_CAR_STATUS_6);
//            boolean b = this.updateById(carInfoCollect);
//            if(!b){
//                Assert.isTrue(false,"修改失败");
//            }
            return result;
        }catch (Exception e){
            e.printStackTrace();
            return Result.error();
        }

    }

    private Result<?> paymentWxJsApi(CarInfoCollect carInfoCollect , String subMchid,Date date) {

        try {
            //可以单独传入http参数，这样参数会自动做URL编码，拼接在URL中
            Map<String, Object> map = MapBuild.hashMapBuild();
            map.put("license", carInfoCollect.getLicense());
            String cardTypeId = carInfoCollect.getCardTypeId();
            CarCardType byId = (CarCardType)redisUtil.get(ParkingRedisConstant.PARKING_TYPE + cardTypeId);
            if(byId==null){
                byId = carCardTypeService.getById(cardTypeId);
            }
            String carType = null;
            if(byId!=null){
                carType = byId.getName();
            }else{
                carType = cardTypeId;
            }
            map.put("carType", carType);
            BigDecimal money = new BigDecimal("0");
            CameraDevice cameraDevice = null;
            //获取设备
            cameraDevice = getCameraDevice(carInfoCollect.getComeSerialno());
            if(cameraDevice == null){
                cameraDevice =  cameraDeviceService.selectByDevice(carInfoCollect.getComeSerialno());
            }
            Assert.isTrue(cameraDevice!=null,"没有该设备");
            money = getMoney(carInfoCollect.getComeSerialno(), cardTypeId, carInfoCollect.getComeInTime(), date);

            map.put("parkCost", money);
            map.put("id",carInfoCollect.getId());
            map.put("costStartTime", carInfoCollect.getComeInTime().getTime()+"");
            map.put("costEndTime", date.getTime()+"");
            map.put("depId", carInfoCollect.getCompanyId());
            map.put("depName", carInfoCollect.getCompanyName());
            map.put("parkingLotId", cameraDevice.getParkingLotId());
            map.put("parkingLotName", carInfoCollect.getParkingName());
            map.put("carTypeId", cardTypeId);
            map.put("cheLianId", carInfoCollect.getId());
            map.put("sub_mchid", "1617522518");
            map.put("openid",carInfoCollect.getOpenid());
            Result<?> result =  QuartzRequestUtil.quartzPostRequest(RedisGetUtil.getPayPath(redisUtil)+"/parking/api/wxParkingPayJsapi", map);
            Result result1 = com.alibaba.fastjson.JSONObject.parseObject(result.getMessage(), Result.class);
            return result1;
        }catch (Exception e){
            e.printStackTrace();
            return Result.error("");
        }
    }
    /**
     *订单支付成功后修改记录状态
     */
   // @Override
//    public void paySuccess(CarInfoCollect carInfoCollect) {
//        if(StringUtils.isNotEmpty(carInfoCollect.getOutSerialno())){//带出场设备序列号的，支付成功后可直接开闸放行，修改进场记录状态
//            try {
//                CarWhiteList byChePai = (CarWhiteList)redisUtil.get(ParkingRedisConstant.PARKING_WHITE_LIST + carInfoCollect.getLicense());
//                String topic = MqttPushClient.getTopic(carInfoCollect.getOutSerialno());//出场主题
//                MqttPushClient.publishOpen(topic);//开闸
//                CarInfoCollect byId = this.getById(carInfoCollect.getId());
//                byId.setChargeType(CommonConstant.PAYMENT_METHOD_0);//支付方式
//                byId.setCarStatus(CommonConstant.SX_CAR_STATUS_1);
//                this.updateById(byId);
//                //显示屏
//                outLedDisplay(carInfoCollect,byChePai,topic,"一路顺风","禁止出场",false);//led显示操作
//            }catch (Exception e){
//                e.printStackTrace();
//            }
//        }else{//没带出场设备序列号的属于提前支付，则修改进场记录状态状态，不需要开闸
//            CarInfoCollect carInfoCollect1 = this.baseMapper.selectById(carInfoCollect.getId());
//            //修改成已支付但未出场状态
//            carInfoCollect1.setCarStatus(CommonConstant.SX_CAR_STATUS_5);
//            carInfoCollect1.setChargeType(CommonConstant.PAYMENT_METHOD_0);//支付方式
//            //获取车场id
//            String parkingLotId  = carInfoCollect.getParkingLotId();
//            ParkingLot parkingLot = (ParkingLot) redisUtil.get(ParkingRedisConstant.PARKING_LOT+parkingLotId);
//            if(parkingLot == null){
//                parkingLot = parkingLotService.getById(parkingLotId);
//            }
//            if(parkingLot == null){
//                Assert.isTrue(false,"该车场不存在");
//            }
//            //获取支付后的免费时间
//            int afterPayFreeTime = parkingLot.getAfterPayFreeTime();
//            if(afterPayFreeTime == 0){
//                afterPayFreeTime = 15;
//            }
//            //订单的结算出场时间+车场的提前支付后免费出场时间=预出场时间
//            Date date = DateUtils.addDateMinut(carInfoCollect.getOutTime(), +afterPayFreeTime);
//            carInfoCollect1.setOutTime(date);
//            carInfoCollect1.setMoney(carInfoCollect.getMoney());
//            this.updateById(carInfoCollect1);
//        }
//    }

    /**
     * 免费放行和结算放行
     * 删除在订单系统生成的订单
     * 修改该记录的车辆状态为已出场、支付方式为现金支付、金额。。。
     * 开闸
     * @return
     */
    @Override
    public Result<?> settlementRelease(CarInfoQuery carInfoQuery) {
        CarInfoCollect carInfoCollect = this.baseMapper.selectById(carInfoQuery.getId());
        carInfoCollect.setCarStatus(CommonConstant.SX_CAR_STATUS_1);
        carInfoCollect.setChargeType(CommonConstant.PAYMENT_METHOD_1);
        carInfoCollect.setAmountReceivedAdvance(carInfoQuery.getAmountReceivedAdvance());
        carInfoCollect.setCurrentAmount(carInfoQuery.getCurrentAmount());
        carInfoCollect.setCompletionTime(new Date());
        carInfoCollect.setDeductionAmount(carInfoQuery.getDeductionAmount());
        if(carInfoQuery.getCurrentAmount().compareTo(new BigDecimal("0"))==0){
            //免费放行
            carInfoCollect.setRemark("免费放行");
        }
        this.baseMapper.updateById(carInfoCollect);
        ParkingLot parkingLot = (ParkingLot)redisUtil.get(ParkingRedisConstant.PARKING_LOT + carInfoCollect.getParkingLotId());
        if(parkingLot==null){
            parkingLot = parkingLotService.getById(carInfoCollect.getParkingLotId());
        }
        //父车场id不为空
        if(StringUtils.isNotEmpty(parkingLot.getParentYard())){
            //新增父车场数据
            insertData(carInfoCollect,parkingLot.getParentYard());
        }
        //开闸
        try {
            //查找是否在黑白名单里面
            CarWhiteList byChePai = (CarWhiteList)redisUtil.get(ParkingRedisConstant.PARKING_WHITE_LIST +carInfoCollect.getParkingLotId()+":"+ carInfoCollect.getLicense());
            if(byChePai==null){
                byChePai = carWhiteListService.getByChePai(carInfoCollect.getLicense());
            }
            //显示屏操作
            String topic = MqttPushClient.getTopic(carInfoQuery.getSerialno());
            outLedDisplay(carInfoCollect,byChePai,topic,"一路顺风","禁止出场",false);//led显示操作
        }catch (Exception e){
            e.printStackTrace();
        }
        // 修改订单
        Map<String,Object> map = new HashMap<>();
        map.put("orderId",carInfoQuery.getId());
        map.put("parkingLotId",carInfoCollect.getParkingLotId());
        map.put("license",carInfoCollect.getLicense());
        Result<?> request = HttpRequestUtil.postRequest(RedisGetUtil.getPayPath(redisUtil) + "/parking/api/updateParkingById", map);
        return Result.OK();
    }

    /**
     * 新增数据
     * @param carInfoCollect
     */
    public void insertData(CarInfoCollect carInfoCollect,String parkingLotId){
        CarInfoCollect collect = new CarInfoCollect();
        collect.setCompanyName(carInfoCollect.getCompanyName());
        ParkingLot parkingLot = (ParkingLot)redisUtil.get(ParkingRedisConstant.PARKING_LOT + carInfoCollect.getParkingLotId());
        if(parkingLot==null){
            parkingLot = parkingLotService.getById(carInfoCollect.getParkingLotId());
        }
        collect.setParkingName(parkingLot.getName());
        collect.setParkingLotId(parkingLotId);
        collect.setCompanyId(parkingLot.getCompanyId());
        collect.setBrand(carInfoCollect.getBrand());
        collect.setLicense(carInfoCollect.getLicense());
        collect.setCarStatus(CommonConstant.SX_CAR_STATUS_0);
        collect.setComeInTime(new Date());
        collect.setComeImagePath(carInfoCollect.getOutImagePath());
        collect.setCarYear(carInfoCollect.getCarYear());
        collect.setCarType(carInfoCollect.getCarType());
        collect.setCarCardColor(carInfoCollect.getCarCardColor());
        String carTypeId=carVehicleService.carTypeId(carInfoCollect.getLicense(),parkingLotId);
        collect.setCardTypeId(carInfoCollect.getCardTypeId());
        if(carTypeId!=null){
            collect.setCardTypeId(carTypeId);
            collect.setCarPayType(CommonConstant.PARKING_CAR_PAY_TYPE_2);
        }else {
            //车牌类型
            collect.setCarPayType(CommonConstant.PARKING_CAR_PAY_TYPE_1);
        }
        CarWhiteList byChePai = (CarWhiteList)redisUtil.get(ParkingRedisConstant.PARKING_WHITE_LIST+ carInfoCollect.getParkingLotId()+":"+ carInfoCollect.getLicense());
        if(byChePai==null){
            byChePai = carWhiteListService.getByChePai(carInfoCollect.getLicense());
        }
        Boolean aBoolean = carWhiteListService.ifFag(byChePai);
        if(aBoolean){
            carInfoCollect.setCarPayType(CommonConstant.PARKING_CAR_PAY_TYPE_0);
        }
        collect.setCarColor(carInfoCollect.getCarColor());
        collect.setComeDevice(carInfoCollect.getOutDevice());
        collect.setComeIpaddr(carInfoCollect.getOutIpaddr());
        collect.setComeSerialno(carInfoCollect.getOutSerialno());
        collect.setTriggerType(carInfoCollect.getTriggerType());
        collect.setComePlateid(carInfoCollect.getOutPlateid());
        collect.setIsFakePlate(carInfoCollect.getIsFakePlate());
        collect.setFeatureCode(carInfoCollect.getFeatureCode());
        collect.setTimeUsed(carInfoCollect.getTimeUsed());
        collect.setConfidence(carInfoCollect.getConfidence());
        collect.setComeInfoText(carInfoCollect.getOutInfoText());
        collect.setDeviceGroupId(carInfoCollect.getDeviceGroupId());
        this.baseMapper.insert(collect);
    }



    @Override
    public Integer getCountByloginTime(Date loginTime,List<String> list) {
        return this.baseMapper.getCountByloginTime(loginTime,list);
    }

    @Override
    public List<CarInfoCollect> getoutCountByloginTime(Date loginTime,List<String> list) {
        return this.baseMapper.getoutCountByloginTime(loginTime,list);
    }

    /**
     * http 上传图片
     * @param carInfo
     */
    @Override
    public void uploadPictures(Object carInfo) {
        try {
            long l = System.currentTimeMillis();
            //String转JSONObject
            JSONObject jsonObject =JSONUtil.parseObj(carInfo);
            //根据key获取值，把值转为JSONObject
            JSONObject alarmInfoPlate=jsonObject.getJSONObject("AlarmInfoPlate");
            //摄像机基本信息
            AlarmInfoPlateQuery alarmInfoPlateQuery = JSONUtil.toBean(alarmInfoPlate, AlarmInfoPlateQuery.class);
            JSONObject resultInfo=alarmInfoPlate.getJSONObject("result");
            if(resultInfo != null){
                //获取车牌基本信息
                JSONObject plateResult=resultInfo.getJSONObject("PlateResult");
                //车辆详细信息转实体接受
                HttpPlateResultQuery httpPlateResultQuery=JSONUtil.toBean(plateResult,HttpPlateResultQuery.class);
                //根据设备序列号获取设备使用情况：入库设备，出库设备
                CameraDeviceVo deviceVo = cameraDeviceService.selectDevice(alarmInfoPlateQuery.getSerialno());
                Assert.isTrue(deviceVo!=null,"找不到设备");
                if(!deviceVo.getRepairFlag().equals(CommonConstant.PARKING_DEVICE_TYPE_0)){
                    Assert.isTrue(true,"不是主设备，不需要进行图片操作");
                }

                //该设备组的主设备
                CameraDevice mainDevice = getMainDevice(deviceVo.getDeviceGroupId());
                Assert.isTrue(mainDevice!=null,"该设备组没有主设备，请检查");
                //车场信息
                ParkingLot parkingLot = (ParkingLot)redisUtil.get(ParkingRedisConstant.PARKING_LOT + mainDevice.getParkingLotId());
                if(parkingLot==null){
                    parkingLot = parkingLotService.getById(mainDevice.getParkingLotId());
                }
                Assert.isTrue(parkingLot!=null,"没有该车场");
                Assert.isTrue(parkingLot.getStateYard()!=false,"该车场未启用");
                //大图片文件流
                if(httpPlateResultQuery.getImageFile()!=null){
                    String format = DateUtils.yyyyMMdd.get().format(new Date());
                    String time = DateUtils.HHmmss.get().format(new Date());
                    String path = "picture/"+alarmInfoPlateQuery.getSerialno()+"/"+format;
                    String fileName = format+"_"+time+"_"+getThree()+".jpg";
                    byte[] bytes = DatatypeConverter.parseBase64Binary(httpPlateResultQuery.getImageFile());
                    log.info("获取图片流总时间："+(System.currentTimeMillis() - l));

                    bytes = PicUtils.compressPicForScale(bytes, 30, "x");// 图片小于300kb
                    log.info("压缩图片总时间："+(System.currentTimeMillis() - l));
                    InputStream inputStream = new ByteArrayInputStream(bytes);
                    MultipartFile multipartFile = new MockMultipartFile(fileName,fileName, ContentType.APPLICATION_OCTET_STREAM.toString(), inputStream);
                    //文件夹名称
                    OSSFile upload = iossFileService.uploadImg(multipartFile, path);

                    log.info("上传阿里云图片总时间："+(System.currentTimeMillis() - l));
                    //查看一下是否有添加了该记录的数据
                    if(httpPlateResultQuery.getLicense().equals("_无_")){
                        Object id = redisUtil.get(ParkingRedisConstant.PARKING_UNLICENSED+mainDevice.getSerialno()+httpPlateResultQuery.getPlateid());
                        if(null==id){
                            redisUtil.set(ParkingRedisConstant.PARKING_UNLICENSED+mainDevice.getSerialno()+httpPlateResultQuery.getPlateid(),upload.getUrl(),60*5);
                        }else{
                            CarInfoCollect carInfoCollect = (CarInfoCollect)id;
                            if(mainDevice.getDeviceUse().equals(CommonConstant.SX_DEVICE_USE_0)){
                                carInfoCollect.setComeImagePath(upload.getUrl());
                            }else{
                                carInfoCollect.setOutImagePath(upload.getUrl());
                            }
                            redisUtil.set(ParkingRedisConstant.PARKING_UNLICENSED+mainDevice.getSerialno()+httpPlateResultQuery.getPlateid(),carInfoCollect,60*5);
                        }
                    }
                    Object id = redisUtil.get(ParkingRedisConstant.PARKING_HTTPIMG + mainDevice.getSerialno() + httpPlateResultQuery.getLicense()+httpPlateResultQuery.getPlateid());
                    if(id==null){
                        redisUtil.set(ParkingRedisConstant.PARKING_HTTPIMG + mainDevice.getSerialno() + httpPlateResultQuery.getLicense()+httpPlateResultQuery.getPlateid(),upload.getUrl(),60*5);
                    }else{

                        DongGuanStaticPictureFileInfo pictureFileInfo = new DongGuanStaticPictureFileInfo();

                        CarInfoCollect carInfoCollect = new CarInfoCollect();
                        carInfoCollect.setId((String)id);
                        if(mainDevice.getDeviceUse().equals(CommonConstant.SX_DEVICE_USE_0)){
                            carInfoCollect.setComeImagePath(upload.getUrl());
                            pictureFileInfo.setPicture(1);
                        }else{
                            carInfoCollect.setOutImagePath(upload.getUrl());
                            pictureFileInfo.setPicture(2);
                        }
                        this.baseMapper.updateById(carInfoCollect);

                        redisUtil.del(ParkingRedisConstant.PARKING_HTTPIMG + mainDevice.getSerialno() + httpPlateResultQuery.getLicense()+httpPlateResultQuery.getPlateid());
                        CarInfoCollect carInfoCollect2 = this.baseMapper.selectById((String) id);
                        //发送图片到mq
                        if(mainDevice.getDeviceUse().equals(CommonConstant.SX_DEVICE_USE_0)){
                            send3DWebsocketInfo(carInfoCollect2,"1");
                        }else{
                            send3DWebsocketInfo(carInfoCollect2,"2");
                        }
                        manualConfirm(carInfoCollect2);
                        if(parkingLot.getUploadStaticTrafficInfoFlag().equals(CommonConstant.UPLOAD_STATIC_TRAFFIC_INFO_FLAG_0)){
                            log.info("设置不上传静态交通");
                            return;
                        }
                        if(parkingLot.getFlagStaticTraffic().equals("0")){
                            log.info("停车场信息未上传静态交通");
                            return;
                        }
                        //上传静态交通图片
                        DgParkingApi dgParkingApi = new DgParkingApi();
                        pictureFileInfo.setParkingOrSectionId(DongGuanStaticConfig.dongGuanComId+mainDevice.getParkingLotId());
                        pictureFileInfo.setOrderId((String)id);
                        pictureFileInfo.setPictureType(2);//这里的情况都是停车场的情况
                        pictureFileInfo.setSize(1000l);
                        pictureFileInfo.setCreateTime(System.currentTimeMillis());
                        pictureFileInfo.setUpdateTime(System.currentTimeMillis());

                        DongGuanStaticUploadPictureStreamInfo streamInfo = new DongGuanStaticUploadPictureStreamInfo();
                        streamInfo.setPictureType("2");
                        streamInfo.setUrlAddress(upload.getUrl());
                        streamInfo.setStartPosition(0l);
                        streamInfo.setCreateTime(System.currentTimeMillis());
                        streamInfo.setUpdateTime(System.currentTimeMillis());
                        DongGuanStaticResponse dongGuanStaticResponse = dgParkingApi.dgUploadPicture( pictureFileInfo,  streamInfo);
                        if(dongGuanStaticResponse.getState()==1){
                            //更新数据
                            CarInfoCollect carInfoCollect1 = new CarInfoCollect();
                            carInfoCollect1.setId((String)id);
                            if(mainDevice.getDeviceUse().equals(CommonConstant.SX_DEVICE_USE_0)){
                                carInfoCollect1.setImgFlagStaticTraffic("1");
                                carInfoCollect1.setImgUploadStaticTrafficTime(new Date());
                            }else{
                                carInfoCollect1.setOutImgFlagStaticTraffic("1");
                                carInfoCollect1.setOutImgUploadStaticTrafficTime(new Date());
                            }

                            this.baseMapper.updateById(carInfoCollect1);
                        }
                        log.info("上传图片到静态交通："+dongGuanStaticResponse.getMessage());


                    }
                }

//                //小图片文件流
//                if(httpPlateResultQuery.getImageFragmentFile()!=null){
//                    byte[] bytes = DatatypeConverter.parseBase64Binary(httpPlateResultQuery.getImageFragmentFile());
//                    bytes = PicUtils.compressPicForScale(bytes, 30, "x");// 图片小于300kb
//                    FileUtils.writeByteArrayToFile(new File("E:/home/img/small.jpg"), bytes);
//                }
            }
            log.info("总时间："+(System.currentTimeMillis() - l));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //需要手动确认开闸的操作
    public void manualConfirm(CarInfoCollect carInfoCollect){
        CarInfoCollect carInfoCollectDe = this.baseMapper.selectById(carInfoCollect.getId());
        CarInfoCollectVo carInfoCollectVo = new CarInfoCollectVo();
        if(carInfoCollectDe == null){
            return;
        }
        BeanUtils.copyProperties(carInfoCollectDe,carInfoCollectVo);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(carInfoCollectDe.getComeInTime() == null){
            return;
        }
        carInfoCollectVo.setStartComeTimes(DateUtils.date2Str(carInfoCollectDe.getComeInTime(),simpleDateFormat));
        if(carInfoCollectDe.getOutTime()!=null){
            if(carInfoCollectDe.getCarPayType().equals(CommonConstant.PARKING_CAR_PAY_TYPE_0)){
                carInfoCollectVo.setMoney(new BigDecimal("0"));
                carInfoCollectVo.setAmountReceivedAdvance(new BigDecimal("0"));//预收金额
                carInfoCollectVo.setCurrentAmount(new BigDecimal("0"));//本次金额
                carInfoCollectVo.setDeductionAmount(new BigDecimal("0"));//减免金额
                String s = DateUtils.dateOperation(carInfoCollectDe.getComeInTime(), carInfoCollectDe.getOutTime());
                carInfoCollectVo.setTimes(s);
                carInfoCollectVo.setEndComeTimes(DateUtils.date2Str(carInfoCollectDe.getOutTime(),simpleDateFormat));
            }else{
                String s = DateUtils.dateOperation(carInfoCollectDe.getComeInTime(), carInfoCollectDe.getOutTime());
                carInfoCollectVo.setTimes(s);
                carInfoCollectVo.setEndComeTimes(DateUtils.date2Str(carInfoCollectDe.getOutTime(),simpleDateFormat));
                CarYardFees dataByInfoId = carYardFeesService.getDataByInfoId(carInfoCollect.getId(), carInfoCollect.getParkingLotId());
                BigDecimal jian = new BigDecimal("0");//商家减免金额
                if(dataByInfoId!=null){
                    jian = dataByInfoId.getCumulativeAmount();
                }
                //减掉商家的那些
                carInfoCollectVo.setDeductionAmount(jian);//减免金额
                //应收-减免=预收金额
                if(carInfoCollectVo.getMoney()==null){
                    carInfoCollectVo.setMoney(new BigDecimal("0"));
                }
                carInfoCollectVo.setAmountReceivedAdvance(carInfoCollectVo.getMoney());//预收金额
                //如果已经线上付了钱则本次金额为0，反之本次金额 = 预收金额
                //查找支付订单
                HashMap<String, Object> paramMap = new HashMap<>();
                paramMap.put("id", carInfoCollectVo.getId());
                if(carInfoCollectVo.getMoney().compareTo(new BigDecimal("0"))==1){
                    Result<?> result = HttpRequestUtil.postRequest(RedisGetUtil.getPayPath(redisUtil)+"/parking/api/whetherToPay", paramMap);

                    if(null == result){
                        log.error("支付服务 不在线");
                        return;
                    }
                    if(result.isSuccess()){//已经支付了
                        carInfoCollectVo.setCurrentAmount(new BigDecimal("0"));//本次金额
                    }else{
                        BigDecimal subtract = carInfoCollectVo.getMoney().subtract(jian);
                        if(subtract.compareTo(new BigDecimal("0"))==-1){
                            subtract = new BigDecimal("0");
                        }
                        carInfoCollectVo.setCurrentAmount(subtract);
                    }
                }else{
                    carInfoCollectVo.setCurrentAmount(new BigDecimal("0"));//本次金额
                }
            }
        }
        MqttPushClient.pushlishRetained("Retained_"+carInfoCollect.getParkingLotId(),JSON.toJSONString(carInfoCollectVo));
        MqttPushClient.pushlish(carInfoCollect.getParkingLotId(),JSON.toJSONString(carInfoCollectVo));
    }
    /**
     * 发送设备的实时信息给3D
     * cameraDeviceId 设备id
     * status 状态
     */
    public void send3DWebsocketInfo(CarInfoCollect carInfoCollect,String state){
        Map<String,Object> map = new HashMap<>();
        map.put("license",carInfoCollect.getLicense());
        map.put("state",state);
        if(state.equals("1")){
            CameraDevice cameraDevice = getCameraDevice(carInfoCollect.getComeSerialno());
            if(cameraDevice==null){
                cameraDeviceService.selectByDevice(carInfoCollect.getOutSerialno());
            }
            if(null == cameraDevice){
                log.info("找不到设备");
                return;
            }
            map.put("img",carInfoCollect.getComeImagePath());
            map.put("time",DateUtil.format(carInfoCollect.getComeInTime(), DatePattern.NORM_DATETIME_PATTERN));
            map.put("parkId",cameraDevice.getScreenCode());
        }else{
            CameraDevice cameraDevice = getCameraDevice(carInfoCollect.getOutSerialno());
            if(cameraDevice==null){
                cameraDeviceService.selectByDevice(carInfoCollect.getOutSerialno());
            }
            if(null == cameraDevice){
                log.info("找不到设备");
                return;
            }
            map.put("img",carInfoCollect.getOutImagePath());
            map.put("time",DateUtil.format(carInfoCollect.getOutTime(), DatePattern.NORM_DATETIME_PATTERN));
            map.put("parkId",cameraDevice.getScreenCode());
        }

        if(StringUtils.isNotEmpty(SysConfigUtil.getVal("carInfo_push_3d"))){
            HttpRequestUtil.postRequest(SysConfigUtil.getVal("carInfo_push_3d"), map);
        }
    }

    /**
     * 获取一个随机的三位数
     * @return
     */
    public String getThree(){
        int i=(int)(Math.random()*900)+100;
        return i+"";
    }
    public CameraDevice getMainDevice(String groupId){
        //获取设备组中的主辅设备
        List<CameraDevice> list = (List<CameraDevice>)redisUtil.get(ParkingRedisConstant.PARKING_DEVICE + groupId);
        if(list!=null){
            for (CameraDevice cameraDevice : list) {
                if(CommonConstant.PARKING_DEVICE_TYPE_0.equals(cameraDevice.getRepairFlag())){
                    return cameraDevice;
                }
            }
        }
        return null;
    }

    /**
     * 发送设备的实时状态给3D
     * serialno   序列号
     * status 状态
     */
    public void send3DWebsocket(String serialno,String status){
        CameraDevice byId = cameraDeviceService.selectByDevice(serialno);
        Map<String,Object> map = new HashMap<>();
        map.put("serialno",byId.getSerialno());
        map.put("largeScreenId",byId.getScreenCode());
        map.put("openOrClose",status);
        HttpRequestUtil.postRequest(SysConfigUtil.getVal("3d_sendWebsocket"), map);
    }



    /**
     * 根据园区Id查询此园区最近一天的出入场记录
     * @param screenCode
     * @return
     */
    @Override
    public Result<?> screenCarInfo(String screenCode) {
        List<String> list=cameraDeviceService.screenDeviceList(screenCode);
        if(list.size()>0){
            CarInfoCollect carInfoCollect=this.baseMapper.screenCarInfo(list);
            if (null != carInfoCollect){
                JSONObject jsonObject=new JSONObject();
                jsonObject.put("license",carInfoCollect.getLicense());
                jsonObject.put("parkId",screenCode);
                if (null != carInfoCollect.getOutTime()){
                    //出场
                    jsonObject.put("state","0");
                    jsonObject.put("img",carInfoCollect.getOutImagePath());
                    jsonObject.put("time",carInfoCollect.getOutTime());
                }else {
                    //进场
                    jsonObject.put("state","1");
                    jsonObject.put("img",carInfoCollect.getComeImagePath());
                    jsonObject.put("time",carInfoCollect.getComeInTime());
                }
                return Result.ok(jsonObject);
            }else {
                return Result.error("此园区暂无出入停车信息");
            }
        }else {
            return Result.error("此园区没有绑定停车设备设备");
        }

    }

    //统计金额
    @Override
    public Result<?> moneyStatistics(CarInfoCollectVo carInfoCollectvo) {
        MoneyStatisticsVo moneyStatisticsVo = baseMapper.moneyStatistics(carInfoCollectvo);
        return Result.OK(moneyStatisticsVo);
    }

    //统计一下这段时间内现金收款：已收金额和减免金额
    @Override
    public MoneyStatisticsVo moneyStatisticss(CarShiftForm dataByUserId) {
        return this.baseMapper.moneyStatisticss(dataByUserId);
    }


//    /**
//     * tcp获取图片上传到oss
//     */
//    public void saveImg(String ip,String imgName,String ossPath){
//        try {
//            //查询出没有图片的记录，进行上传图片
//            if(!redisUtil.hasKey(ParkingRedisConstant.PARKING_IMG)){
//                redisUtil.lock(ParkingRedisConstant.PARKING_IMG,3600);
//                List<CarInfoCollect> carInfoCollects = this.baseMapper.getImgIsNull();
//                for (CarInfoCollect carInfoCollect : carInfoCollects) {
//                    if(StringUtils.isEmpty(carInfoCollect.getComeImagePath())){
//
//                    }
//
//                }
//
//            }else{
//                log.error("正在上传图片中=================================");
//            }
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//            String path = "E:/home/img/";
//            Socket socket = SocketClient.open(ip);
//            SocketWriter.getivsresult(socket.getOutputStream());
//            Result read = SocketSendParameter.read(socket.getInputStream());
//            Map map = (Map)read.getResult();
//            //储存在本地的路径
//            Object jsonStr = map.get("imgUrl");
//            if(jsonStr==null){
//                return;
//            }
//            SocketClient.close(socket);
//            //压缩图片大小
//            byte[] bytes = FileUtils.readFileToByteArray(new File(jsonStr.toString()));
//            bytes = PicUtils.compressPicForScale(bytes, 30, "x");
//            FileUtils.writeByteArrayToFile(new File(path+imgName), bytes);
//
//
//
//            //上传图片到oss
//            File file = new File(path+imgName);
//            InputStream inputStream = new FileInputStream(file);
//            MultipartFile multipartFile = new MockMultipartFile(file.getName(),file.getName(), ContentType.APPLICATION_OCTET_STREAM.toString(), inputStream);
//            //文件夹名称
      //      OSSFile upload = iossFileService.uploadImg(multipartFile, ossPath);
//            //上传成功后删除本地图片
//        }catch (Exception e){
//            e.printStackTrace();
//        }finally {
//            redisUtil.del(ParkingRedisConstant.PARKING_IMG);
//        }
//    }


}

package org.jeecg.modules.parking.mapper;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarShiftForm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.query.CarShiftFormQuery;
import org.jeecg.modules.parking.vo.CarShiftFormVo;

/**
 * @Description: 结算交班表
 * @Author: jeecg-boot
 * @Date:   2022-04-15
 * @Version: V1.0
 */
public interface CarShiftFormMapper extends BaseMapper<CarShiftForm> {

    /**
     * 查看是否有没交接的记录
     * @param id
     * @return
     */
    List<CarShiftForm> selectByIdAndTime(@Param("id") String id);

    List<CarShiftFormVo> pageList(Page<CarShiftFormVo> page, @Param("q") CarShiftFormQuery carShiftFormQuery);
}

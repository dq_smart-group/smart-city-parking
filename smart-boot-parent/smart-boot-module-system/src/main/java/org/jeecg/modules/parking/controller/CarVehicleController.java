package org.jeecg.modules.parking.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.modules.parking.entity.CarVehicle;
import org.jeecg.modules.parking.query.CarVehicleQuery;
import org.jeecg.modules.parking.service.ICarCardTypeService;
import org.jeecg.modules.parking.service.ICarVehicleService;
import org.jeecg.modules.parking.vo.CarVehicleVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

/**
 * @Description: 车辆信息表
 * @Author: jeecg-boot
 * @Date: 2021-12-09
 * @Version: V1.0
 */
@Api(tags = "车辆信息表")
@RestController
@RequestMapping("/parking/carVehicle")
@Slf4j
public class CarVehicleController extends JeecgController<CarVehicle, ICarVehicleService> {
    @Autowired
    private ICarVehicleService carVehicleService;
    @Autowired
    private ICarCardTypeService carCardTypeService;

    @Autowired
    private ISysDepartService sysDepartService;

    /**
     * 分页列表查询
     *
     * @param carVehicleQuery
     * @param pageNo
     * @param pageSize
     * @param req
     * @return
     */
    @AutoLog(value = "车辆信息表-分页列表查询")
    @ApiOperation(value = "车辆信息表-分页列表查询", notes = "车辆信息表-分页列表查询")
    @GetMapping(value = "/list")
    public Result<?> queryPageList(CarVehicleQuery carVehicleQuery,
                                   @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                   @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                   HttpServletRequest req) {

        if (!LoginUserUtil.isAdmin()) {
            SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
            if (sysDepart != null) {
                carVehicleQuery.setCompanyId(sysDepart.getId());
            } else {
                carVehicleQuery.setCompanyId("0");
            }
        }
        Page<CarVehicleVo> page = new Page<CarVehicleVo>(pageNo, pageSize);
        IPage<CarVehicleVo> pageList = carVehicleService.pageList(page, carVehicleQuery);
        return Result.OK(pageList);
    }

    /**
     * 添加
     *
     * @param carVehicle
     * @return
     */
    @AutoLog(value = "车辆信息表-添加")
    @ApiOperation(value = "车辆信息表-添加", notes = "车辆信息表-添加")
    @PostMapping(value = "/add")
    public Result<?> add(@RequestBody CarVehicle carVehicle) {

        Result result = carVehicleService.addcarVehicleSave(carVehicle);
        return result;
    }

    /**
     * 编辑
     *
     * @param carVehicle
     * @return
     */
    @AutoLog(value = "车辆信息表-编辑")
    @ApiOperation(value = "车辆信息表-编辑", notes = "车辆信息表-编辑")
    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody CarVehicle carVehicle) {

        Result result = carVehicleService.updatecarVehicleById(carVehicle);
        return result;
    }

    /**
     * 通过id删除
     *
     * @param id
     * @return
     */
    @AutoLog(value = "车辆信息表-通过id删除")
    @ApiOperation(value = "车辆信息表-通过id删除", notes = "车辆信息表-通过id删除")
    @DeleteMapping(value = "/delete")
    public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
        carVehicleService.removeById(id);
        return Result.OK("删除成功!");
    }

    /**
     * 批量删除
     *
     * @param ids
     * @return
     */
    @AutoLog(value = "车辆信息表-批量删除")
    @ApiOperation(value = "车辆信息表-批量删除", notes = "车辆信息表-批量删除")
    @DeleteMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
        this.carVehicleService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 通过id查询
     *
     * @param id
     * @return
     */
    @AutoLog(value = "车辆信息表-通过id查询")
    @ApiOperation(value = "车辆信息表-通过id查询", notes = "车辆信息表-通过id查询")
    @GetMapping(value = "/queryById")
    public Result<?> queryById(@RequestParam(name = "id", required = true) String id) {
        CarVehicle carVehicle = carVehicleService.getById(id);
        if (carVehicle == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(carVehicle);
    }

    /**
     * 导出excel
     *
     * @param request
     * @param carVehicle
     */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarVehicle carVehicle) {
        return super.exportXls(request, carVehicle, CarVehicle.class, "车辆信息表");
    }

    /**
     * 通过excel导入数据
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarVehicle.class);
    }

}

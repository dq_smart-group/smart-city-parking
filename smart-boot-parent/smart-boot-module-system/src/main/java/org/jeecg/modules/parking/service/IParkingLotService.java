package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daqing.entity.DongGuanStaticParkingLogBaseData;
import com.daqing.entity.DongGuanStaticSysSection;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.ParkingLot;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.query.CarMonthlyRentQuery;
import org.jeecg.modules.parking.query.ParkingLotQuery;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.parking.vo.ParkingLotVo;

import java.math.BigDecimal;
import java.util.List;

/**
 * @Description: 车场表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
public interface IParkingLotService extends IService<ParkingLot> {

    IPage<ParkingLotVo> pageList(Page<ParkingLotVo> page, ParkingLotQuery parkingLotQuery);

    /**
     * 是管理员就查询全部车场，不是管理员就获取登陆人对应的所有公司下的车场
     */
    List<ParkingLotAndCamVo> getParkingLotByCompanyId(String companyId);

    /**
     * 获取车场信息
     * @return
     */
    List<ParkingLot> getList();


    /**
     *
     * @return
     */
    BigDecimal getMoneyByRenewalDuration(CarMonthlyRentQuery carMonthlyRentQuery);

    /**
     * 停车场基础数据上传
     * @param data
     * @return
     */
    Result<?> parkingLotInformationUpload(DongGuanStaticParkingLogBaseData data);

    /**
     * 道路基础数据上传
     * @param data
     * @return
     */
    Result<?> reloadInformationUpload(DongGuanStaticSysSection data);

    /**
     *更新redis数据
     */
    void toUpdate();

    List<ParkingLotAndCamVo> getSentryBoxLotByUserId(String companyId);

    /**
     * 根据车场标识符查询车场
     * @param identifier
     * @return
     */
    ParkingLot getParkingLotByIdentifier(String identifier);

    /**
     * 根据公司id来查询停车管理需要的数据
     * @param departId
     * @return
     */
    Result<?> getParkingManagementDataByDepartId(String departId);
}

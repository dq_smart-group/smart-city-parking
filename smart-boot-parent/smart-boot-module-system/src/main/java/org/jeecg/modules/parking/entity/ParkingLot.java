package org.jeecg.modules.parking.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 车场表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
@Data
@TableName("parking_lot")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="parking_lot对象", description="车场表")
public class ParkingLot implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**车场名称*/
	@Excel(name = "车场名称", width = 15)
    @ApiModelProperty(value = "车场名称")
    private java.lang.String name;
	/**车场地址*/
	@Excel(name = "车场地址", width = 15)
    @ApiModelProperty(value = "车场地址")
    private java.lang.String address;

	/**车场地址*/
	@Excel(name = "车场地址", width = 15)
    @ApiModelProperty(value = "车场地址")
    private java.lang.String fullAddress;

	/**公司id*/
	@Excel(name = "公司id", width = 15)
    @ApiModelProperty(value = "公司id")
    private java.lang.String companyId;
    /**总车位*/
    @Excel(name = "总车位", width = 15)
    @ApiModelProperty(value = "总车位")
    private java.lang.String truckSpace;
    /**车场状态*/
    @Excel(name = "车场状态", width = 15)
    @ApiModelProperty(value = "车场状态")
    private Boolean stateYard;
    /**车场标识符*/
    @Excel(name = "车场标识符", width = 15)
    @ApiModelProperty(value = "车场标识符")
    private java.lang.String carIdentifier;


    /**
     * 微信支付后免费出场时间
     */
    private int afterPayFreeTime;


    //子商户号
    @TableField(exist = false)
    private String subMchid;


    /**
     * 同一个车牌号在多少秒内只识别一次
     */
    private Integer times;

    /**
     *出场时无入场记录记录如何处理：0：不收费直接放行，1：人工处理
     */
    private String noAdmissionRecord;

    /**
     * 是否上传了静态交通
     */
    private String flagStaticTraffic;
    /**上传静态交通时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "上传静态交通时间")
    private java.util.Date uploadStaticTrafficTime;

    /**
     * 父车场id
     */
    private String parentYard;

    /**
     * 道路停车还是停车场
     */
    private String roadOrParkingLot;

    /**
     * 进出场数据是否开始上传静态交通
     */
    private String uploadStaticTrafficInfoFlag;


}

package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.ParkingLot;
@Data
public class ParkingLotVo extends ParkingLot {
    //公司名称
    private String departName;
}

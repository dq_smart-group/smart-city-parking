package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarCharge;

import java.util.List;

@Data
public class CarChargeVo extends CarCharge {

    private String username;
    private String realname;
    private String parkingName;

    private List<String> deviceGroupIdS;
}

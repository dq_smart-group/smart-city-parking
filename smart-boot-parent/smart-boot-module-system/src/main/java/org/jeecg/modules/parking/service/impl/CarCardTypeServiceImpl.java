package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarCardType;
import org.jeecg.modules.parking.entity.Depart;
import org.jeecg.modules.parking.mapper.CarCardTypeMapper;
import org.jeecg.modules.parking.query.CarCardTypeQuery;
import org.jeecg.modules.parking.service.ICarCardTypeService;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 车牌类型
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
@Service
public class CarCardTypeServiceImpl extends ServiceImpl<CarCardTypeMapper, CarCardType> implements ICarCardTypeService {


    @Autowired
    private ISysDepartService sysDepartService;

    @Autowired
    private IParkingLotService parkingLotService;

    @Override
    public List<Depart> getDepartlist() {
        return baseMapper.getDepartlist();
    }

    @Override
    public List<Depart> getDepartByUserId(String loginUserId) {
        return baseMapper.getDepartByUserId(loginUserId);
    }

    @Override
    public IPage<CarCardType> pageList(Page<CarCardType> page, CarCardTypeQuery carCardTypeQuery) {
        List<CarCardType> list=this.baseMapper.pageList(page,carCardTypeQuery);
        return page.setRecords(list);
    }

    /**
     * 获取登陆人对应的公司下的车牌类型,是管理员就获取全部
     * @param companyId
     * @return
     */
    @Override
    public List<ParkingLotAndCamVo> getCardTypeByparkingLotId(String companyId) {

        List<ParkingLotAndCamVo> list = new ArrayList<>();
        //调用查出公司车场的的接口
        List<ParkingLotAndCamVo> parkingLotByCompanyId = parkingLotService.getParkingLotByCompanyId(companyId);
        if (parkingLotByCompanyId.size() > 0) {
            for (ParkingLotAndCamVo parkingLotAndCamVo : parkingLotByCompanyId) {
                List<ParkingLotAndCamVo> children = parkingLotAndCamVo.getChildren();
                if (children.size() > 0) {
                    List<ParkingLotAndCamVo> lotAndCamVos=new ArrayList<>();
                    for (ParkingLotAndCamVo child : children) {
                        List<ParkingLotAndCamVo> byParkingLotId = this.baseMapper.getCardTypeByparkingLotId(child.getValue());
                        if (byParkingLotId.size() > 0) {
                            child.setChildren(byParkingLotId);
                            lotAndCamVos.add(child);
                        }
                    }
                    if(lotAndCamVos.size()>0) {
                        parkingLotAndCamVo.setChildren(lotAndCamVos);
                    }else {
                        parkingLotAndCamVo.setChildren(null);
                    }
                }
                if(null!=parkingLotAndCamVo.getChildren()) {
                    list.add(parkingLotAndCamVo);
                }

            }
        }


        return list;
    }

    @Override
    public List<CarCardType> getCardTypeByParkingId(String parkingId) {
        return this.baseMapper.getCardTypeByParkingId(parkingId);
    }



    /**
     *
     * @param carCardType
     * @return
     */
    @Override
    public Result addCarCardTypesave(CarCardType carCardType) {

        QueryWrapper<CarCardType>Wrapper=new QueryWrapper<>();
        Wrapper.eq("dep_id",carCardType.getDepId());
        Wrapper.eq("name",carCardType.getName());

        List<CarCardType>dd=this.list(Wrapper);

        if (dd.size()!=0){
            return Result.error("已存在该车牌类型！");
        }else {
            this.save(carCardType);
            return Result.OK("添加成功！");
        }
    }

    /**
     *
     * @param carCardType
     * @return
     */
    @Override
    public Result updateCarCardTypeById(CarCardType carCardType) {
        QueryWrapper<CarCardType> Wrapper=new QueryWrapper<>();
        Wrapper.ne("id",carCardType.getId());
        Wrapper.eq("dep_id",carCardType.getDepId());
        Wrapper.eq("name",carCardType.getName());

        List<CarCardType>dd=this.list(Wrapper);

        if (dd.size()!=0){
            return Result.error("已存在该车牌类型！");
        }else {
            this.updateById(carCardType);
            return Result.OK("编辑成功!");
        }
    }

    @Override
    public List<CarCardType> getList() {
        return this.baseMapper.selectList(null);
    }


}

package org.jeecg.modules.quartz.api;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.daqing.entity.DongGuanStaticHeartBeatTestInfo;
import com.daqing.entity.DongGuanStaticResponse;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.DySmsEnum;
import org.jeecg.common.util.DySmsHelper;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.config.sign.util.HttpUtils;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.history.service.ICarAccessRecordHistoryService;
import org.jeecg.modules.history.service.ICarInfoCollectHistoryService;
import org.jeecg.modules.parking.api.DgParkingApi;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import org.jeecg.modules.parking.entity.CarWhiteList;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.service.*;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("/quartz/openapi")
public class QuartzOpenApi {


//    @Autowired
//    private ICarAccessRecordService carAccessRecordService;

    @Autowired
    private ICarInfoCollectService carInfoCollectService;

    @Autowired
    private ICarAccessRecordHistoryService carAccessRecordHistoryService;

    @Autowired
    private ICarInfoCollectHistoryService carInfoCollectHistoryService;

    @Autowired
    private ICarStandardService carStandardService;

    @Autowired
    private ILedDisplayService ledDisplayService;

    @Autowired
    private ISysDepartService departService;

    @Autowired
    private IParkingLotService parkingLotService;

    @Autowired
    private ICarWhiteListService carWhiteListService;

    @Autowired
    private ICameraDeviceService cameraDeviceService;

    //默认一次性保存2000条
    private static Integer saveCount = 2000;

    //默认保存7天前的数据
    private static Integer saveBeforeDay = 7;

    //数据阀值八百万
    private static Integer thresholdCount = 8 * 100 * 100 * 100;

    /**
     * 检测白名单是否过期
     */
    @PostMapping("/detectionWhitelistExpired")
    public void detectionWhitelistExpired(){
        String date = DateUtils.formatDate(new Date(), "yyyy-MM-dd hh:mm:ss");
        List<CarWhiteList> whiteLists = carWhiteListService.detectionWhitelistExpired(date);
        if(whiteLists.size()>0){
            log.info("过期名单：{}",whiteLists);
            String ids = whiteLists.stream().map(i -> i.getId()).collect(Collectors.joining(","));
            Result<?> result = carWhiteListService.SwitchChangeClick(ids, "0", "0");
            log.info("白名单过期生效修改结果：{}",result.getResult());
        }
    }

    /**
     * 将数据保存到历史中
     * @param request     条数   天数   数据阀值
     * @return
     */
//    @PostMapping("/saveDataToHistory")
//    @Transactional
//    public void saveDataToHistory(HttpServletRequest request){
//        //获取参数
//        String attributeParams = HttpUtils.getAttributeParams(request);
//        String carAccessRecordTable = "car_access_record_history";
//        String carInfoCollectTable = "car_info_collect_history";
//
//        saveDataToCarAccessRecordHistory(attributeParams,carAccessRecordTable);
//        saveDataToCarInfoCollectHistory(attributeParams,carInfoCollectTable);
//    }

    /**
     * 新增carAccessRecord历史数据
     * @param attributeParams
     * @param tableName
     */
//    public void saveDataToCarAccessRecordHistory(String attributeParams , String tableName){
//        try{
//            //获取参数
//            if(!StringUtils.isEmpty(attributeParams)){
//                //获取时间
//                String[] arr = attributeParams.split(",");
//                int paramLength = arr.length;
//                if(paramLength < 4){
//                    if(paramLength == 1 ){
//                        saveCount = Integer.parseInt(arr[0]);
//                    }else if(paramLength == 2){
//                        saveCount = Integer.parseInt(arr[0]);
//                        saveBeforeDay = Integer.parseInt(arr[1]);
//                    }else if(paramLength == 3){
//                        saveCount = Integer.parseInt(arr[0]);
//                        saveBeforeDay = Integer.parseInt(arr[1]);
//                        thresholdCount = Integer.parseInt(arr[2]);
//                    }
//                }
//            }
//            //查询saveBeforeDay天前的数据量
//            //获取当前时间减去saveBeforeDay的时间
//            //将时分秒重置为0
//            DateTime dateTime = DateUtil.beginOfDay(DateUtil.offset(new Date(), DateField.DAY_OF_WEEK, -saveBeforeDay));
//            String strDate = DateUtil.format(dateTime, "yyyy-MM-dd HH:mm:ss");
//            //获取数据数量
//            int carAccessRecordDataCount = carAccessRecordService.getListCountBeforeDay(strDate);
//            if(carAccessRecordDataCount == 0){
//                log.info("没有需要新增的数据");
//                return;
//            }
//            //判断是否存在表
//            List<String> isExist = carAccessRecordService.hasTable(tableName);
//            if(isExist.isEmpty()){
//                log.error("需要新增的历史表不存在，表名为:[{}]",tableName);
//                Assert.isTrue(false,"需要新增的历史表不存在，表名为"+tableName+"");
//            }
//            tableName = isExist.get(isExist.size()-1);
//            if(StringUtils.isEmpty(tableName)){
//                log.error("需要新增的历史表不存在，表名为:[{}]",tableName);
//                Assert.isTrue(false,"需要新增的历史表不存在，表名为"+tableName+"");
//            }
//
//            //判断表的数据是否超出thresholdCount
//            int carAccessRecordHistoryDataCount = carAccessRecordHistoryService.getListCount(tableName);
//            if(carAccessRecordHistoryDataCount > thresholdCount){
//                //创建新表 获取表的最后一个字符加1
//                tableName ="car_access_record_history_" + (Integer.parseInt(tableName.substring(tableName.lastIndexOf("_")+1))+1)+"";
//                carAccessRecordHistoryService.createHistoryTable(tableName);
//                log.info("创建表，表名为:[{}]",tableName);
//            }
//            log.info("保存历史数据表名:[{}],每次保存条数:[{}],保存几天前的数据:[{}],日期为:[{}],数据数量为:[{}]",tableName,saveCount,saveBeforeDay,strDate,carAccessRecordDataCount);
//            //查询需要转移的数据
//            List<CarAccessRecord> listData = carAccessRecordService.getListBeforeDay(strDate);
//            int size = listData.size();
//            int number =  size / saveCount;
//            long startDate = System.currentTimeMillis();
//            boolean b = inseatDataToCarAccessRecordHistory(listData, number, tableName);
//            long endDate = System.currentTimeMillis();
//            if(!b){
//                Assert.isTrue(false,"历史数据新增失败");
//            }
//
//            //删除表数据
//            boolean b2 = carAccessRecordService.deleteToBeforeDay(strDate);
//            if(!b2){
//                Assert.isTrue(false,"原数据删除失败");
//            }
//            log.info("历史数据新增完毕表名为:[{}],新增数量:[{}],新增耗时:[{}]毫秒",tableName,size,(endDate-startDate));
//            return;
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//    }


    /**
     * 新增carInfoCollect历史数据
     * @param attributeParams
     * @param tableName
     */
    public void saveDataToCarInfoCollectHistory(String attributeParams , String tableName){
        try{
            //获取参数
            if(!StringUtils.isEmpty(attributeParams)){
                //获取时间
                String[] arr = attributeParams.split(",");
                int paramLength = arr.length;
                if(paramLength < 4){
                    if(paramLength == 1 ){
                        saveCount = Integer.parseInt(arr[0]);
                    }else if(paramLength == 2){
                        saveCount = Integer.parseInt(arr[0]);
                        saveBeforeDay = Integer.parseInt(arr[1]);
                    }else if(paramLength == 3){
                        saveCount = Integer.parseInt(arr[0]);
                        saveBeforeDay = Integer.parseInt(arr[1]);
                        thresholdCount = Integer.parseInt(arr[2]);
                    }
                }
            }
            //查询saveBeforeDay天前的数据量
            //获取当前时间减去saveBeforeDay的时间
            //将时分秒重置为0
            DateTime dateTime = DateUtil.beginOfDay(DateUtil.offset(new Date(), DateField.DAY_OF_WEEK, -saveBeforeDay));
            String strDate = DateUtil.format(dateTime, "yyyy-MM-dd HH:mm:ss");
            //获取数据数量
            int carInfoCollectDataCount = carInfoCollectService.getListCountBeforeDay(strDate);
            if(carInfoCollectDataCount == 0){
                log.info("没有需要新增的数据");
                return;
            }
            //判断是否存在表
            List<String> isExist = carInfoCollectService.hasTable(tableName);
            if(isExist.isEmpty()){
                log.error("需要新增的历史表不存在，表名为:[{}]",tableName);
                Assert.isTrue(false,"需要新增的历史表不存在，表名为"+tableName+"");
            }
            tableName = isExist.get(isExist.size()-1);
            if(StringUtils.isEmpty(tableName)){
                log.error("需要新增的历史表不存在，表名为:[{}]",tableName);
                Assert.isTrue(false,"需要新增的历史表不存在，表名为"+tableName+"");
            }

            //判断表的数据是否超出thresholdCount
            int carInfoCollectHistoryDataCount = carInfoCollectHistoryService.getListCount(tableName);
            if(carInfoCollectHistoryDataCount > thresholdCount){
                //创建新表 获取表的最后一个字符加1
                tableName ="car_info_collect_history_" + (Integer.parseInt(tableName.substring(tableName.lastIndexOf("_")+1))+1)+"";
                carInfoCollectHistoryService.createHistoryTable(tableName);
                log.info("创建表，表名为:[{}]",tableName);
            }
            log.info("保存历史数据表名:[{}],每次保存条数:[{}],保存几天前的数据:[{}],日期为:[{}],数据数量为:[{}]",tableName,saveCount,saveBeforeDay,strDate,carInfoCollectDataCount);
            //查询需要转移的数据
            List<CarInfoCollect> listData = carInfoCollectService.getListBeforeDay(strDate);
            int size = listData.size();
            int number =  size / saveCount;
            long startDate = System.currentTimeMillis();
            boolean b = insertDataToCarInfoCollectHistory(listData, number, tableName);
            long endDate = System.currentTimeMillis();

            if(!b){
                Assert.isTrue(false,"历史数据新增失败");
            }
            //删除表数据
            boolean b2 = carInfoCollectService.deleteToBeforeDay(strDate);
            if(!b2){
                Assert.isTrue(false,"原数据删除失败");
            }
            log.info("历史数据新增完毕表名为:[{}],新增数量:[{}],新增耗时:[{}]毫秒",tableName,size,(endDate-startDate));
            return;
        }catch (Exception e){
            e.printStackTrace();
        }
    }



//    @GetMapping("/testSaveData")
//    public Result<?> testSaveData(){
//        CarAccessRecord carAccessRecord = null;
//        List<CarAccessRecord> listData = new ArrayList<>();
//        for (int i = 0; i < 3; i++) {
//            carAccessRecord = new CarAccessRecord();
//            carAccessRecord.setId(UUID.randomUUID().toString().replace("-","").substring(0,16));
//            carAccessRecord.setCreateBy("lym");
//            carAccessRecord.setCreateTime(new Date());
//            carAccessRecord.setUpdateBy("lym");
//            carAccessRecord.setUpdateTime(new Date());
//            carAccessRecord.setCarInfoId(UUID.randomUUID().toString().replace("-","").substring(0,16));
//            carAccessRecord.setLicense(UUID.randomUUID().toString().replace("-","").substring(0,16));
//            carAccessRecord.setCarStatus("1");
//            carAccessRecord.setComeInTime(new Date());
//            carAccessRecord.setGetOutTime(new Date());
//            carAccessRecord.setAllLongTime("3");
//            listData.add(carAccessRecord);
//        }
//        int number = listData.size() / saveCount;
//        long l = System.currentTimeMillis();
//        boolean b = saveDataToHistory(listData, number, "car_access_record_history_1");
//        if(!b){
//            return Result.error();
//        }
//
//
//        System.out.println(System.currentTimeMillis() - l);
//        return Result.ok();
//    }


    /**
     * 保存数据到历史表中
     * @param list
     * @return
     */
//    private boolean inseatDataToCarAccessRecordHistory(List<CarAccessRecord> list , int number , String tableName){
//        List<CarAccessRecord> newList  = new ArrayList<>();
//        List<CarAccessRecord> remainderList = new ArrayList<>();
//        int count = 0;
//        for (CarAccessRecord carAccessRecord : list) {
//            newList.add(carAccessRecord);
//            if(newList.size() == saveCount){
//                //开始新增
//                int i = carAccessRecordHistoryService.saveDataToHistory(newList,tableName);
//                if(i > 0 ){
//                    count++;
//                    newList.clear();
//                    continue;
//                }
//            }
//            if(number == count){
//                remainderList.add(carAccessRecord);
//            }
//        }
//        if(remainderList.size() == 0){
//            return true;
//        }
//        int i = carAccessRecordHistoryService.saveDataToHistory(remainderList,tableName);
//        if(i == 0){
//            return false;
//        }
//        return true;
//    }



    /**
     * 保存数据到历史表中
     * @param list
     * @return
     */
    private boolean insertDataToCarInfoCollectHistory(List<CarInfoCollect> list , int number , String tableName){
        List<CarInfoCollect> newList  = new ArrayList<>();
        List<CarInfoCollect> remainderList = new ArrayList<>();
        int count = 0;
        for (CarInfoCollect carInfoCollect : list) {
            newList.add(carInfoCollect);
            if(newList.size() == saveCount){
                //开始新增
                int i = carInfoCollectHistoryService.saveDataToHistory(newList,tableName);
                if(i > 0 ){
                    count++;
                    newList.clear();
                    continue;
                }
            }
            if(number == count){
                remainderList.add(carInfoCollect);
            }
        }
        if(remainderList.size() == 0){
            return true;
        }
        int i = carInfoCollectHistoryService.saveDataToHistory(remainderList,tableName);
        if(i == 0){
            return false;
        }
        return true;
    }


    /**
     * 添加基本信息到缓存
     */
    @GetMapping("addRedisInfo")
    public Result<?> addRedisInfo(){
        carStandardService.addRedisInfo();
        return Result.OK();
    }

    /**
     * 晚上定时修改led屏幕的声音和亮度
     */
    @GetMapping("updateVoiceAndBrightness")
    public Result updateVoiceAndBrightness(){
        log.info("晚上定时修改led屏幕的声音和亮度=================================");
        ledDisplayService.updateVoiceAndBrightness();
        return Result.OK();
    }
    /**
     * 早上定时修改led屏幕的声音和亮度
     */
    @GetMapping("updateVoiceAndBrightnessMorning")
    public Result updateVoiceAndBrightnessMorning(){
        log.info("早上定时修改led屏幕的声音和亮度=================================");
        ledDisplayService.updateVoiceAndBrightnessMorning();
        return Result.OK();
    }

    /**
     * 定时检查设备状态：在线、离线
     * @return
     */
    @GetMapping("checkTheEquipmentStatus")
    public Result checkTheEquipmentStatus(){
        cameraDeviceService.checkTheEquipmentStatus();
        return Result.OK();
    }

    /**
     * 上传图片到阿里云
     */
    @PostMapping("imgOss")
    public void imgOss(){
        carStandardService.addRedisInfo();
    }

    /**
     * 检查月卡过期时间，还有两天过期时发送短信通知
     */
    @PostMapping("monthCardTimeout")
    public void monthCardTimeout(){
        //查询白名单和有效的月卡
        List<CarWhiteList> whiteLists = carWhiteListService.list(Wrappers.<CarWhiteList>lambdaQuery()
                .eq(CarWhiteList::getEnable, "1")
                .eq(CarWhiteList::getNeedAlarm, "0")
                .eq(CarWhiteList::getParkType, "1"));
        //筛选月卡过期时间小于两天的月卡
        List<CarWhiteList> lists = whiteLists.stream().filter(i -> {
            long overdueTime = DateUtils.addDateMinut(new Date(), 2 * 24 * 60).getTime();
            long nowTime = i.getOverdueTime().getTime();
            return overdueTime > nowTime && i.getOverdueTime().getTime() > new Date().getTime();
        }).collect(Collectors.toList());
        Map<String, String> departCollect = departService.list().stream().collect(Collectors.toMap(SysDepart::getId, SysDepart::getDepartName));
        Map<String, String> parkingLoCollect = parkingLotService.list().stream().collect(Collectors.toMap(ParkingLot::getId, ParkingLot::getName));
        //发送短信通知（连发两天，一天一次）
        for (CarWhiteList list : lists) {
            JSONObject obj = new JSONObject();
            obj.put("plate",list.getPlate());
            obj.put("company",departCollect.get(list.getCompanyId()));
            obj.put("parkingLot",parkingLoCollect.get(list.getParkingLotId()));
            obj.put("overdueTime",DateUtil.format(list.getOverdueTime(),"yyyy年MM月dd日 HH时mm分ss秒"));
//            log.info("月卡通知成功(未实际发送短信),{}",obj);
            try {
                log.info("月卡通知成功,{}",obj);
                DySmsHelper.sendSms(list.getPhone(),obj, DySmsEnum.CRAD_NOTICE_TEMPLATE_CODE);
            } catch (ClientException e) {
                log.warn("月卡通知失败,{}",obj);
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * 定时停车场接口系统心跳检测
     */
    @PostMapping("dgHeartbeat")
    public void dgHeartbeat(){
        DongGuanStaticHeartBeatTestInfo testInfo = new DongGuanStaticHeartBeatTestInfo();
        DgParkingApi dgParkingApi = new DgParkingApi();
        testInfo.setUpdateTime(new Date().getTime());
        testInfo.setRemark("数字城市平台");
        DongGuanStaticResponse beatTestInfo = dgParkingApi.dgHeartBeatTestInfo(testInfo);
        if(1==beatTestInfo.getState()){
            log.info("定时心跳检测成功,{}",beatTestInfo);
        }else {
            log.warn("定时心跳检测失败,{}",beatTestInfo);
        }
    }

}

package org.jeecg.modules.parking.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.parking.entity.CarTopUp;
import org.jeecg.modules.parking.service.ICarTopUpService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.parking.vo.CarTopUpVo;
import org.jeecg.modules.parking.vo.TopUpVo;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: car_top_up
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
@Api(tags="car_top_up")
@RestController
@RequestMapping("/parking/carTopUp")
@Slf4j
public class CarTopUpController extends JeecgController<CarTopUp, ICarTopUpService> {
	@Autowired
	private ICarTopUpService carTopUpService;


	 /**
	  * 充值/退费
	  * @param topUpVo
	  * @return
	  */
	 @AutoLog(value = "car_top_up-充值/退费")
	 @ApiOperation(value="car_top_up-充值/退费", notes="car_top_up-充值/退费")
	 @PutMapping(value = "/edit")
	 public Result<?> edit(@RequestBody TopUpVo topUpVo) {
		 carTopUpService.edit(topUpVo);
		 return Result.OK("编辑成功!");
	 }

	/**
	 * 分页列表查询
	 *
	 * @param carTopUp
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "car_top_up-分页列表查询")
	@ApiOperation(value="car_top_up-分页列表查询", notes="car_top_up-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarTopUpVo carTopUp,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<CarTopUpVo> queryWrapper = QueryGenerator.initQueryWrapper(carTopUp, req.getParameterMap());
		Page<CarTopUp> page = new Page<CarTopUp>(pageNo, pageSize);
		IPage<CarTopUpVo> pageList = carTopUpService.pageVo(page, queryWrapper,carTopUp);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carTopUp
	 * @return
	 */
	@AutoLog(value = "car_top_up-添加")
	@ApiOperation(value="car_top_up-添加", notes="car_top_up-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarTopUp carTopUp) {
		carTopUpService.save(carTopUp);
		return Result.OK("添加成功！");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_top_up-通过id删除")
	@ApiOperation(value="car_top_up-通过id删除", notes="car_top_up-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carTopUpService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "car_top_up-批量删除")
	@ApiOperation(value="car_top_up-批量删除", notes="car_top_up-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carTopUpService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_top_up-通过id查询")
	@ApiOperation(value="car_top_up-通过id查询", notes="car_top_up-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarTopUp carTopUp = carTopUpService.getById(id);
		if(carTopUp==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carTopUp);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carTopUp
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarTopUp carTopUp) {
        return super.exportXls(request, carTopUp, CarTopUp.class, "car_top_up");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarTopUp.class);
    }

}

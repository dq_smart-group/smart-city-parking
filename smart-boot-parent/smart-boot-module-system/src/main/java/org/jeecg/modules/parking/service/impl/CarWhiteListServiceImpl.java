package org.jeecg.modules.parking.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;
import org.apache.shiro.SecurityUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CityRedisConstant;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.authentication.utils.MapBuild;
import org.jeecg.modules.authentication.utils.QuartzRequestUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.entity.CarRenewalHistory;
import org.jeecg.modules.parking.entity.CarWhiteList;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.mapper.CarWhiteListMapper;
import org.jeecg.modules.parking.mqtt.MqttPushClient;
import org.jeecg.modules.parking.query.CarWhiteListQuery;
import org.jeecg.modules.parking.service.ICarRenewalHistoryService;
import org.jeecg.modules.parking.service.ICarWhiteListService;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.vo.CarWhiteListVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecg.modules.utils.RedisGetUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 车牌白名单表
 * @Author: jeecg-boot
 * @Date: 2021-12-02
 * @Version: V1.0
 */
@Service
@Slf4j
public class CarWhiteListServiceImpl extends ServiceImpl<CarWhiteListMapper, CarWhiteList> implements ICarWhiteListService {

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private  ICameraDeviceService cameraDeviceService;

    @Autowired
    private CarRenewalHistoryServiceImpl carRenewalHistoryService;

    @Autowired
    private ICarRenewalHistoryService historyService;

    @Value("${jeecg.path.upload}")
    private String upLoadPath;

    @Autowired
    private ISysDepartService departService;

    @Autowired
    private IParkingLotService parkingLotService;

    @Override
    public Page<CarWhiteListVo> selectListPage(Page page, CarWhiteListQuery carWhiteListQuery) {
        List<CarWhiteListVo> list = this.baseMapper.selectListPage(page, carWhiteListQuery);
        return page.setRecords(list);
    }

    /**
     * 批量修改名单状态（黑白名单）
     *
     * @param ids
     * @param value
     * @return
     */
    @Override
    public Result<?> needAlarm(String ids, String value) {
        CarWhiteList whiteList = null;
        if (ids.indexOf(",") != -1) {
            List<String> idList = Arrays.stream(ids.split(",")).collect(Collectors.toList());
            String[] id = idList.toArray(new String[idList.size()]);
            if (id != null && id.length > 0) {
                whiteList = new CarWhiteList();
                whiteList.setNeedAlarm(value);
                boolean b = this.update(whiteList, new LambdaQueryWrapper<CarWhiteList>().in(CarWhiteList::getId, id));
                if (!b) {
                    Assert.isTrue(false, "修改异常");
                }
            }
            updateRedisData(ids);
            return Result.ok("操作成功");
        } else {
            whiteList = new CarWhiteList();
            whiteList.setNeedAlarm(value);
            whiteList.setId(ids);
            boolean b = this.updateById(whiteList);
            if (!b) {
                Assert.isTrue(false, "修改异常");
            }
            updateRedisData(ids);
            return Result.ok("操作成功");
        }

    }

    /**
     * 根据车牌查找信息
     *
     * @param chepai
     * @return
     */
    @Override
    public CarWhiteList getByChePai(String chepai) {
        CarWhiteList carWhiteLists = this.baseMapper.getByChePai(chepai);
        return carWhiteLists;
    }

    /**
     * 黑白名单修改
     * @param carWhiteList
     * @return
     */
    @Override
    public Result<?> updateCarWhiteListById(CarWhiteList carWhiteList) {
        LambdaQueryWrapper<CarWhiteList> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(CarWhiteList::getId,carWhiteList.getId());
        wrapper.eq(CarWhiteList::getCompanyId, carWhiteList.getCompanyId());
        wrapper.eq(CarWhiteList::getParkingLotId, carWhiteList.getParkingLotId());
        wrapper.eq(CarWhiteList::getNeedAlarm, carWhiteList.getNeedAlarm());
        wrapper.eq(CarWhiteList::getParkType, carWhiteList.getParkType());
        wrapper.eq(CarWhiteList::getPlate, carWhiteList.getPlate());
        List<CarWhiteList> carWhiteLists = this.list(wrapper);
        if (carWhiteLists.size() > 0) {
            return  Result.error("该名单已存在");
        } else {
            this.updateById(carWhiteList);
            redisUtil.set(ParkingRedisConstant.PARKING_WHITE_LIST+carWhiteList.getParkingLotId()+":"+carWhiteList.getPlate(),carWhiteList);
            return   Result.ok("修改成功");
        }
    }

    @Override
    public List<CarWhiteList> selectList() {
        List<CarWhiteList> carWhiteLists = this.baseMapper.selectList(null);
        return carWhiteLists;
    }

    /**
     * 是否生效

     * @param ids
     * @param value
     * @param needAlarm
     * @return
     */
    @Override
    public Result<?> SwitchChangeClick(String ids, String value, String needAlarm) {
        CarWhiteList carWhiteList = null;

        if(ids.indexOf(",") != -1){
            String[] id = ids.split(",");
            carWhiteList = new CarWhiteList();
            carWhiteList.setEnable(value);
            carWhiteList.setNeedAlarm(needAlarm);
            boolean b = this.update(carWhiteList,
                    new LambdaQueryWrapper<CarWhiteList>()
                            .in(CarWhiteList::getId, id));
            if(!b){
                Assert.isTrue(false,"修改异常");
            }

            for (String s : id) {
                CarWhiteList carWhiteList1 = baseMapper.selectById(s);
                Assert.isTrue(StringUtils.isNotBlank(carWhiteList1.getPlate()), "参数异常");
                Assert.isTrue(carWhiteList1.getUploadDevice()!=null, "参数异常");
                Assert.isTrue(carWhiteList1.getOverdueTime()!=null, "参数异常");
                Assert.isTrue(carWhiteList1.getEnableTime()!=null, "参数异常");
                //调用黑白名单上传云或者删除云
                uploadTheCloud(
                        carWhiteList1.getEnableTime(),
                        carWhiteList1.getOverdueTime(),
                        carWhiteList1.getParkingLotId(),
                        carWhiteList1.getUploadDevice(),
                        carWhiteList1.getPlate(),
                        carWhiteList1.getEnable(),
                        carWhiteList1.getNeedAlarm()
                );

            }

            updateRedisData(ids);
            return Result.OK("操作成功");
        }else{
            carWhiteList = new CarWhiteList();
            carWhiteList.setId(ids);
            carWhiteList.setEnable(value);
            carWhiteList.setNeedAlarm(needAlarm);
            boolean b = this.updateById(carWhiteList);
            if(!b){
                Assert.isTrue(false,"修改异常");
            }
            updateRedisData(ids);

            CarWhiteList carWhiteList1 = baseMapper.selectById(ids);
            Assert.isTrue(StringUtils.isNotBlank(carWhiteList1.getPlate()), "参数异常");
            Assert.isTrue(carWhiteList1.getUploadDevice()!=null, "参数异常");
            Assert.isTrue(carWhiteList1.getOverdueTime()!=null, "参数异常");
            Assert.isTrue(carWhiteList1.getEnableTime()!=null, "参数异常");
            //调用黑白名单上传云或者删除云
            uploadTheCloud(
                    carWhiteList1.getEnableTime(),
                    carWhiteList1.getOverdueTime(),
                    carWhiteList1.getParkingLotId(),
                    carWhiteList1.getUploadDevice(),
                    carWhiteList1.getPlate(),
                    carWhiteList1.getEnable(),
                    carWhiteList1.getNeedAlarm()
            );

            return Result.OK("操作成功");
        }

    }

    //修改白名单在redis里面的数据
    public void updateRedisData(String ids){
        String[] id = ids.split(",");
        for (String s : id) {
            CarWhiteList carWhiteList = this.baseMapper.selectById(s);
            redisUtil.set(ParkingRedisConstant.PARKING_WHITE_LIST+carWhiteList.getParkingLotId()+":"+carWhiteList.getPlate(),carWhiteList);
        }
    }

    /**
     * 是否上传设备
     * @param ids
     * @param value
     * @param needAlarm
     * @return
     */
    @Override
    public Result<?> SwitchChangeShow(String ids, boolean value, String needAlarm) {

        CarWhiteList carWhiteList = null;
        if(ids.indexOf(",") != -1){
            String[] id = ids.split(",");
            carWhiteList = new CarWhiteList();
            carWhiteList.setUploadDevice(value);
            carWhiteList.setNeedAlarm(needAlarm);
            boolean b = this.update(carWhiteList,
                    new LambdaQueryWrapper<CarWhiteList>()
                            .in(CarWhiteList::getId, id));
            if(!b){
                Assert.isTrue(false,"修改异常");
            }
            updateRedisData(ids);

            for (String s : id) {
                CarWhiteList carWhiteList1 = baseMapper.selectById(s);
                Assert.isTrue(StringUtils.isNotBlank(carWhiteList1.getPlate()), "参数异常");
                Assert.isTrue(carWhiteList1.getUploadDevice()!=null, "参数异常");
                Assert.isTrue(carWhiteList1.getOverdueTime()!=null, "参数异常");
                Assert.isTrue(carWhiteList1.getEnableTime()!=null, "参数异常");
                //调用黑白名单上传云或者删除云
                uploadTheCloud(
                        carWhiteList1.getEnableTime(),
                        carWhiteList1.getOverdueTime(),
                        carWhiteList1.getParkingLotId(),
                        carWhiteList1.getUploadDevice(),
                        carWhiteList1.getPlate(),
                        carWhiteList1.getEnable(),
                        carWhiteList1.getNeedAlarm()
                );
            }


            return Result.OK("操作成功");
        }else{
            carWhiteList = new CarWhiteList();
            carWhiteList.setId(ids);
            carWhiteList.setUploadDevice(value);
            carWhiteList.setNeedAlarm(needAlarm);
            boolean b = this.updateById(carWhiteList);
            if(!b){
                Assert.isTrue(false,"修改异常");
            }
            updateRedisData(ids);

            CarWhiteList carWhiteList1 = baseMapper.selectById(ids);
            Assert.isTrue(StringUtils.isNotBlank(carWhiteList1.getPlate()), "参数异常");
            Assert.isTrue(carWhiteList1.getUploadDevice()!=null, "参数异常");
            Assert.isTrue(carWhiteList1.getOverdueTime()!=null, "参数异常");
            Assert.isTrue(carWhiteList1.getEnableTime()!=null, "参数异常");

            //调用黑白名单上传云或者删除云
            uploadTheCloud(
                    carWhiteList1.getEnableTime(),
                    carWhiteList1.getOverdueTime(),
                    carWhiteList1.getParkingLotId(),
                    carWhiteList1.getUploadDevice(),
                    carWhiteList1.getPlate(),
                    carWhiteList1.getEnable(),
                    carWhiteList1.getNeedAlarm()
            );

            return Result.OK("操作成功");
        }
    }

    @Override
    public Result<?> RenewalChangeShow(String id, Boolean value) {
        CarWhiteList whiteList = getById(id);
        whiteList.setOnlineRenewal(value);
        updateById(whiteList);
        return Result.OK();
    }

    @Transactional
    @Override
    public Result<?> Renewal(CarRenewalHistory carRenewalHistory) {
        //更新过期时间
        CarWhiteList whiteList = getById(carRenewalHistory.getCarWhiteId());
        Assert.isTrue(null!=whiteList,"未找到对应月卡信息");
        redisUtil.set(ParkingRedisConstant.PARKING_WHITE_LIST+whiteList.getParkingLotId()+":"+whiteList.getPlate(),whiteList);
        if(whiteList.getOverdueTime().getTime() <= new Date().getTime()){
            whiteList.setEnableTime(new Date());
            whiteList.setOverdueTime(new Date());
        }
        Date minut = DateUtils.addDateMinut(whiteList.getOverdueTime(), carRenewalHistory.getRenewalDuration() * 30 * 24 * 60);
        whiteList.setOverdueTime(minut);
        whiteList.setEnable("1");
        updateById(whiteList);
        //生成记录表
        carRenewalHistory.setPastTime(whiteList.getOverdueTime());
        carRenewalHistoryService.save(carRenewalHistory);
        return Result.OK();
    }

    @Override
    public Result<?> getCarWhiteByPlate(String plate, String openid) {
        if(null==plate&&null!=openid){
            return Result.OK(this.baseMapper.selectListByOpenid(openid));
        }else {
            Page<CarWhiteList> page = new Page<CarWhiteList>(1, 999999999);
            CarWhiteListQuery carWhiteListQuery = new CarWhiteListQuery();
            carWhiteListQuery.setNeedAlarm("0");
            carWhiteListQuery.setPlate(plate);
            return Result.OK(this.baseMapper.selectListPage(page, carWhiteListQuery));
        }
    }

    @Override
    public void addRenrwal(CarWhiteList whiteList, Result<?> result) {
        whiteList.setNeedAlarm("0");
        whiteList.setEnable("1");
        whiteList.setOnlineRenewal(true);
        whiteList.setEnableTime(new Date());
        Date minut = DateUtils.addDateMinut(new Date(),  30 * 24 * 60);
        whiteList.setOverdueTime(minut);
        whiteList.setParkType("1");
        whiteList.setUploadDevice(false);
        String[] split = whiteList.getCompanyId().split("/");
        whiteList.setCompanyId(split[0]);
        whiteList.setParkingLotId(split[1]);
        CarWhiteList list = getOne(Wrappers.<CarWhiteList>lambdaQuery().eq(CarWhiteList::getPlate, whiteList.getPlate()).eq(CarWhiteList::getParkingLotId, whiteList.getParkingLotId()));
        Assert.isTrue(null==list,"当前车场的月卡记录已经存在，如需代充，请先绑定该车牌");
        save(whiteList);
        //生成续费记录
        CarRenewalHistory carRenewalHistory = new CarRenewalHistory();
        carRenewalHistory.setCarWhiteId(whiteList.getId());
        carRenewalHistory.setRenewalDuration(1);
        carRenewalHistory.setAmount(String.valueOf(result.getResult()));
        historyService.save(carRenewalHistory);
    }

    /**
     * 判断白名单是否有效和是否过期
     * @return
     */
    @Override
    public Boolean ifFag(CarWhiteList byChePai){
        if(byChePai!=null){
            if(byChePai.getEnable().equals("1")){//白名单有效
                if(!byChePai.getNeedAlarm().equals("1")){
                    //到期时间
                    Date overdueTime = byChePai.getOverdueTime();
                    SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat sdf1=new SimpleDateFormat("yyyy年MM月dd日HH时mm分");
                    //没到期
                    if(!DateUtils.dateCompare(sdf.format(overdueTime))){
                        return true;
                    }
                }
            }}
        return false;
    }

    @Override
    public Boolean ifBlack(CarWhiteList byChePai) {
        if(byChePai!=null){
            if(byChePai.getEnable().equals("1")){//白名单有效
                if(byChePai.getNeedAlarm().equals("1")){
                    return true;
                }
            }}
        return false;
    }

    @Transactional
    @Override
    public Result<?> generateRenewalOrder(String id, String openid, Integer renewalTime, BigDecimal amount, String plateTypeId, String companyId, String companyName, String parkingLotName, String plate, String phone) {
        if(StringUtils.isBlank(id)&& StringUtils.isNotBlank(plateTypeId)){
            //充值
            CarWhiteList whiteList = new CarWhiteList();
            whiteList.setPlateTypeId(plateTypeId);
            whiteList.setNeedAlarm("0");
            whiteList.setEnable("0");
            whiteList.setOnlineRenewal(true);
            whiteList.setEnableTime(new Date());
            Date minut = DateUtils.addDateMinut(new Date(),  30 * 24 * 60);
            whiteList.setOverdueTime(minut);
            whiteList.setParkType("1");
            whiteList.setPlate(plate);
            whiteList.setUploadDevice(false);
            whiteList.setPhone(phone);
            String[] split = companyId.split("/");
            whiteList.setCompanyId(split[0]);
            whiteList.setParkingLotId(split[1]);
            CarWhiteList list = getOne(Wrappers.<CarWhiteList>lambdaQuery().eq(CarWhiteList::getPlate, whiteList.getPlate()).eq(CarWhiteList::getParkingLotId, whiteList.getParkingLotId()));
            Assert.isTrue(null==list,"当前车场的月卡记录已经存在或者被加入黑名单");
            save(whiteList);
            id = whiteList.getId();
        }
        try {
            //查询月卡是否存在
            CarWhiteList whiteList = baseMapper.selectById(id);
            Assert.isTrue(null!=whiteList,"月卡不存在！");
            //生成月卡支付订单
            Result<?> result = generateOrders(whiteList,openid,renewalTime,amount,companyName,parkingLotName,plate);
            if(!result.isSuccess()){
                if(StringUtils.isNotBlank(plateTypeId)){
                    removeById(id);
                }
                return Result.error(result.getMessage(),result.getCode());
            }
            return result;
        } catch (Exception e){
            if(StringUtils.isNotBlank(plateTypeId)){
                removeById(id);
            }
            e.printStackTrace();
            return Result.error();
        }
    }

    @Override
    public String getPhone(String openid) {
        return baseMapper.getPhone(openid);
    }

    @Override
    public ModelAndView exportXls(HttpServletRequest request, CarWhiteListQuery object, Class<CarWhiteList> clazz, String title) {
        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();

        List<CarWhiteList> exportList = null;

        Page<CarWhiteList> page = new Page<CarWhiteList>(1, Integer.MAX_VALUE);
        object.setNeedAlarm("0");
        List<CarWhiteListVo> pageList = this.baseMapper.selectListPage(page, object);
        List<CarWhiteList> carWhiteLists = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            carWhiteLists.addAll(pageList);
        }
//        carWhiteLists.addAll(pageList);

        // 过滤选中数据
        String selections = request.getParameter("selections");
        if (oConvertUtils.isNotEmpty(selections)) {
            List<String> selectionList = Arrays.asList(selections.split(","));
            exportList = carWhiteLists.stream().filter(item -> {
                try {
                    return selectionList.contains(PropertyUtils.getProperty(item, "id").toString());
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }).collect(Collectors.toList());
        } else {
            exportList = carWhiteLists;
        }

        // Step.3 AutoPoi 导出Excel
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, title); //此处设置的filename无效 ,前端会重更新设置一下
        mv.addObject(NormalExcelConstants.CLASS, clazz);
        //update-begin--Author:liusq  Date:20210126 for：图片导出报错，ImageBasePath未设置--------------------
        ExportParams exportParams=new ExportParams(title + "报表", "导出人:" + sysUser.getRealname(), title);
        exportParams.setImageBasePath(upLoadPath);
        //update-end--Author:liusq  Date:20210126 for：图片导出报错，ImageBasePath未设置----------------------
        mv.addObject(NormalExcelConstants.PARAMS,exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, exportList);
        return mv;
    }

    @Override
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response, Class<CarWhiteList> clazz) {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        for (Map.Entry<String, MultipartFile> entity : fileMap.entrySet()) {
            MultipartFile file = entity.getValue();// 获取上传文件对象
            ImportParams params = new ImportParams();
            params.setTitleRows(2);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try {
                long start = System.currentTimeMillis();
                List<CarWhiteList> list = ExcelImportUtil.importExcel(file.getInputStream(), clazz, params);
                //查询所有公司
                Map<String, String> depCot = departService.list().stream().collect(Collectors.toMap(SysDepart::getOrgCode,SysDepart::getId));
                //查询所有车场
                Map<String, String> pklCot = parkingLotService.list().stream().collect(Collectors.toMap(ParkingLot::getCarIdentifier,ParkingLot::getId));
                List<CarWhiteList> collect = list.stream().map(i -> {
                    i.setOnlineRenewal(true);
                    i.setNeedAlarm("0");
                    i.setEnable("1");
                    i.setParkType("1");
                    i.setUploadDevice(true);
                    i.setCompanyId(depCot.get(i.getOrgCode()));
                    i.setParkingLotId(pklCot.get(i.getCarIdentifier()));
                    return i;
                }).filter(i-> StringUtils.isNotBlank(i.getCompanyId())&&StringUtils.isNotBlank(i.getParkingLotId())).collect(Collectors.toList());
                //update-begin-author:taoyan date:20190528 for:批量插入数据
                //判断数据是否重复------
                List<CarWhiteList> carWhiteLists = list();
                List<CarWhiteList> lists = carWhiteLists.stream().map(i -> {
                    CarWhiteList whiteList = new CarWhiteList();
                    whiteList.setParkingLotId(i.getParkingLotId());
                    whiteList.setCompanyId(i.getCompanyId());
                    whiteList.setPlate(i.getPlate());
                    return whiteList;
                }).collect(Collectors.toList());
                List<CarWhiteList> whiteLists = collect.stream().filter(i ->{
                    CarWhiteList whiteList = new CarWhiteList();
                    whiteList.setParkingLotId(i.getParkingLotId());
                    whiteList.setCompanyId(i.getCompanyId());
                    whiteList.setPlate(i.getPlate());
                    return !lists.contains(whiteList);
                }).collect(Collectors.toList());
                //------判断数据是否重复
                saveBatch(whiteLists);
                //400条 saveBatch消耗时间1592毫秒  循环插入消耗时间1947毫秒
                //1200条  saveBatch消耗时间3687毫秒 循环插入消耗时间5212毫秒
                log.info("消耗时间" + (System.currentTimeMillis() - start) + "毫秒");
                //update-end-author:taoyan date:20190528 for:批量插入数据
                int nu = list.size() - collect.size();
                int cf= collect.size() - whiteLists.size();
                return Result.ok("文件导入成功！数据行数：" + whiteLists.size() + "；排除" + nu +"条不符合标准"+"和"+cf+"条重复数据");
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            } finally {
                try {
                    file.getInputStream().close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return Result.error("文件导入失败！");
    }

    @Override
    public List<CarWhiteList> detectionWhitelistExpired(String date) {
        return this.baseMapper.detectionWhitelistExpired(date);
    }


    /**
     * 生成月卡支付订单
     *
     * @param whiteList
     * @param openid
     * @param renewalTime
     * @param amount
     * @param companyName
     * @param parkingLotName
     * @param plate
     * @return
     */
    private Result<?> generateOrders(CarWhiteList whiteList, String openid, Integer renewalTime, BigDecimal amount, String companyName, String parkingLotName, String plate) {
        Map<String, Object> map = MapBuild.hashMapBuild();
        map.put("whiteId",whiteList.getId());
        map.put("companyId",whiteList.getCompanyId());
        map.put("companyName",companyName);
        map.put("parkingLotId",whiteList.getParkingLotId());
        map.put("parkingLotName",parkingLotName);
        map.put("renewalTime",renewalTime);
        map.put("renewalAmount",amount);
        map.put("openid",openid);
        map.put("plate",plate);
        //TODO 根据公司获取子商户号是否为空
        //根据公司获取子商户号
        SysDepart sysDepart = (SysDepart) redisUtil.get(CityRedisConstant.SYS_DEPART + whiteList.getCompanyId());
        map.put("subMchid",sysDepart.getSubMchid());
        Result<?> result =  QuartzRequestUtil.quartzPostRequest(RedisGetUtil.getPayPath(redisUtil)+"/parking/api/wxRenewalPayJsapi", map);
        return com.alibaba.fastjson.JSONObject.parseObject(result.getMessage(), Result.class);
    }

    /**
     * 黑白名单新增
     * @param carWhiteList
     * @return
     */
    @Override
    public Result<?> addCarWhiteListsave(CarWhiteList carWhiteList) {

        Assert.isTrue(StringUtils.isNotBlank(carWhiteList.getPlate()), "参数异常");
        Assert.isTrue(carWhiteList.getUploadDevice()!=null, "参数异常");
        Assert.isTrue(carWhiteList.getOverdueTime()!=null, "参数异常");
        Assert.isTrue(carWhiteList.getEnableTime()!=null, "参数异常");

        if (carWhiteList.getOverdueTime().getTime() < carWhiteList.getEnableTime().getTime()) {
            return Result.error("过期时间要大于生效时间");
        }

        //手动设置死一个停车类型
        carWhiteList.setParkType("1");
        carWhiteList.setOnlineRenewal(true);
        LambdaQueryWrapper<CarWhiteList> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CarWhiteList::getParkingLotId, carWhiteList.getParkingLotId());
        wrapper.eq(CarWhiteList::getNeedAlarm, carWhiteList.getNeedAlarm());
        wrapper.eq(CarWhiteList::getPlate, carWhiteList.getPlate());
        System.out.println(carWhiteList.getNeedAlarm());
        String needAlarm = carWhiteList.getNeedAlarm();
        //新增白名单判断黑白名单是否有这条数据
        if ("0".equals(needAlarm)){
            List<CarWhiteList> carWhiteLists = this.list(wrapper);
            if (carWhiteLists.size() > 0) {
                return  Result.error("该白名单已存在");
            } else {
                LambdaQueryWrapper<CarWhiteList> wrapper1 = new LambdaQueryWrapper<>();
                wrapper1.eq(CarWhiteList::getCompanyId, carWhiteList.getCompanyId());
                wrapper1.eq(CarWhiteList::getParkingLotId, carWhiteList.getParkingLotId());
                wrapper1.eq(CarWhiteList::getPlate, carWhiteList.getPlate());
                wrapper1.eq(CarWhiteList::getNeedAlarm, "1");
                List<CarWhiteList> selectBlacklist = this.list(wrapper1);
                if (selectBlacklist.size()>0){
                    return  Result.error("该名单已存在黑名单，修改回白名单即可。");
                }else {

                    //调用黑白名单上传云或者删除云
                    uploadTheCloud(
                            carWhiteList.getEnableTime(),
                            carWhiteList.getOverdueTime(),
                            carWhiteList.getParkingLotId(),
                            carWhiteList.getUploadDevice(),
                            carWhiteList.getPlate(),
                            carWhiteList.getEnable(),
                            carWhiteList.getNeedAlarm()
                    );

                    this.save(carWhiteList);
                    redisUtil.set(ParkingRedisConstant.PARKING_WHITE_LIST+carWhiteList.getParkingLotId()+":"+carWhiteList.getPlate(),carWhiteList);
                    return   Result.ok("新增成功");
                }
            }
        } else if ("1".equals(needAlarm)) {
            //新增黑名单判断黑白名单是否有这条数据
            List<CarWhiteList> carBlacklist = this.list(wrapper);
            if (carBlacklist.size() > 0) {
                return Result.error("该黑名单已存在");
            } else {

                LambdaQueryWrapper<CarWhiteList> wrapper1 = new LambdaQueryWrapper<>();
                wrapper1.eq(CarWhiteList::getParkingLotId, carWhiteList.getParkingLotId());
                wrapper1.eq(CarWhiteList::getPlate, carWhiteList.getPlate());
                wrapper1.eq(CarWhiteList::getNeedAlarm, "0");
                List<CarWhiteList> selectWhiteLists = this.list(wrapper1);
                if (selectWhiteLists.size() > 0) {
                    return Result.error("该名单已存在白名单，修改回黑名单即可。");
                } else {

                    //调用黑白名单上传云或者删除云
                    uploadTheCloud(
                            carWhiteList.getEnableTime(),
                            carWhiteList.getOverdueTime(),
                            carWhiteList.getParkingLotId(),
                            carWhiteList.getUploadDevice(),
                            carWhiteList.getPlate(),
                            carWhiteList.getEnable(),
                            carWhiteList.getNeedAlarm()
                    );
                    this.save(carWhiteList);
                    redisUtil.set(ParkingRedisConstant.PARKING_WHITE_LIST+carWhiteList.getParkingLotId()+":"+carWhiteList.getPlate(),carWhiteList);
                    return   Result.ok("新增成功");
                }
            }
        }else {//特殊情况，没有传黑白名单状态的过来的处理
            List<CarWhiteList> carWhiteLists = this.list(wrapper);
            if (carWhiteLists.size() > 0) {
                return  Result.error("该名单已存在");
            } else {
                this.save(carWhiteList);
                redisUtil.set(ParkingRedisConstant.PARKING_WHITE_LIST+carWhiteList.getParkingLotId()+":"+carWhiteList.getPlate(),carWhiteList);
                return   Result.ok("新增成功");
            }
        }

    }



    /**
     *
     * @param EnableTime    开始时间
     * @param OverdueTime   过期时间
     * @param ParkingLotId  车场id
     * @param UploadDevice  是否上传
     * @param Plate         车牌
     * @param Enable        是否有效
     * @param NeedAlarm     黑白名单
     */
    public  void uploadTheCloud(Date EnableTime,Date OverdueTime,
                                String ParkingLotId,Boolean UploadDevice,
                                String Plate,String Enable,String NeedAlarm){

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String EnableTime1 = simpleDateFormat.format(EnableTime);
        String OverdueTime1 = simpleDateFormat.format(OverdueTime);

        LambdaQueryWrapper<CameraDevice> lambdaQueryWrapper = new LambdaQueryWrapper();
        lambdaQueryWrapper.eq(CameraDevice::getParkingLotId, ParkingLotId);
        List<CameraDevice> list = cameraDeviceService.list(lambdaQueryWrapper);
        //判断是否上传设备白名单
        if (UploadDevice.equals(true)) {

            if (list.size() > 0) {
                for (CameraDevice cameraDevice : list) {
                    MqttPushClient.pushWhiteorBlack(
                            cameraDevice.getSerialno(),
                            0,
                            Plate,
                            Integer.parseInt(Enable),
                            Integer.parseInt(NeedAlarm),
                            EnableTime1,
                            OverdueTime1
                    );
                }
            }
        }
        //判断是否上传设备白名单
        if (UploadDevice.equals(false)) {
            if (list.size() > 0) {
                for (CameraDevice cameraDevice : list) {
                    MqttPushClient.pushWhiteorBlack(
                            cameraDevice.getSerialno(),
                            1,
                               Plate,
                            Integer.parseInt(Enable),
                            Integer.parseInt(NeedAlarm),
                            EnableTime1,
                            OverdueTime1
                    );
                }
            }
        }


    }


}

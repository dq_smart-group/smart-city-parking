package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarTimeSlot;

import java.util.List;

@Data
public class CarTimeSlotVo extends CarTimeSlot {
    private String deviceName;
    private String deviceUse;
    private String departName;
    private String parkingLotId;
    private String parkingName;
    private String deviceGroupId;
    private List<String> executionTimes;

    private String carSentryBoxId;
}

package org.jeecg.modules.parking.api;

import me.zhyd.oauth.utils.StringUtils;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Crc16_test {


    /**
     * 一个字节包含位的数量 8
     */
    private static final int BITS_OF_BYTE = 8;
    /**
     * 多项式
     */
    private static final int POLYNOMIAL = 0xA001;
    /**
     * 初始值
     */
    private static final int INITIAL_VALUE = 0xFFFF;

    /**
     * CRC16 编码
     *
     * @param bytes 编码内容
     * @return 编码结果
     */
    public static int crc16(int[] bytes) {
        int res = INITIAL_VALUE;
        for (int data : bytes) {
            res = res ^ data;
            for (int i = 0; i < BITS_OF_BYTE; i++) {
                res = (res & 0x0001) == 1 ? (res >> 1) ^ POLYNOMIAL : res >> 1;
            }
        }
        return revert(res);
    }

    /**
     * 翻转16位的高八位和低八位字节
     *
     * @param src 翻转数字
     * @return 翻转结果
     */
    private static int revert(int src) {
        int lowByte = (src & 0xFF00) >> 8;
        int highByte = (src & 0x00FF) << 8;
        return lowByte | highByte;
    }

    public static void main(String[] args) throws UnsupportedEncodingException {

            final int[] data = {
                    0x01,
                    0x10,
                    0x00,
                    0x0C,
                    0x00,
                    0x02,
                    0x04,
                    0x00,
                    0x00,
                    0x00,
                    0x00
            };

      String data1= "00 64 FF FF 30 0B 01 C7 EB BD BB B7 D1 31 30 D4 AA";
        final int[] data2= {
                0x00 ,0x64 ,0xFF, 0xFF ,0x05 ,0x08, 0xE5 ,0x07 ,0x0c, 0x1b, 0x02 ,0x10, 0x36, 0x06
        };
        int[] one = Crc16_test.one(data1);

        final int res = Crc16_test.crc16(one);
        final String hex = Integer.toHexString(res);
       System.out.println(hex);

        String a="扑街";
        try {
            byte[] b=a.getBytes("GB2312");
            System.out.println(bytesToHexFun1(b));

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    //将byte数组转成16进制字符串
    public static String bytesToHexFun1(byte[] bytes) {
        char[] HEX_CHAR = {'0', '1', '2', '3', '4', '5',
                '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        // 一个byte为8位，可用两个十六进制位标识
        char[] buf = new char[bytes.length * 2];
        int a = 0;
        int index = 0;
        for(byte b : bytes) { // 使用除与取余进行转换
            if(b < 0) {
                a = 256 + b;
            } else {
                a = b;
            }
            buf[index++] = HEX_CHAR[a / 16];
            buf[index++] = HEX_CHAR[a % 16];
        }

        String d = "";
        String s = new String(buf);
        for (int i = 0; i < s.length(); i=i+2) {
            d+=" "+s.substring(i,i+2);
        }
        return d.trim();
    }

    /**
     * 获取内容长度16进制
     * @param bytes
     */
    public static String getLen(byte[] bytes) {
        char[] HEX_CHAR = {'0', '1', '2', '3', '4', '5',
                '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        // 一个byte为8位，可用两个十六进制位标识
        char[] buf = new char[bytes.length * 2];
        int a = 0;
        int index = 0;
        for(byte b : bytes) { // 使用除与取余进行转换
            if(b < 0) {
                a = 256 + b;
            } else {
                a = b;
            }
            buf[index++] = HEX_CHAR[a / 16];
            buf[index++] = HEX_CHAR[a % 16];
        }
        String s = new String(buf);
        int i = s.length() / 2;
        String s1 = Integer.toHexString(i);
        return s1;
    }


    public static   int[] one(String one){
        String[] s = one.trim().split(" ");
        int[] array = new int[s.length];
        for (int i = 0; i < s.length; i++) {
            array[i] = Integer.parseInt(s[i], 16);
        }
        return array;

    }







}

package org.jeecg.modules.parking.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.lettuce.core.dynamic.annotation.Param;
import org.jeecg.modules.parking.entity.CarStandard;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.query.CarStandardQuery;
import org.jeecg.modules.parking.vo.CarStandardVo;

import java.util.List;

/**
 * @Description: 收费标准表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
public interface CarStandardMapper extends BaseMapper<CarStandard> {

    List<CarStandardVo> pageList(Page<CarStandardVo> page, @Param("q") CarStandardQuery q);


}

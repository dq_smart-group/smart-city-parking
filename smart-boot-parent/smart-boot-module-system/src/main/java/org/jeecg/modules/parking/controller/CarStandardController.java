package org.jeecg.modules.parking.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.parking.entity.CarStandard;
import org.jeecg.modules.parking.query.CalculatedAmountQuery;
import org.jeecg.modules.parking.query.CarStandardQuery;
import org.jeecg.modules.parking.service.ICarStandardService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.parking.util.GetMoney;
import org.jeecg.modules.parking.vo.CarStandardVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDataLogService;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 收费标准表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
@Api(tags="收费标准表")
@RestController
@RequestMapping("/parking/carStandard")
@Slf4j
public class CarStandardController extends JeecgController<CarStandard, ICarStandardService> {
	@Autowired
	private ICarStandardService carStandardService;
	@Autowired
	private ISysDepartService sysDepartService;
	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private ISysDataLogService sysDataLogService;
	
	/**
	 * 分页列表查询
	 *
	 * @param carStandardQuery
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "收费标准表-分页列表查询")
	@ApiOperation(value="收费标准表-分页列表查询", notes="收费标准表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarStandardQuery carStandardQuery,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carStandardQuery.setCompanyId(sysDepart.getId());
			}else{
				carStandardQuery.setCompanyId("0");
			}

		}
		Page<CarStandardVo> page = new Page<CarStandardVo>(pageNo, pageSize);
		IPage<CarStandardVo> pageList = carStandardService.pageList(page, carStandardQuery);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carStandard
	 * @return
	 */
	@AutoLog(value = "收费标准表-添加")
	@ApiOperation(value="收费标准表-添加", notes="收费标准表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarStandardQuery carStandard) {
		return carStandardService.saveCarStandard(carStandard);
	}
	
	/**6
	 *  编辑
	 *
	 * @param carStandard
	 * @return
	 */
	@AutoLog(value = "收费标准表-编辑")
	@ApiOperation(value="收费标准表-编辑", notes="收费标准表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarStandardQuery carStandard) {
		Result result = carStandardService.updateCarStandardById(carStandard);
		return result;
	}
	 /**
	  *  收费标准测算
	  *
	  * @param calculatedAmountQuery
	  * @return
	  */
	 @PostMapping("/calculatedAmount")
	public Result<?> calculatedAmount(@RequestBody CalculatedAmountQuery calculatedAmountQuery){
	 	try {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = DateUtils.str2Date(calculatedAmountQuery.getEndValue(), simpleDateFormat);
			Date date1 = DateUtils.str2Date(calculatedAmountQuery.getStartValue(), simpleDateFormat);
			BigDecimal money = carStandardService.getMoney(calculatedAmountQuery.getCarStandardId(), date1, date,true);
			return Result.OK(money);
		}catch (Exception e){
			return Result.error("出现异常");
		}
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "收费标准表-通过id删除")
	@ApiOperation(value="收费标准表-通过id删除", notes="收费标准表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		CarStandard byId = carStandardService.getById(id);
		String[] split = byId.getLicensePlateType().split(",");
		for (String s : split) {
			redisUtil.del(ParkingRedisConstant.PARKING_STANDARD+byId.getParkingLotId()+":"+s);
		}
		carStandardService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "收费标准表-批量删除")
	@ApiOperation(value="收费标准表-批量删除", notes="收费标准表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		String[] split = ids.split(",");
		for (String s : split) {
			CarStandard byId = carStandardService.getById(s);
			String[] splits = byId.getLicensePlateType().split(",");
			for (String s1 : split) {
				redisUtil.del(ParkingRedisConstant.PARKING_STANDARD+byId.getParkingLotId()+":"+s1);
			}
		}
		this.carStandardService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "收费标准表-通过id查询")
	@ApiOperation(value="收费标准表-通过id查询", notes="收费标准表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarStandard carStandard = carStandardService.getById(id);
		if(carStandard==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carStandard);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carStandard
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarStandard carStandard) {
        return super.exportXls(request, carStandard, CarStandard.class, "收费标准表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarStandard.class);
    }

}

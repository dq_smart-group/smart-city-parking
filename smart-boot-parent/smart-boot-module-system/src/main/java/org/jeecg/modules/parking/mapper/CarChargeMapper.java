package org.jeecg.modules.parking.mapper;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarCharge;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.vo.CarChargeVo;
import org.jeecg.modules.system.entity.SysUser;

/**
 * @Description: 收费员表
 * @Author: jeecg-boot
 * @Date:   2022-03-25
 * @Version: V1.0
 */
public interface CarChargeMapper extends BaseMapper<CarCharge> {

    List<CarChargeVo> pageList(Page<CarChargeVo> page,@Param("q") CarCharge q);

    /**
     * 将数据存入到redis中
     * @return
     */
    List<CarChargeVo> setListToRedis();

    List<CarCharge> getCarChargeByUserId(@Param("userId") String userId,@Param("id") String id);

    /**
     * 判断登录的是不是收费员
     * @param id
     * @return
     */
    List<CarCharge> isCharge(@Param("id") String id);


    /**
     * 查询出自己公司下所有的收费员
     * @return
     */
    List<SysUser> getSysUserByChange(@Param("companyId") String companyId);
}

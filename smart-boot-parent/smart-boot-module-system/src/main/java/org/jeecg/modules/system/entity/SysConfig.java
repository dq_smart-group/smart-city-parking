package org.jeecg.modules.system.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: sys_config
 * @Author: jeecg-boot
 * @Date:   2021-12-03
 * @Version: V1.0
 */
@Data
@TableName("sys_config")
public class SysConfig implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    private java.lang.String id;
	/**配置分类id*/
	@Excel(name = "配置分类id", width = 15)
    private java.lang.String configTabId;
	/**字段名称*/
	@Excel(name = "字段名称", width = 15)
    private java.lang.String menuName;
	/**类型(文本框,单选按钮...)*/
	@Excel(name = "类型(文本框,单选按钮...)", width = 15)
	@Dict(dicCode = "config_control_type")
    private java.lang.String type;
	/**规则 单选框和多选框*/
	@Excel(name = "规则 单选框和多选框", width = 15)
    private java.lang.String parameter;
	/**上传文件格式1单图2多图3文件*/
	@Excel(name = "上传文件格式1单图2多图3文件", width = 15)
    private java.lang.Integer uploadType;
	/**规则*/
	@Excel(name = "规则", width = 15)
    private java.lang.String required;
	/**多行文本框的宽度*/
	@Excel(name = "多行文本框的宽度", width = 15)
    private java.lang.Integer width;
	/**多行文框的高度*/
	@Excel(name = "多行文框的高度", width = 15)
    private java.lang.Integer high;
	/**默认值*/
	@Excel(name = "默认值", width = 15)
    private java.lang.String dataValue;
	/**配置名称*/
	@Excel(name = "配置名称", width = 15)
    private java.lang.String title;
	/**配置简介*/
	@Excel(name = "配置简介", width = 15)
    private java.lang.String info;
	/**排序*/
	@Excel(name = "排序", width = 15)
    private java.lang.Integer sort;
	/**是否隐藏*/
	@Excel(name = "是否隐藏", width = 15)
	@Dict(dicCode = "config_tab_status")
    private java.lang.Integer status;
}

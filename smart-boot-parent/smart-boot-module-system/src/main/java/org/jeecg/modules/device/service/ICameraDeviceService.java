package org.jeecg.modules.device.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.device.entity.CameraDevice;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.device.query.CameraDeviceQuery;
import org.jeecg.modules.device.vo.CameraDeviceVo;
import org.jeecg.modules.parking.vo.CameraDeviceChVo;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;

import java.util.List;

/**
 * @Description: 停车场摄像机设备信息表
 * @Author: jeecg-boot
 * @Date:   2021-11-25
 * @Version: V1.0
 */
public interface ICameraDeviceService extends IService<CameraDevice> {
    //条件查询
    IPage<CameraDeviceVo> listPage(Page<CameraDeviceVo> page, CameraDeviceQuery cameraDeviceQuery);




    /*根据查询设备*/
    CameraDeviceVo selectDevice(String serlino);

    /**
     * 根据设备号查询设备
     * @param serlino
     * @return
     */
    CameraDevice selectByDevice(String serlino);


    /**
     * 根据公司编码查询出公司下的设备
     * @param companyId 公司id
     * @return
     */
    List<CameraDevice> selectByOrgCode(String companyId);


    /**
     * 根据设备组id查询车场内所有的设备
     * @param id
     * @return
     */
    List<ParkingLotAndCamVo> getByParkingLotId(String id);


    /**
     * 根据车场id查询车场内所有的设备
     * @param lotId
     * @return
     */
    List<CameraDevice> getParkingManagementDeviceByLotId(String lotId);

    /**
     * 获取登录人下的设备 级联
     * @return
     */
    List<ParkingLotAndCamVo> selectByCompanyId();

    /**
     * 添加级联设备信息到redis中
     */
    void saveDeviceGroupInRedis();
    /**
     * 更新redis数据
     */
    void updateRedisDeviceGroup();

    /**
     * 添加设备
     * @param cameraDevice
     * @return
     */
    Result<?> saveCameraDevice(CameraDevice cameraDevice);

    /**
     * 修改设备
     * @param cameraDevice
     * @return
     */
    Result<?> updateByCameraDevice(CameraDevice cameraDevice);

    List<CameraDevice> getList();

    List<CameraDeviceChVo> getCameraDeviceChVoByParkingLotId(String parkingLotId);

    /**
     * 更新数据到redis
     * @param groupId
     */
    void setDataInRedis(String groupId);


    /**
     * 修改设备ip
     * @param cameraDevice
     * @return
     */
    Result<?> modifyIP(CameraDevice cameraDevice);

    /**
     * 根据设备组id查找该设备组的主设备
     * @param collect
     * @return
     */
    List<CameraDevice> getMainDeviceByGroupIds(List<String> collect);


    /**
     * 根据园区编码查询此园区下的所有设备
     * @param screenCode
     * @return
     */
    List<String> screenDeviceList(String screenCode);

    /**
     * 根据园区id查找设备
     * @param parkId
     * @return
     */
    List<CameraDevice> getDeviceByParkId(String parkId);

    /**
     * 定时检查设备状态：在线、离线
     * @return
     */
    void checkTheEquipmentStatus();

}

package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class CalculatedAmountQuery {
    //收费标准id
    private String carStandardId;
    //进场时间
    private String startValue;
    //出场时间
    private String endValue;
}

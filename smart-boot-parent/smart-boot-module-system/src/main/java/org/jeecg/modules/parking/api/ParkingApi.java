package org.jeecg.modules.parking.api;

import cn.hutool.core.lang.Assert;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.daqing.config.DongGuanStaticConfig;
import com.daqing.entity.DongGuanStaticParkingLotBusInfo;
import com.daqing.entity.DongGuanStaticPictureFileInfo;
import com.daqing.entity.DongGuanStaticResponse;
import com.daqing.entity.DongGuanStaticUploadPictureStreamInfo;
import com.xkcoding.http.util.StringUtil;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.authentication.utils.HttpRequestUtil;
import org.jeecg.modules.authentication.utils.MapBuild;
import org.jeecg.modules.authentication.utils.QuartzRequestUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.device.vo.CameraDeviceVo;
import org.jeecg.modules.parking.entity.*;
import org.jeecg.modules.parking.mqtt.MqttConfiguration;
import org.jeecg.modules.parking.mqtt.MqttPushClient;
import org.jeecg.modules.parking.query.CarMonthlyRentQuery;
import org.jeecg.modules.parking.service.*;
import org.jeecg.modules.parking.service.impl.CarCommercialServiceImpl;
import org.jeecg.modules.parking.service.impl.CarConsumptionRecordServiceImpl;
import org.jeecg.modules.parking.service.impl.CarInfoCollectServiceImpl;
import org.jeecg.modules.parking.service.impl.CarYardFeesServiceImpl;
import org.jeecg.modules.parking.vo.PlayBackVo;
import org.jeecg.modules.system.util.SysConfigUtil;
import org.jeecg.modules.utils.RedisGetUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
@RequestMapping("/parking/parkingapi")
@Slf4j
public class ParkingApi {

    private CarCommercialServiceImpl commercialService;
    public RedisTemplate redisTemplate;
    CarInfoCollectServiceImpl carInfoCollectService;
    CarYardFeesServiceImpl carYardFeesService;
    CarConsumptionRecordServiceImpl consumptionRecordService;
    private ICarWhiteListService carWhiteListService;
    private IParkingLotService parkingLotService;

    private ICarShiftFormService carShiftFormService;
    private ICarBannerService bannerService;

    private ICarDeviceGroupService carDeviceGroupService;

    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private ICameraDeviceService cameraDeviceService;
    @Autowired
    private MqttConfiguration mqttConfiguration;





    /**
     * 根据车牌号查找进场信息
     */
    @GetMapping(value = "/getInfoByPlate/plate")
    public Result<?> getInfoByPlate(String plate){
        Assert.isTrue(StringUtil.isNotEmpty(plate),"请输入车牌号");
        return carInfoCollectService.getInfoByPlate(plate);
    }


    /**
     * 根据出场时间生成订单
     * @return
     */
    @PostMapping(value = "/generateOrder")
    public Result<?> generateOrder(@RequestBody Map<String,String> map){
        String id = map.get("id");
        String openid = map.get("openid");
        Assert.isTrue(StringUtil.isNotEmpty(id),"参数异常");
        Assert.isTrue(StringUtil.isNotEmpty(openid),"参数异常");
        return carInfoCollectService.generateOrder(id,openid);
    }


    /**
     *没带出场设备序列号的，订单支付成功后修改记录状态
     */
//    @PostMapping(value = "/paySuccess")
//    public void paySuccess(@RequestBody CarInfoCollect carInfoCollect){
//         carInfoCollectService.paySuccess(carInfoCollect);
//    }


    //-----------------------------------------商户接口start----------------------------------------------------


    @PutMapping(value = "/edit")
    public Result<?> edit(@RequestBody CarCommercial carCommercial) {
        commercialService.edit(carCommercial);
        return Result.OK("编辑成功!");
    }

    /**
     * 商户支付首页
     * @param id
     * @return
     */
    @GetMapping(value = "index")
    public Result<?> queryPageList(@RequestParam(name="id") String id,
                                   @RequestParam(name="key",required = false) String key) {
        return commercialService.queryPageList(id,key);
    }

    /**
     * 查询车辆订单
     * @return
     */
    @PostMapping("listCar")
    public Result<?> listCar(@RequestBody Map<String,String> map){
        return commercialService.listCar(map);

    }

    /**
     * 支付：：
     * 1：生成车场商户消费记录
     *:2：如果是动态二维码，刷新key
     * 3：扣除商家费用
     * 4：生成商户消费订单
     */
    @Transactional
    @PostMapping("payCar")
    public Result<?> payCar(
            @RequestBody Map<String,String> map) throws Exception {
        return commercialService.payCar(map);
    }

    /**
     * 验证key是否被使用，防止订单重复提交
     */
    @Transactional
    @GetMapping("verifyKey")
    public Result<?> payCar(@RequestParam(name="key") String key){
        try {
            consumptionRecordService.getOne(Wrappers.<CarConsumptionRecord>lambdaQuery().eq(CarConsumptionRecord::getNote,key));
            return Result.OK();
        }catch (Exception e){
            return Result.error("订单已失效");
        }
    }



    /**
     * 商户登录
     * 账号不存在
     * 账号已禁用
     * 密码错误
     */
    @PostMapping("login")
    public Result<?> login(@RequestBody Map<String,String> userInfo){
        CarCommercial commercial = commercialService.login(userInfo);
        return Result.OK(commercial);
    }

    /**
     * 修改密码
     * @param info
     * @return
     */
    @PostMapping("/updatePassword")
    public Result updatePassword(@RequestBody Map<String,String> info){
        commercialService.updatePassword(info);
        return Result.OK();
    }

    /**
     * 扣款设置
     * @param carCommercial
     * @return
     */
    @PostMapping("/deductionUpdate")
    public Result deductionUpdate(@RequestBody CarCommercial carCommercial){
        commercialService.deductionUpdate(carCommercial);
        return Result.OK();
    }

    /**
     * 根据商户ID查询充值记录
     * @param info
     * @return
     */
    @PostMapping("/rechargeRecordList")
    public Result rechargeRecordList(@RequestBody Map<String,String> info){
        String id = info.get("id");
        return Result.OK(commercialService.rechargeRecordList(id));
    }

    /**
     * 根据商户ID查询消费记录
     * @param info
     * @return
     */
    @PostMapping("/consumptionRecordList")
    public Result consumptionRecordList(@RequestBody Map<String,String> info){
        String id = info.get("id");
        return Result.OK(commercialService.consumptionRecordList(id));
    }

    /**
     * 添加交班记录
     * @param data
     * @return
     */
    @PostMapping("/addCarShiftForm")
    public Result addCarShiftForm(@RequestBody Map<String,String> data){
        return carShiftFormService.addCarShiftForm(data);
    }

    //____________________________________网页二维码start____________________________________
    /**
     * 获取二维码广告图片
     */
    @PostMapping("/getBannerImg")
    public Result getBannerImg(){
        List<CarBanner> carBanners = bannerService.list(Wrappers.<CarBanner>lambdaQuery()
                .eq(CarBanner::getBannerGroup, "商户二维码广告")
                .eq(CarBanner::getIsShow, "1"));
        List<String> collect = carBanners.stream().map(i -> i.getImageUrl()).collect(Collectors.toList());
        return Result.OK(collect);
    }

    /**
     * 判断商户是否登录（cookie是否存在）
     *      存在：返回商户二维码信息
     *      不存在：重新登录
     */
    @PostMapping("/getIfLogin")
    public Result getIfLogin(@RequestBody Map<String,String> userInfo){
        String cookie = userInfo.get("cookie");
        CarCommercial commercial = null==commercialService.getById(cookie)?null:commercialService.getById(cookie);
        return Result.OK(commercial);
    }


    //____________________________________网页二维码end____________________________________



    //-----------------------------------------商户接口end----------------------------------------------------

    //-----------------------------------------月卡接口start----------------------------------------------------

    /**
     * 根据车牌查询月卡记录
     */
    @PostMapping("/getCarWhiteByPlate")
    public Result<?> getCarWhiteByPlate(@RequestBody Map<String,String> map){
        String plate = map.get("plate");
        String openid = map.get("openid");
        return carWhiteListService.getCarWhiteByPlate(plate,openid);
    }

    /**
     * 查询所有车场
     */
    @PostMapping("/getParkLotList")
    public Result<?> getParkLotList(){
        return Result.OK(parkingLotService.getParkingLotByCompanyId(null));
    }


    /**
     * 计算金额
     */
    @PostMapping("/CalculateAmount")
    public Result<?> calculateAmount(@RequestBody Map<String,String> map){
        Assert.isTrue(null!=map.get("renewalDuration"),"时长不能为空");
        Integer renewalDuration = Integer.valueOf(map.get("renewalDuration"));
        String parkingLotId = map.get("parkingLotId");
        String czorxf = map.get("czorxf");
        String plateTypeId = map.get("plateTypeId");
        CarMonthlyRentQuery carMonthlyRentQuery = new CarMonthlyRentQuery();
        carMonthlyRentQuery.setParkingLotId(parkingLotId);
        carMonthlyRentQuery.setCzorxf(czorxf);
        carMonthlyRentQuery.setPlateType(plateTypeId);
        carMonthlyRentQuery.setRenewalDuration(renewalDuration);
        BigDecimal moneyByRenewalDuration = parkingLotService.getMoneyByRenewalDuration(carMonthlyRentQuery);
        return Result.OK(moneyByRenewalDuration);
    }

    /**
     * 新增月卡记录
     */
    @Transactional
    @PostMapping("/addRenrwal")
    public Result<?> addRenrwal(@RequestBody CarWhiteList whiteList){
        Map<String, String> map = new HashMap<>();
        map.put("renewalDuration","1");
        map.put("parkingLotId",whiteList.getParkingLotId());
        map.put("czorxf","1");
        Result<?> result = calculateAmount(map);
        carWhiteListService.addRenrwal(whiteList,result);
        return Result.OK();
    }

    /**
     * 续费：
     * 	1、更改过期时间
     * 	2、生成记录表
     */
    @PostMapping("/Renewal")
    public Result<?> Renewal(@RequestBody CarRenewalHistory carRenewalHistory){
        return carWhiteListService.Renewal(carRenewalHistory);
    }

    /**
     * 生成月卡充值/续费订单
     * @return
     */
    @PostMapping(value = "/generateRenewalOrder")
    public Result<?> generateRenewalOrder(@RequestBody Map<String,String> map){
        String id = map.get("id");
        String openid = map.get("openid");
        Integer renewalTime = Integer.valueOf(map.get("renewalTime"));
        String companyId = map.get("companyId");
        String plateTypeId = map.get("plateTypeId");
        String companyName = map.get("companyName");
        String parkingLotName = map.get("parkingLotName");
        String plate = map.get("plate");
        String phone = carWhiteListService.getPhone(openid);
        BigDecimal amount = BigDecimal.valueOf(Integer.parseInt(map.get("amount")));
        Assert.isTrue(StringUtil.isNotEmpty(openid),"参数异常");
        return carWhiteListService.generateRenewalOrder(id,openid,renewalTime,amount,plateTypeId,companyId,companyName,parkingLotName,plate,phone);
    }

    /**
     * 退款
     */

    //-----------------------------------------月卡接口end----------------------------------------------------


    /**
     * 进出场数据上传静态交通数据
     * @param data
     * @return
     */
    @PostMapping(value = "/dgParkingLotBusInfo")
    public Result<?> dgParkingLotBusInfo(@RequestBody DongGuanStaticParkingLotBusInfo data){
        DgParkingApi dgParkingApi = new DgParkingApi();
        data.setParkingLotId(DongGuanStaticConfig.dongGuanComId+data.getParkingLotId());
        CarDeviceGroup byId = carDeviceGroupService.getById(data.getEntryId());
        data.setEntryId(DongGuanStaticConfig.dongGuanComId+byId.getSentryBoxId());
        DongGuanStaticResponse dongGuanStaticResponse = dgParkingApi.dgParkingLotBusInfo(data);
        if(dongGuanStaticResponse.getState()==1){
            return Result.OK(dongGuanStaticResponse);
        }
        return Result.error(dongGuanStaticResponse.getMessage());
    }

    /**
     * 进出场图片上传到静态交通数据
     * @param data
     * @param dataInfo
     * @return
     */
    @PostMapping(value = "/dgUploadPicture")
    public Result<?> dgUploadPicture(@RequestBody DongGuanStaticPictureFileInfo data, DongGuanStaticUploadPictureStreamInfo dataInfo){
        DgParkingApi dgParkingApi = new DgParkingApi();
        data.setParkingOrSectionId(DongGuanStaticConfig.dongGuanComId+data.getParkingOrSectionId());
        DongGuanStaticResponse dongGuanStaticResponse = dgParkingApi.dgUploadPicture(data,dataInfo);
        if(dongGuanStaticResponse.getState()==1){
            return Result.OK(dongGuanStaticResponse);
        }
        return Result.error(dongGuanStaticResponse.getMessage());
    }


    /**
     *
     * @return
     */
    @GetMapping("remake")
    public String remake(String serialNumber){
        Assert.isTrue(StringUtils.isNotEmpty(serialNumber),"参数异常");
        String[] number=serialNumber.split("=");
        MqttPushClient.manualCapture(serialNumber);
        String imgPath = "";
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        imgPath = (String) redisUtil.get(ParkingRedisConstant.PARKING_IMG + serialNumber);
        redisUtil.del(ParkingRedisConstant.PARKING_IMG + serialNumber);
        if(StringUtil.isEmpty(imgPath)){
            return null;
        }
        System.out.println(JSON.toJSONString(imgPath));
        return imgPath;
    }

    /**
     * 开闸
     * @param cameraDevice
     * @return
     * @throws Exception
     */
    @PostMapping(value = "/openGate")
    public void openGate(@RequestBody CameraDeviceVo cameraDevice) throws Exception{

        String serialno = cameraDevice.getSerialno();
        Assert.isTrue(StringUtils.isNotEmpty(serialno),"参数异常");
        MqttPushClient.publishOpen(MqttPushClient.getTopic(serialno));
    }

    /**
     * 关闸
     *
     * @param cameraDevice
     * @return
     */
    @PostMapping(value = "/closeGate")
    public void closeGate(@RequestBody CameraDeviceVo cameraDevice) throws Exception{
        String serialno = cameraDevice.getSerialno();
        Assert.isTrue(StringUtils.isNotEmpty(serialno),"参数异常");
        MqttPushClient.publishClose(MqttPushClient.getTopic(cameraDevice.getSerialno()));
        carInfoCollectService.send3DWebsocket(serialno,"2");
    }



    /**
     * 重拍成功
     * @param serialno
     * @return
     * @throws Exception
     */
    @AutoLog(value = "重拍操作")
    @GetMapping("/manualCapture")
    public void manualCapture(String serialno)throws Exception{
        Assert.isTrue(StringUtils.isNotEmpty(serialno),"参数异常");
        MqttPushClient.manualCapture(serialno);
    }


    /**
     * 根据园区id读取该园区下面设备的状态
     */
    @GetMapping("/readRoadGateState")
    public void readRoadGateState(String parkId){
        Assert.isTrue(!StringUtils.isEmpty(parkId),"参数异常");
        List<CameraDevice> list = cameraDeviceService.getDeviceByParkId(parkId);
        for (CameraDevice cameraDevice : list) {
            MqttPushClient.readState(MqttPushClient.getTopic(cameraDevice.getSerialno()));
        }
    }


    @PostMapping("/getDate")
    public void getDate(@RequestBody Map<String,Object> map){
        System.out.println(map);
    }

    /**
     * 获取到云端播放视频的路径
     * @return
     */
    @GetMapping("getVideoUrl")
    public Result<?> getVideoUrl(String id){
        if(StringUtils.isEmpty(id)){
            return Result.error("参数异常");
        }
        CameraDevice byId = cameraDeviceService.getById(id);
        if(null == byId){
            return Result.error("没有该设备");
        }
        if(StringUtils.isEmpty(byId.getNvrNumber())){
            return Result.error("该设备没有设置云端nvr编号");
        }
        String[] split = byId.getNvrNumber().split(",");
        if(split.length!=2){
            return Result.error("出现异常");
        }
        Map<String,Object> map = MapBuild.hashMapBuild();
        map.put("deviceId",split[0]);
        map.put("channelId",split[1]);
        Result<?> request = QuartzRequestUtil.quartzPostRequests(SysConfigUtil.getVal("get_cloud_video_url"), map);
        if(request == null){
            return Result.error("服务正在维护或更新");
        }
        log.info(request.toString());
        if(request.isSuccess()){
            Map mapTypes = JSON.parseObject(request.getResult().toString());
            Map maps = JSON.parseObject(mapTypes.get("data").toString());
            //System.out.println(maps);
            return Result.OK(maps);
        }else{
            return Result.error("请求失败");
        }
    }

    /**
     * 获取到云端播放视频的路径
     * @return
     */
    @GetMapping("getVideoUrl3d")
    public Result<?> getVideoUrl3d(String deviceId,String channelId){
        if(StringUtils.isEmpty(deviceId)||StringUtils.isEmpty(channelId)){
            return Result.error("参数异常");
        }
        Map<String,Object> map = MapBuild.hashMapBuild();
        map.put("deviceId",deviceId);
        map.put("channelId",channelId);
        Result<?> request = QuartzRequestUtil.quartzPostRequests(SysConfigUtil.getVal("get_cloud_video_url"), map);
        log.info(request.toString());
        if(request.isSuccess()){
            Map mapTypes = JSON.parseObject(request.getResult().toString());
            Map maps = JSON.parseObject(mapTypes.get("data").toString());
            //System.out.println(maps);
            return Result.OK();
        }else{
            return Result.error("请求失败");
        }
    }

    /**
     * 获取某个监控的某段时间内的回放
     * @param id
     * @return
     */
    @GetMapping("getPlayBack")
    public Result<?> getPlayBack(String id,String startTime,String endTime){
        if(StringUtils.isEmpty(id)||StringUtils.isEmpty(startTime)||StringUtils.isEmpty(endTime)){
            return Result.error("参数异常");
        }
        CameraDevice byId = cameraDeviceService.getById(id);
        if(null == byId){
            return Result.error("没有该设备");
        }
        if(StringUtils.isEmpty(byId.getNvrNumber())){
            return Result.error("该设备没有设置云端nvr编号");
        }
        String[] split = byId.getNvrNumber().split(",");
        if(split.length!=2){
            return Result.error("出现异常");
        }
        Map<String,Object> map = MapBuild.hashMapBuild();
        map.put("deviceId",split[0]);
        map.put("channelId",split[1]);
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        Result<?> request = QuartzRequestUtil.quartzPostRequests(SysConfigUtil.getVal("getPlayBack"), map);//
        log.info(request.toString());
        if(request.isSuccess()){
            Map mapTypes = JSON.parseObject(request.getResult().toString());
            Map maps = JSON.parseObject(mapTypes.get("data").toString());
            JSONArray recordList = JSON.parseArray(maps.get("recordList").toString());
            List<PlayBackVo> playBackVos = recordList.toJavaList(PlayBackVo.class);
            return Result.OK(playBackVos);
        }else{
            return Result.error("请求失败");
        }
    }

    /**
     * 播放回放视频返回播放路径
     * @param id
     * @param startTime
     * @param endTime
     * @return
     */
    @GetMapping("starPlayBack")
    public Result<?> starPlayBack(String id,String startTime,String endTime){
        if(StringUtils.isEmpty(id)||StringUtils.isEmpty(startTime)||StringUtils.isEmpty(endTime)){
            return Result.error("参数异常");
        }
        CameraDevice byId = cameraDeviceService.getById(id);
        if(null == byId){
            return Result.error("没有该设备");
        }
        if(StringUtils.isEmpty(byId.getNvrNumber())){
            return Result.error("该设备没有设置云端nvr编号");
        }
        String[] split = byId.getNvrNumber().split(",");
        if(split.length!=2){
            return Result.error("出现异常");
        }
        Map<String,Object> map = MapBuild.hashMapBuild();
        map.put("deviceId",split[0]);
        map.put("channelId",split[1]);
        map.put("startTime",startTime);
        map.put("endTime",endTime);
        Result<?> request = QuartzRequestUtil.quartzPostRequests(SysConfigUtil.getVal("starPlayBack"), map);
        log.info(request.toString());
        if(request.isSuccess()){
            Map mapTypes = JSON.parseObject(request.getResult().toString());
            //System.out.println(maps);
            return Result.OK(mapTypes);
        }else{
            return Result.error("请求失败");
        }
    }







    /*****************************************************H5停车管理接口************************************************/
    /**
     * 根据公司id来查询停车管理需要的数据
     * @param map
     * @return
     */
    @PostMapping("/getParkingManagementDataByDepartId")
    public Result<?> getParkingManagementDataByDepartId(@RequestBody Map<String,Object> map){
        Object departId = map.get("departId");
        if(departId == null){
            return Result.error("参数为空");
        }
        String id = departId.toString();
        if(StringUtil.isEmpty(id)){
            return Result.error("参数为空");
        }
        return parkingLotService.getParkingManagementDataByDepartId(id);
    }








    /**
     * 获取到云端播放视频的路径
     * @return
     */
    @GetMapping("test")
    public Result<?> test(){
        Map<String,Object> map = MapBuild.hashMapBuild();
        map.put("deviceId","34020000001180000001");
        map.put("channelId","34020000001320000008");
        Result<?> request = QuartzRequestUtil.quartzPostRequests(SysConfigUtil.getVal("get_cloud_video_url"), map);
        Map maps = null;
        if(request.getResult() != null){
            Map mapTypes = JSON.parseObject(request.getResult().toString());
            if(mapTypes.get("data") != null){
                maps = JSON.parseObject(mapTypes.get("data").toString());
            }else{
                return Result.error(request.getMessage());
            }
        }else{
            return Result.error(request.getMessage());
        }
        System.out.println(maps);
        return request;
    }

    /*****************************************************手动重连mqtt服务************************************************/
    /**
     * 手动刷新连接mqtt服务
     */
    @GetMapping("refreshMqtt")
    public void refreshMqtt(){
        MqttClient client  = MqttPushClient.getClient();
        //如果还有连接的先断开连接
        try{
            if(client.isConnected()){
                client.disconnect();
                log.info("MQTT断开连接成功");
            }
        }catch (Exception e){
            e.printStackTrace();
            log.error("MQTT断开连接失败, msg:" + e.getMessage());
        }
        //开始连接mqtt服务，只连接一次
        try{
            mqttConfiguration.getMqttPushClients();
        }catch (Exception e){
            e.printStackTrace();
            log.error("MQTT连接失败, msg:" + e.getMessage());
        }
    }
}

package org.jeecg.modules.parking.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.math.BigDecimal;

@Data
public class MoneyStatisticsVo {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal payMoneyTotal;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal reductionMoneyTotal;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private BigDecimal netReceiptsMoneyTotal;
}

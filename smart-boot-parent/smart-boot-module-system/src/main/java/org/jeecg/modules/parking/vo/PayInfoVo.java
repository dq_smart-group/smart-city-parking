package org.jeecg.modules.parking.vo;

import lombok.Data;

import java.util.Date;

@Data
public class PayInfoVo {

    /**
     * 出入库记录id
     */
    private String id;
    /**
     * 车牌号
     */
    private String license;
    /**
     * 入库时间
     */
    private String comeInTime;
    /**
     * 车辆状态
     */
    private String carStatus;
    /**
     * 公司名字
     */
    private String companyName;
    /**
     * 公司id
     */
    private String companyId;
    /**
     * 车库名称
     */
    private String parkingLotName;
    /**
     * 费用
     */
    private String cost;
    /**
     * 预计出库时间
     */
    private Date outTime;
    /**
     * 等待出库时间
     */
    private Date waitOutTime;

}

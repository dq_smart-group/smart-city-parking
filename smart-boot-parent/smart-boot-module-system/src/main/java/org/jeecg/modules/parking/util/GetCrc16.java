package org.jeecg.modules.parking.util;

import cn.hutool.core.codec.Base64;
import cn.hutool.core.date.DateUtil;
import org.jeecg.common.util.DateUtils;
import org.jeecg.modules.parking.api.Crc16_test;
import org.jeecg.modules.parking.api.classHexstringToByteUtil;

import java.io.UnsupportedEncodingException;
import java.util.Date;

public class GetCrc16 {
    /**
     * 一个字节包含位的数量 8
     */
    private static final int BITS_OF_BYTE = 8;
    /**
     * 多项式
     */
    private static final int POLYNOMIAL = 0xA001;
    /**
     * 初始值
     */
    private static final int INITIAL_VALUE = 0xFFFF;

    private static final char[] DIGITS
            = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};


    /**
     * CRC16 编码
     *
     * @param bytes 编码内容
     * @return 编码结果
     */
    public static int crc16(int[] bytes) {
        int res = INITIAL_VALUE;
        for (int data : bytes) {
            res = res ^ data;
            for (int i = 0; i < BITS_OF_BYTE; i++) {
                res = (res & 0x0001) == 1 ? (res >> 1) ^ POLYNOMIAL : res >> 1;
            }
        }
        return revert(res);
    }

    /**
     * 翻转16位的高八位和低八位字节
     *
     * @param src 翻转数字
     * @return 翻转结果
     */
    private static int revert(int src) {
        int lowByte = (src & 0xFF00) >> 8;
        int highByte = (src & 0x00FF) << 8;
        return lowByte | highByte;
    }

    /**
     * 根据串码获取出CRC16校验值并接返回
     *
     * @param str
     * @return
     */
    public static String getCrc16(String str) {
        String trim = str.trim();
        int[] one = GetCrc16.getS16toI16(trim);

        final int res = GetCrc16.crc16(one);
        String hex = Integer.toHexString(res);
        if (hex.length() == 1) {
            trim = trim + " " + "0" + hex + " " + "00";
        } else if (hex.length() == 2) {
            trim = trim + " " + hex + " " + "00";
        } else if (hex.length() == 3) {
            trim = trim + " " + "0" + hex.substring(0, 1) + " " + hex.substring(1, 3);
        } else if (hex.length() == 4) {
            trim = trim + " " + hex.substring(0, 2) + " " + hex.substring(2, 4);
        }
        return trim;
    }

    /**
     * 把字符串16进制转为int16进制
     *
     * @param one
     * @return
     */
    public static int[] getS16toI16(String one) {
        String[] s = one.split(" ");
        int[] array = new int[s.length];
        for (int i = 0; i < s.length; i++) {
            array[i] = Integer.parseInt(s[i], 16);
        }
        return array;

    }
    /**
     * base64转16进制
     * @param
     * @return
     */
    public static final String base64To16(String data){
        byte[] decoded = java.util.Base64.getDecoder().decode(data);
        String s = toHex(decoded);
        String str="";
        for (int i = 0; i < s.length();i = i + 2) {
            str += " " + s.substring(i, i + 2);
        }
        return str.trim();
    }
    public static final String toHex(byte[] data) {
        final StringBuffer sb = new StringBuffer(data.length * 2);
        for (int i = 0; i < data.length; i++) {
            sb.append(DIGITS[(data[i] >>> 4) & 0x0F]);
            sb.append(DIGITS[data[i] & 0x0F]);
        }
        return sb.toString();
    }
    /**
     * 下载临时广告语
     * 将汉字转换为内容长度+GB2312格式
     *
     * @param str 汉字内容
     *  @param hang 下载在第几行
     *  @param color 字体颜色
     * @return
     */
    //将byte数组转成16进制字符串
    public static String bytesToHexFun1(String str,String hang,String color) throws Exception {
        //根据转入的汉字转GB2312编码的字节数组
        byte[] bytes = str.getBytes("GB2312");
        //调用根据字节数组转16进制方法
        char[] buf = getchar(bytes);

        String d = GetCrc16.getLen(bytes);
        String lens = GetCrc16.getLens(bytes);
        int dl = 19 + Integer.parseInt(lens, 16);
        String s = new String(buf);
        for (int i = 0; i < s.length(); i = i + 2) {
            d += " " + s.substring(i, i + 2);
        }
        String s1 = "00 64 FF FF 62";
        String col = "";
        if(color.equals("R")){
            col = "ee 00 00 00 00 00 00 00";
        }else if(color.equals("G")){
            col = "00 ee 00 00 00 00 00 00";
        }else if(color.equals("B")){
            col = "00 00 ee 00 00 00 00 00";
        }
        String s2 = "15 03 00 0A 15 01 03 03"+" "+col;
        s2 = hang+" "+s2;
        s1 = s1 + " " + Integer.toHexString(dl) + " " + s2;
        return s1.trim() + " " + d;
    }

    /**
     * 下载广告语
     * 将汉字转换为内容长度+GB2312格式
     * @param color 字体颜色
     * @param str 汉字内容
     * @param hang 下载在第几行
     * @param type 文字显示方式
     * @return
     */
    //将byte数组转成16进制字符串
    public static String downloadLanguage(String str,String hang,String color,String type) throws Exception {
        byte[] bytes = str.getBytes("GB2312");
        //调用方法
        char[] buf = getchar(bytes);

        String d = GetCrc16.getLen(bytes);
        String lens = GetCrc16.getLens(bytes);
        int dl = 20 + Integer.parseInt(lens, 16);
        String s = new String(buf);
        for (int i = 0; i < s.length(); i = i + 2) {
            d += " " + s.substring(i, i + 2);
        }
        String col = "";
        if(color.equals("R")){
            col = "ee 00 00 00 00 00 00 00";
        }else if(color.equals("G")){
            col = "00 ee 00 00 00 00 00 00";
        }else if(color.equals("B")){
            col = "00 00 ee 00 00 00 00 00";
        }
        String s1 = "00 64 FF FF 67";
        String s2 = "00 0C";
        String s3 = "01 00 02 15 01 03";
        s2 = hang+" "+s2+" "+type+" "+s3+" "+col;
        s1 = s1 + " " + Integer.toHexString(dl) + " " + s2;
        return s1.trim() + " " + d;
    }

    /**
     * 显示广告语
     */
    public static String displayLanguage(String str) throws Exception {
        String s1 = "00 64 FF FF 68 02";
        return s1 + " " + str.trim();
    }

    /**
     * 获取内容长度16进制
     *
     * @param bytes
     */
    public static String getLen(byte[] bytes) {
        char[] buf = getchar(bytes);
        String s = new String(buf);
        int i = s.length() / 2;
        String s1 = Integer.toHexString(i);
        if (s1.length() == 1) {
            s1 = "0" + s1 + " " + "00";
        } else if (s1.length() == 2) {
            s1 = s1 + " " + "00";
        } else if (s1.length() == 3) {
            s1 = "0" + s1.substring(0, 1) + " " + s1.substring(1, 3);
        } else if (s1.length() == 4) {
            s1 = s1.substring(0, 2) + " " + s1.substring(2, 4);
        }
        return s1;
    }

    /**
     * 获取内容长度16进制
     *
     * @param bytes
     */
    public static String getLens(byte[] bytes) {

        char[] buf = getchar(bytes);
        String s = new String(buf);

        int i = s.length() / 2;
        String s1 = Integer.toHexString(i);
        return s1;
    }


    /**
     * 根据字节数组转16进制方法
     *
     * @param bytes
     * @return
     */
    public static char[] getchar(byte[] bytes) {
        char[] HEX_CHAR = {'0', '1', '2', '3', '4', '5',
                '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        // 一个byte为8位，可用两个十六进制位标识
        char[] buf = new char[bytes.length * 2];
        int a = 0;
        int index = 0;
        for (byte b : bytes) { // 使用除与取余进行转换
            if (b < 0) {
                a = 256 + b;
            } else {
                a = b;
            }
            buf[index++] = HEX_CHAR[a / 16];
            buf[index++] = HEX_CHAR[a % 16];
        }
        return buf;
    }

    /**
     * 获取二维码的串码
     *
     * @param
     * @throws Exception
     */
    public static String getQrCode(String str3) throws Exception {
        str3 = str3;
        String str="";
        if(str3.length()/2==255) {
           str = "00 C8 01 00 E5";
        }else {
             str = "00 C8 FF FF E5";

        }
        int len = str3.length() / 2;
        String s1 = Integer.toHexString(len);
        String str2 = "";
        for (int i = 0; i < str3.length(); i=i+2) {
            str2 = str2+" "+str3.substring(i,i+2);
        }
        if (s1.length() == 1) {
            s1 = "0" + s1 + " " + "00";
        } else if (s1.length() == 2) {
            s1 = s1 + " " + "00";
        } else if (s1.length() == 3) {
            s1 =s1.substring(1, 3)+" " + "0" + s1.substring(0, 1);
        } else if (s1.length() == 4) {
            s1 = s1.substring(0, 2) + " " + s1.substring(2, 4);
        }
        str = str+" "+s1+" "+str2.trim();
        return str;
    }

    /**
     * 获取播报语音的串码
     * @param str 播报内容
     * @param type OTP:为操作选项字。
     * @return
     */
    public static String getVoice(String str,String type)throws Exception{
        String strs = "00 64 FF FF 30";
        byte[] bytes = str.getBytes("GB2312");
        //调用方法
        char[] buf = getchar(bytes);
        String s = new String(buf);
        s = type+s;
        //播报内容编码
        String d = "";
        for (int i = 0; i < s.length(); i = i + 2) {
            d += " " + s.substring(i, i + 2);
        }
        //内容长度
        String lens = Integer.toHexString(s.length()/2);
        if(lens.length()==1){
            lens = "0"+lens;
        }
        strs = strs+" "+lens+" "+d.trim();
        return strs;
    }

    /**
     * 获取调整音量的串码
     * @param sum
     * @return
     */
    public static String getVolume(Integer sum){
        String str = " 00 64 FF FF 0D 01";
        String s = Integer.toHexString(sum);
        if(s.length()==1){
            s = "0"+s;
        }
        str = str+" "+s;
        return str;
    }

    /**
     * 同步时间
     * @param
     * @throws Exception
     */
    public static String synchronizationTime(){
        String str = " 00 64 FF FF 05 08";
        int year = DateUtils.getYear();
        int month = DateUtils.getMonth();
        int day = DateUtils.getDay();
        //DateUtils
        //年
        String s = Integer.toHexString(year);
        if(s.length()==3){
            str = str +" "+ s.substring(1, 3)+" "+"0"+s.substring(0, 1);
        }else if(s.length()==4){
            str = str +" "+ s.substring(2, 4)+" "+s.substring(0, 2);
        }
        //月
        String months = Integer.toHexString(month);
        if(months.length()==1){
            str = str +" 0"+months;
        }else{
            str = str +" "+months;
        }
        //日
        String days = Integer.toHexString(day);
        if(days.length()==1){
            str = str +" 0"+days;
        }else{
            str = str +" "+days;
        }
        //星期几
        int i = DateUtil.dayOfWeek(new Date());
        String weeks = Integer.toHexString(i);
        if(weeks.length()==1){
            str = str +" 0"+weeks;
        }else{
            str = str +" "+weeks;
        }
        int hour = DateUtil.thisHour(true);
        String hours = Integer.toHexString(hour);
        if(hours.length()==1){
            str = str +" 0"+hours;
        }else{
            str = str +" "+hours;
        }
        int minute = DateUtil.thisMinute();
        String minutes = Integer.toHexString(minute);
        if(minutes.length()==1){
            str = str +" 0"+minutes;
        }else{
            str = str +" "+minutes;
        }
        int second = DateUtil.thisSecond();
        String seconds = Integer.toHexString(second);
        if(seconds.length()==1){
            str = str +" 0"+second;
        }else{
            str = str +" "+second;
        }
        String crc16 = GetCrc16.getCrc16(str);
        return crc16;
    }

    /**
     * 调整显示屏亮度
     */
    public static String adjustBrightness(Integer s){
        String str = "00 64 FF FF 0C 01";
        String s1 = Integer.toHexString(s);
        if(s1.length()==1){
            str = str +" 0"+s1;
        }else{
            str = str +" "+s1;
        }
        String s31 = GetCrc16.getCrc16(str);
        byte[] aa21 = classHexstringToByteUtil.hexString2Bytes(s31);
        String encode31 = Base64.encode(aa21);
        return encode31;
    }

    public static void main(String[] args) throws Exception {
        //getQrCode("https://www.baidu.com/");


    }
}

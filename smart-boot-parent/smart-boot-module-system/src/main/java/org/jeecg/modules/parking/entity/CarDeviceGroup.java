package org.jeecg.modules.parking.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 设备组表
 * @Author: jeecg-boot
 * @Date:   2022-04-02
 * @Version: V1.0
 */
@Data
@TableName("car_device_group")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_device_group对象", description="设备组表")
public class CarDeviceGroup implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private Date updateTime;
	/**公司id*/
	@Excel(name = "公司id", width = 15)
    @ApiModelProperty(value = "公司id")
    private String companyId;
	/**车场id*/
	@Excel(name = "车场id", width = 15)
    @ApiModelProperty(value = "车场id")
    private String parkingLotId;
	/**车场id*/
	@Excel(name = "岗亭id", width = 15)
    @ApiModelProperty(value = "岗亭id")
    private String sentryBoxId;
	/**设备组名*/
	@Excel(name = "设备组名", width = 15)
    @ApiModelProperty(value = "设备组名")
    private String groupName;
    /**
     * 是否上传了静态交通
     */
    private String flagStaticTraffic;
}

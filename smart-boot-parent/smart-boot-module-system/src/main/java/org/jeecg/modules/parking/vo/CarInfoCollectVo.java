package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Data
public class CarInfoCollectVo extends CarInfoCollect {
    /**
     * 入库开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startComeTime;
    private String startComeTimes;
    /**
     * 入库结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endComeTime;
    private String endComeTimes;

    /**
     * 出库开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startOutTime;
    /**
     * 出库结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOutTime;

    /**
     * 进场设备名称
     */
    private String comeName;

    /**
     * 出场设备名称
     */
    private String outName;

    /**
     * 车牌类型名称
     */
    private String carTypeName;

    private String parkingAddress;


    private String parkingId;

    private String carStatuslists;

    private List<String> carStatuslist;

    private String type;

    private String fullAddress;
    /**
     * 停车时间
     */
    private String times;
}

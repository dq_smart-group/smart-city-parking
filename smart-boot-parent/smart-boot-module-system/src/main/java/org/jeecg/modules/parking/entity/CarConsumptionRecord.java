package org.jeecg.modules.parking.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: car_consumption_record
 * @Author: jeecg-boot
 * @Date:   2022-03-23
 * @Version: V1.0
 */
@Data
@TableName("car_consumption_record")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_consumption_record对象", description="car_consumption_record")
public class CarConsumptionRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	/**消费记录id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "消费记录id")
    private java.lang.String id;
	/**商户id*/
	@Excel(name = "信息收集id", width = 15)
    @ApiModelProperty(value = "信息收集id")
    private java.lang.String commercialTenantId;
	/**信息收集id*/
	@Excel(name = "信息收集id", width = 15)
    @ApiModelProperty(value = "信息收集id")
    private java.lang.String infoCollectId;
	/**减免金额*/
	@Excel(name = "减免金额", width = 15)
    @ApiModelProperty(value = "减免金额")
    private java.math.BigDecimal creditAmount;
	/**减免分钟数*/
	@Excel(name = "减免分钟数", width = 15)
    @ApiModelProperty(value = "减免分钟数")
    private java.lang.Integer creditMinute;
	/**支付时间*/
	@Excel(name = "支付时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "支付时间")
    private java.util.Date paymentTime;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String note;
}

package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.modules.parking.entity.CarCommercial;
import org.jeecg.modules.parking.entity.CarRechargeRecord;
import org.jeecg.modules.parking.mapper.CarRechargeRecordMapper;
import org.jeecg.modules.parking.service.ICarRechargeRecordService;
import org.jeecg.modules.parking.vo.CarRechargeRecordVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: car_recharge_record
 * @Author: jeecg-boot
 * @Date:   2022-03-23
 * @Version: V1.0
 */
@Service
public class CarRechargeRecordServiceImpl extends ServiceImpl<CarRechargeRecordMapper, CarRechargeRecord> implements ICarRechargeRecordService {

    @Autowired
    private CarCommercialServiceImpl commercialService;
    @Autowired
    private ISysDepartService sysDepartService;

    @Override
    public IPage<CarRechargeRecordVo> pageList(Page<CarRechargeRecord> page, QueryWrapper<CarRechargeRecordVo> queryWrapper, CarRechargeRecordVo carRechargeRecord) {
        QueryWrapper<CarRechargeRecord> carRechargeRecordQueryWrapper = new QueryWrapper<>();
        BeanUtils.copyProperties(queryWrapper,carRechargeRecordQueryWrapper);
        if(null!=carRechargeRecord.getPaymentTime()){
            //日期查询
            String paytime = carRechargeRecord.getPaymentTime();
            String[] split = paytime.split("#");
            Assert.notNull(split[0],"请输入开始日期");
            Assert.notNull(split[1],"请输入结束日期");
            carRechargeRecordQueryWrapper = carRechargeRecordQueryWrapper.gt("pay_time",split[0]).and(i->{
                i.lt("pay_time",split[1]);
            });
        }
        Page<CarRechargeRecord> recordPage = this.page(page, carRechargeRecordQueryWrapper);
        List<CarRechargeRecord> records = recordPage.getRecords();
        IPage<CarRechargeRecordVo> recordVoIPage = new Page<>();
        BeanUtils.copyProperties(recordPage, recordVoIPage);
        if(records.size()>0) {
            //避免重复请求数据库，一次性查询出商户信息
            List<String> ids = records.stream().map(CarRechargeRecord::getCommercialTenantId).collect(Collectors.toList());
            List<String> collect = ids.stream().distinct().collect(Collectors.toList());
            List<CarCommercial> carCommercials;
            if(!LoginUserUtil.isAdmin()) {
                SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
                if (sysDepart != null) {
                    carCommercials = commercialService.listByIds(collect).stream().filter(i -> i.getCompanyId().equals(sysDepart.getId())).collect(Collectors.toList());
                } else {
                    carCommercials = commercialService.listByIds(collect).stream().filter(i -> i.getCompanyId().equals("0")).collect(Collectors.toList());
                }
            }else {
                carCommercials = commercialService.listByIds(collect);
            }
            //转换为map，便于匹配数据
            Map<String, CarCommercial> collectMap = carCommercials.stream().collect(Collectors.toMap(CarCommercial::getId, i -> i));
            if(collectMap.size()>0) {
                List<CarRechargeRecordVo> carRechargeRecordVos = records.stream().map(i -> {
                    CarRechargeRecordVo recordVo = new CarRechargeRecordVo();
                    CarCommercial commercial = collectMap.get(i.getCommercialTenantId());
                    BeanUtils.copyProperties(commercial, recordVo);
                    BeanUtils.copyProperties(i, recordVo);
                    return recordVo;
                }).collect(Collectors.toList());
                recordVoIPage.setRecords(carRechargeRecordVos);
            }else {
                recordVoIPage.setRecords(new ArrayList<>());
            }
        }
        return recordVoIPage;
    }
}

package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarDeviceGroup;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CarDeviceGroupQuery extends CarDeviceGroup {
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;
}

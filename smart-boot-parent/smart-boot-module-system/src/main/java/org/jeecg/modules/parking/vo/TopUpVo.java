package org.jeecg.modules.parking.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class TopUpVo {

    //商户id
    private String id;
    //充值数值
    private BigDecimal rechargeValue;
    //退款金额
    private BigDecimal refundAmount;
    //充值状态
    private Boolean rechargeStatus;
    //充值类型
    private Integer rechargeType;

}

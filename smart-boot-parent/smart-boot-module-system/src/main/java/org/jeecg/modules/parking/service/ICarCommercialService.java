package org.jeecg.modules.parking.service;

import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarCommercial;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * @Description: car_commercial
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
public interface ICarCommercialService extends IService<CarCommercial> {

    void saveCarCommercial(CarCommercial carCommercial);

    void edit(CarCommercial carCommercial);
}

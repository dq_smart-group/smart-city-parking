package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class ShouFei {
    //本段最高收费
    private String zg;
    //价格
    private String jy;
    //分钟
    private String fz;
    //免费分钟
    private String sf;
    //起步价
    private String sfs;
    //开始时
    private Integer shour;
    //开始分
    private Integer smiu;
    //结束时间
    private Integer ohour;
    //结束时间
    private Integer omiu;
}

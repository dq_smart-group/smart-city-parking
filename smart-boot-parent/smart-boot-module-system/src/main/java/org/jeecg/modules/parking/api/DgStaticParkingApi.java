package org.jeecg.modules.parking.api;

import com.daqing.config.DongGuanStaticConfig;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.util.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/parking/dgstatic")
public class DgStaticParkingApi {


    @Autowired
    private RedisUtil redisUtil;


    @GetMapping("/updateDgStaticConfig")
    public void updateDgStaticConfig(){
        this.setDgStaticConfig();

    }

    public void setDgStaticConfig(){
        //再次写需要执行的方法
        DongGuanStaticConfig.setConfig( redisUtil.getStr(CacheConstant.DONG_GUAN_COM_ID),
                redisUtil.getStr(CacheConstant.DONG_GUAN_APP_ID),
                redisUtil.getStr(CacheConstant.DONG_GUAN_REQUEST_KEY),
                redisUtil.getStr(CacheConstant.DONG_GUAN_IS_ENVIRONMENT),
                redisUtil.getStr(CacheConstant.DONG_GUAN_DEV_REQUEST_HOST),
                redisUtil.getStr(CacheConstant.DONG_GUAN_PROD_REQUEST_HOST));

        DongGuanStaticConfig.setRequestUrl
                (redisUtil.getStr(CacheConstant.DONG_GUAN_PARKING_BASIS_INFO),
                redisUtil.getStr(CacheConstant.DONG_GUAN_PARKING_CHANNEL_BASIS_INFO),
                redisUtil.getStr(CacheConstant.DONG_GUAN_PARKING_IN_OUT_BASIS_INFO),
                redisUtil.getStr(CacheConstant.DONG_GUAN_CREATE_IMAGE_FILE),
                redisUtil.getStr(CacheConstant.DONG_GUAN_IN_OUT_PLATE_NUMBER_IMAGE_STREAM),
                redisUtil.getStr(CacheConstant.DONG_GUAN_ROAD_BASIS_INFO),
                redisUtil.getStr(CacheConstant.DONG_GUAN_ROAD_IN_OUT_INFO),
                redisUtil.getStr(CacheConstant.DONG_GUAN_SURPLUS_SYNC),
                redisUtil.getStr(CacheConstant.DOMG_GUAN_SYSTEM_HEART_DANCE));
    }

}

package org.jeecg.modules.parking.api;

public class classHexstringToByteUtil {
    public byte[] hexToByte(String hex) {
        /***先去掉16进制字符串的空格*/
        hex = hex.replace(" ", "");
        /***字节数组长度为16进制字符串长度的一半*/
        int byteLength = hex.length() / 2;
        byte[] bytes = new byte[byteLength];
        int m = 0;
        int n = 0;
        for (int i = 0; i < byteLength; i++) {
            m = i * 2 + 1;
            n = m + 1;
            int intHex = Integer.decode("0x" + hex.substring(i * 2, m) + hex.substring(m, n));
            bytes[i] = Byte.valueOf((byte) intHex);
        }
        return bytes;
    }

    /**
     * 将16进制转base64
     * @param hex
     * @return
     */
    public static byte[] hexString2Bytes(String hex) {
        hex = hex.replace(" ", "");
        if ((hex == null) || (hex.equals(""))) {
            return null;

        } else if (hex.length() % 2 != 0) {
            return null;

        } else {
            hex = hex.toUpperCase();
            int len = hex.length() / 2;
            byte[] b = new byte[len];
            char[] hc = hex.toCharArray();
            for (int i = 0; i < len; i++) {
                int p = 2 * i;

                b[i] = (byte) (charToByte(hc[p]) << 4 | charToByte(hc[p + 1]));
            }
            return b;
        }
    }
    private static byte charToByte(char c) {
        return (byte) "0123456789ABCDEF".indexOf(c);

    }


}

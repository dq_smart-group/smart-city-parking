package org.jeecg.modules.parking.controller;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.xkcoding.http.util.StringUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.parking.entity.CarRenewalHistory;
import org.jeecg.modules.parking.entity.CarWhiteList;
import org.jeecg.modules.parking.query.CarMonthlyRentQuery;
import org.jeecg.modules.parking.query.CarWhiteListQuery;
import org.jeecg.modules.parking.service.ICarWhiteListService;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.vo.CarWhiteListVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;

 /**
 * @Description: 车牌白名单表
 * @Author: jeecg-boot
 * @Date:   2021-12-02
 * @Version: V1.0
 */
@Api(tags="车牌白名单表")
@RestController
@RequestMapping("/parking/carWhiteList")
@Slf4j
public class CarWhiteListController extends JeecgController<CarWhiteList, ICarWhiteListService> {
	@Autowired
	private ICarWhiteListService carWhiteListService;

	 @Autowired
	 private ISysDepartService sysDepartService;
	 @Autowired
	 private RedisUtil redisUtil;
	 @Autowired
	 private IParkingLotService parkingLotService;
	 /**
	  * 白名单车牌查询
	  * @param carWhiteListQuery
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	@AutoLog(value = "车牌白名单表-分页列表查询")
	@ApiOperation(value="车牌白名单表-分页列表查询", notes="车牌白名单表-分页列表查询")
	@GetMapping(value = "/whitelist")
	public Result<?> queryPageWhiteList(CarWhiteListQuery carWhiteListQuery,
										@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
										@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
										HttpServletRequest req) {
		if (!LoginUserUtil.isAdmin()) {
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if (sysDepart != null) {
				carWhiteListQuery.setCompanyId(sysDepart.getId());
			} else {
				carWhiteListQuery.setCompanyId("0");
			}
		}
		carWhiteListQuery.setNeedAlarm("0");
		Page<CarWhiteList> page = new Page<CarWhiteList>(pageNo, pageSize);
		IPage<CarWhiteListVo> pageList = carWhiteListService.selectListPage(page, carWhiteListQuery);
		return Result.OK(pageList);
	}

	 @AutoLog(value = "车牌白名单表-分页列表查询")
	 @ApiOperation(value="车牌白名单表-分页列表查询", notes="车牌白名单表-分页列表查询")
	 @GetMapping(value = "/blacklist")
	 public Result<?> queryPageBlackList(CarWhiteListQuery carWhiteListQuery,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 carWhiteListQuery.setNeedAlarm("1");
		 Page<CarWhiteList> page = new Page<CarWhiteList>(pageNo, pageSize);
		 IPage<CarWhiteListVo> pageList = carWhiteListService.selectListPage(page, carWhiteListQuery);
		 return Result.OK(pageList);
	 }

	 /**
	 *   添加
	 *
	 * @param carWhiteList
	 * @return
	 */
	@AutoLog(value = "车牌白名单表-添加")
	@ApiOperation(value="车牌白名单表-添加", notes="车牌白名单表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarWhiteList carWhiteList) {
		Result result=carWhiteListService.addCarWhiteListsave(carWhiteList);

		return result;
	}
	
	/**
	 *  编辑
	 *
	 * @param carWhiteList
	 * @return
	 */
	@AutoLog(value = "车牌白名单表-编辑")
	@ApiOperation(value="车牌白名单表-编辑", notes="车牌白名单表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarWhiteList carWhiteList) {
		Result result=carWhiteListService.updateCarWhiteListById(carWhiteList);
		return result;
	}

	 /**
	  * 批量将黑名单转白名单
	  * @param map
	  * @return
	  */
	 @PutMapping("/editBatchList")
	 public Result<?> editBatchList(@RequestBody Map<String,String> map) {
		 //id值
		 String ids = map.get("id");
		 //上下架值
		 String value = map.get("value");
		 Assert.isTrue(StringUtil.isNotEmpty(ids),"参数异常");
		 Assert.isTrue(StringUtil.isNotEmpty(value),"参数异常");
		 //判断参数是否为字符串的0和1
		 if(!value.equals(CommonConstant.CURRENCY_STATUS_0) && !value.equals(CommonConstant.CURRENCY_STATUS_1)){
			 Assert.isTrue(false,"参数异常");
		 }
		 return carWhiteListService.needAlarm(ids,value);
	 }






	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "车牌白名单表-通过id删除")
	@ApiOperation(value="车牌白名单表-通过id删除", notes="车牌白名单表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		CarWhiteList byId = carWhiteListService.getById(id);
		redisUtil.del(ParkingRedisConstant.PARKING_WHITE_LIST+byId.getParkingLotId()+":"+byId.getPlate());
		carWhiteListService.removeById(id);
		carWhiteListService.uploadTheCloud(
				byId.getEnableTime(),
				byId.getOverdueTime(),
				byId.getParkingLotId(),
				false,
				byId.getPlate(),
				byId.getEnable(),
				byId.getNeedAlarm());
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "车牌白名单表-批量删除")
	@ApiOperation(value="车牌白名单表-批量删除", notes="车牌白名单表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		String[] split = ids.split(",");
		if(split.length>0){
			for (String s : split) {
				CarWhiteList byId = carWhiteListService.getById(s);
				redisUtil.del(ParkingRedisConstant.PARKING_WHITE_LIST+byId.getParkingLotId()+":"+byId.getPlate());
				carWhiteListService.uploadTheCloud(
						byId.getEnableTime(),
						byId.getOverdueTime(),
						byId.getParkingLotId(),
						false,
						byId.getPlate(),
						byId.getEnable(),
						byId.getNeedAlarm());
			}
		}
		this.carWhiteListService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}



	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "车牌白名单表-通过id查询")
	@ApiOperation(value="车牌白名单表-通过id查询", notes="车牌白名单表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarWhiteList carWhiteList = carWhiteListService.getById(id);
		if(carWhiteList==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carWhiteList);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carWhiteList
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarWhiteListQuery carWhiteList) {
        return carWhiteListService.exportXls(request, carWhiteList, CarWhiteList.class, "车牌白名单表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return carWhiteListService.importExcel(request, response, CarWhiteList.class);
    }



	 /**
	  * 是否生效
	  * @param map
	  * @return
	  */
	 @PutMapping("/SwitchChangeClick")
	 public Result<?> SwitchChangeClick(@RequestBody Map<String,String> map){
		 String ids = map.get("ids");
		 String value = map.get("value");
		 String needAlarm =map.get("needAlarm");
		 Assert.isTrue((!StringUtils.isEmpty(ids)),"参数异常");
		 Assert.isTrue((!StringUtils.isEmpty(value)),"参数异常");
		 Assert.isTrue((!StringUtils.isEmpty(needAlarm)),"参数异常");
		 if(!value.equals(CommonConstant.STATUS_0) && !value.equals(CommonConstant.STATUS_1)){
			 Assert.isTrue(false,"参数异常");
		 }
		 return carWhiteListService.SwitchChangeClick(ids,value,needAlarm);
	 }

	 /**
	  * 是否上传设备
	  * @param map
	  * @return
	  */
	 @PutMapping("/SwitchChangeShow")
	 public Result<?> SwitchChangeShow(@RequestBody Map<String,String> map){
		 String ids = map.get("ids");
		 String value = map.get("value");
		 String needAlarm =map.get("needAlarm");
		 Assert.isTrue((!StringUtils.isEmpty(ids)),"参数异常");
		 Assert.isTrue((!StringUtils.isEmpty(value)),"参数异常");
		 Assert.isTrue((!StringUtils.isEmpty(needAlarm)),"参数异常");
		 if(!value.equals(CommonConstant.CAR_uploadDevice_IS_CLICK_TRUE) && !value.equals(CommonConstant.CAR_uploadDevice_IS_CLICK_FLASE)){
			 Assert.isTrue(false,"参数异常");
		 }
		 Boolean value1 =Boolean.valueOf(value);
		 return carWhiteListService.SwitchChangeShow(ids,value1,needAlarm);
	 }

	 /**
	  * 是否在线续费
	  * @return
	  */
	 @PostMapping("/RenewalChangeShow")
	 public Result<?> RenewalChangeShow(@RequestBody Map<String,String> map){
		 String id = map.get("id");
		 Boolean value = Boolean.valueOf(map.get("value"));
		 Assert.isTrue((!StringUtils.isEmpty(id)),"参数异常");
		 return carWhiteListService.RenewalChangeShow(id,value);
	 }

	 /**
	  * 计算金额
	  */
	 @PostMapping("/CalculateAmount")
	 public Result<?> CalculateAmount(@RequestBody Map<String,String> map){
		 Assert.isTrue(null!=map.get("renewalDuration"),"时长不能为空");
		 Integer renewalDuration = Integer.valueOf(map.get("renewalDuration"));
		 String parkingLotId = map.get("parkingLotId");
		 String czorxf = map.get("czorxf");
		 String plateTypeId = map.get("plateTypeId");
		 CarMonthlyRentQuery carMonthlyRentQuery = new CarMonthlyRentQuery();
		 carMonthlyRentQuery.setParkingLotId(parkingLotId);
		 carMonthlyRentQuery.setCzorxf(czorxf);
		 carMonthlyRentQuery.setPlateType(plateTypeId);
		 carMonthlyRentQuery.setRenewalDuration(renewalDuration);
		 BigDecimal moneyByRenewalDuration = parkingLotService.getMoneyByRenewalDuration(carMonthlyRentQuery);
		 return Result.OK(moneyByRenewalDuration);
	 }

	 /**
	  * 续费：
	  * 	1、更改过期时间
	  * 	2、生成记录表
	  */
	 @PostMapping("/Renewal")
	 public Result<?> Renewal(@RequestBody CarRenewalHistory carRenewalHistory){
		 return carWhiteListService.Renewal(carRenewalHistory);
	 }


 }

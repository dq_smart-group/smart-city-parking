package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.LedDisplay;

@Data
public class LedDisplayVo extends LedDisplay {
    //设备名称
    private String name;
    //设备序列号
    private String serialNumber;
    //公司名称
    private String companyName;
    //车场名称
    private String parkingName;
    private String parkingLotId;
    private String deviceGroupId;

    private String carSentryBoxId;
    private String isoffline;
}

package org.jeecg.modules.parking.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 车牌白名单表
 * @Author: jeecg-boot
 * @Date:   2021-12-02
 * @Version: V1.0
 */
@Data
@TableName("car_white_list")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_white_list对象", description="车牌白名单表")
public class CarWhiteList implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**车牌*/
	@Excel(name = "车牌", width = 15)
    @ApiModelProperty(value = "车牌")
    private java.lang.String plate;
	/**是否黑名单*/
//	@Excel(name = "是否黑名单,0:白名单，1黑名单", width = 15)
    @ApiModelProperty(value = "是否黑名单")
    private java.lang.String needAlarm;
	/**是否有效*/
//	@Excel(name = "是否有效", width = 15)
    @ApiModelProperty(value = "是否有效")
    @Dict(dicCode = "sx_enable")
    private java.lang.String enable;
	/**是否有效*/
//	@Excel(name = "是否在线续费", width = 15)
    @ApiModelProperty(value = "是否在线续费")
    private Boolean onlineRenewal;
    /**车牌类型*/
    @Excel(name = "车牌类型", width = 15)
    @ApiModelProperty(value = "车牌类型")
    private java.lang.String plateTypeId;
    /**停车类型：0：日租、1：月租、2：年租*/
//    @Excel(name = "停车类型 ，0：日租，1：月租,2:年租", width = 15)
    @Dict(dicCode = "park_type")
    @ApiModelProperty(value = "停车类型 ，0：日租，1：月租,2:年租")
    private java.lang.String parkType;
	/**当前名单生效时间*/
	@Excel(name = "当前名单生效时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "当前名单生效时间")
    private java.util.Date enableTime;
	/**当前名单过期时间*/
	@Excel(name = "当前名单过期时间", width = 30, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "当前名单过期时间")
    private java.util.Date overdueTime;
    /**公司id**/
//    @Excel(name = "公司id", width = 15)
    @ApiModelProperty(value = "公司id")
    private java.lang.String companyId;
    /**车场id*/
//    @Excel(name = "车场id", width = 15)
    @ApiModelProperty(value = "车场id")
    private String parkingLotId;

    /**
     * 白名单是否上传设备
     */
    @ApiModelProperty(value = "白名单是否上传设备")
    private Boolean uploadDevice;
    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id")
    private String userId;
    /**
     * 电话
     */
    @Excel(name = "电话", width = 15)
    @ApiModelProperty(value = "电话")
    private String phone;

    /**
     * 公司编码
     */
    @TableField(exist = false)
    @Excel(name = "公司编码", width = 15)
    @ApiModelProperty(value = "公司编码")
    private String orgCode;

    /**
     * 车场编码
     */
    @TableField(exist = false)
    @Excel(name = "车场编码", width = 15)
    @ApiModelProperty(value = "车场编码")
    private String carIdentifier;
}

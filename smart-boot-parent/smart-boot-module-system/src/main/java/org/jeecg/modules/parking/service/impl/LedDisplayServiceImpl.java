package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.entity.LedDisplay;
import org.jeecg.modules.parking.mapper.LedDisplayMapper;
import org.jeecg.modules.parking.mqtt.MqttPushClient;
import org.jeecg.modules.parking.query.LedDisplayQuery;
import org.jeecg.modules.parking.service.ILedDisplayService;
import org.jeecg.modules.parking.vo.LedDisplayVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description: led显示操作表
 * @Author: jeecg-boot
 * @Date:   2022-01-05
 * @Version: V1.0
 */
@Service
public class LedDisplayServiceImpl extends ServiceImpl<LedDisplayMapper, LedDisplay> implements ILedDisplayService {
    @Autowired
    private ICameraDeviceService cameraDeviceService;

    @Autowired
    private ILedDisplayService ledDisplayService;

    @Autowired
    private RedisUtil redisUtil;
    @Override
    public IPage<LedDisplayVo> pageList(Page<LedDisplayVo> page, LedDisplayQuery ledDisplayQuery) {
        return page.setRecords(this.baseMapper.pageList(page,ledDisplayQuery));
    }

    /**
     * 添加设备
     * @param ledDisplay
     */
    @Override
    public Result<?> saveLedDisplay(LedDisplay ledDisplay) {
        //调整音量大小
        if(ledDisplay.getVolume()!=null){
            CameraDevice byId1 = cameraDeviceService.getById(ledDisplay.getEquipmentId());
            String topic = MqttPushClient.getTopic(byId1.getSerialno());
            MqttPushClient.pushVolume(topic,ledDisplay.getVolume());
        }
        //查重
        LambdaQueryWrapper<LedDisplay> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(LedDisplay::getEquipmentId,ledDisplay.getEquipmentId());
        List<LedDisplay> ledDisplays = this.baseMapper.selectList(wrapper);
        if(ledDisplays.size()>0){
            return Result.error("已添加有设备");
        }else{
            this.baseMapper.insert(ledDisplay);
            redisUtil.set(ParkingRedisConstant.PARKING_LED+ledDisplay.getEquipmentId(),ledDisplay);

            return Result.OK("添加成功！");
        }
    }

    @Override
    public Result<?> updateLedDisplayById(LedDisplay ledDisplay) {
        LedDisplay byId = this.baseMapper.selectById(ledDisplay.getId());
        if(byId.getVolume()!=ledDisplay.getVolume()){
            //调整音量大小
            CameraDevice byId1 = cameraDeviceService.getById(ledDisplay.getEquipmentId());
            String topic = MqttPushClient.getTopic(byId1.getSerialno());
            MqttPushClient.pushVolume(topic,ledDisplay.getVolume());
        }
        //查重
        LambdaQueryWrapper<LedDisplay> wrapper = new LambdaQueryWrapper<>();
        wrapper.ne(LedDisplay::getId,ledDisplay.getId());
        wrapper.eq(LedDisplay::getEquipmentId,ledDisplay.getEquipmentId());
        List<LedDisplay> ledDisplays = this.baseMapper.selectList(wrapper);
        if(ledDisplays.size()>0){
            return Result.error("已添加有设备");
        }else{
            redisUtil.del(ParkingRedisConstant.PARKING_LED+byId.getEquipmentId());
            this.baseMapper.updateById(ledDisplay);
            redisUtil.set(ParkingRedisConstant.PARKING_LED+ledDisplay.getEquipmentId(),ledDisplay);
            return Result.OK("修改成功！");
        }
    }

    @Override
    public List<LedDisplay> getList() {
        return this.baseMapper.selectList(null);
    }

    /**
     * 定时修改led屏幕的声音和亮度
     */
    @Override
    public void updateVoiceAndBrightness() {
        List<CameraDevice> cameraDevices = cameraDeviceService.getList();
        List<CameraDevice> cameraDevices1 = new ArrayList<>();
        for (CameraDevice cameraDevice : cameraDevices) {
            //主设备
            if(CommonConstant.PARKING_DEVICE_TYPE_0.equals(cameraDevice.getRepairFlag())){
                cameraDevices1.add(cameraDevice);
            }
        }
        List<LedDisplay> list = ledDisplayService.getList();
        for (CameraDevice cameraDevice : cameraDevices1) {
            Integer sum = 50;//音量大小
            Integer sums = 50;//屏幕亮度
            for (LedDisplay ledDisplay : list) {
                if(cameraDevice.getId().equals(ledDisplay.getEquipmentId())){
                    if(ledDisplay.getNigntBrightness()!=null){
                        sums = ledDisplay.getNigntBrightness();
                    }
                    if(ledDisplay.getNigntVolumeSize()!=null){
                        sum = ledDisplay.getNigntVolumeSize();
                    }
                }
            }
            //调整音量大小
            MqttPushClient.pushVolume(MqttPushClient.getTopic(cameraDevice.getSerialno()),sum);
            MqttPushClient.adjustBrightness(MqttPushClient.getTopic(cameraDevice.getSerialno()),sums);
        }
    }

    /**
     * 早上定时修改led屏幕的声音和亮度
     */
    @Override
    public void updateVoiceAndBrightnessMorning() {
        List<CameraDevice> cameraDevices = cameraDeviceService.getList();
        List<CameraDevice> cameraDevices1 = new ArrayList<>();
        for (CameraDevice cameraDevice : cameraDevices) {
            //主设备
            if(CommonConstant.PARKING_DEVICE_TYPE_0.equals(cameraDevice.getRepairFlag())){
                cameraDevices1.add(cameraDevice);
            }
        }
        List<LedDisplay> list = ledDisplayService.getList();
        for (CameraDevice cameraDevice : cameraDevices1) {
            Integer sum = 100;//音量大小
            Integer sums = 100;//屏幕亮度
            for (LedDisplay ledDisplay : list) {
                if(cameraDevice.getId().equals(ledDisplay.getEquipmentId())){
                    if(ledDisplay.getNigntVolumeSize()!=null){
                        sum = ledDisplay.getVolume();
                    }
                }
            }
            //调整音量大小
            MqttPushClient.pushVolume(MqttPushClient.getTopic(cameraDevice.getSerialno()),sum);
            MqttPushClient.adjustBrightness(MqttPushClient.getTopic(cameraDevice.getSerialno()),sums);
        }
    }
}

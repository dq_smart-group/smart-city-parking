package org.jeecg.modules.parking.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.jeecg.modules.parking.entity.CarRenewalHistory;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class CarRenewalHistoryVo extends CarRenewalHistory {

    /**车牌*/
    @Excel(name = "车牌", width = 15)
    private java.lang.String plate;

    /**当前名单过期时间*/
    @Excel(name = "当前名单过期时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.util.Date overdueTime;

}

package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarTimeSlot;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CarTimeSlotQuery extends CarTimeSlot {
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;
    private String tem;
    /**
     * 设备是入口还是出口
     */
    private String flag;
}

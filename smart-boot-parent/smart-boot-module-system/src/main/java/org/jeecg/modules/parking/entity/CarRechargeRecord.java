package org.jeecg.modules.parking.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: car_recharge_record
 * @Author: jeecg-boot
 * @Date:   2022-03-23
 * @Version: V1.0
 */
@Data
@TableName("car_recharge_record")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_recharge_record对象", description="car_recharge_record")
public class CarRechargeRecord implements Serializable {
    private static final long serialVersionUID = 1L;

	/**充值记录id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "充值记录id")
    private java.lang.String id;
	/**商户id*/
	@Excel(name = "商户id", width = 15)
    @ApiModelProperty(value = "商户id")
    private java.lang.String commercialTenantId;
	/**余额*/
	@Excel(name = "余额", width = 15)
    @ApiModelProperty(value = "余额")
    private java.math.BigDecimal balance;
	/**充值金额*/
	@Excel(name = "充值金额", width = 15)
    @ApiModelProperty(value = "充值金额")
    private java.math.BigDecimal rechargeAmount;
	/**充值分钟*/
	@Excel(name = "充值分钟", width = 15)
    @ApiModelProperty(value = "充值分钟")
    private java.lang.Integer rechargeMinute;
	/**支付时间*/
	@Excel(name = "支付时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "支付时间")
    private java.util.Date payTime;
}

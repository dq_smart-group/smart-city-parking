package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarStandard;
import org.jeecg.modules.parking.query.ShouFei;
import org.jeecg.modules.parking.query.TwentyFourShouFei;

import java.util.List;

@Data
public class CarStandardVo extends CarStandard {
    //车场名称
    private String parkingName;
    //公司名称
    private String companyName;
    //车牌类型名称
    private String carTypeName;
    //分段收费集合
    private List<ShouFei> ShouFeiList;
    //24小时收费集合
    private List<TwentyFourShouFei> TwentyFourShouFeiList;
    private List<String> licensePlateTypes;
}

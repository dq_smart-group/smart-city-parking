package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class HttpPlateResultQuery {
    private String imageFile;
    private String imageFragmentFile;
    private String license;
    private String plateid;
}

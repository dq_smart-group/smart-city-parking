package org.jeecg.modules.parking.service.impl;

import cn.hutool.core.date.DateField;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.entity.*;
import org.jeecg.modules.parking.mapper.CarStandardMapper;
import org.jeecg.modules.parking.query.CarStandardQuery;
import org.jeecg.modules.parking.query.MerchantAmountQuery;
import org.jeecg.modules.parking.query.ShouFei;
import org.jeecg.modules.parking.query.TwentyFourShouFei;
import org.jeecg.modules.parking.service.*;
import org.jeecg.modules.parking.vo.CarStandardVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDataLogService;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 收费标准表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
@Service
public class CarStandardServiceImpl extends ServiceImpl<CarStandardMapper, CarStandard> implements ICarStandardService {

    @Autowired
    private ICarCardTypeService carCardTypeService;
    @Autowired
    private ICarStandardService carStandardService;
    @Autowired
    private ISysDataLogService sysDataLogService;
    @Autowired
    private ICarWhiteListService carWhiteListService;
    @Autowired
    private ILedDisplayService ledDisplayService;
    @Autowired
    private ICarTimeSlotService carTimeSlotService;
    @Autowired
    private ICameraDeviceService cameraDeviceService;

    @Autowired
    private ISysDepartService sysDepartService;

    @Autowired
    private IParkingLotService parkingLotService;

    @Autowired
    private ICarChargeService carChargeService;


    @Autowired
    private RedisUtil redisUtil;
    @Override
    public IPage<CarStandardVo> pageList(Page<CarStandardVo> page, CarStandardQuery carStandardQuery) {
        List<CarStandardVo> list = this.baseMapper.pageList(page,carStandardQuery);
        if(list.size()>0){
            for (CarStandardVo carStandardVo : list) {
                if(carStandardVo.getLicensePlateType()!=null){
                    String[] split = carStandardVo.getLicensePlateType().split(",");
                    if(split.length>0){
                        List<String> LicensePlateTypes = new ArrayList<>();
                        for (String s : split) {
                            CarCardType byId = carCardTypeService.getById(s);
                            if(byId!=null){
                                String carTypeName = carStandardVo.getCarTypeName();
                                if(StringUtils.isNotEmpty(carTypeName)){
                                    carTypeName = carTypeName +"，"+byId.getName();
                                }else{
                                    carTypeName = byId.getName();
                                }
                                carStandardVo.setCarTypeName(carTypeName);
                            }
                            LicensePlateTypes.add(s);
                        }
                        carStandardVo.setLicensePlateTypes(LicensePlateTypes);
                    }
                }
            }
        }
        List<CarStandardVo> collect = list.stream().map(item -> {
            if(item.getChargeType().equals("3")){
                if (item.getHourlyChargeType().equals("0")) {
                    String hourlyCharge = item.getHourlyCharge();
                    JSONArray jsonArray = JSONArray.fromObject(hourlyCharge);
                    if (jsonArray != null) {
                        List<ShouFei> shouFeiList = new ArrayList<>();
                        for (Object o : jsonArray) {
                            JSONObject jsonObject = JSONObject.fromObject(o);
                            ShouFei shouFei = (ShouFei) JSONObject.toBean(jsonObject, ShouFei.class);
                            shouFeiList.add(shouFei);
                        }
                        item.setShouFeiList(shouFeiList);
                    }
                }else if(item.getHourlyChargeType().equals("1")){
                    String hourlyCharge = item.getHourlyCharge();
                    JSONArray jsonArray = JSONArray.fromObject(hourlyCharge);
                    if (jsonArray != null) {
                        List<TwentyFourShouFei> twentyFourShouFeis = new ArrayList<>();
                        for (Object o : jsonArray) {
                            JSONObject jsonObject = JSONObject.fromObject(o);
                            TwentyFourShouFei twentyFourShouFei = (TwentyFourShouFei) JSONObject.toBean(jsonObject, TwentyFourShouFei.class);
                            twentyFourShouFeis.add(twentyFourShouFei);
                        }
                        item.setTwentyFourShouFeiList(twentyFourShouFeis);
                    }
                }
            }
            return item;
        }).collect(Collectors.toList());
        return page.setRecords(collect);
    }

    /**
     * 根据车场id和车牌类型id查询收费规则
     * @param parkingId 车场id
     * @param typeId 车牌类型id
     * @return
     */
    @Override
    public CarStandard getCarStandard(String parkingId, String typeId) {
        return this.getCarStandards(parkingId,typeId,"3");
    }

    /**
     * 根据车场id和车牌类型id查询收费规则
     * @param parkingId 车场id
     * @param typeId 车牌类型id
     * @return
     */
    public CarStandard getCarStandards(String parkingId, String typeId,String chargeTyp) {
        LambdaQueryWrapper<CarStandard> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CarStandard::getEnableFlag,true);
        wrapper.eq(CarStandard::getParkingLotId,parkingId);
        wrapper.eq(CarStandard::getChargeType,chargeTyp);
        wrapper.last("and find_in_set("+typeId+",license_plate_type) limit 1");
        List<CarStandard> list = this.list(wrapper);
        if(list.size()>0){
            return list.get(0);
        }
        return null;
    }


    /**
     * 新增收费标准
     * @param carStandard
     */
    @Override
    public Result saveCarStandard(CarStandardQuery carStandard) {
        if(carStandard.getEnableFlag()){
            List<String> licensePlateTypes = carStandard.getLicensePlateTypes();
            if(licensePlateTypes.size()>0){
                //判断收费标准里面是否重复的车牌类型的收费标准
                for (String licensePlateType : licensePlateTypes) {
                    carStandard.setLicensePlateType(licensePlateType);
                    List<CarStandard> carStandards = selectListByLicensePlateTypes(carStandard);
                    if(carStandards.size()>0){
                        return Result.error("同车场下不能有两个收费类型和车牌类型相同的收费规则同时启用,请检查");
                    }
                }
            }
            addById(carStandard);
            String[] split = carStandard.getLicensePlateType().split(",");
            for (String s : split) {
                CarStandard carStandard1 = this.baseMapper.selectById(carStandard.getId());
                redisUtil.set(ParkingRedisConstant.PARKING_STANDARD+carStandard.getParkingLotId()+":"+s,carStandard1);
            }
            sysDataLogService.addDataLog("car_standard", carStandard.getId(), JSON.toJSONString(carStandard));
            return Result.OK("添加成功");

        }else{
            addById(carStandard);
            String[] split = carStandard.getLicensePlateType().split(",");
            for (String s : split) {
                CarStandard carStandard1 = this.baseMapper.selectById(carStandard.getId());
                redisUtil.set(ParkingRedisConstant.PARKING_STANDARD+carStandard.getParkingLotId()+":"+s,carStandard1);
            }
            sysDataLogService.addDataLog("car_standard", carStandard.getId(), JSON.toJSONString(carStandard));
            return Result.OK("添加成功");
        }
    }
    //添加保存数据
    public void addById(CarStandardQuery carStandard){
        List<String> licensePlateTypes = carStandard.getLicensePlateTypes();
        if(licensePlateTypes.size()>0){
            String licensePlateTypeList = "";
            for (String licensePlateType : licensePlateTypes) {
                if(licensePlateTypeList!=""){
                    licensePlateTypeList = licensePlateTypeList+","+licensePlateType;
                }else{
                    licensePlateTypeList = licensePlateType;
                }
            }
            carStandard.setLicensePlateType(licensePlateTypeList);
        }
        this.baseMapper.insert(carStandard);
    }

    public List<CarStandard> selectListByLicensePlateTypes(CarStandardQuery carStandard){
        QueryWrapper<CarStandard> wrapper = new QueryWrapper<>();
        wrapper.eq("enable_flag",true);
        wrapper.eq("parking_lot_id",carStandard.getParkingLotId());
        wrapper.eq("charge_type",carStandard.getChargeType());
        if(StringUtils.isNotEmpty(carStandard.getId())){
            wrapper.ne("id",carStandard.getId());
        }
        wrapper.last("and find_in_set("+carStandard.getLicensePlateType()+",license_plate_type)");
        List<CarStandard> carStandards = this.baseMapper.selectList(wrapper);
        return carStandards;
    }

    //修改收费标准
    @Override
    public Result updateCarStandardById(CarStandardQuery carStandard) {
        if(carStandard.getEnableFlag()){
            List<String> licensePlateTypes = carStandard.getLicensePlateTypes();
            if(licensePlateTypes.size()>0){
                //判断收费标准里面是否重复的车牌类型的收费标准
                for (String licensePlateType : licensePlateTypes) {
                    carStandard.setLicensePlateType(licensePlateType);
                    List<CarStandard> carStandards = selectListByLicensePlateTypes(carStandard);
                    if(carStandards.size()>0){
                        return Result.error("同车场下不能有两个收费类型和车牌类型相同的收费规则同时启用,请检查");
                    }
                }
            }
            CarStandard carStandard1 = this.baseMapper.selectById(carStandard.getId());
            String[] split1 = carStandard1.getLicensePlateType().split(",");
            for (String s : split1) {
                redisUtil.del(ParkingRedisConstant.PARKING_STANDARD+carStandard.getParkingLotId()+":"+s);
            }
            updateById(carStandard);
            String[] split = carStandard.getLicensePlateType().split(",");
            CarStandard carStandard2 = new CarStandard();
            BeanUtils.copyProperties(carStandard,carStandard2);
            for (String s : split) {
                redisUtil.set(ParkingRedisConstant.PARKING_STANDARD+carStandard.getParkingLotId()+":"+s,carStandard2);
            }
            sysDataLogService.addDataLog("car_standard", carStandard.getId(), JSON.toJSONString(carStandard));
            return Result.OK("修改成功");

        }else{
            CarStandard carStandard1 = this.baseMapper.selectById(carStandard.getId());
            String[] split1 = carStandard1.getLicensePlateType().split(",");
            for (String s : split1) {
                redisUtil.del(ParkingRedisConstant.PARKING_STANDARD+carStandard.getParkingLotId()+":"+s);
            }
            updateById(carStandard);
            String[] split = carStandard.getLicensePlateType().split(",");
            CarStandard carStandard2 = new CarStandard();
            BeanUtils.copyProperties(carStandard,carStandard2);
            for (String s : split) {
                redisUtil.set(ParkingRedisConstant.PARKING_STANDARD+carStandard.getParkingLotId()+":"+s,carStandard2);
            }
            sysDataLogService.addDataLog("car_standard", carStandard.getId(), JSON.toJSONString(carStandard));
            return Result.OK("修改成功");
        }
    }
    //修改保存数据
    public void updateById(CarStandardQuery carStandard){
        List<String> licensePlateTypes = carStandard.getLicensePlateTypes();
        if(licensePlateTypes.size()>0){
            String licensePlateTypeList = "";
            for (String licensePlateType : licensePlateTypes) {
                if(licensePlateTypeList!=""){
                    licensePlateTypeList = licensePlateTypeList+","+licensePlateType;
                }else{
                    licensePlateTypeList = licensePlateType;
                }
            }
            carStandard.setLicensePlateType(licensePlateTypeList);
        }
        this.baseMapper.updateById(carStandard);
    }
    @Override
    public BigDecimal getMoney(String carStandardId, Date rt, Date ct,Boolean sflag) throws Exception{
        CarStandard carStandard = carStandardService.getById(carStandardId);
        //小时收费类型（1：24小时收费  0：分段收费）
        String hourlyChargeType = carStandard.getHourlyChargeType();
        //全天最高收费
        BigDecimal highestMoney = carStandard.getHighestMoney();
        //免费分钟
        Integer freeMinutes = carStandard.getFreeMinutes();
        //免费分钟是否参与收费
        Boolean chargingFlag = carStandard.getChargingFlag();
        //收费规则
        String hourlyCharge = carStandard.getHourlyCharge();
        //作用:当为true的时候，如果当前时间段不够一个单位则按一个单位算，但算多出来的时间如果下个时间段收费的话，这个时间就不会算钱
        Boolean enablesFlag = carStandard.getEnablesFlag();

        //入场时间
        Date date = new Date();
        Date date1 = rt;
        Date date2 = ct;

        //要交费的金额
        BigDecimal money = new BigDecimal(0);
        //计算出进场时间和出场时间相差多少天
        int i = DateUtils.daysBetween(date1, date2);
        //入场时
        Integer rshour = Integer.parseInt(DateUtils.formatShorthour(date1));
        //入场分
        Integer rsmiu = Integer.parseInt(DateUtils.formatShortmiu(date1));
        //入场时间
        int rtimes = rshour * 60 + rsmiu;
        //出场时
        Integer cshour = Integer.parseInt(DateUtils.formatShorthour(date2));
        //出场分
        Integer csmiu = Integer.parseInt(DateUtils.formatShortmiu(date2));
        //出场时间
        int ctimes = cshour * 60 + csmiu+24*i*60;
        //总时长
        int times = ctimes - rtimes;
        if(times<=0){
            return new BigDecimal("0");
        }
        //分段收费
        if(hourlyChargeType.equals("0")){
            JSONArray jsonArray = JSONArray.fromObject(hourlyCharge);
            if (jsonArray != null) {
                //转换为实体类后的收费规则
                List<ShouFei> shouFeiList = new ArrayList<>();
                for (Object o : jsonArray) {
                    JSONObject jsonObject = JSONObject.fromObject(o);
                    ShouFei shouFei = (ShouFei) JSONObject.toBean(jsonObject, ShouFei.class);
                    shouFeiList.add(shouFei);
                }
                //是否减去了免费时间
                boolean flag = false;
                //是否已经添加了起步价
                boolean qflag = false;
                for (int m = 0; m < i+2; m++) {
                    for (ShouFei shouFei : shouFeiList) {
                        //开始时
                        Integer shour = shouFei.getShour();
                        //开始分
                        Integer smiu = shouFei.getSmiu();
                        //计费开始时间
                        int stimes = shour * 60 + smiu+24*(m-1)*60;
                        //结束时
                        Integer ohour = shouFei.getOhour();
                        //结束分
                        Integer omiu = shouFei.getOmiu();
                        //计费结束时间
                        int otimes = 0;
                        if(shour>ohour){
                            otimes = ohour * 60 + omiu+24*(m-1)*60+24*60;
                        }else{
                            otimes = ohour * 60 + omiu+24*(m-1)*60;
                        }
                        //需要算费的时间
                        int i2 = otimes - stimes;

                        //免费分钟
                        String sf = shouFei.getSf();
                        //起步价
                        String sfs = shouFei.getSfs();
                        //本段最高收费
                        String zg = shouFei.getZg();
                        //价格
                        String jy = shouFei.getJy();
                        //分钟
                        String fz = shouFei.getFz();
                        //减去不算费的时间
                        int i1 = 0;
                        if(stimes<rtimes){
                            i1 = ctimes - rtimes;
                        }else{
                            i1 = ctimes - stimes;
                        }
                        //判断入场时间是否在这段收费时间里
                        if(rtimes>otimes){
                            continue;
                        }
                        //如果入场时间小于开始时间
                        if(rtimes<stimes){
                            //减去免费分钟
                            int i3 = 0;
                            if(i1>i2){
                                i3 = i2;
                            }else{
                                i3 = i1;
                            }
                            if(!chargingFlag){
                                if(!flag){
                                    flag = true;
                                    if(sflag){
                                        i3 = i3 - Integer.parseInt(sf);
                                    }

                                }
                            }

                            if(i3<0){
                                if(i1>i2){
                                    rtimes = otimes-i3;
                                    continue;
                                }else{
                                    return money;
                                }

                            }
                            //times = times - stimes;
                            //单价次数
                            int c = 0;
                            if(i3%Integer.parseInt(fz)!=0){
                                c = i3/Integer.parseInt(fz)+1;
                                //更新入场时间为收费的结束时间
                                if(enablesFlag){
                                    rtimes = otimes+Integer.parseInt(fz)-i3%Integer.parseInt(fz);
                                }
                            }else{
                                c = i3/Integer.parseInt(fz);
                                //更新入场时间为收费的结束时间
                                rtimes = otimes;
                            }
                            BigDecimal dmoney = new BigDecimal(0);
                            BigDecimal bigJy = new BigDecimal(jy);
                            if(!qflag){
                                qflag = true;
                                BigDecimal bigSfs = new BigDecimal(sfs);
                                //dmoney = bigSfs;
                                dmoney = bigSfs.add(BigDecimal.valueOf(c-1).multiply(bigJy));
                            }else{
                                dmoney = BigDecimal.valueOf(c).multiply(bigJy);
                            }
                            //是否超出本段收费的最高收费
                            BigDecimal bigZg = new BigDecimal(zg);
                            if(dmoney.compareTo(bigZg)==1){
                                money = money.add(bigZg);
                            }else{
                                money = money.add(dmoney);
                            }
                            if(!(i1>i2)){
                                //结束
                                return money;
                            }

                        }else if(rtimes>=stimes){//入场时间大于计费开始时间
                            int i4 = i2 - (rtimes - stimes);
                            //减去免费分钟
                            int i3 = 0;
                            if(i1>i4){
                                i3 = i4;
                            }else{
                                i3 = i1;
                            }
                            if(!chargingFlag){
                                if(!flag){
                                    flag = true;
                                    if(sflag){
                                        i3 = i3 - Integer.parseInt(sf);
                                    }
                                }
                            }

                            if(i3<0){
                                if(i1>i4){
                                    rtimes = otimes-i3;
                                    continue;
                                }else{
                                    return money;
                                }
                            }
                            //单价次数
                            int c = 0;
                            if(i3%Integer.parseInt(fz)!=0){
                                c = i3/Integer.parseInt(fz)+1;
                                //更新入场时间为收费的结束时间
                                if(enablesFlag){
                                    rtimes = otimes+Integer.parseInt(fz)-i3%Integer.parseInt(fz);
                                }
                            }else{
                                c = i3/Integer.parseInt(fz);
                                //更新入场时间为收费的结束时间
                                rtimes = otimes;

                            }
                            BigDecimal dmoney = new BigDecimal(0);
                            BigDecimal bigSfs = new BigDecimal(sfs);
                            BigDecimal bigJy = new BigDecimal(jy);
                            if(!qflag){
                                qflag = true;
                                dmoney = bigSfs.add(BigDecimal.valueOf(c-1).multiply(bigJy));
                            }else{
                                dmoney = BigDecimal.valueOf(c).multiply(bigJy);
                            }
                            //是否超出本段收费的最高收费
                            BigDecimal bigZg = new BigDecimal(zg);
                            if(dmoney.compareTo(bigZg)==1){
                                money = money.add(bigZg);
                            }else{
                                money = money.add(dmoney);
                            }
                            //结束
                            if(!(i1>i4)){
                                //return
                                return money;
                            }
                        }
                    }
                }
                //System.out.println("总金额a"+money);
            }
        }else{//24小时收费
            JSONArray jsonArray = JSONArray.fromObject(hourlyCharge);
            if (jsonArray != null) {
                List<TwentyFourShouFei> list = new ArrayList<>();
                for (Object o : jsonArray) {
                    JSONObject jsonObject = JSONObject.fromObject(o);
                    TwentyFourShouFei twentyFourShouFei = (TwentyFourShouFei) JSONObject.toBean(jsonObject, TwentyFourShouFei.class);
                    list.add(twentyFourShouFei);
                }
                for (int m = 0; m < i+1; m++) {
                    int i1 = 1440;
                    if(ctimes<=1440){
                        i1=ctimes;
                    }
                    //待的时间
                    int i2 = i1 - rtimes;
                    if(ctimes>1440){
                        rtimes = 0;
                        ctimes = ctimes - 1440;
                    }
                    if(m==0){
                        if(freeMinutes!=null&& freeMinutes!=0){//减去免费分钟
                            if(sflag){
                                i2 = i2 - freeMinutes;
                            }
                        }
                    }
                    //几个小时
                    int c = 0;
                    if(i2%60!=0){
                        c = i2/60+1;
                    }else{
                        c = i2/60;
                    }
                    for (TwentyFourShouFei twentyFourShouFei : list) {
                        if (Integer.parseInt(twentyFourShouFei.getTime())==c){
                            BigDecimal bigDecimal = new BigDecimal(twentyFourShouFei.getMoney());
                            if(bigDecimal.compareTo(highestMoney)==1){
                                money = money.add(highestMoney);
                            }else{
                                money = money.add(bigDecimal);
                            }
                        }
                    }
                }
            }
        }
        return money;

    }

    /**
     * 添加
     * 白名单信息
     * 收费标准信息
     * led显示屏信息
     * 时间段管理信息
     * 车场信息
     * 车辆类型信息
     * 设备信息
     * 到缓存
     */
    @Override
    public void addRedisInfo() {

        //白名单信息
        List<CarWhiteList> CarWhiteMap = carWhiteListService.selectList();
        if(CarWhiteMap.size()>0){
            for (CarWhiteList carWhiteList : CarWhiteMap) {
                redisUtil.set(ParkingRedisConstant.PARKING_WHITE_LIST+carWhiteList.getPlate(),carWhiteList);
            }
        }

        //收费标准信息
        List<CarStandard> list = getList();
        if(list.size()>0){
            for (CarStandard carStandard : list) {
                String[] split = carStandard.getLicensePlateType().split(",");
                for (String s : split) {
                    redisUtil.set(ParkingRedisConstant.PARKING_STANDARD+carStandard.getParkingLotId()+":"+s,carStandard);
                }
            }
        }

        //led显示屏信息
        List<LedDisplay> ledDisplays = ledDisplayService.getList();
        if(ledDisplays.size()>0){
            for (LedDisplay ledDisplay : ledDisplays) {
                redisUtil.set(ParkingRedisConstant.PARKING_LED+ledDisplay.getEquipmentId(),ledDisplay);
            }
        }

        //时间段管理信息
        List<CarTimeSlot> carTimeSlots = carTimeSlotService.getList();
        if(carTimeSlots.size()>0){
            for (CarTimeSlot carTimeSlot : carTimeSlots) {
                String[] split = carTimeSlot.getExecutionTime().split(",");
                for (String s : split) {
                    redisUtil.set(ParkingRedisConstant.PARKING_TIME_SLOT+carTimeSlot.getDeviceId()+":"+carTimeSlot.getType()+":"+s,carTimeSlot);
                }
            }
        }

        //车辆类型信息
        List<CarCardType> carCardTypes = carCardTypeService.getList();
        if(carCardTypes.size()>0){
            for (CarCardType carCardType : carCardTypes) {
                redisUtil.set(ParkingRedisConstant.PARKING_TYPE+carCardType.getId(),carCardType);
            }
        }

        //设备信息
        List<CameraDevice> cameraDevices = cameraDeviceService.getList();
        if(cameraDevices.size()>0){
            for (CameraDevice cameraDevice : cameraDevices) {
                redisUtil.set(ParkingRedisConstant.PARKING_DEVICE+cameraDevice.getSerialno(),cameraDevice);
            }
        }


        //车场信息
        List<ParkingLot> parkingLots = parkingLotService.getList();
        if(parkingLots.size() > 0){
            for (ParkingLot parkingLot : parkingLots) {
                redisUtil.set(ParkingRedisConstant.PARKING_LOT+parkingLot.getId(),parkingLot);
            }
        }

        //设备信息(按设备组id储存)
        if(cameraDevices.size()>0){
            Map<String,List<CameraDevice>> map = new HashMap<>();
            Map<String,List<CameraDevice>> map1 = new HashMap<>();
            List<CameraDevice> cameraDevices1;
            List<CameraDevice> cameraDevices2;
            for (CameraDevice cameraDevice : cameraDevices) {
                if(CommonConstant.PARKING_DEVICE_TYPE_2.equals(cameraDevice.getRepairFlag())){

                    //为抓拍设备
                    String deviceGroupId = cameraDevice.getDeviceGroupId();
                    cameraDevices2 = map1.get(deviceGroupId);
                    if(null == cameraDevices2){
                        cameraDevices2 = new ArrayList<>();
                        map1.put(deviceGroupId,cameraDevices2);
                    }
                    cameraDevices2.add(cameraDevice);
                    continue;
                }
                String deviceGroupId = cameraDevice.getDeviceGroupId();
                cameraDevices1 = map.get(deviceGroupId);
                if(null == cameraDevices1){
                    cameraDevices1 = new ArrayList<>();
                    map.put(deviceGroupId,cameraDevices1);
                }
                cameraDevices1.add(cameraDevice);

            }
            for (String s : map.keySet()) {
                redisUtil.set(ParkingRedisConstant.PARKING_DEVICE+s,map.get(s));
            }
            for (String s : map1.keySet()) {
                redisUtil.set(ParkingRedisConstant.MAKE_FLAG+s,map1.get(s));
            }
        }


        /**
         * 将数据存入到redis中
         */
        carChargeService.setListToRedis(redisUtil);


    }

    /**
     * 计算出商家减免分钟数的钱
     * @param q
     * @return
     */
    @Override
    public BigDecimal getMerchantAmount(MerchantAmountQuery q) throws Exception {
        CarStandard carStandard = (CarStandard) redisUtil.get(ParkingRedisConstant.PARKING_STANDARD + q.getParkingLotId() + ":" + q.getTypeId());
        if (carStandard == null) {
            carStandard = carStandardService.getCarStandard(q.getParkingLotId(), q.getTypeId());
        }
        if (carStandard == null) {
            return new BigDecimal("0");
        }
        //入场时间
        Date comeInDate = q.getComeInDate();
        //出场时间：
        Date date = DateUtils.addDateMinut(comeInDate, q.getMinutes());
        BigDecimal money = getMoney(carStandard.getId(), comeInDate, date, false);
        return money;
    }

    public List<CarStandard> getList(){
        LambdaQueryWrapper<CarStandard> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CarStandard::getEnableFlag,true);
        wrapper.eq(CarStandard::getChargeType,"3");
        return this.list(wrapper);
    }


}

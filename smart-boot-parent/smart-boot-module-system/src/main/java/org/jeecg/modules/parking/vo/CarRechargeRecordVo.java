package org.jeecg.modules.parking.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecg.modules.parking.entity.CarRechargeRecord;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
public class CarRechargeRecordVo extends CarRechargeRecord {

    /**账号*/
    @Excel(name = "账号", width = 15)
    private java.lang.String account;
    /**名称*/
    @Excel(name = "名称", width = 15)
    private java.lang.String name;
    /**电话*/
    @Excel(name = "电话", width = 15)
    private java.lang.String phone;
    /**名称*/
    @Excel(name = "名称", width = 15)
    private java.lang.String paymentTime;

}

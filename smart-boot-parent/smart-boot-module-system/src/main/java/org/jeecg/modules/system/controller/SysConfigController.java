package org.jeecg.modules.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
//import com.nbw580.unicom.gd.sdk.common.Common;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
//import org.jeecg.common.constant.ConfigConstant;
import org.jeecg.common.system.query.QueryGenerator;
//import org.jeecg.common.util.SysConfigUtil;
//import org.jeecg.config.UnicomConfig;
//import org.jeecg.enums.SysConfigStatusEnum;
//import org.jeecg.modules.cmcc.jf.sdk.CmccJfProductSdk;
import org.jeecg.enums.SysConfigStatusEnum;
import org.jeecg.modules.system.query.SysConfigQuery;
import org.jeecg.modules.system.entity.SysConfig;
import org.jeecg.modules.system.service.ISysConfigService;
//import org.jeecg.modules.wx.mp.controller.WxMpConfigController;
//import org.jeecg.modules.wx.pay.config.WxPayConfigController;
//import org.jeecg.pojo.query.SysConfigQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @Description: sys_config
 * @Author: jeecg-boot
 * @Date: 2019-09-23
 * @Version: V1.0
 */
@RestController
@RequestMapping("/system/sysConfig")
@Slf4j
public class SysConfigController {

	@Autowired
	private ISysConfigService sysConfigService;

//	@Autowired
//	private WxMpConfigController wxConfigController;
//	@Autowired
//	private WxPayConfigController wxPayConfigController;

	/**
	 * 分页列表查询
	 *
	 * @param sysConfig
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@GetMapping(value = "/list")
	public Result<IPage<SysConfig>> queryPageList(SysConfig sysConfig,
												  @RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
												  @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
												  HttpServletRequest req) {
		Result<IPage<SysConfig>> result = new Result<IPage<SysConfig>>();
		QueryWrapper<SysConfig> queryWrapper = QueryGenerator.initQueryWrapper(sysConfig, req.getParameterMap());
		Page<SysConfig> page = new Page<SysConfig>(pageNo, pageSize);
		IPage<SysConfig> pageList = sysConfigService.page(page, queryWrapper);
		result.setSuccess(true);
		result.setResult(pageList);
		return result;
	}

	/**
	 * 添加
	 *
	 * @param sysConfig
	 * @return
	 */
	@PostMapping(value = "/add")
	public Result<SysConfig> add(@RequestBody SysConfig sysConfig) {
		Result<SysConfig> result = new Result<SysConfig>();
		try {
			sysConfigService.save(sysConfig);
			result.success("添加成功！");
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			result.error500("操作失败");
		}
		return result;
	}

	/**
	 * 编辑
	 *
	 * @param sysConfig
	 * @return
	 */
	@PutMapping(value = "/edit")
	public Result<SysConfig> edit(@RequestBody SysConfig sysConfig) {
		Result<SysConfig> result = new Result<SysConfig>();
		SysConfig sysConfigEntity = sysConfigService.getById(sysConfig.getId());
		if (sysConfigEntity == null) {
			result.error500("未找到对应实体");
		} else {
			boolean ok = sysConfigService.updateById(sysConfig);
			//TODO 返回false说明什么？
			if (ok) {
				result.success("修改成功!");
			}
		}

		return result;
	}

	/**
	 * 通过id删除
	 *
	 * @param id
	 * @return
	 */
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name = "id", required = true) String id) {
		try {
			sysConfigService.removeById(id);
		} catch (Exception e) {
			log.error("删除失败", e.getMessage());
			return Result.error("删除失败!");
		}
		return Result.ok("删除成功!");
	}

	/**
	 * 批量删除
	 *
	 * @param ids
	 * @return
	 */
	@DeleteMapping(value = "/deleteBatch")
	public Result<SysConfig> deleteBatch(@RequestParam(name = "ids", required = true) String ids) {
		Result<SysConfig> result = new Result<SysConfig>();
		if (ids == null || "".equals(ids.trim())) {
			result.error500("参数不识别！");
		} else {
			this.sysConfigService.removeByIds(Arrays.asList(ids.split(",")));
			result.success("删除成功!");
		}
		return result;
	}

	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@GetMapping(value = "/queryById")
	public Result<SysConfig> queryById(@RequestParam(name = "id", required = true) String id) {
		Result<SysConfig> result = new Result<SysConfig>();
		SysConfig sysConfig = sysConfigService.getById(id);
		if (sysConfig == null) {
			result.error500("未找到对应实体");
		} else {
			result.setResult(sysConfig);
			result.setSuccess(true);
		}
		return result;
	}

	/**
	 * 通过标识查询
	 *
	 * @param flag
	 * @return
	 */
	@GetMapping(value = "/queryByFlag")
	public Result<List<SysConfig>> queryByFlag(@RequestParam(name = "flag", required = true) String flag) {
		Result<List<SysConfig>> result = new Result<>();
		SysConfigQuery query = new SysConfigQuery();
		query.setTabFlag(flag);
		query.setTabStatus(SysConfigStatusEnum.XS.getValue());
		query.setStatus(SysConfigStatusEnum.XS.getValue());
		List<SysConfig> list = sysConfigService.queryByFlag(query);
		result.setResult(list);
		result.setSuccess(true);
		return result;
	}

	/**
	 * 保存
	 *
	 * @return
	 */
	@PostMapping("/save/{flag}")
	public Result save(@PathVariable String flag, @RequestBody Map<String, Object> objectMap) {
		Result result = new Result();
		try {
			sysConfigService.save(objectMap);
			result.setSuccess(true);
			result.setMessage("保存成功");
			// 刷新配置
//			this.refreshConfig(flag);
		} catch (Exception e) {
			result.setSuccess(false);
			result.setMessage(e.getMessage());
		}
		return result;
	}


	/**
	 * 刷新配置
	 *
	 * @param flag 配置标识
	 */
//	private void refreshConfig(String flag) {
//		if (ConfigConstant.WEIXIN_MP.equals(flag)) {
//			wxConfigController.initWxMpConfig();
//		} else if (ConfigConstant.WEIXIN_PAY.equals(flag)) {
//			wxPayConfigController.initWxPayConfig();
//		} else if (ConfigConstant.SYS_CONFIG.equals(flag)) {
//			String proxy = SysConfigUtil.getVal(ConfigConstant.BASE_PROXY);
//			if (StrUtil.isNotEmpty(proxy)) {
//				try {
//					proxy = proxy.replace("：", ":");
//					String[] proxyList = proxy.split(":");
//					CmccJfProductSdk.setProxy(proxyList[0], Integer.parseInt(proxyList[1]));
//				} catch (Exception ex) {
//					CmccJfProductSdk.setProxy(null, null);
//					System.out.println("基础配置：配置代理失败：" + proxy);
//				}
//			} else {
//				CmccJfProductSdk.setProxy(null, null);
//			}
//		} else if (ConfigConstant.UNICOM_CONFIG.equals(flag)) {
//			UnicomConfig.unicomConfig();
//		}
//	}

	/**
	 * 获取值
	 *
	 * @param key
	 * @return
	 */
	@GetMapping("/getVal")
	public Result getVal(@RequestParam(name = "key", required = true) String key) {
		Result result = new Result();
		SysConfig config = sysConfigService.getConfig(key);
		result.setSuccess(true);
		result.setResult(config);
		return result;
	}
}

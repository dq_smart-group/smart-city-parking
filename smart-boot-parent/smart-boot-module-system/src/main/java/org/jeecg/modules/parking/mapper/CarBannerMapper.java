package org.jeecg.modules.parking.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarBanner;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 停车场banner广告
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
public interface CarBannerMapper extends BaseMapper<CarBanner> {

    /**
     * 获取banner中的group
     * @return
     */
    List<String> getBannerGroup();

    /**
     * 根据组来获取banner
     * @param group
     * @return
     */
    List<CarBanner> getCarBanner(@Param("group") String group);
}

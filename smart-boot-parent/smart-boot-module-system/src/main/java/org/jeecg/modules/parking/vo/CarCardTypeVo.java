package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarCardType;
@Data
public class CarCardTypeVo extends CarCardType {

    //公司名
    private String departName;

    //车场名称
    private String parkingLotName;
}

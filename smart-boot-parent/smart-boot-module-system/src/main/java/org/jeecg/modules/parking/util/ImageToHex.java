package org.jeecg.modules.parking.util;

import cn.hutool.core.codec.Base64;
import net.coobird.thumbnailator.Thumbnails;
import org.jeecg.common.util.PicUtils;
import org.jeecg.modules.parking.api.MQTTTest_tcp;
import org.jeecg.modules.parking.api.classHexstringToByteUtil;
import org.jeecg.modules.parking.mqtt.MqttPushClient;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.MemoryImageSource;
import java.awt.image.PixelGrabber;
import java.io.*;

public class ImageToHex {
    public static void main(String[] args) throws Exception {
        // get();


    }

    /**
     *
     *
     * @throws Exception
     */

    /**
     * 将图片转为单位图数据
     * @param Chepai  车牌
     * @param topic  发布主题
     * @param cont  二维码下面的内容
     * @param flag 是否开启播报
     * @throws Exception
     */
    public static void PiczhuangDan(String Chepai,String topic,String cont,Boolean flag) throws Exception {
        BufferedImage sourceImg = ImageIO.read(new File("D:/img/" + Chepai + ".bmp"));

        int h = sourceImg.getHeight();
        int w = sourceImg.getWidth();
        int[] pixels = new int[w * h]; // 定义一数组，用来存储图片的象素
        int gray;
        PixelGrabber pg = new PixelGrabber(sourceImg, 0, 0, w, h, pixels, 0, w);
        try {
            pg.grabPixels(); // 读取像素值
        } catch (InterruptedException e) {
            System.err.println("处理被异常中断！请重试！");
        }
        for (int j = 0; j < h; j++) // 扫描列 {
            for (int i = 0; i < w; i++) // 扫描行
            { // 由红，绿，蓝值得到灰度值
                gray = (int) (((pixels[w * j + i] >> 16) & 0xff) * 0.8);
                gray += (int) (((pixels[w * j + i] >> 8) & 0xff) * 0.1);
                gray += (int) (((pixels[w * j + i]) & 0xff) * 0.1);
                pixels[w * j + i] = (255 << 24) | (gray << 16) | (gray << 8)
                        | gray;
            }

        MemoryImageSource s = new MemoryImageSource(w, h, pixels, 0, w);
        Image img = Toolkit.getDefaultToolkit().createImage(s);
        BufferedImage buf = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_BINARY);
        buf.createGraphics().drawImage(img, 0, 0, null);
        ImageIO.write(buf, "BMP", new File("D:/img/" + Chepai + ".bmp"));

        getStr(Chepai,topic,cont,flag);
    }








    /**
     * 获取图片转换为16进制数据
     * @param url 文件名\
     * @param topic 发布的主题
     * @param cont 二维码下面的文字内容
     * @param flag 是否开启播报
     * @return
     */
    public static void getStr(String url,String topic,String cont,Boolean flag) {
        try{
            StringBuffer sb = new StringBuffer();
            FileInputStream fis = new FileInputStream("D:/img/"+url+".bmp");
            java.io.ByteArrayOutputStream bos=new java.io.ByteArrayOutputStream();


            byte[] buff=new byte[1024];
            int len=0;
            while((len=fis.read(buff))!=-1){
                bos.write(buff,0,len);

            }
            byte[] result=bos.toByteArray();

            String str=byte2HexStr(result);
            String strs = "";
            for (int i = 0; i < str.length(); i=i+2) {
                strs = strs+" "+str.substring(i,i+2);
            }
            bos.close();
            sendOrder(str,cont,topic,flag);
        }catch(Exception e){
            e.printStackTrace();
        }

    }

    /**
     *生成二维码到led屏
     * str3 二维码16进制内容
     * cont 显示文字内容
     * topic 发布的主题
     * bobaoflag 是否开启播报
     */
    public static void sendOrder(String str3,String cont,String topic,Boolean bobaoflag)throws Exception{
        //cont="测算测算测算测算测算测算测算测算测算";
        cont = cont;
        //根据转入的汉字转GB2312编码的字节数组
        byte[] bytes = cont.getBytes("GB2312");
        //调用根据字节数组转16进制方法
        char[] buf = GetCrc16.getchar(bytes);
        //文本内容的16进制
        String s = new String(buf);
        //文本内容的长度16进制
        String len = GetCrc16.getLens(bytes);
        if(len.length()==1){
            len = "0"+len;
        }
        //前面特定格式
        String str1 = null;
        if(bobaoflag){
            str1 = "0101003c0081";
        }else{
            str1 = "0101003c0080";
        }
        //特定格式+显示文字的长度加文字内容
        str1 = str1+len+s;
        //特定格式+显示文字的长度加文字内容+二维码单位图数据
        str3 = str1+str3;
        System.out.println(str3);
        String qrCode = GetCrc16.getQrCode(str3);
        String s3 = GetCrc16.getCrc16(qrCode);
        byte[] aa2 = classHexstringToByteUtil.hexString2Bytes(s3);
        String encode3 = Base64.encode(aa2);
        MqttPushClient.pushlishOrder(topic,encode3);
        //MQTTTest_tcp.publish(encode3);
    }

    /**
     * 将图片转为单位图数据
     * @throws Exception
     */
    public static void get()throws Exception{
        BufferedImage sourceImg = ImageIO.read(new File("D:/img/jam.bmp"));

        int h = sourceImg.getHeight();
        int w = sourceImg.getWidth();
        int[] pixels = new int[w * h]; // 定义一数组，用来存储图片的象素
        int gray;
        PixelGrabber pg = new PixelGrabber(sourceImg, 0, 0, w, h, pixels, 0, w);
        try {
            pg.grabPixels(); // 读取像素值
        } catch (InterruptedException e) {
            System.err.println("处理被异常中断！请重试！");
        }
        for (int j = 0; j < h; j++) // 扫描列 {
            for (int i = 0; i < w; i++) // 扫描行
            { // 由红，绿，蓝值得到灰度值
                gray = (int) (((pixels[w * j + i] >> 16) & 0xff) * 0.8);
                gray += (int) (((pixels[w * j + i] >> 8) & 0xff) * 0.1);
                gray += (int) (((pixels[w * j + i]) & 0xff) * 0.1);
                pixels[w * j + i] = (255 << 24) | (gray << 16) | (gray << 8)
                        | gray;
            }

        MemoryImageSource s= new MemoryImageSource(w,h,pixels,0,w);
        Image img =Toolkit.getDefaultToolkit().createImage(s);
        BufferedImage buf = new BufferedImage(w, h, BufferedImage.TYPE_BYTE_BINARY);
        buf.createGraphics().drawImage(img, 0, 0, null);
        ImageIO.write(buf, "BMP", new File("D:/img/one.bmp"));
    }





    public static String byte2HexStr(byte[] b) {
        String hs="";
        String stmp="";
        for (int n=0;n<b.length;n++) {
            stmp=(Integer.toHexString(b[n] & 0XFF));
            if (stmp.length()==1) hs=hs+"0"+stmp;
            else hs=hs+stmp;
        }
        return hs.toUpperCase();
    }





    private static byte uniteBytes(String src0, String src1) {
        byte b0 = Byte.decode("0x" + src0).byteValue();
        b0 = (byte) (b0 << 4);
        byte b1 = Byte.decode("0x" + src1).byteValue();
        byte ret = (byte) (b0 | b1);
        return ret;
    }





    public static String bytesToHexString(byte[] src){

        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();

    }






}

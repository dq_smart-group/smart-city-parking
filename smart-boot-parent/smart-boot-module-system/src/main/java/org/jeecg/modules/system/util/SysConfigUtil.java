package org.jeecg.modules.system.util;

import cn.hutool.core.util.StrUtil;
import org.jeecg.common.util.SpringContextUtils;
import org.jeecg.modules.system.entity.SysConfig;
import org.jeecg.modules.system.service.ISysConfigService;

import java.math.BigDecimal;

public class SysConfigUtil {
    private static ISysConfigService sysConfigService;

    /**
     * @return
     */
    public static ISysConfigService getConfigService() {
        if (sysConfigService == null) {
            sysConfigService = SpringContextUtils.getBean(ISysConfigService.class);
        }
        return sysConfigService;
    }

    /**
     * 获取配置
     *
     * @param key
     * @return SysConfig
     */
    public static SysConfig getConfig(String key) {
        return getConfigService().getConfig(key);
    }

    /**
     * 通过标识获取值
     *
     * @param key
     * @return String
     */
    public static String getVal(String key) {
        return getConfigService().getVal(key);
    }

    /**
     * 通过标识获取值
     *
     * @param key
     * @return
     */
    public static BigDecimal getBigDecimalVal(String key) {
        return getConfigService().getBigDecimalVal(key);
    }

    /**
     * 通过标识获取值
     *
     * @param key
     * @return String[]
     */
    public static String[] getValArray(String key) {
        return getConfigService().getValArray(key);
    }

    /**
     * 通过标识获取值
     *
     * @param key
     * @return Boolean
     */
    public static Boolean getValBoolean(String key) {
        return getConfigService().getValBoolean(key);
    }


    /**
     * 通过标识获取值
     *
     * @param key
     * @return
     */
    public static Integer getValInteger(String key) {
        String val = getConfigService().getVal(key);
        return  StrUtil.isEmpty(val) ? null : Integer.parseInt(val);
    }
}

package org.jeecg.modules.parking.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecg.modules.parking.entity.CarConsumptionRecord;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class CarConsumptionRecordVo extends CarConsumptionRecord {

    /**进场时间*/
    @Excel(name = "入库时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.util.Date comeInTime;
    /**出库时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.util.Date outTime;
    /**车辆品牌*/
    @Excel(name = "车辆品牌", width = 15)
    private java.lang.String brand;
    /**车牌号*/
    @Excel(name = "车牌号", width = 15)
    private java.lang.String license;
    /**车辆状态*/
    @Excel(name = "车辆状态", width = 15)
    @Dict(dicCode = "car_status")
    private java.lang.String carStatus;
    /**入库车牌截图*/
    @Excel(name = "入库车牌截图", width = 15)
    private java.lang.String comeImagePath;
    /**出库车牌截图*/
    @Excel(name = "出库车牌截图", width = 15)
    private java.lang.String outImagePath;
    /**车辆年份*/
    @Excel(name = "车辆年份", width = 15)
    private java.lang.String carYear;
    /**车辆类型*/
    @Excel(name = "车辆类型", width = 15)
    private java.lang.String carType;
    /**车牌颜色*/
    @Excel(name = "车牌颜色", width = 15)
    @Dict(dicCode = "car_card_color")
    private java.lang.String carCardColor;
    /**车牌类型*/
    @Excel(name = "车牌类型", width = 15)
    @Dict(dicCode = "car_card_type")
    private java.lang.String carCardType;
    /**车身颜色*/
    @Excel(name = "车身颜色", width = 15)
    private java.lang.String carColor;
    /**入场设备名称*/
    @Excel(name = "入场设备名称", width = 15)
    private java.lang.String comeDevice;
    /**出场设备名称*/
    @Excel(name = "出场设备名称", width = 15)
    private java.lang.String outDevice;
    /**入场设备 ip 地址*/
    @Excel(name = "入场设备 ip 地址", width = 15)
    private java.lang.String comeIpaddr;
    /**出场设备 ip 地址*/
    @Excel(name = "出场设备 ip 地址", width = 15)
    private java.lang.String outIpaddr;
    /**入场设备序列号，设备唯一*/
    @Excel(name = "入场设备序列号，设备唯一", width = 15)
    private java.lang.String comeSerialno;
    /**出场设备序列号，设备唯一*/
    @Excel(name = "出场设备序列号，设备唯一", width = 15)
    private java.lang.String outSerialno;
    /**触发类型*/
    @Excel(name = "触发类型", width = 15)
    @Dict(dicCode = "sx_triggerType")
    private java.lang.String triggerType;
    /**识别结果车牌 ID*/
    @Excel(name = "识别结果车牌 ID", width = 15)
    private java.lang.String plateid;
    /**车牌真伪*/
    @Excel(name = "车牌真伪", width = 15)
    @Dict(dicCode = "sx_is_fake")
    private java.lang.String isFakePlate;
    /**车辆特征码*/
    @Excel(name = "车辆特征码", width = 15)
    private java.lang.String featureCode;
    /**识别所用时间*/
    @Excel(name = "识别所用时间", width = 15)
    private java.lang.String timeUsed;
    /**识别结果可信度*/
    @Excel(name = "识别结果可信度", width = 15)
    private java.lang.String confidence;
    /**入库推送报文*/
    @Excel(name = "入库推送报文", width = 15)
    private java.lang.String comeInfoText;
    /**出库推送报文*/
    @Excel(name = "出库推送报文", width = 15)
    private java.lang.String outInfoText;
    /**备注*/
    @Excel(name = "备注", width = 15)
    private java.lang.String remark;

    /**车牌类型id；特殊车牌类型记录*/
    @Excel(name = "车牌类型id；特殊车牌类型记录", width = 15)
    private java.lang.String cardTypeId;
    /**公司名称**/
    @Excel(name = "公司名称", width = 15)
    private java.lang.String companyName;
    /**公司id**/
    @Excel(name = "公司id", width = 15)
    private java.lang.String companyId;
    /**车库名称**/
    @Excel(name = "车库名称", width = 15)
    private java.lang.String parkingName;

    private java.lang.String payTime;

}

package org.jeecg.modules.parking.mapper;

import java.util.List;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarCardType;
import org.jeecg.modules.parking.entity.CarVehicle;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.entity.Depart;
import org.jeecg.modules.parking.query.CarVehicleQuery;
import org.jeecg.modules.parking.vo.CarVehicleVo;

/**
 * @Description: 车辆信息表
 * @Author: jeecg-boot
 * @Date: 2021-12-09
 * @Version: V1.0
 */
public interface CarVehicleMapper extends BaseMapper<CarVehicle> {

    IPage<CarVehicleVo> pageList(Page<CarVehicleVo> page, @Param("q") CarVehicleQuery q);

    /**
     * 根据车牌号和公司id查询车牌类型
     * @param license
     * @param parkId
     * @return
     */
    String carTypeId(@Param("license")String license,@Param("parkId")String parkId);
}

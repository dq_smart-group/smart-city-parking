package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarShiftForm;

@Data
public class CarShiftFormVo extends CarShiftForm {
    //收费员名称
    private String username;
}

package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarStandard;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.query.CarStandardQuery;
import org.jeecg.modules.parking.query.MerchantAmountQuery;
import org.jeecg.modules.parking.vo.CarStandardVo;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @Description: 收费标准表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
public interface ICarStandardService extends IService<CarStandard> {

    IPage<CarStandardVo> pageList(Page<CarStandardVo> page, CarStandardQuery carStandardQuery);


    /**
     * 根据车场id和车牌类型id查询收费规则
     * @param parkingId 车场id
     * @param typeId 车牌类型id
     * @return
     */
    CarStandard getCarStandard(String parkingId,String typeId);

    CarStandard getCarStandards(String parkingId,String typeId,String charge);

    //新增收费标准
    Result  saveCarStandard(CarStandardQuery carStandard);

    //修改收费标准
    Result updateCarStandardById(CarStandardQuery carStandard);
    /**
     * 计算金额
     * @param carStandardId
     * @param rt
     * @param ct
     * @return
     */
    BigDecimal getMoney(String carStandardId, Date rt, Date ct,Boolean sflag)throws Exception;

    /**
     * 添加
     * 白名单信息
     * 收费标准信息
     * led显示屏信息
     * 时间段管理信息
     * 车场信息
     * 车辆类型信息
     * 设备信息
     * 到缓存
     */
    void addRedisInfo();

    /**
     * 计算出商家减免分钟数的钱
     * @param merchantAmountQuery
     * @return
     */
    BigDecimal getMerchantAmount(MerchantAmountQuery merchantAmountQuery) throws Exception;
}

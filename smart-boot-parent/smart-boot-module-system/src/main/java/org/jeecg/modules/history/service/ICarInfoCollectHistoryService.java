package org.jeecg.modules.history.service;

import org.jeecg.modules.history.entity.CarInfoCollectHistory;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.entity.CarInfoCollect;

import java.util.List;

/**
 * @Description: car_info_collect_history_1
 * @Author: jeecg-boot
 * @Date:   2022-03-02
 * @Version: V1.0
 */
public interface ICarInfoCollectHistoryService extends IService<CarInfoCollectHistory> {


    /**
     * 获取表格数据数量
     * @param tableName
     * @return
     */
    int getListCount(String tableName);

    /**
     * 创建表
     * @param tableName
     */
    void createHistoryTable(String tableName);

    /**
     * 保存数据到历史
     * @param list
     * @return
     */
    int saveDataToHistory(List<CarInfoCollect> list, String tableName);

}

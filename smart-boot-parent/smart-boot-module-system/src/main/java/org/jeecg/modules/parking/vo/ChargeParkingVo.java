package org.jeecg.modules.parking.vo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ChargeParkingVo {
    private String parkingLotId;
    private String parkingName;
    /**
     * 本班入车数
     */
    private Integer comeCount;
    /**
     * 本班出车数
     */
    private Integer outCount;
    /**
     * 已收合计
     */
    private BigDecimal totalReceived;
    /**
     * 减免合计
     */
    private BigDecimal totalRelief;
}

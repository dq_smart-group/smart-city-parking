package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class PlateResultQuery {
    private String bright;
    private String carBright;
    private String carColor;
    private String colorType;
    private String colorValue;
    private String confidence;
    private String direction;
    private String imagePath;
    private String isoffline;
    private String license;
    private String plateid;
    private String location;
    private String timeStamp;
    private String timeUsed;
    private String triggerType;
    private String type;
    private String is_fake_plate;
    private String feature_code;



}

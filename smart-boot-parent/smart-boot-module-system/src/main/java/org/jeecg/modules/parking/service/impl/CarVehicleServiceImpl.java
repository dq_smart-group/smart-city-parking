package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarVehicle;
import org.jeecg.modules.parking.mapper.CarVehicleMapper;
import org.jeecg.modules.parking.query.CarVehicleQuery;
import org.jeecg.modules.parking.service.ICarVehicleService;
import org.jeecg.modules.parking.vo.CarVehicleVo;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 车辆信息表
 * @Author: jeecg-boot
 * @Date: 2021-12-09
 * @Version: V1.0
 */
@Service
public class CarVehicleServiceImpl extends ServiceImpl<CarVehicleMapper, CarVehicle> implements ICarVehicleService {

    /**
     * 分页查询
     *
     * @param page
     * @param carVehicleQuery
     * @return
     */
    @Override
    public IPage<CarVehicleVo> pageList(Page<CarVehicleVo> page, CarVehicleQuery carVehicleQuery) {
        return baseMapper.pageList(page, carVehicleQuery);
    }

    /**
     * 根据车牌号和车场id查询车牌类型
     *
     * @param license
     * @param parkId
     * @return
     */
    @Override
    public String carTypeId(String license, String parkId) {
        String id = this.baseMapper.carTypeId(license, parkId);
        return id;
    }

    /**
     * 车辆信息新增
     *
     * @param carVehicle
     */
    @Override
    public Result addcarVehicleSave(CarVehicle carVehicle) {
        LambdaQueryWrapper<CarVehicle> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.eq(CarVehicle::getParkingLotId, carVehicle.getParkingLotId());
        lambdaQueryWrapper.eq(CarVehicle::getLicense, carVehicle.getLicense());
        List<CarVehicle> carVehicles = baseMapper.selectList(lambdaQueryWrapper);
        if (carVehicles.size() != 0) {
            return Result.error("已存在该车辆信息！");
        } else {
            this.save(carVehicle);
            return Result.OK("添加成功！");
        }
    }

    /**
     * 车辆信息修改
     * @param carVehicle
     * @return
     */
    @Override
    public Result updatecarVehicleById(CarVehicle carVehicle) {
        LambdaQueryWrapper<CarVehicle> lambdaQueryWrapper = new LambdaQueryWrapper<>();
        lambdaQueryWrapper.ne(CarVehicle::getId, carVehicle.getId());
        lambdaQueryWrapper.eq(CarVehicle::getParkingLotId, carVehicle.getParkingLotId());
        lambdaQueryWrapper.eq(CarVehicle::getLicense, carVehicle.getLicense());
        List<CarVehicle> carVehicles = baseMapper.selectList(lambdaQueryWrapper);
        if (carVehicles.size() != 0) {
            return Result.error("已存在该车辆信息！");
        } else {
            this.updateById(carVehicle);
            return Result.OK("修改成功！");
        }
    }


}

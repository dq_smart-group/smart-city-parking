package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarCardType;
import org.jeecg.modules.parking.entity.Depart;
import org.jeecg.modules.parking.query.CarCardTypeQuery;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Description: 车牌类型
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface ICarCardTypeService extends IService<CarCardType> {

    List<Depart> getDepartlist();

    List<Depart> getDepartByUserId(String loginUserId);


    IPage<CarCardType> pageList(Page<CarCardType> page, CarCardTypeQuery carCardTypeQuery);

    //获取登陆人对应的公司下的车牌类型,是管理员就获取全部
    List<ParkingLotAndCamVo> getCardTypeByparkingLotId(String companyId);

    List<CarCardType> getCardTypeByParkingId(String parkingId);



    //车牌类型新增
    Result addCarCardTypesave(CarCardType carCardType);

    //车牌类型修改
    Result updateCarCardTypeById(CarCardType carCardType);

    List<CarCardType> getList();


}

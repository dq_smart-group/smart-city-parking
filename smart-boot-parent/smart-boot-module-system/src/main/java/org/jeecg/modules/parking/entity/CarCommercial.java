package org.jeecg.modules.parking.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: car_commercial
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
@Data
@TableName("car_commercial")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_commercial对象", description="car_commercial")
public class CarCommercial implements Serializable {
    private static final long serialVersionUID = 1L;

	/**商户id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "商户id")
    private java.lang.String id;
	/**公司id*/
    @Excel(name = "公司id", width = 15)
    @ApiModelProperty(value = "公司id")
    private java.lang.String companyId;
	/**车场id*/
    @Excel(name = "车场id", width = 15)
    @ApiModelProperty(value = "车场id")
    private java.lang.String parkingLotId;
	/**账号*/
	@Excel(name = "账号", width = 15)
    @ApiModelProperty(value = "账号")
    private java.lang.String account;
    /**账号*/
    @Excel(name = "盐值", width = 15)
    @ApiModelProperty(value = "盐值")
    private java.lang.String salt;
	/**名称*/
	@Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    private java.lang.String name;
	/**密码*/
	@Excel(name = "密码", width = 15)
    @ApiModelProperty(value = "密码")
    private java.lang.String password;
	/**电话*/
	@Excel(name = "电话", width = 15)
    @ApiModelProperty(value = "电话")
    private java.lang.String phone;
	/**地址*/
	@Excel(name = "地址", width = 15)
    @ApiModelProperty(value = "地址")
    private java.lang.String address;
	/**开始时间*/
	@Excel(name = "开始时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "开始时间")
    private java.util.Date startTime;
	/**结束时间*/
	@Excel(name = "结束时间", width = 15, format = "yyyy-MM-dd")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "结束时间")
    private java.util.Date endTime;
	/**优惠类型--1：固定扣款；2：折扣率*/
	@Excel(name = "优惠类型--1：固定扣款；2：折扣率", width = 15)
    @ApiModelProperty(value = "优惠类型--1：固定扣款；2：折扣率")
    private java.lang.String preferentialType;
	/**单价（1元/分钟）*/
	@Excel(name = "单价（1元/分钟）", width = 15)
    @ApiModelProperty(value = "单价（1元/分钟）")
    private java.math.BigDecimal unitPrice;
	/**状态--1：启用；0：禁止*/
	@Excel(name = "状态--1：启用；0：禁止", width = 15)
    @ApiModelProperty(value = "状态--1：启用；0：禁止")
    private java.lang.Integer state;
	/**签名（key）*/
	@Excel(name = "签名（key）", width = 15)
    @ApiModelProperty(value = "签名（key）")
    private java.lang.String signature;
	/**说明*/
	@Excel(name = "说明", width = 15)
    @ApiModelProperty(value = "说明")
    private java.lang.String remark;
	/**折扣率*/
	@Excel(name = "折扣率", width = 15)
    @ApiModelProperty(value = "折扣率")
    private java.math.BigDecimal discountRate;
	/**剩余分钟数*/
	@Excel(name = "剩余分钟数", width = 15)
    @ApiModelProperty(value = "剩余分钟数")
    private java.lang.Integer minutesRemaining;
	/**单次减免分钟数*/
	@Excel(name = "单次减免分钟数", width = 15)
    @ApiModelProperty(value = "单次减免分钟数")
    private java.lang.Integer singleReduction;
	/**静态二维码*/
	@Excel(name = "静态二维码", width = 15)
    @ApiModelProperty(value = "静态二维码")
    private java.lang.String staticQuickMark;
	/**动态二维码*/
	@Excel(name = "动态二维码", width = 15)
    @ApiModelProperty(value = "动态二维码")
    private java.lang.String quickMark;
	/**余额*/
	@Excel(name = "余额", width = 15)
    @ApiModelProperty(value = "余额")
    private java.math.BigDecimal balance;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
}

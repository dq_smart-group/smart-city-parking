package org.jeecg.modules.parking.service.impl;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.util.DateUtils;
import org.jeecg.common.util.PasswordUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.parking.api.WebSocketApi;
import org.jeecg.modules.parking.entity.*;
import org.jeecg.modules.parking.mapper.CarCommercialMapper;
import org.jeecg.modules.parking.query.MerchantAmountQuery;
import org.jeecg.modules.parking.service.ICarCommercialService;
import org.jeecg.modules.parking.service.ICarStandardService;
import org.jeecg.modules.parking.vo.CommercialPayInfoVo;
import org.jeecg.modules.system.util.SysConfigUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: car_commercial
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
@Service
public class CarCommercialServiceImpl extends ServiceImpl<CarCommercialMapper, CarCommercial> implements ICarCommercialService {

    @Autowired
    private CarTopUpServiceImpl carTopUpService;

    @Autowired
    public RedisTemplate redisTemplate;

    @Autowired
    CarInfoCollectServiceImpl carInfoCollectService;

    @Autowired
    CarYardFeesServiceImpl carYardFeesService;

    @Autowired
    CarConsumptionRecordServiceImpl consumptionRecordService;

    @Autowired
    private ICarStandardService carStandardService;

    @Autowired
    private CarRechargeRecordServiceImpl carRechargeRecordService;

    @Autowired
    private WebSocketApi webSocketApi;


    @Override
    public void saveCarCommercial(CarCommercial carCommercial) {
        if (null == carCommercial.getPassword()) {
            carCommercial.setPassword("123456");
        }

        String salt = oConvertUtils.randomGen(8);
        carCommercial.setSalt(salt);
        //密码盐值加密
        carCommercial.setPassword(PasswordUtil.encrypt(carCommercial.getAccount(),carCommercial.getPassword(),salt));
        //折扣率
        carCommercial.setDiscountRate(BigDecimal.valueOf(100));
        //剩余分钟数
        carCommercial.setMinutesRemaining(0);
        //单次减免分钟数
        carCommercial.setSingleReduction(0);
        //余额
        carCommercial.setBalance(BigDecimal.valueOf(0));
        this.baseMapper.insert(carCommercial);

        //新增充值记录
        CarTopUp carTopUp = new CarTopUp();
        carTopUp.setCommercialTenantId(carCommercial.getId());
        carTopUp.setLastTopUp(0);
        carTopUpService.save(carTopUp);
    }

    @Override
    public void edit(CarCommercial carCommercial) {
        //生成随机的key存进redis中，二维码只生效一次（动态二维码）
        String uid = UUID.randomUUID()+"--"+carCommercial.getId();
        String serverName = SysConfigUtil.getVal("serverName");
        String key = "";
        if(null!=carCommercial.getParkingLotId()& !"".equals(carCommercial.getParkingLotId())) {
            carCommercial.setStaticQuickMark(serverName +"?id=" + carCommercial.getId() + "-" + carCommercial.getParkingLotId());
        }
        if(Objects.equals(carCommercial.getStaticQuickMark(), "1")){
            //静态
            carCommercial.setStaticQuickMark(serverName+"?id="+carCommercial.getId()+"-"+carCommercial.getParkingLotId());
        }else if(Objects.equals(carCommercial.getStaticQuickMark(), "2")) {
            //动态
            key = String.format(CommonConstant.QUICKMARK+"%s", carCommercial.getId());
            carCommercial.setQuickMark(serverName +"?id="+carCommercial.getId()+"&key="+uid);
            carCommercial.setStaticQuickMark(null);
            carCommercial.setParkingLotId(null);

            //如果是重新生成的二维码则清除之前的key
            if(redisTemplate.hasKey(key)){

                redisTemplate.delete(key);
            }
            redisTemplate.opsForValue().set(key,uid);
        }
        this.updateById(carCommercial);
    }

    public CarCommercial login(Map<String, String> userInfo) {
        String account = userInfo.get("account");
        Assert.notNull(account,"账号不能为空");
        String password = userInfo.get("password");
        Assert.notNull(password,"密码不能为空");
        CarCommercial commercial = getOne(Wrappers.<CarCommercial>lambdaQuery().eq(CarCommercial::getAccount, account));
        Assert.notNull(commercial,"账号不存在");
        String encrypt = PasswordUtil.encrypt(account, password, commercial.getSalt());
        Assert.isTrue(Objects.equals(encrypt,commercial.getPassword()),"密码错误");
        Assert.isTrue(commercial.getState()!=0,"账号已禁用");
        return commercial;
    }

    public void updatePassword(Map<String, String> info) {
        String id = info.get("id");
        String password = info.get("password");
        Assert.notNull(password,"修改密码不能为空");
        CarCommercial commercial = getById(id);
        String salt = oConvertUtils.randomGen(8);
        commercial.setSalt(salt);
        //密码盐值加密
        commercial.setPassword(PasswordUtil.encrypt(commercial.getAccount(),password,salt));
        updateById(commercial);
    }

    public Result<?> queryPageList(String id, String key) {
        CarCommercial commercial = getById(id);
        long nowTime = DateUtil.date().getTime();
        if (null == commercial) {
            //静态
            String[] split = id.split("-");
            CarCommercial commercialServiceById = getById(split[0]);
            if (null == commercialServiceById||!split[1].equals(String.valueOf(commercialServiceById.getParkingLotId()))) {
                return Result.error("商户不存在");
            }
            long endTime = commercialServiceById.getEndTime().getTime();
            if (nowTime > endTime) {
                return Result.error("商户已过期");
            }
            return Result.OK(commercialServiceById);
        } else {
            //动态
            //获取redis中的key，跟传入的key对比，如果一样则是第一次使用，否则就是二次非法使用
            String redisKey = CommonConstant.QUICKMARK + id;
            String oldKey = (String) redisTemplate.opsForValue().get(redisKey);
            if (null == oldKey) {
                //如果Redis不存在，但是页面传过来的key与数据库中的相同，说明Redis未同步之前的商户信息，则给他添加上
                String oKey = commercial.getQuickMark().split("key=")[1];
                if(oKey.equals(key)){
                    redisTemplate.opsForValue().set(redisKey,oKey);
                    long endTime = commercial.getEndTime().getTime();
                    if (nowTime > endTime) {
                        return Result.error("商户已过期");
                    }
                    return Result.OK(commercial);
                }
                return Result.error("二维码不存在");
            } else {
                if (Objects.equals(oldKey, key)) {
                    long endTime = commercial.getEndTime().getTime();
                    if (nowTime > endTime) {
                        return Result.error("商户已过期");
                    }
                    return Result.OK(commercial);
                }
                return Result.error("二维码已失效");
            }
        }
    }

    public Result<?> listCar(Map<String, String> map) {
        String carNumber = map.get("carNumber");
        String commercialID = map.get("commercialID");
        String parkingLotId = map.get("parkingLotId");
        //根据车牌号和车场id查询出入场信息记录
        CarInfoCollect carInfoCollect = carInfoCollectService.getOne(Wrappers.<CarInfoCollect>lambdaQuery()
                .eq(CarInfoCollect::getLicense, carNumber)
                .eq(CarInfoCollect::getParkingLotId, parkingLotId)
                .in(CarInfoCollect::getCarStatus,CommonConstant.SX_CAR_STATUS_0,CommonConstant.SX_CAR_STATUS_4,CommonConstant.SX_CAR_STATUS_6));
        if(null==carInfoCollect){
            return Result.error("未找出车辆信息");
        }
        CommercialPayInfoVo commercialPayInfoVo = new CommercialPayInfoVo();
        BeanUtils.copyProperties(carInfoCollect,commercialPayInfoVo);
        //查询商户信息
        CarCommercial commercial = getById(commercialID);
        //设置有效时间：入场时间加减免数+（车场商户累计收费）
        Integer singleReduction = commercial.getSingleReduction();
        long time = carInfoCollect.getComeInTime().getTime() + singleReduction * 60 * 1000;
        commercialPayInfoVo.setValidTime(new Date(time));
        //查询是否有车场累计收费记录
        CarYardFees yardFees = carYardFeesService.getOne(Wrappers.<CarYardFees>lambdaQuery().eq(CarYardFees::getParkingLotId, parkingLotId)
                .eq(CarYardFees::getInfoCollectId, carInfoCollect.getId()));
        if(null!=yardFees){
            long yardTime = carInfoCollect.getComeInTime().getTime() + singleReduction + yardFees.getCumulativeMinute() * 60 * 1000;
            commercialPayInfoVo.setValidTime(new Date(yardTime));
        }
        commercialPayInfoVo.setSingleReduction(commercial.getSingleReduction());
        return Result.OK(commercialPayInfoVo);
    }

    @Transactional
    public Result<?> payCar(Map<String, String> map) throws Exception {
        String infoCollectId = map.get("infoCollectId");
        String commercialID = map.get("commercialID");
        String parkingLotId = map.get("parkingLotId");
        Integer singleReduction = Integer.valueOf(map.get("singleReduction"));
        Integer state = Integer.valueOf(map.get("state"));
        //1，生成车场商户消费记录
        CarYardFees carYardFees = new CarYardFees();
        carYardFees.setInfoCollectId(infoCollectId);
        carYardFees.setParkingLotId(parkingLotId);
        carYardFees.setCumulativeMinute(singleReduction);
        carYardFees.setCumulativeAmount(BigDecimal.ZERO);
        CarYardFees yardFees = carYardFeesService.getOne(Wrappers.<CarYardFees>lambdaQuery().eq(CarYardFees::getParkingLotId, parkingLotId)
                .eq(CarYardFees::getInfoCollectId, infoCollectId));
        if(null!=yardFees){
            //累计收费分钟
            carYardFees.setCumulativeMinute(yardFees.getCumulativeMinute()+singleReduction);
            carYardFees.setId(yardFees.getId());
            carYardFees.setCumulativeAmount(yardFees.getCumulativeAmount());
        }

        CarCommercial commercial = getById(commercialID);
        //2，如果是动态二维码，刷新key
        String key = String.format(CommonConstant.QUICKMARK+"%s", commercial.getId());
        //3：扣除商家费用：查询车场收费金额
        //      1.1：剩余分钟数足够，直接扣除
        //      1.2：剩余分钟数不足，还要扣除余额
        //      1.3：剩余分钟数不足，还要扣除余额，余额不足，提示错误
        //      1.4：如果商户没有剩余分钟数，计算金额，余额不足，提示错误
        CarConsumptionRecord carConsumptionRecord = new CarConsumptionRecord();
        carConsumptionRecord.setInfoCollectId(infoCollectId);
        carConsumptionRecord.setCommercialTenantId(commercialID);
        carConsumptionRecord.setPaymentTime(new Date());
        carConsumptionRecord.setNote(key);
        CarInfoCollect carInfoCollect = carInfoCollectService.getById(infoCollectId);
        MerchantAmountQuery amountQuery = new MerchantAmountQuery();
        amountQuery.setParkingLotId(parkingLotId);
        amountQuery.setTypeId(carInfoCollect.getCardTypeId());
        amountQuery.setComeInDate(carInfoCollect.getComeInTime());
        amountQuery.setMinutes(singleReduction);
        //计算收费金额
        BigDecimal merchantAmount = carStandardService.getMerchantAmount(amountQuery);
        //如果查询车场收费金额为0，则不用扣费，只需要生成记录
        if(!merchantAmount.equals(BigDecimal.ZERO)){
            if (null != commercial.getMinutesRemaining() && 0 != commercial.getMinutesRemaining()) {
                if (commercial.getMinutesRemaining() >= singleReduction) {
                    //# 1.1
                    commercial.setMinutesRemaining(commercial.getMinutesRemaining() - singleReduction);
                    carConsumptionRecord.setCreditMinute(singleReduction);
                    carConsumptionRecord.setCreditAmount(BigDecimal.ZERO);
                } else if (commercial.getMinutesRemaining() < singleReduction) {
                    //# 1.2
                    //多余分钟
                    int extraMinute = singleReduction - commercial.getMinutesRemaining();
                    //计算金额
                    amountQuery.setMinutes(extraMinute);
                    BigDecimal amount = carStandardService.getMerchantAmount(amountQuery);
                    //扣分钟
                    commercial.setMinutesRemaining(0);
                    carConsumptionRecord.setCreditMinute(commercial.getMinutesRemaining());
                    //扣余额
                    //1.3
                    org.springframework.util.Assert.isTrue(commercial.getBalance().compareTo(amount)>-1,"商户余额不足");
                    commercial.setBalance(commercial.getBalance().subtract(amount));
                    carConsumptionRecord.setCreditAmount(amount);
                }
            } else {
                //1.4
                org.springframework.util.Assert.isTrue(commercial.getBalance().compareTo(merchantAmount)>-1,"商户余额不足");
                commercial.setMinutesRemaining(0);
                commercial.setBalance(commercial.getBalance().subtract(merchantAmount));
                carConsumptionRecord.setCreditMinute(singleReduction);
                carConsumptionRecord.setCreditAmount(BigDecimal.ZERO);
            }
            //4：生成商户消费订单
            consumptionRecordService.save(carConsumptionRecord);
        }
        if(state==2){
            //刷新redis的key和更新数据库
            String uid = UUID.randomUUID()+"--"+commercial.getId();
            String serverName = String.valueOf(SysConfigUtil.getVal("serverName"));
            commercial.setQuickMark(serverName+"?id="+commercial.getId()+"&key="+uid);
            //如果是重新生成的二维码则清除之前的key
            if(redisTemplate.hasKey(key)){
                redisTemplate.delete(key);
            }
            redisTemplate.opsForValue().set(key,uid);
        }
        //发送websocket通知
        webSocketApi.pushMessage(commercial.getId(),commercial.getQuickMark());
        //更新商户信息
        updateById(commercial);
        //累计收费记录
        carYardFees.setCumulativeAmount(carYardFees.getCumulativeAmount().add(merchantAmount));
        carYardFeesService.saveOrUpdate(carYardFees);
        return Result.OK("支付成功");
    }

    public void deductionUpdate(CarCommercial carCommercial) {
        //确认折扣率and减免分钟数是否为负数
        Assert.isTrue(carCommercial.getDiscountRate().compareTo(BigDecimal.ZERO)!=-1,"折扣率不能为负数");
        Assert.isTrue(carCommercial.getSingleReduction()>0,"减免分钟数不能为负数");
        updateById(carCommercial);
    }

    public List rechargeRecordList(String id) {
        //查询充值记录，组织list
        List<CarRechargeRecord> recordList = carRechargeRecordService.list(Wrappers.<CarRechargeRecord>lambdaQuery().eq(CarRechargeRecord::getCommercialTenantId, id));
        if(null==recordList){
            return new ArrayList<>();
        }
        List<String> collect = recordList.stream().map(i -> {
            String number = i.getRechargeAmount().compareTo(BigDecimal.ZERO) > 0 ? i.getRechargeAmount()+"元" : i.getRechargeMinute()+"分钟";
            return DateUtils.formatDate(i.getPayTime(), "yyyy年MM月dd日HH时mm分ss秒") + " 充值：" + number;
        }).collect(Collectors.toList());
        return collect;
    }

    public List consumptionRecordList(String id) {
        //查询消费记录，组织list
        List<CarConsumptionRecord> recordList = consumptionRecordService.list(Wrappers.<CarConsumptionRecord>lambdaQuery().eq(CarConsumptionRecord::getCommercialTenantId, id));
        if(null==recordList){
            return new ArrayList<>();
        }
        List<String> collect = recordList.stream().map(i -> {
            String amount = i.getCreditAmount().compareTo(BigDecimal.ZERO) > 0 ? i.getCreditAmount() + "元":"";
            String minute = i.getCreditMinute() > 0 ? i.getCreditAmount() + "分钟":"";
            return DateUtils.formatDate(i.getPaymentTime(), "yyyy年MM月dd日HH时mm分ss秒") + " 消费了：" + amount+ minute;
        }).collect(Collectors.toList());
        return collect;

    }
}

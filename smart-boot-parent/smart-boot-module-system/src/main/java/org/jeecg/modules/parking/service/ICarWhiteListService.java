package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarRenewalHistory;
import org.jeecg.modules.parking.entity.CarWhiteList;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.query.CarWhiteListQuery;
import org.jeecg.modules.parking.vo.CarWhiteListVo;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @Description: 车牌白名单表
 * @Author: jeecg-boot
 * @Date: 2021-12-02
 * @Version: V1.0
 */
public interface ICarWhiteListService extends IService<CarWhiteList> {

    /**
     * 分页查询
     *
     * @param page
     * @param carWhiteListQuery
     * @return
     */
    Page<CarWhiteListVo> selectListPage(Page page, CarWhiteListQuery carWhiteListQuery);

    /**
     * 批量修改名单状态（黑白名单）
     *
     * @param ids
     * @param value
     * @return
     */
    Result<?> needAlarm(String ids, String value);

    /**
     * 根据车牌查找
     *
     * @param chepai
     * @return
     */
    CarWhiteList getByChePai(String chepai);

    //新增黑白名单
    Result<?> addCarWhiteListsave(CarWhiteList carWhiteList);
//修改黑白名单
    Result<?> updateCarWhiteListById(CarWhiteList carWhiteList);

    List<CarWhiteList> selectList();
//是否生效
    Result<?> SwitchChangeClick(String ids, String value, String needAlarm);
    //是否上传设备
    Result<?> SwitchChangeShow(String ids, boolean value, String needAlarm);

    Result<?> RenewalChangeShow(String id, Boolean value);

    Result<?> Renewal(CarRenewalHistory carRenewalHistory);

    Result<?> getCarWhiteByPlate(String plate, String openid);

    void addRenrwal(CarWhiteList whiteList, Result<?> result);
    /**
     * 判断白名单是否有效和是否过期
     * @return
     */
    Boolean ifFag(CarWhiteList byChePai);

    /**
     * 判断是否是黑名单
     * @return
     */
    Boolean ifBlack(CarWhiteList byChePai);

    /**
     * @param EnableTime   开始时间
     * @param OverdueTime  过期时间
     * @param ParkingLotId 车场id
     * @param UploadDevice 是否上传
     * @param Plate        车牌
     * @param Enable       是否有效
     * @param NeedAlarm    黑白名单
     */
    public void uploadTheCloud(Date EnableTime, Date OverdueTime,
                               String ParkingLotId, Boolean UploadDevice,
                               String Plate, String Enable, String NeedAlarm);

    Result<?> generateRenewalOrder(String id, String openid, Integer renewalTime, BigDecimal amount, String plateTypeId, String companyId, String companyName, String parkingLotName, String plate, String phone);

    String getPhone(String openid);

    ModelAndView exportXls(HttpServletRequest request, CarWhiteListQuery carWhiteList, Class<CarWhiteList> carWhiteListClass, String 车牌白名单表);

    Result<?> importExcel(HttpServletRequest request, HttpServletResponse response, Class<CarWhiteList> carWhiteListClass);

    List<CarWhiteList> detectionWhitelistExpired(String date);
}

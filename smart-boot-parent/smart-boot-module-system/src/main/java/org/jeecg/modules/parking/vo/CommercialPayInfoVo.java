package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarInfoCollect;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CommercialPayInfoVo extends CarInfoCollect {

    /**
     * 减免分钟
     */
    private Integer singleReduction;

    /**
     * 有效时至
     */
    private Date validTime;


}

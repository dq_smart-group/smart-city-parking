package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarTimeSlot;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.query.CarTimeSlotQuery;
import org.jeecg.modules.parking.vo.CarTimeSlotVo;

import java.util.List;

/**
 * @Description: 时间段管理表
 * @Author: jeecg-boot
 * @Date:   2022-01-23
 * @Version: V1.0
 */
public interface ICarTimeSlotService extends IService<CarTimeSlot> {

    IPage<CarTimeSlotVo> pageList(Page<CarTimeSlotVo> page, CarTimeSlotQuery carTimeSlotQuery);

    /**
     * 添加
     * @param carTimeSlot
     */
    Result saveCarTimeSlot(CarTimeSlot carTimeSlot);

    /**
     * 修改
     * @param carTimeSlot
     * @return
     */
    Result updateCarTimeSlotById(CarTimeSlot carTimeSlot);

    /**
     * 根据设备id和车辆类型和星期几查找时间段管理
     * @param cameraId 设备id
     * @param type 车辆类型
     * @param day 星期几
     * @return
     */
    List<CarTimeSlot> getCarTimeSlotSByCameraIdAndType(String cameraId,String type,Integer day);

    List<CarTimeSlot> getList();

}

package org.jeecg.modules.history.service;

import org.jeecg.modules.history.entity.CarAccessRecordHistory;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @Description: car_access_record_history_1
 * @Author: jeecg-boot
 * @Date:   2022-03-01
 * @Version: V1.0
 */
public interface ICarAccessRecordHistoryService extends IService<CarAccessRecordHistory> {

    /**
     * 获取表格数据数量
     * @param tableName
     * @return
     */
    int getListCount(String tableName);

    /**
     * 创建表
     * @param tableName
     */
    void createHistoryTable(String tableName);

    /**
     * 保存数据到历史
     * @param list
     * @return
     */
//    int saveDataToHistory(List<CarAccessRecord> list,String tableName);
}

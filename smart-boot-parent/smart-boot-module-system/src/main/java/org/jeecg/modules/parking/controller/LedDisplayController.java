package org.jeecg.modules.parking.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.entity.LedDisplay;
import org.jeecg.modules.parking.mqtt.MqttPushClient;
import org.jeecg.modules.parking.query.LedDisplayQuery;
import org.jeecg.modules.parking.query.LedQuery;
import org.jeecg.modules.parking.service.ILedDisplayService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.parking.vo.LedDisplayVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: led显示操作表
 * @Author: jeecg-boot
 * @Date:   2022-01-05
 * @Version: V1.0
 */
@Api(tags="led显示操作表")
@RestController
@RequestMapping("/parking/ledDisplay")
@Slf4j
public class LedDisplayController extends JeecgController<LedDisplay, ILedDisplayService> {
	@Autowired
	private ILedDisplayService ledDisplayService;
	@Autowired
	private ICameraDeviceService cameraDeviceService;
	@Autowired
	private ISysDepartService sysDepartService;

	@Autowired
	private RedisUtil redisUtil;
	
	/**
	 * 分页列表查询
	 *
	 * @param ledDisplayQuery
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "led显示操作表-分页列表查询")
	@ApiOperation(value="led显示操作表-分页列表查询", notes="led显示操作表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(LedDisplayQuery ledDisplayQuery,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				ledDisplayQuery.setCompanyId(sysDepart.getId());
			}else{
				ledDisplayQuery.setCompanyId("0");
			}
		}
		Page<LedDisplayVo> page = new Page<LedDisplayVo>(pageNo, pageSize);
		IPage<LedDisplayVo> pageList = ledDisplayService.pageList(page, ledDisplayQuery);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param ledDisplay
	 * @return
	 */
	@AutoLog(value = "led显示操作表-添加")
	@ApiOperation(value="led显示操作表-添加", notes="led显示操作表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody LedDisplay ledDisplay) {
		return ledDisplayService.saveLedDisplay(ledDisplay);
	}
	
	/**
	 *  编辑
	 *
	 * @param ledDisplay
	 * @return
	 */
	@AutoLog(value = "led显示操作表-编辑")
	@ApiOperation(value="led显示操作表-编辑", notes="led显示操作表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody LedDisplay ledDisplay) {
		LedDisplay byId = ledDisplayService.getById(ledDisplay.getId());
		if(byId.getVolume()!=ledDisplay.getVolume()){
			//调整音量大小
			CameraDevice byId1 = cameraDeviceService.getById(ledDisplay.getEquipmentId());
			String topic = MqttPushClient.getTopic(byId1.getSerialno());
			MqttPushClient.pushVolume(topic,ledDisplay.getVolume());
		}

		return ledDisplayService.updateLedDisplayById(ledDisplay);
	}

	 /**
	  * 下载广告语
	  *
	  * @param ledQuery
	  * @return
	  */
	 @AutoLog(value = "下载广告语")
	 @ApiOperation(value="下载广告语", notes="下载广告语")
	 @PostMapping(value = "/downloadAdvertisement")
	 public Result<?> downloadAdvertisement(@RequestBody LedQuery ledQuery) throws Exception{
	 	MqttPushClient.pushBytesToHexFun(MqttPushClient.getTopic(ledQuery.getSerialno()),ledQuery.getText(),ledQuery.getHang(),ledQuery.getColor(),ledQuery.getType());
		 return Result.OK("下载成功");
	 }
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "led显示操作表-通过id删除")
	@ApiOperation(value="led显示操作表-通过id删除", notes="led显示操作表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		LedDisplay byId = ledDisplayService.getById(id);
		redisUtil.del(ParkingRedisConstant.PARKING_LED+byId.getEquipmentId());
		ledDisplayService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "led显示操作表-批量删除")
	@ApiOperation(value="led显示操作表-批量删除", notes="led显示操作表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		String[] split = ids.split(",");
		for (String s : split) {
			LedDisplay byId = ledDisplayService.getById(s);
			redisUtil.del(ParkingRedisConstant.PARKING_LED+byId.getEquipmentId());
		}
		this.ledDisplayService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "led显示操作表-通过id查询")
	@ApiOperation(value="led显示操作表-通过id查询", notes="led显示操作表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		LedDisplay ledDisplay = ledDisplayService.getById(id);
		if(ledDisplay==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(ledDisplay);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param ledDisplay
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, LedDisplay ledDisplay) {

        return super.exportXls(request, ledDisplay, LedDisplay.class, "led显示操作表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, LedDisplay.class);
    }


}

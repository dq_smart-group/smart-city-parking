package org.jeecg.modules.device.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.mapper.CameraDeviceMapper;
import org.jeecg.modules.device.query.CameraDeviceQuery;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.device.vo.CameraDeviceVo;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.mapper.CarDeviceGroupMapper;
import org.jeecg.modules.parking.query.CameraListQuery;
import org.jeecg.modules.parking.service.ICarDeviceGroupService;
import org.jeecg.modules.parking.service.ICarInfoCollectService;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.tcp.SocketClient;
import org.jeecg.modules.parking.tcp.SocketWriter;
import org.jeecg.modules.parking.vo.CameraDeviceChVo;
import org.jeecg.modules.parking.vo.CarInfoCollectVo;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.Socket;
import java.util.*;

/**
 * @Description: 停车场摄像机设备信息表
 * @Author: jeecg-boot
 * @Date:   2021-11-25
 * @Version: V1.0
 */
@Service
@Slf4j
public class CameraDeviceServiceImpl extends ServiceImpl<CameraDeviceMapper, CameraDevice> implements ICameraDeviceService {

    @Autowired
    private IParkingLotService parkingLotService;

    @Autowired
    private ISysDepartService departService;

    @Autowired
    private ISysDepartService sysDepartService;

    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private ICarInfoCollectService carInfoCollectService;


    @Autowired
    private ICarDeviceGroupService carDeviceGroupService;



    @Override
    public IPage<CameraDeviceVo> listPage(Page<CameraDeviceVo> page, CameraDeviceQuery cameraDeviceQuery) {
        List<CameraDeviceVo> list=this.baseMapper.listPage(page,cameraDeviceQuery);
        return page.setRecords(list);
    }

    /**
     * 根据序列号查出设备用处
     * @param serlino  设备序列号
     * @return
     */
    @Override
    public CameraDeviceVo selectDevice(String serlino) {
        return this.baseMapper.selectByDevice(serlino);
    }
    /**
     * 根据设备号查询设备
     * @param serlino
     * @return
     */
    @Override
    public CameraDevice selectByDevice(String serlino) {
        LambdaQueryWrapper<CameraDevice> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(CameraDevice::getSerialno,serlino);
        CameraDevice cameraDevice=this.getOne(queryWrapper,false);
        if (cameraDevice==null){
            return null;
        }
        return cameraDevice;
    }

    /**
     * 根据公司编码查询出公司下的设备
     * @param companyId 公司id
     * @return
     */
    @Override
    public List<CameraDevice> selectByOrgCode(String companyId) {
        return this.baseMapper.selectByOrgCode(companyId);
    }

    /**
     * 根据车场id查询车场内所有的设备
     * @param id
     * @return
     */
    @Override
    public List<ParkingLotAndCamVo> getByParkingLotId(String id) {
        return this.baseMapper.getByParkingLotId(id);
    }

    /**
     * 根据车场id查询车场内所有的设备
     * @param lotId
     * @return
     */
    @Override
    public List<CameraDevice> getParkingManagementDeviceByLotId(String lotId) {
        return baseMapper.selectList(new LambdaQueryWrapper<CameraDevice>().eq(CameraDevice::getParkingLotId,lotId));
    }

    /**
     * 获取登录人下的设备 级联
     * @return
     */
    @Override
    public List<ParkingLotAndCamVo> selectByCompanyId() {

        String companyId = null;
        if(!LoginUserUtil.isAdmin()){
            companyId = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode()).getId();
            if(StringUtils.isEmpty(companyId)){
                companyId = "0";
            }
        }
        List<ParkingLotAndCamVo> list = null;
        if(companyId == null){
            list = (List<ParkingLotAndCamVo>)redisUtil.get(ParkingRedisConstant.PARKING_DEVICE_EQUIPMENT + "admin");
        }else{
            list = (List<ParkingLotAndCamVo>)redisUtil.get(ParkingRedisConstant.PARKING_DEVICE_EQUIPMENT + companyId);
        }
        if(list!=null){
            if(list.size()>0){
                return list;
            }
        }
        return selectDeviceByCompanyId(companyId);
    }

    /**
     * 更新redis数据
     */
    @Override
    public void updateRedisDeviceGroup() {
        SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
        List<ParkingLotAndCamVo> parkingLotAndCamVos = selectDeviceByCompanyId(sysDepart.getId());
        redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_EQUIPMENT+sysDepart.getId(),parkingLotAndCamVos);
        //管理员
        List<ParkingLotAndCamVo> parkingLotAndCamVoss = selectDeviceByCompanyId(null);
        redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_EQUIPMENT + "admin",parkingLotAndCamVoss);
    }
    /**
     * 加载级联设备的信息到redis中
     */
    public void saveDeviceGroupInRedis(){
        List<SysDepart> sysDeparts = sysDepartService.queryDept();
        for (SysDepart sysDepart : sysDeparts) {
            List<ParkingLotAndCamVo> parkingLotAndCamVos = selectDeviceByCompanyId(sysDepart.getId());
            if(parkingLotAndCamVos.size()>0){
                redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_EQUIPMENT + sysDepart.getId(),parkingLotAndCamVos);
            }
        }
        //管理员
        List<ParkingLotAndCamVo> parkingLotAndCamVos = selectDeviceByCompanyId(null);
        redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_EQUIPMENT + "admin",parkingLotAndCamVos);
    }

    /**
     * 查询设备根据公司id
     */
    public List<ParkingLotAndCamVo> selectDeviceByCompanyId(String companyId){
        List<CameraDevice> mainDeviceByGroupIds = this.baseMapper.getMainDeviceByGroupIds(null);
        List<ParkingLotAndCamVo> parkingLotByCompanyId = carDeviceGroupService.selectDeviceGroupByCompanyId(companyId);

        List<ParkingLotAndCamVo> lotAndCamVoArrayLists = new ArrayList<>();
        if(parkingLotByCompanyId.size()>0){
            //公司
            for (ParkingLotAndCamVo parkingLotAndCamVo : parkingLotByCompanyId) {
                List<ParkingLotAndCamVo> children = parkingLotAndCamVo.getChildren();
                List<ParkingLotAndCamVo> lotAndCamVoArrayList = new ArrayList<>();
                if(children.size()>0){
                    //车场
                    for (ParkingLotAndCamVo child : children) {
                        List<ParkingLotAndCamVo> lotAndCamVos = child.getChildren();
                        List<ParkingLotAndCamVo> parkingLotAndCamVos = new ArrayList<>();
                        if(lotAndCamVos.size()>0){
                            //岗亭
                            for (ParkingLotAndCamVo lotAndCamVo : lotAndCamVos) {
                                List<ParkingLotAndCamVo> children1 = lotAndCamVo.getChildren();
                                List<ParkingLotAndCamVo> parkingLotAndCamVos1 = new ArrayList<>();
                                //获取到设备组
                                for (ParkingLotAndCamVo andCamVo : children1) {
                                    //List<ParkingLotAndCamVo> byParkingLotId = this.baseMapper.getByParkingLotId(andCamVo.getValue());
                                    List<ParkingLotAndCamVo> byParkingLotId = new ArrayList<>();
                                    for (CameraDevice mainDeviceByGroupId : mainDeviceByGroupIds) {
                                        if(mainDeviceByGroupId.getDeviceGroupId().equals(andCamVo.getValue())){
                                            ParkingLotAndCamVo lotAndCamVo1 = new ParkingLotAndCamVo();
                                            lotAndCamVo1.setValue(mainDeviceByGroupId.getId());
                                            lotAndCamVo1.setLabel(mainDeviceByGroupId.getDeviceName());
                                            byParkingLotId.add(lotAndCamVo1);
                                            break;
                                        }
                                    }
                                    if(byParkingLotId.size()>0){
                                        andCamVo.setChildren(byParkingLotId);
                                        parkingLotAndCamVos1.add(andCamVo);
                                    }
                                }
                                lotAndCamVo.setChildren(parkingLotAndCamVos1);
                                if(parkingLotAndCamVos1.size()>0){
                                    parkingLotAndCamVos.add(lotAndCamVo);
                                }
                            }
                        }
                        child.setChildren(parkingLotAndCamVos);
                        if(parkingLotAndCamVos.size()>0){
                            lotAndCamVoArrayList.add(child);
                        }
                    }
                    if(lotAndCamVoArrayList.size()>0){
                        parkingLotAndCamVo.setChildren(lotAndCamVoArrayList);
                        lotAndCamVoArrayLists.add(parkingLotAndCamVo);
                    }
                }
            }
        }
        return lotAndCamVoArrayLists;
    }

    /**
     * 添加设备
     * @param cameraDevice
     * @return
     */
    @Override
    public Result<?> saveCameraDevice(CameraDevice cameraDevice) {
        //查询有没有重复设备的，根据设备号
        boolean listBySerialno = getListBySerialno(cameraDevice);
        if(listBySerialno){
            if(cameraDevice.getRepairFlag().equals(CommonConstant.PARKING_DEVICE_TYPE_0)){//设置为主设备的情况
                //查看该设备组是否已经设置有主设备
                updateByRepairFlag(cameraDevice.getId(),cameraDevice.getDeviceGroupId(),CommonConstant.PARKING_DEVICE_TYPE_0);
            }else{//设置为辅助设备的情况，判断该设备组有没有主设备，没有则默认为主设备
                cameraDevice = ifcameraDeviceByDeviceGroup(cameraDevice);
            }
            this.baseMapper.insert(cameraDevice);
            redisUtil.set(ParkingRedisConstant.PARKING_DEVICE+cameraDevice.getSerialno(),cameraDevice);
            setDataInRedis(cameraDevice.getDeviceGroupId());
            updateRedisDeviceGroup();
            return Result.OK("添加成功");
        }else{
            return Result.error("已添加有该设备，请勿重复添加");
        }
    }

    public CameraDevice ifcameraDeviceByDeviceGroup(CameraDevice cameraDevice){
        List<String> list = new ArrayList<>();
        list.add(cameraDevice.getDeviceGroupId());
        List<CameraDevice> mainDeviceByGroupIds = getMainDeviceByGroupIds(list);
        if(mainDeviceByGroupIds.size()>0){
            return cameraDevice;
        }
        cameraDevice.setRepairFlag(CommonConstant.PARKING_DEVICE_TYPE_0);
        return cameraDevice;
    }
    /**
     *
     * @param id
     * @param groupId 设备组id
     * @param repairFlag 设备类型 0:主设备，1：辅设备，2：补拍设备
     */
    public void updateByRepairFlag(String id,String groupId,String repairFlag){
        //查看该设备组是否已经设置有主设备
        List<CameraDevice> cameraDeviceList = this.baseMapper.getByGroupIdAndRepairFlag(id,groupId,repairFlag);
        if(cameraDeviceList.size()>0){
            for (CameraDevice device : cameraDeviceList) {
                if(CommonConstant.PARKING_DEVICE_TYPE_0.equals(device.getRepairFlag())){
                    device.setRepairFlag(CommonConstant.PARKING_DEVICE_TYPE_1);
                    this.baseMapper.updateById(device);
                }
            }
        }
    }

    @Override
    public Result<?> updateByCameraDevice(CameraDevice cameraDevice) {
        boolean listBySerialno = getListBySerialno(cameraDevice);
        if(listBySerialno){
            if(cameraDevice.getRepairFlag().equals(CommonConstant.PARKING_DEVICE_TYPE_0)){
                //查看该设备组是否已经设置有主设备
                updateByRepairFlag(cameraDevice.getId(),cameraDevice.getDeviceGroupId(),CommonConstant.PARKING_DEVICE_TYPE_0);
            }else{
                cameraDevice = ifcameraDeviceByDeviceGroup(cameraDevice);
            }
            CameraDevice cameraDevice1 = this.baseMapper.selectById(cameraDevice.getId());
            if(!cameraDevice.getDeviceGroupId().equals(cameraDevice1.getDeviceGroupId())){
                setDataInRedis(cameraDevice1.getDeviceGroupId());
            }
            if(!cameraDevice.getSerialno().equals(cameraDevice1.getSerialno())){
                redisUtil.del(ParkingRedisConstant.PARKING_DEVICE+cameraDevice1.getSerialno());
            }
            this.baseMapper.updateById(cameraDevice);
            if(!cameraDevice1.getDeviceGroupId().equals(cameraDevice.getDeviceGroupId())){
                setDataInRedis(cameraDevice1.getDeviceGroupId());
            }
            redisUtil.set(ParkingRedisConstant.PARKING_DEVICE+cameraDevice.getSerialno(),cameraDevice);
            setDataInRedis(cameraDevice.getDeviceGroupId());
            updateRedisDeviceGroup();
            return Result.OK("编辑成功");
        }else{
            return Result.error("已添加有该设备，请勿重复添加");
        }
    }

    /**
     * 添加数据到redis
     * @param groupId
     */
    public void setDataInRedis(String groupId){
        redisUtil.del(ParkingRedisConstant.PARKING_DEVICE+groupId,ParkingRedisConstant.MAKE_FLAG+groupId);
        LambdaQueryWrapper<CameraDevice> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CameraDevice::getDeviceGroupId,groupId);
        wrapper.eq(CameraDevice::getIsoffline,CommonConstant.CURRENCY_STATUS_0);
        List<CameraDevice> cameraDevices = this.baseMapper.selectList(wrapper);
        //设备信息(按设备组id储存)
        if(cameraDevices.size()>0){
            Map<String,List<CameraDevice>> map = new HashMap<>();
            Map<String,List<CameraDevice>> map1 = new HashMap<>();
            List<CameraDevice> cameraDevices1;
            List<CameraDevice> cameraDevices2;
            for (CameraDevice cameraDevice : cameraDevices) {
                if(CommonConstant.PARKING_DEVICE_TYPE_2.equals(cameraDevice.getRepairFlag())){
                    //为抓拍设备
                    String deviceGroupId = cameraDevice.getDeviceGroupId();
                    cameraDevices2 = map1.get(deviceGroupId);
                    if(null == cameraDevices2){
                        cameraDevices2 = new ArrayList<>();
                        map1.put(deviceGroupId,cameraDevices2);
                    }
                    cameraDevices2.add(cameraDevice);
                    continue;
                }
                String deviceGroupId = cameraDevice.getDeviceGroupId();
                cameraDevices1 = map.get(deviceGroupId);
                if(null == cameraDevices1){
                    cameraDevices1 = new ArrayList<>();
                    map.put(deviceGroupId,cameraDevices1);
                }
                cameraDevices1.add(cameraDevice);

            }
            for (String s : map.keySet()) {
                redisUtil.set(ParkingRedisConstant.PARKING_DEVICE+s,map.get(s));
            }
            for (String s : map1.keySet()) {
                redisUtil.set(ParkingRedisConstant.MAKE_FLAG+s,map1.get(s));
            }
        }

    }



    @Override
    public Result<?> modifyIP(CameraDevice cameraDevice) {
        try {
            CameraDevice cameraDevice1 = this.baseMapper.selectById(cameraDevice.getId());
            Socket socket = SocketClient.open(cameraDevice1.getIpaddr());
            if(socket!=null){
                SocketWriter.setNetworkparam(socket.getOutputStream(),cameraDevice.getIpaddr());
                SocketClient.close(socket);
                this.baseMapper.updateById(cameraDevice);
                return Result.error("修改成功");
            }
        }catch (IOException e){
            e.printStackTrace();
        }
        return Result.error("修改失败");
    }

    /**
     * 根据设备组id查找该设备组的主设备
     * @param collect
     * @return
     */
    @Override
    public List<CameraDevice> getMainDeviceByGroupIds(List<String> collect) {
        return this.baseMapper.getMainDeviceByGroupIds(collect);
    }

    /**
     * 根据园区id查询此园区下的所有设备
     * @param screenCode
     * @return
     */
    @Override
    public List<String> screenDeviceList(String screenCode) {
        List<String> list=this.baseMapper.screenDeviceList(screenCode);
        if (list.size()>0){
            return list;
        }
        return null;
    }

    /**
     * 根据园区id查找设备
     * @param parkId
     * @return
     */
    @Override
    public List<CameraDevice> getDeviceByParkId(String parkId) {
        return this.baseMapper.getDeviceByParkId(parkId);
    }

    /**
     * 定时检查设备状态：在线、离线
     * 检测出来状态和数据库的原本数据状态不一样，则需要更新缓存设备组信息
     * @return
     */
    @Override
    public void checkTheEquipmentStatus() {
        List<CameraDevice> list = this.baseMapper.selectByParkingStateYard();
        Set<String> deviceGroupIds = new HashSet();
        if(list.size()>0){
            for (CameraDevice cameraDevice : list) {
                boolean b = redisUtil.hasKey(ParkingRedisConstant.PARKING_DEVICE_HEARTBEAT + cameraDevice.getSerialno());
                if(b){
                    if(!cameraDevice.getIsoffline().equals(CommonConstant.CURRENCY_STATUS_0)){//与原数据库的数据不一样，把设备组id保存下来实时更新
                        deviceGroupIds.add(cameraDevice.getDeviceGroupId());
                    }
                    cameraDevice.setIsoffline(CommonConstant.CURRENCY_STATUS_0);
                }else{
                    if(!cameraDevice.getIsoffline().equals(CommonConstant.CURRENCY_STATUS_1)){//与原数据库的数据不一样，把设备组id保存下来实时更新
                        deviceGroupIds.add(cameraDevice.getDeviceGroupId());
                    }
                    cameraDevice.setIsoffline(CommonConstant.CURRENCY_STATUS_1);
                }
            }
            this.updateBatchById(list);
            //更新设备的redis缓存
            for (String deviceGroupId : deviceGroupIds) {
                log.info("由于检测到设备的是否在线状态发生变化，实时更新redis状态");
                setDataInRedis(deviceGroupId);
            }
        }
    }

    @Override
    public List<CameraDevice> getList() {
        LambdaQueryWrapper<CameraDevice> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CameraDevice::getIsoffline,CommonConstant.CURRENCY_STATUS_0);
        return this.baseMapper.selectList(wrapper);
    }

    @Override
    public List<CameraDeviceChVo> getCameraDeviceChVoByParkingLotId(String parkingLotId) {
        List<CameraDeviceChVo> list= new ArrayList<>();
        LambdaQueryWrapper<CameraDevice> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CameraDevice::getParkingLotId,parkingLotId);
        List<CameraDevice> cameraDevices = this.baseMapper.selectList(wrapper);

        for (int i = 0; i < cameraDevices.size(); i++) {
            CameraDeviceChVo cameraDeviceChVo = new CameraDeviceChVo();
            CarInfoCollectVo carInfoCollectvo = new CarInfoCollectVo();
            CameraDevice cameraDevice = cameraDevices.get(i);
            cameraDeviceChVo.setDevicename(cameraDevice.getDeviceName());
            cameraDeviceChVo.setRtsp("rtsp://"+cameraDevice.getIpaddr()+":8557/h264");
            cameraDeviceChVo.setSerialno(cameraDevice.getSerialno());
            cameraDeviceChVo.setDeviceUse(cameraDevice.getDeviceUse());
            cameraDeviceChVo.setId(i+1+"");
            if(cameraDevice.getDeviceUse().equals("0")){
                //入口
                carInfoCollectvo.setComeSerialno(cameraDevice.getSerialno());
                List<String> lists = new ArrayList<>();
                lists.add(CommonConstant.SX_CAR_STATUS_0);
                lists.add(CommonConstant.SX_CAR_STATUS_1);
                lists.add(CommonConstant.SX_CAR_STATUS_4);
                lists.add(CommonConstant.SX_CAR_STATUS_5);
                lists.add(CommonConstant.SX_CAR_STATUS_6);
                carInfoCollectvo.setCarStatuslist(lists);
                carInfoCollectvo.setCarStatuslists("0,1,4,5,6");
            }else if(cameraDevice.getDeviceUse().equals("1")){
                //出口
                carInfoCollectvo.setOutSerialno(cameraDevice.getSerialno());
                List<String> lists = new ArrayList<>();
                lists.add(CommonConstant.SX_CAR_STATUS_1);
                carInfoCollectvo.setCarStatuslist(lists);
                carInfoCollectvo.setCarStatuslists("1");
            }

            Page<CarInfoCollectVo> page = new Page<CarInfoCollectVo>(1, 10);
            Page<CarInfoCollectVo> carInfoCollectVoPage = carInfoCollectService.selectData(page, carInfoCollectvo);
            cameraDeviceChVo.setDataVoList(carInfoCollectVoPage.getRecords());
            list.add(cameraDeviceChVo);
        }
        return list;
    }


    public boolean getListBySerialno(CameraDevice cameraDevice){
        LambdaQueryWrapper<CameraDevice> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CameraDevice::getSerialno,cameraDevice.getSerialno());
        if(cameraDevice.getId()!=null){
            wrapper.ne(CameraDevice::getId,cameraDevice.getId());
        }
        List<CameraDevice> cameraDevices = this.baseMapper.selectList(wrapper);
        if(cameraDevices.size()>0){
            return false;
        }else{
            return true;
        }
    }
}

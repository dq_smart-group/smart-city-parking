package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class AlarmInfoPlateQuery {
    private String channel;
    private String deviceName;
    private String ipaddr;
    private String serialno;
    private String user_data;
}

package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarWhiteList;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CarWhiteListQuery extends CarWhiteList {
    /**
     * 有效时间开始
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startenableTime;
    /**
     * 有效时间结束
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endenableTime;
    /**
     * 无效时间开始
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startdueTime;
    /**
     * 无效时间结束
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date enddueTime;

    /**创建开始时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    /**创建结束时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;
    //公司名称
    private String departName;

    //车场名称
    private String parkingLotName;
}

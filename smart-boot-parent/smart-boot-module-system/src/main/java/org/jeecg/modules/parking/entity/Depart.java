package org.jeecg.modules.parking.entity;


import lombok.Data;

@Data
public class Depart   {
    //公司id
   private String id;

   //公司名
   private String departName;

}

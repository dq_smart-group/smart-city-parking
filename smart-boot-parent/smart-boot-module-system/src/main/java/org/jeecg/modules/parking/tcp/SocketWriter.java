package org.jeecg.modules.parking.tcp;

import cn.hutool.json.JSONObject;

import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

public class SocketWriter {

    /**
     * 设置网络参数：
     * ip 修改目标端口
     * @param os
     */
    public static boolean setNetworkparam(OutputStream os, String ip) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        map1.put("ip",ip);
        map1.put("netmask","255.255.255.0");
        map1.put("gateway","192.168.1.1");
        map1.put("dns","114.114.114.114");
        map.put("cmd","set_networkparam");
        map.put("id","123456");
        map.put("body",map1);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return SocketSendParameter.sendCmd(os,cmd);
    }



    /**
     * @Description 获取设备实时视频uri
     */
    public static boolean getRtspUri(OutputStream os){
        Map map = new HashMap();
        map.put("cmd","get_rtsp_uri");
        map.put("id","7777777");
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return SocketSendParameter.sendCmd(os,cmd);
    }

    /**
     * 获取当前图片
     * @param os
     * @return
     */
    public static boolean getSnapshot(OutputStream os){
        Map map = new HashMap();
        map.put("cmd","get_snapshot");
        map.put("id","123456");
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return SocketSendParameter.sendCmd(os,cmd);
    }

    /**
     * 获取最近一次识别结果
     * @param os
     * @return
     */
    public static boolean getivsresult(OutputStream os){
        Map map = new HashMap();
        map.put("cmd","getivsresult");
        map.put("image",true);
        map.put("format","json");
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return SocketSendParameter.sendCmd(os,cmd);
    }

    /**
     * 获取记录图片
     * @param os
     * @param id
     * @return
     */
    public static boolean getImage(OutputStream os, Integer id){
        Map map = new HashMap();
        map.put("cmd","get_image");
        map.put("id",id);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return SocketSendParameter.sendCmd(os,cmd);
    }

}

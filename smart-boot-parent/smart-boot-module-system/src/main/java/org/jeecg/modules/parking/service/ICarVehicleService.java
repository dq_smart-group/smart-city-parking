package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarVehicle;
import org.jeecg.modules.parking.query.CarVehicleQuery;
import org.jeecg.modules.parking.vo.CarVehicleVo;

/**
 * @Description: 车辆信息表
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface ICarVehicleService extends IService<CarVehicle> {

    //分页查询
    IPage<CarVehicleVo> pageList(Page<CarVehicleVo> page, CarVehicleQuery carVehicleQuery);

    /**
     * 根据车牌号和车场id查询车牌类型
     * @param license
     * @param parkId
     * @return
     */
    String carTypeId(String license,String parkId);
     //车辆信息新增
     Result addcarVehicleSave(CarVehicle carVehicle);
    //车辆信息编辑
    Result updatecarVehicleById(CarVehicle carVehicle);
}

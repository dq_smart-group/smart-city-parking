package org.jeecg.modules.device.vo;

import lombok.Data;
import org.jeecg.modules.device.entity.CameraDevice;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CameraDeviceVo extends CameraDevice {
    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 车场名称
     */
    private String parkingName;
    /**
     * 岗亭名称
     */
    private String sentryBoxName;
    /**
     * 公司名称
     */
    private String companyName;
    /**
     * 设备组名
     */
    private String groupName;
    //园区主标题
    private String titleName;



    /**
     * 开闸原因
     */
    private String whyOpenGate;
}

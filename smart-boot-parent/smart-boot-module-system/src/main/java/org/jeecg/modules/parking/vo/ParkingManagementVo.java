package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.jeecg.modules.device.entity.CameraDevice;

import java.util.List;

@Data
public class ParkingManagementVo {

    private String value;

    private String label;

    List<CameraDevice> cameraDeviceList;

}

package org.jeecg.modules.parking.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.parking.entity.CarTimeSlot;
import org.jeecg.modules.parking.query.CarTimeSlotQuery;
import org.jeecg.modules.parking.service.ICarTimeSlotService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.parking.vo.CarTimeSlotVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 时间段管理表
 * @Author: jeecg-boot
 * @Date:   2022-01-23
 * @Version: V1.0
 */
@Api(tags="时间段管理表")
@RestController
@RequestMapping("/parking/carTimeSlot")
@Slf4j
public class CarTimeSlotController extends JeecgController<CarTimeSlot, ICarTimeSlotService> {
	@Autowired
	private ICarTimeSlotService carTimeSlotService;
	 @Autowired
	 private ISysDepartService sysDepartService;
	 @Autowired
	 private RedisUtil redisUtil;
	/**
	 * 分页列表查询
	 *
	 * @param carTimeSlotQuery
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "时间段管理表-分页列表查询")
	@ApiOperation(value="时间段管理表-分页列表查询", notes="时间段管理表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarTimeSlotQuery carTimeSlotQuery,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carTimeSlotQuery.setCompanyId(sysDepart.getId());
			}else{
				carTimeSlotQuery.setCompanyId("0");
			}

		}
		Page<CarTimeSlotVo> page = new Page<CarTimeSlotVo>(pageNo, pageSize);
		IPage<CarTimeSlotVo> pageList = carTimeSlotService.pageList(page, carTimeSlotQuery);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carTimeSlot
	 * @return
	 */
	@AutoLog(value = "时间段管理表-添加")
	@ApiOperation(value="时间段管理表-添加", notes="时间段管理表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarTimeSlot carTimeSlot) {
		Result result = carTimeSlotService.saveCarTimeSlot(carTimeSlot);
		return result;
	}
	
	/**
	 *  编辑
	 *
	 * @param carTimeSlot
	 * @return
	 */
	@AutoLog(value = "时间段管理表-编辑")
	@ApiOperation(value="时间段管理表-编辑", notes="时间段管理表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarTimeSlot carTimeSlot) {
		Result result = carTimeSlotService.updateCarTimeSlotById(carTimeSlot);
		return result;
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "时间段管理表-通过id删除")
	@ApiOperation(value="时间段管理表-通过id删除", notes="时间段管理表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		CarTimeSlot byId = carTimeSlotService.getById(id);
		String[] split = byId.getExecutionTime().split(",");
		for (String s : split) {
			redisUtil.del(ParkingRedisConstant.PARKING_TIME_SLOT+byId.getDeviceId()+":"+byId.getType()+":"+s);
		}
		carTimeSlotService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "时间段管理表-批量删除")
	@ApiOperation(value="时间段管理表-批量删除", notes="时间段管理表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		String[] splits = ids.split(",");
		for (String spli : splits) {
			CarTimeSlot byId = carTimeSlotService.getById(spli);
			String[] split = byId.getExecutionTime().split(",");
			for (String s : split) {
				redisUtil.del(ParkingRedisConstant.PARKING_TIME_SLOT+byId.getDeviceId()+":"+byId.getType()+":"+s);
			}
		}
		this.carTimeSlotService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "时间段管理表-通过id查询")
	@ApiOperation(value="时间段管理表-通过id查询", notes="时间段管理表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarTimeSlot carTimeSlot = carTimeSlotService.getById(id);
		if(carTimeSlot==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carTimeSlot);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carTimeSlot
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarTimeSlot carTimeSlot) {
        return super.exportXls(request, carTimeSlot, CarTimeSlot.class, "时间段管理表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarTimeSlot.class);
    }

}

package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.parking.entity.CarConsumptionRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.vo.CarConsumptionRecordVo;

/**
 * @Description: car_consumption_record
 * @Author: jeecg-boot
 * @Date:   2022-03-23
 * @Version: V1.0
 */
public interface ICarConsumptionRecordService extends IService<CarConsumptionRecord> {

    IPage<CarConsumptionRecordVo> pageList(Page<CarConsumptionRecord> page, QueryWrapper<CarConsumptionRecordVo> queryWrapper, CarConsumptionRecordVo carConsumptionRecord);
}

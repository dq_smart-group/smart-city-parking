package org.jeecg.modules.history.service.impl;

import org.jeecg.modules.history.entity.CarAccessRecordHistory;
import org.jeecg.modules.history.mapper.CarAccessRecordHistoryMapper;
import org.jeecg.modules.history.service.ICarAccessRecordHistoryService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Description: car_access_record_history_1
 * @Author: jeecg-boot
 * @Date:   2022-03-01
 * @Version: V1.0
 */
@Service
public class CarAccessRecordHistoryServiceImpl extends ServiceImpl<CarAccessRecordHistoryMapper, CarAccessRecordHistory> implements ICarAccessRecordHistoryService {

    /**
     * 获取表格数据数量
     * @param tableName
     * @return
     */
    @Override
    public int getListCount(String tableName) {
        return baseMapper.getListCount(tableName);
    }

    /**
     * 创建表
     * @param tableName
     */
    @Override
    public void createHistoryTable(String tableName) {
        baseMapper.createHistoryTable(tableName);
    }

    /**
     * 保存数据到历史
     * @param list
     * @return
     */
//    @Override
//    @Transactional
//    public int saveDataToHistory(List<CarAccessRecord> list,String tableName) {
//        return baseMapper.saveDataToHistory(list,tableName);
//    }
}

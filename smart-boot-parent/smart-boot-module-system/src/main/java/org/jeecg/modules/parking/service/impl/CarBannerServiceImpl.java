package org.jeecg.modules.parking.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.checkerframework.checker.units.qual.C;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarBanner;
import org.jeecg.modules.parking.mapper.CarBannerMapper;
import org.jeecg.modules.parking.service.ICarBannerService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.List;

/**
 * @Description: 停车场banner广告
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
@Service
public class CarBannerServiceImpl extends ServiceImpl<CarBannerMapper, CarBanner> implements ICarBannerService {

    /**
     * 是否点击跳转
     * @param ids    id值
     * @param value    value值 0否 1是
     * @return
     */
    @Override
    public Result<?> onSwitchChangeClick(String ids, String value) {

        CarBanner carBanner = null;
        if(ids.indexOf(",") != -1){
            String[] id = ids.split(",");
            carBanner = new CarBanner();
            carBanner.setIsClick(value);
            boolean b = this.update(carBanner,
                    new LambdaQueryWrapper<CarBanner>()
                            .in(CarBanner::getId, id));
            if(!b){
                Assert.isTrue(false,"修改异常");
            }
            return Result.OK("操作成功");
        }else{
            carBanner = new CarBanner();
            carBanner.setId(ids);
            carBanner.setIsClick(value);
            boolean b = this.updateById(carBanner);
            if(!b){
                Assert.isTrue(false,"修改异常");
            }
            return Result.OK("操作成功");
        }
    }

    /**
     * 是否显示
     * @param ids
     * @param value
     * @return
     */
    @Override
    public Result<?> onSwitchChangeShow(String ids, String value) {
        CarBanner carBanner = null;
        if(ids.indexOf(",") != -1){
            String[] id = ids.split(",");
            carBanner = new CarBanner();
            carBanner.setIsShow(value);
            boolean b = this.update(carBanner,
                    new LambdaQueryWrapper<CarBanner>()
                            .in(CarBanner::getId, id));
            if(!b){
                Assert.isTrue(false,"修改异常");
            }
            return Result.OK("操作成功");
        }else{
            carBanner = new CarBanner();
            carBanner.setId(ids);
            carBanner.setIsShow(value);
            boolean b = this.updateById(carBanner);
            if(!b){
                Assert.isTrue(false,"修改异常");
            }
            return Result.OK("操作成功");
        }
    }

    @Override
    public Result<?> getBannerGroup() {
        List<String> list  = baseMapper.getBannerGroup();
        return Result.OK(list);
    }

    /**
     * 根据组来获取banner
     * @param group
     * @return
     */
    @Override
    public Result<?> getCarBanner(String group) {
        List<CarBanner> list = baseMapper.getCarBanner(group);
        return Result.OK(list);
    }
}

package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daqing.config.DongGuanStaticConfig;
import com.daqing.entity.DongGuanStaticResponse;
import com.daqing.entity.DongGuanStaticSentryBoxInfo;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.api.DgParkingApi;
import org.jeecg.modules.parking.entity.CarSentryBox;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.mapper.CarSentryBoxMapper;
import org.jeecg.modules.parking.service.ICarSentryBoxService;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: car_sentry_box
 * @Author: jeecg-boot
 * @Date:   2022-05-05
 * @Version: V1.0
 */
@Slf4j
@Service
public class CarSentryBoxServiceImpl extends ServiceImpl<CarSentryBoxMapper, CarSentryBox> implements ICarSentryBoxService {

    @Autowired
    private IParkingLotService parkingLotService;

    @Override
    public IPage<CarSentryBox> pageList(CarSentryBox carSentryBox, Page<CarSentryBox> page, LambdaQueryWrapper<CarSentryBox> queryWrapper) {
        Page<CarSentryBox> sentryBoxPage = page(page, queryWrapper);
        List<CarSentryBox> records = sentryBoxPage.getRecords();
        Map<String, String> collect = parkingLotService.list().stream()
                .filter(i-> null == carSentryBox.getParkingLotName() || carSentryBox.getParkingLotName().equals(i.getName()))
                .collect(Collectors.toMap(ParkingLot::getId, ParkingLot::getName));
        for (CarSentryBox record : records) {
            record.setParkingLotName(collect.get(record.getParkingLotId()));
        }
        page.setRecords(records);
        return page;
    }

    @Transactional
    @Override
    public void add(CarSentryBox carSentryBox) {
        //新增数据库
        carSentryBox.setIsStaticUp(false);
        save(carSentryBox);
//        DgParkingApi dgParkingApi = new DgParkingApi();
//        DongGuanStaticSentryBoxInfo dongGuanStaticSentryBoxInfo = new DongGuanStaticSentryBoxInfo();
//
//        //上传停车场通道（岗亭）基础数据
//        BeanUtils.copyProperties(carSentryBox,dongGuanStaticSentryBoxInfo);
//        dongGuanStaticSentryBoxInfo.setEntryId(DongGuanStaticConfig.dongGuanComId+carSentryBox.getId());
//        DongGuanStaticResponse boxInfo = dgParkingApi.dgSentryBoxInfo(dongGuanStaticSentryBoxInfo, carSentryBox.getCreateTime(), carSentryBox.getUpdateTime());
//        if(1==boxInfo.getState()){
//            log.info("上传停车场通道（岗亭）基础数据成功,{}",boxInfo);
//        }else {
//            removeById(carSentryBox);
//            Assert.isTrue(1==boxInfo.getState(),boxInfo.getMessage());
//            log.info("上传停车场通道（岗亭）基础数据失败,{}",boxInfo);
//        }

    }

    @Transactional
    @Override
    public Result<?> dgSentryBoxInfo(DongGuanStaticSentryBoxInfo data) {
        CarSentryBox carSentryBox = this.baseMapper.selectById(data.getEntryId());
        data.setEntryId(DongGuanStaticConfig.dongGuanComId+data.getEntryId());
        data.setParkingLotId(DongGuanStaticConfig.dongGuanComId+data.getParkingLotId());
        DgParkingApi dgParkingApi = new DgParkingApi();
        DongGuanStaticResponse boxInfo = dgParkingApi.dgSentryBoxInfo(data, carSentryBox.getCreateTime(), carSentryBox.getUpdateTime());
        if(1==boxInfo.getState()){
            log.info("上传停车场通道（岗亭）基础数据成功,{}",boxInfo);
            //修改状态，更新时间
            carSentryBox.setIsStaticUp(true);
            carSentryBox.setStaticUpTime(new Date());
            updateById(carSentryBox);
            return Result.OK(boxInfo.getMessage());
        }else {
            log.info("上传停车场通道（岗亭）基础数据失败,{}",boxInfo);
            return Result.error(boxInfo.getMessage());
        }
    }
}

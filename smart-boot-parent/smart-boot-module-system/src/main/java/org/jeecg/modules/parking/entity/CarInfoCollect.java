package org.jeecg.modules.parking.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 停车车辆信息收集表
 * @Author: jeecg-boot
 * @Date:   2021-11-24
 * @Version: V1.0
 */
@Data
@TableName("car_info_collect")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_info_collect对象", description="停车车辆信息收集表")
public class CarInfoCollect implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
    /**进场时间*/
    @Excel(name = "入库时间", width = 15, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "入库时间")
    private java.util.Date comeInTime;
    /**出库时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "出库时间")
    private java.util.Date outTime;
	/**车辆品牌*/
	@Excel(name = "车辆品牌", width = 15)
    @ApiModelProperty(value = "车辆品牌")
    private java.lang.String brand;
	/**车牌号*/
	@Excel(name = "车牌号", width = 15)
    @ApiModelProperty(value = "车牌号")
    private java.lang.String license;
	/**车辆状态*/
	@Excel(name = "车辆状态", width = 15)
    @ApiModelProperty(value = "车辆状态")
    @Dict(dicCode = "car_status")
    private java.lang.String carStatus;
	/**入库车牌截图*/
	@Excel(name = "入库车牌截图", width = 15)
    @ApiModelProperty(value = "入库车牌截图")
    private java.lang.String comeImagePath;
    /**出库车牌截图*/
    @Excel(name = "出库车牌截图", width = 15)
    @ApiModelProperty(value = "出库车牌截图")
    private java.lang.String outImagePath;
	/**车辆年份*/
	@Excel(name = "车辆年份", width = 15)
    @ApiModelProperty(value = "车辆年份")
    private java.lang.String carYear;
	/**车辆类型*/
	@Excel(name = "车辆类型", width = 15)
    @ApiModelProperty(value = "车辆类型")
    private java.lang.String carType;
	/**车牌颜色*/
	@Excel(name = "车牌颜色", width = 15)
    @ApiModelProperty(value = "车牌颜色")
    @Dict(dicCode = "car_card_color")
    private java.lang.String carCardColor;
	/**车牌类型*/
	@Excel(name = "车牌类型", width = 15)
    @ApiModelProperty(value = "车牌类型")
    @Dict(dicCode = "car_card_type")
    private java.lang.String carCardType;
	/**车身颜色*/
	@Excel(name = "车身颜色", width = 15)
    @ApiModelProperty(value = "车身颜色")
    @Dict(dicCode = "body_color")
    private java.lang.String carColor;
	/**入场设备名称*/
	@Excel(name = "入场设备名称", width = 15)
    @ApiModelProperty(value = "入场设备名称")
    private java.lang.String comeDevice;
	/**出场设备名称*/
    @Excel(name = "出场设备名称", width = 15)
    @ApiModelProperty(value = "出场设备名称")
    private java.lang.String outDevice;
	/**入场设备 ip 地址*/
	@Excel(name = "入场设备 ip 地址", width = 15)
    @ApiModelProperty(value = "入场设备 ip 地址")
    private java.lang.String comeIpaddr;
    /**出场设备 ip 地址*/
    @Excel(name = "出场设备 ip 地址", width = 15)
    @ApiModelProperty(value = "出场设备 ip 地址")
    private java.lang.String outIpaddr;
	/**入场设备序列号，设备唯一*/
	@Excel(name = "入场设备序列号，设备唯一", width = 15)
    @ApiModelProperty(value = "入场设备序列号，设备唯一")
    private java.lang.String comeSerialno;
    /**出场设备序列号，设备唯一*/
    @Excel(name = "出场设备序列号，设备唯一", width = 15)
    @ApiModelProperty(value = "出场设备序列号，设备唯一")
    private java.lang.String outSerialno;
	/**触发类型*/
	@Excel(name = "触发类型", width = 15)
    @ApiModelProperty(value = "触发类型")
    @Dict(dicCode = "sx_triggerType")
    private java.lang.String triggerType;
    /**识别结果车牌 ID*/
    @Excel(name = "进场识别结果车牌 ID", width = 15)
    @ApiModelProperty(value = "进场识别结果车牌 ID")
    private java.lang.String comePlateid;
    /**识别结果车牌 ID*/
    @Excel(name = "出场识别结果车牌 ID", width = 15)
    @ApiModelProperty(value = "出场识别结果车牌 ID")
    private java.lang.String outPlateid;
	/**车牌真伪*/
	@Excel(name = "车牌真伪", width = 15)
    @ApiModelProperty(value = "车牌真伪")
    @Dict(dicCode = "sx_is_fake")
    private java.lang.String isFakePlate;
	/**车辆特征码*/
	@Excel(name = "车辆特征码", width = 15)
    @ApiModelProperty(value = "车辆特征码")
    private java.lang.String featureCode;
	/**识别所用时间*/
	@Excel(name = "识别所用时间", width = 15)
    @ApiModelProperty(value = "识别所用时间")
    private java.lang.String timeUsed;
	/**识别结果可信度*/
	@Excel(name = "识别结果可信度", width = 15)
    @ApiModelProperty(value = "识别结果可信度")
    private java.lang.String confidence;
	/**入库推送报文*/
	@Excel(name = "入库推送报文", width = 15)
    @ApiModelProperty(value = "入库推送报文")
    private java.lang.String comeInfoText;
    /**出库推送报文*/
    @Excel(name = "出库推送报文", width = 15)
    @ApiModelProperty(value = "出库推送报文")
    private java.lang.String outInfoText;
    /**备注*/
    @Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;

    /**车牌类型id；特殊车牌类型记录*/
    @Excel(name = "车牌类型id；特殊车牌类型记录", width = 15)
    @ApiModelProperty(value = "车牌类型id；特殊车牌类型记录")
    private java.lang.String cardTypeId;
    /**公司名称**/
    @Excel(name = "公司名称", width = 15)
    @ApiModelProperty(value = "公司名称")
    private java.lang.String companyName;
    /**公司id**/
    @Excel(name = "公司id", width = 15)
    @ApiModelProperty(value = "公司id")
    private java.lang.String companyId;
    /**车库名称**/
    @Excel(name = "车库名称", width = 15)
    @ApiModelProperty(value = "车库名称")
    private java.lang.String parkingName;
    /**车库id**/
    @Excel(name = "车库id", width = 15)
    @ApiModelProperty(value = "车库id")
    private java.lang.String parkingLotId;

    /**停车费**/
    @Excel(name = "停车费", width = 15)
    @ApiModelProperty(value = "停车费")
    private BigDecimal money;
    /**
     * 完成时间
     */
    private Date completionTime;


    /**
     * openid
     */
    @TableField(exist = false)
    private String openid;

    /**
     * 无牌车业务json数据
     */
    private String unlicensedBusinessJson;

    /**
     * 收费类型
     */
    private String chargeType;//0:线上支付，1：现金支付
    /**
     * 预收金额
     */
    private BigDecimal amountReceivedAdvance;
    /**
     * 本次金额
     */
    private BigDecimal currentAmount;
    /**
     * 减免金额
     */
    private BigDecimal deductionAmount;
    /**
     * 停车总时长
     */
    private String totalParkingTime;

    /**
     * 支付车类型：0：月租车，1：临时车，2：固定车
     */
    private String carPayType;


    /**
     * 设备组id
     */
    private String deviceGroupId;

    /**
     * 基本数据是否上传了静态交通
     */
    private String flagStaticTraffic;
    /**上传静态交通时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "上传静态交通时间")
    private java.util.Date uploadStaticTrafficTime;
    /**
     * 图片是否上传了静态交通
     */
    private String imgFlagStaticTraffic;
    /**上传静态交通时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "上传静态交通时间")
    private java.util.Date imgUploadStaticTrafficTime;

    /**
     * 出场基本数据是否上传了静态交通
     */
    private String outFlagStaticTraffic;
    /**出场数据上传静态交通时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "出场数据上传静态交通时间")
    private java.util.Date outUploadStaticTrafficTime;

    /**
     * 出场图片是否上传了静态交通
     */
    private String outImgFlagStaticTraffic;
    /**出场图片上传静态交通时间*/
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "出场图片上传静态交通时间")
    private java.util.Date outImgUploadStaticTrafficTime;


}

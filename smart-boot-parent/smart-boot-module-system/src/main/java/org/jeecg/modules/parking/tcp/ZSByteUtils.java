package org.jeecg.modules.parking.tcp;


import cn.hutool.core.date.DateUtil;
import cn.hutool.json.JSONObject;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 描述 :
 * 包名 : PACKAGE_NAME
 * 作者 : GaoXiang
 * 时间 : 19-12-13
 * 版本 : V1.0
 */
public class ZSByteUtils {


    /**
     * 发送数据
     */
    static boolean sendCmd(OutputStream os, String cmd){
        try {
            os.write(getCmd(cmd));
            os.flush();
        } catch (IOException e) {
            //System.out.println("输入或者发送有误，原因：" + e.getMessage());
            //e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 发送心跳
     */
    static boolean sendHeart(OutputStream os){
        try {
            byte[] header = {'V','Z',1,0,0,0,0,0};
            os.write(header);
            os.flush();
        } catch (IOException e) {
            //System.out.println("输入或者发送有误，原因：" + e.getMessage());
            //e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * @Description 返回信息的格式化处理
     * @author yanzengbao
     * @date 2019/12/20 下午2:35
     */
    public static Map onRecv(byte[] data, int len) throws Exception {
        Map map = new HashMap();
        String str = new String(data,"gb2312");
        if (len > 20 * 1024) {
            //带图片数据
            map = onIVSResultRecv(str.getBytes(), data,len);
        } else {
            //普通的指令响应
            System.out.println("没有处理图片");
            map.put("jsonStr",str);
        }
        return map;
    }

    /**
     * @Description 用来处理图片
     * @author yanzengbao
     * @date 2019/12/20 下午2:11
     */
    public static Map onIVSResultRecv(byte[] newData, byte[] data, int len) throws Exception {
        //返回信息的map集合
        Map map = new HashMap();
        //格式化以后json字符串
        String jsonStr = "";
        //存储图片的url
        String imgUrl = "";

        //接收到识别结果的处理
        if (data[0] == 'I' && data[1] == 'R') {
//            parseBinIVSResult(data, len);
            System.out.println("暂时用不到的二进制的结构体处理");
        } else {
            //未中文乱码处理的json字符串截取
            int pos = 0;
            while (true) {
                if (data[pos] == 0) { break;}
                pos++;
            }
            //中文乱码处理以后的json字符串截取
            int pos1 = 0;
            while (true) {
                if (newData[pos1] == 0) { break;}
                pos1++;
            }
            //中文乱码处理好以后的json字符串
            jsonStr = new String(newData, 0, pos1);
            // 此处改为通过json来解析车牌识别结果,来获取车牌图片的大小
            int nImgSize = len - pos - 1;
            // 保存图片
            imgUrl = saveImage(data, pos + 1, nImgSize);
        }
        map.put("jsonStr",jsonStr);
        map.put("imgUrl",imgUrl);
        return map;
    }

    /**
     * @Description 保存图片的方法
     * @author yanzengbao
     * @date 2019/12/23 上午11:09
     */
    public static String saveImage(byte[] buff, int pos, int len) {
        //String format = DateUtil.format(new Date(), "yyyyMMdd");
        String pathCarImg = "";
        //文件目录的全路径
        String imgPath = "E:/home/img/";
        try {
            File file = new File(imgPath);
            if (!file.exists() && !file.isDirectory()) {
                boolean isMk = file.mkdirs();
                System.out.println("目录创建状态：" + isMk);
            }
            //图片的全路径
            String hh = DateUtil.format(new Date(), "HH");
            String yyyyMMdd = DateUtil.format(new Date(), "yyyyMMdd");
            pathCarImg = imgPath + yyyyMMdd+"_"+DateUtil.format(new Date(), "HHmmss")+"_"+getThree()+".jpg";
            FileOutputStream outputStream = new FileOutputStream(pathCarImg);
            DataOutputStream out = new DataOutputStream(outputStream);
            //写入
            out.write(buff, pos, len);
            out.close();
        } catch (IOException io) {
            System.out.println(io.getMessage());
            System.out.println("IO异常，保存图片失败！图片路径：" + imgPath);
        }
        return pathCarImg;
    }

    /**
     * 获取一个随机的三位数
     * @return
     */
    public static String getThree(){
        int i=(int)(Math.random()*900)+100;
        return i+"";
    }


    /**
     * number-设备唯一编号
     * 获取设备信息
     */
    static boolean sendGetsn(OutputStream os, String number){
//        String cmd = "{\"cmd\":\"getsn\",\"id\":\"123456\"}";
//        System.out.println("设备信息" + number);
        Map map = new HashMap();
        map.put("cmd","getsn");
        map.put("id",number);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return sendCmd(os,cmd);
    }


    /**
     * number-设备唯一编号
     * 配置推送的方式：主动推送，JSON格式，带图片
     */
    public static boolean sendConfig(OutputStream os, String number){
        int enable = 1;
        int fmt = 1;
        int image = 1;
        String cmd = String.format("{"+
                        "\"cmd\" : \"ivsresult\","+
                        "\"id\":"+ "\"" + number + "\"," +
                        "\"enable\" : %s,"+
                        "\"format\" : \"%s\","+
                        "\"image\" : %s"+
                        "}",
                enable!=0?"true":"false",
                fmt !=0 ?"json":"bin",
                image!=0?"true":"false");
        return sendCmd(os,cmd);
    }

    /**
     * number-设备唯一编号
     * @Description 重启设备
     * @author yanzengbao
     * @date 2019/12/16 下午5:24
     */
    public static boolean rebootDev(OutputStream os, String number){

        Map map = new HashMap();
        map.put("cmd","reboot_dev");
        map.put("id",number);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return sendCmd(os,cmd);
    }

    /**
     * @Description  获取最近一次识别结果
     * @author yanzengbao
     * @date 2019/12/17 上午11:43
     */
    public static boolean getNewIvsresult(OutputStream os, boolean image){
        Map map = new HashMap();
        map.put("cmd","getivsresult");
        map.put("format","json");
        map.put("image",image);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return sendCmd(os,cmd);
    }

    /**
     * @Description  手动触发车牌识别
     * @author yanzengbao
     * @date 2019/12/17 下午1:31
     */
    public static boolean manualTrigger(OutputStream os){
        Map map = new HashMap();
        map.put("cmd","trigger");
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return sendCmd(os,cmd);
    }

    /**
     * @Description 抓取当前图片
     * @author yanzengbao
     * @date 2019/12/17 下午2:17
     */
    public static boolean graspImage(OutputStream os){
        Map map = new HashMap();
        map.put("cmd","get_snapshot");
        map.put("id","7777777");
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return sendCmd(os,cmd);
    }

    /**
     * @Description 获取设备实时视频uri
     * @author yanzengbao
     * @date 2019/12/17 下午2:46
     */
    public static boolean getRtspUri(OutputStream os){
        Map map = new HashMap();
        map.put("cmd","get_rtsp_uri");
        map.put("id","7777777");
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return sendCmd(os,cmd);
    }

    /**
     * 设置网络参数：
     * @param os
     */
    public static boolean setNetworkparam(OutputStream os) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        map1.put("ip","192.168.1.102");
        map1.put("netmask","255.255.255.0");
        map1.put("gateway","192.168.1.1");
        map1.put("dns","114.114.114.114");
        map.put("cmd","set_networkparam");
        map.put("id","123456");
        map.put("body",map1);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return sendCmd(os,cmd);
    }
    /**
     *得到在线设备信息，不含自己
     */
    public static boolean getOvzid(OutputStream os) {
        Map map = new HashMap();
        Map map1 = new HashMap();
        map1.put("type","get_ovzid");
        map.put("cmd","dg_json_request");
        map.put("id","123456");
        map.put("body",map1);
        JSONObject jsonObject = new JSONObject(map);
        String cmd = String.valueOf(jsonObject);
        return sendCmd(os,cmd);
    }


    public static boolean getMaxRecord(OutputStream os){
        String cmd = "{\"cmd\":\"get_max_rec_id\",\"id\":\"123\"}";
        return sendCmd(os,cmd);
    }


    public static boolean getRecord(OutputStream os, Integer id){
        String cmd = "{\"cmd\":\"get_record\",\"id\":"+id+",\"format\":\"json\",\"image\":false}";
        return sendCmd(os,cmd);
    }



    /**
     * 合并两个byte数组
     */
    static byte[] addBytes(byte[] data1, byte[] data2) {
        byte[] data3 = new byte[data1.length + data2.length];
        System.arraycopy(data1, 0, data3, 0, data1.length);
        System.arraycopy(data2, 0, data3, data1.length, data2.length);
        return data3;
    }

    /**
     * 获取协议头信息
     */
    static byte[] getHeader(String cmd){
        int len =  cmd.getBytes().length;
        byte[] header = {'V','Z',0,0,0,0,0,0};
        header[4] += (byte) ((len >>24) & 0xFF);
        header[5] += (byte) ((len >>16) & 0xFF);
        header[6] += (byte) ((len >>8) & 0xFF);
        header[7] += (byte) (len & 0xFF);
        return header;
    }

    /**
     * 生成协议完整byte数组
     */
    public static byte[] getCmd(String cmd){
        return addBytes(getHeader(cmd),cmd.getBytes());
    }

    /**
     * 接收指定长度的数据，收完为止
     */
    public static int recvBlock(InputStream in, byte[] buff, int len)
    {
        try
        {
            int totleRecvLen = 0;
            int recvLen;
            while(totleRecvLen < len)
            {
                recvLen = in.read(buff, totleRecvLen, len - totleRecvLen);
                totleRecvLen += recvLen;
            }
            return len;
        }
        catch(Exception e)
        {
            System.out.println("获取协议超时！");
            System.out.println("Error:"+e);
            return -1;
        }
    }

    /**
     * 验证头信息，获取数据Body长度
     */
    public static int recvPacketSize(InputStream in)
    {
        byte[] header = new byte[8];
        int recvLen = recvBlock(in, header, 8);
        //System.out.println("检测数据包头：");
        //System.out.println(Arrays.toString(header));
        if(recvLen <= 0)
        {
            //长时间无内容
            return -1;
        }

        if(header[0] != 'V' ||header[1] != 'Z')
        {
            //格式不对
            return -1;
        }

        if(header[2] == 1)
        {
            //心跳包
            return 0;
        }
        return convBytesToInt(header,4);
    }

    /**
     * 4bytes 转为int，要考虑机器的大小端问题
     */
    public static int convBytesToInt(byte[] buff,int offset)
    {

        int len,byteValue;
        len = 0;
        byteValue = (0x000000FF & ((int)buff[offset]));
        len += byteValue<<24;
        byteValue = (0x000000FF & ((int)buff[offset+1]));
        len += byteValue<<16;
        byteValue = (0x000000FF & ((int)buff[offset+2]));
        len += byteValue<<8;
        byteValue = (0x000000FF & ((int)buff[offset+3]));
        len += byteValue;
        return len;
    }



}

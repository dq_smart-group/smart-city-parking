package org.jeecg.modules.parking.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: led显示操作表
 * @Author: jeecg-boot
 * @Date:   2022-01-05
 * @Version: V1.0
 */
@Data
@TableName("led_display")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="led_display对象", description="led显示操作表")
public class LedDisplay implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**设备id*/
	@Excel(name = "设备id", width = 15)
    @ApiModelProperty(value = "设备id")
    private java.lang.String equipmentId;
	/**第一行字体*/
	@Excel(name = "第一行字体", width = 15)
    @ApiModelProperty(value = "第一行字体")
    private java.lang.String oneText;
	/**第一行字体颜色*/
	@Excel(name = "第一行字体颜色", width = 15)
    @ApiModelProperty(value = "第一行字体颜色")
    private java.lang.String oneColor;
	/**第二行字体*/
	@Excel(name = "第二行字体", width = 15)
    @ApiModelProperty(value = "第二行字体")
    private java.lang.String twoText;
	/**第二行字体颜色*/
	@Excel(name = "第二行字体颜色", width = 15)
    @ApiModelProperty(value = "第二行字体颜色")
    private java.lang.String twoColor;
	/**第三行字体*/
	@Excel(name = "第三行字体", width = 15)
    @ApiModelProperty(value = "第三行字体")
    private java.lang.String threeText;
	/**第三行字体颜色*/
	@Excel(name = "第三行字体颜色", width = 15)
    @ApiModelProperty(value = "第三行字体颜色")
    private java.lang.String threeColor;
	/**第四行字体*/
	@Excel(name = "第四行字体", width = 15)
    @ApiModelProperty(value = "第四行字体")
    private java.lang.String fourText;
	/**第四行字体颜色*/
	@Excel(name = "第四行字体颜色", width = 15)
    @ApiModelProperty(value = "第四行字体颜色")
    private java.lang.String fourColor;
	/**是否开启播报*/
	@Excel(name = "是否开启播报", width = 15)
    @ApiModelProperty(value = "是否开启播报")
    private java.lang.Boolean enableFlag;
	/**播报内容*/
	@Excel(name = "播报内容", width = 15)
    @ApiModelProperty(value = "播报内容")
    private java.lang.String broadcastText;
    /**音量大小*/
    @Excel(name = "音量大小", width = 15)
    @ApiModelProperty(value = "音量大小")
    private java.lang.Integer volume;
    private String companyId;
    /**
     * 晚上10:00后音量大小
     */
    private Integer nigntVolumeSize;
    /**
     * 晚上10:00后屏幕亮度
     */
    private Integer nigntBrightness;
}

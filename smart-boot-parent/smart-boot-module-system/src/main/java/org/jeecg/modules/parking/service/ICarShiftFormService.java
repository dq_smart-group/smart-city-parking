package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.modules.parking.entity.CarShiftForm;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.query.CarShiftFormQuery;
import org.jeecg.modules.parking.vo.CarShiftFormVo;

import java.util.Map;

/**
 * @Description: 结算交班表
 * @Author: jeecg-boot
 * @Date:   2022-04-15
 * @Version: V1.0
 */
public interface ICarShiftFormService extends IService<CarShiftForm> {

    /**
     * 添加交班记录
     * @param data
     * @return
     */
    Result addCarShiftForm(Map<String, String> data);

    CarShiftForm getDataByUserId(String userId);

    IPage<CarShiftFormVo> pageList(Page<CarShiftFormVo> page, CarShiftFormQuery carShiftFormQuery);
}

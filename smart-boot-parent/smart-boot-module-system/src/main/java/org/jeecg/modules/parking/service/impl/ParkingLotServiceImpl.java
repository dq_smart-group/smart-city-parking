package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.daqing.config.DongGuanStaticConfig;
import com.daqing.entity.DongGuanStaticParkingLogBaseData;
import com.daqing.entity.DongGuanStaticResponse;
import com.daqing.entity.DongGuanStaticSysSection;
import com.sun.corba.se.spi.ior.ObjectKey;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.CityRedisConstant;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.service.ICameraDeviceService;
import org.jeecg.modules.parking.api.DgParkingApi;
import org.jeecg.modules.parking.entity.CarSentryBox;
import org.jeecg.modules.parking.entity.CarStandard;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.mapper.ParkingLotMapper;
import org.jeecg.modules.parking.query.CarMonthlyRentQuery;
import org.jeecg.modules.parking.query.ParkingLotQuery;
import org.jeecg.modules.parking.service.ICarSentryBoxService;
import org.jeecg.modules.parking.service.ICarStandardService;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.parking.vo.ParkingLotVo;
import org.jeecg.modules.parking.vo.ParkingManagementVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Description: 车场表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
@Service
public class ParkingLotServiceImpl extends ServiceImpl<ParkingLotMapper, ParkingLot> implements IParkingLotService {

    @Autowired
    private ICameraDeviceService cameraDeviceService;
    @Autowired
    private ISysDepartService sysDepartService;
    @Autowired
    private RedisUtil redisUtil;
    @Autowired
    private ICarStandardService carStandardService;
    @Autowired
    private ICarSentryBoxService iCarSentryBoxService;
    @Override
    public IPage<ParkingLotVo> pageList(Page<ParkingLotVo> page, ParkingLotQuery parkingLotQuery) {
        List<ParkingLotVo> list = this.baseMapper.pageList(page,parkingLotQuery);
        return page.setRecords(list);
    }


    /**
     * 是管理员就查询全部车场，不是管理员就获取登陆人对应的所有公司下的车场
     */
    @Override
    public List<ParkingLotAndCamVo> getParkingLotByCompanyId(String companyId) {
        List<ParkingLotAndCamVo> list = new ArrayList<>();
        if(companyId!=null){
            SysDepart byId = sysDepartService.getById(companyId);
            if(byId!=null){
                ParkingLotAndCamVo parkingLotAndCamVo = new ParkingLotAndCamVo();
                parkingLotAndCamVo.setValue(byId.getId());
                parkingLotAndCamVo.setLabel(byId.getDepartName());

                List<ParkingLotAndCamVo> parkingLotByCompanyId = this.baseMapper.getParkingLotByCompanyId(companyId);
                if(parkingLotByCompanyId.size()>0){

                    parkingLotAndCamVo.setChildren(parkingLotByCompanyId);
                    list.add(parkingLotAndCamVo);
                }
            }
        }else{
            List<SysDepart> sysDeparts = sysDepartService.queryDept();
            if(sysDeparts.size()>0){
                for (SysDepart sysDepart : sysDeparts) {
                    ParkingLotAndCamVo parkingLotAndCamVo = new ParkingLotAndCamVo();
                    parkingLotAndCamVo.setValue(sysDepart.getId());
                    parkingLotAndCamVo.setLabel(sysDepart.getDepartName());
                    List<ParkingLotAndCamVo> parkingLotByCompanyId = this.baseMapper.getParkingLotByCompanyId(sysDepart.getId());
                    if(parkingLotByCompanyId.size()>0){

                        parkingLotAndCamVo.setChildren(parkingLotByCompanyId);
                        list.add(parkingLotAndCamVo);
                    }

                }
            }
        }
        return list;
    }



    @Override
    public List<ParkingLotAndCamVo> getSentryBoxLotByUserId(String companyId) {
        List<ParkingLotAndCamVo> list = new ArrayList<>();
        if(companyId!=null){
            SysDepart byId = sysDepartService.getById(companyId);
            if(byId!=null){
                ParkingLotAndCamVo parkingLotAndCamVo = new ParkingLotAndCamVo();
                parkingLotAndCamVo.setValue(byId.getId());
                parkingLotAndCamVo.setLabel(byId.getDepartName());

                List<ParkingLotAndCamVo> parkingLotByCompanyId = this.baseMapper.getParkingLotByCompanyId(companyId);
                if(parkingLotByCompanyId.size()>0){
                    //查询车场下对应的岗亭
                    List<ParkingLotAndCamVo> sentryBoxChildren = getSentryBoxChildren(parkingLotByCompanyId);
                    if (sentryBoxChildren.size()>0){
                        parkingLotAndCamVo.setChildren(sentryBoxChildren);
                        list.add(parkingLotAndCamVo);
                    }
                }
            }
        }else{
            List<SysDepart> sysDeparts = sysDepartService.queryDept();
            if(sysDeparts.size()>0){
                for (SysDepart sysDepart : sysDeparts) {
                    ParkingLotAndCamVo parkingLotAndCamVo = new ParkingLotAndCamVo();
                    parkingLotAndCamVo.setValue(sysDepart.getId());
                    parkingLotAndCamVo.setLabel(sysDepart.getDepartName());
                    List<ParkingLotAndCamVo> parkingLotByCompanyId = this.baseMapper.getParkingLotByCompanyId(sysDepart.getId());
                    if(parkingLotByCompanyId.size()>0){
                        //查询车场下对应的岗亭
                        List<ParkingLotAndCamVo> sentryBoxChildren = getSentryBoxChildren(parkingLotByCompanyId);
                        if (sentryBoxChildren.size()>0){
                            parkingLotAndCamVo.setChildren(sentryBoxChildren);
                            list.add(parkingLotAndCamVo);
                        }
                    }

                }
            }
        }
        return list;
    }

    /**
     * 根据车场标识符查询车场
     * @param identifier
     * @return
     */
    @Override
    public ParkingLot getParkingLotByIdentifier(String identifier) {
        return baseMapper.getParkingLotByIdentifier(identifier);
    }


    /**
     * 根据公司id来查询停车管理需要的数据
     * @param departId
     * @return
     */
    @Override
    public Result<?> getParkingManagementDataByDepartId(String departId) {
        SysDepart sysDepart = (SysDepart)redisUtil.get(CityRedisConstant.SYS_DEPART + departId);
        if(sysDepart == null){
            sysDepart = sysDepartService.getById(departId);
            if(sysDepart == null){
                return Result.error("该公司不存在");
            }
        }
        //通过公司id来查询车场信息
        List<ParkingManagementVo> parkingManagementVos = baseMapper.getParkingManagementByCompanyId(departId);
        if(parkingManagementVos == null || parkingManagementVos.isEmpty()){
            return Result.error("该公司下无车场");
        }
        for (ParkingManagementVo parkingManagementVo : parkingManagementVos) {
            List<CameraDevice> cameraDevices = cameraDeviceService.getParkingManagementDeviceByLotId(parkingManagementVo.getValue());
            if(cameraDevices != null && !cameraDevices.isEmpty()){
                parkingManagementVo.setCameraDeviceList(cameraDevices);
            }
        }
        return Result.OK(parkingManagementVos);
    }

    /**
     * 查询车场下对应的岗亭
     * @param parkingLotByCompanyId
     * @return
     */
    private List<ParkingLotAndCamVo> getSentryBoxChildren(List<ParkingLotAndCamVo> parkingLotByCompanyId) {
        List<ParkingLotAndCamVo> lotAndCamVos = new ArrayList<>();
        if(parkingLotByCompanyId.size()>0){
            for (ParkingLotAndCamVo parkingLotAndCamVo : parkingLotByCompanyId) {
                List<CarSentryBox> list = iCarSentryBoxService.list(Wrappers.<CarSentryBox>lambdaQuery().eq(CarSentryBox::getParkingLotId, parkingLotAndCamVo.getValue()));
                if(list.size()>0){
                    List<ParkingLotAndCamVo> collect = list.stream().map(i -> {
                        ParkingLotAndCamVo camVo = new ParkingLotAndCamVo();
                        camVo.setValue(i.getId());
                        camVo.setLabel(i.getSentryBoxName());
                        return camVo;
                    }).collect(Collectors.toList());
                    if(collect.size()>0) {
                        parkingLotAndCamVo.setChildren(collect);
                        lotAndCamVos.add(parkingLotAndCamVo);
                    }
                }
            }
        }
        return lotAndCamVos;
    }

    @Override
    public List<ParkingLot> getList() {
        return baseMapper.getList();
    }

    @Override
    public BigDecimal getMoneyByRenewalDuration(CarMonthlyRentQuery carMonthlyRentQuery) {
        //根据车场和车牌类型查出收费规则
        CarStandard carStandard = carStandardService.getCarStandards(carMonthlyRentQuery.getParkingLotId(), carMonthlyRentQuery.getPlateType(),"1");
        if(carStandard == null){
            return null;
        }
        BigDecimal money = new BigDecimal("0");
        if(carMonthlyRentQuery.getRenewalDuration()==null){
            return null;
        }
        BigDecimal bigDecimal = new BigDecimal(carMonthlyRentQuery.getRenewalDuration() + "");
        money = bigDecimal.multiply(carStandard.getMoney());
        return money;
    }

    //更新redis数据
    public void toUpdate(){
        //车场信息
        List<ParkingLot> parkingLots = baseMapper.getList();
        if(parkingLots.size() > 0){
            for (ParkingLot parkingLot : parkingLots) {
                redisUtil.set(ParkingRedisConstant.PARKING_LOT+parkingLot.getId(),parkingLot);
            }
        }
    }

    /**
     * 停车场基础数据上传
     * @param data
     * @return
     */
    @Override
    public Result<?> parkingLotInformationUpload(DongGuanStaticParkingLogBaseData data) {
        ParkingLot parkingLot = this.baseMapper.selectById(data.getParkingLotId());
        if(parkingLot == null){
            return Result.error("找不到停车场");
        }
        data.setParkingLotId(DongGuanStaticConfig.dongGuanComId+data.getParkingLotId());
        DgParkingApi dgParkingApi = new DgParkingApi();
        DongGuanStaticResponse response = dgParkingApi.dgParkingLotBaseData(data, parkingLot);
        if(null==response){
            return Result.error("出现异常，请联系管理员");
        }
        if(response.getState()==1){
            parkingLot.setFlagStaticTraffic("1");
            parkingLot.setUploadStaticTrafficTime(new Date());
            this.updateById(parkingLot);
            toUpdate();
            return Result.OK(response.getMessage());
        }else{
            return Result.error(response.getMessage());
        }
    }

    /**
     * 道路基础数据上传
     * @param data
     * @return
     */
    @Override
    public Result<?> reloadInformationUpload(DongGuanStaticSysSection data) {
        ParkingLot parkingLot = this.baseMapper.selectById(data.getComId());
        if(parkingLot == null){
            return Result.error("找不到该道路");
        }
        DgParkingApi dgParkingApi = new DgParkingApi();
        DongGuanStaticResponse response = dgParkingApi.dgSysSection(data, parkingLot);
        if(response==null){
            return Result.error("出现异常，请联系管理员");
        }
        if(response.getState()==1){
            parkingLot.setFlagStaticTraffic("1");
            parkingLot.setUploadStaticTrafficTime(new Date());
            this.updateById(parkingLot);
            toUpdate();
            return Result.OK(response.getMessage());
        }else{
            return Result.error(response.getMessage());
        }
    }

}

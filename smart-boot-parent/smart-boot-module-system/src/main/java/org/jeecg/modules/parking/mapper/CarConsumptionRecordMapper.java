package org.jeecg.modules.parking.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarConsumptionRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: car_consumption_record
 * @Author: jeecg-boot
 * @Date:   2022-03-23
 * @Version: V1.0
 */
public interface CarConsumptionRecordMapper extends BaseMapper<CarConsumptionRecord> {

}

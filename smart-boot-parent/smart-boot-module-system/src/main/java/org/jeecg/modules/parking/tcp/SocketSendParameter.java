package org.jeecg.modules.parking.tcp;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.jeecg.common.api.vo.Result;

import java.io.*;
import java.util.*;

public class SocketSendParameter {

    /**
     * 发送数据
     */
    public static boolean sendCmd(OutputStream os, String cmd){
        try {
            os.write(getCmd(cmd));
            os.flush();
        } catch (IOException e) {
            System.err.println("输入或者发送有误，原因：" + e.getMessage());
            //e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * 读取数据
     * @param os
     * @return
     */
   public  static Result read(InputStream os){

       try {
           //获取并验证头文件，获取数据块大小
           int sn_len = ZSByteUtils.recvPacketSize(os);
           if(sn_len == -1){
               System.out.println("垃圾信息");
           }else if(sn_len == 0){
               System.out.println("===============收到心跳包===================");
           }else{
               //接收实际数据
               byte[] data = new byte[sn_len];
               //数据长度
               int recvLen = ZSByteUtils.recvBlock(os, data, sn_len);

               //这里处理图片，返回图片url 和截取后的json字符串
               if (recvLen > 0) {
                   Map map = ZSByteUtils.onRecv(data, recvLen);

                   //handleReturnMsg(map);
                   return Result.OK(map);
               } else {
                   System.out.println("垃圾信息==============>> socket error!");
               }
           }
       } catch (Exception e) {
           System.out.println("返回值错误！原因：" + e.getMessage());
           //e.printStackTrace();
       }
        return Result.error("");
   }

    /**
     * @Description  todo 处理信息
     * @author yanzengbao
     * @date 2019/12/17 下午5:55
     */
    public static void handleReturnMsg(Map map){


        //图片的url
        String imgUrl = (String)map.get("imgUrl");
        String cameraNumber = (String)map.get("number");
        //返回信息的json字符串
        String str = (String)map.get("jsonStr");
        System.out.println("图片url=========>>" + imgUrl + "相机编号========>>>" +  cameraNumber );

        Map<String, Object> map1 = parseJSON2Map(JSONObject.parseObject(str));

        String cmd = (String)map1.get("cmd");
        System.out.println("map1::::::" + cmd);

        if("ivs_result".equals(cmd)){
            System.out.println("进入if判断内：返回的信息：指令:过车记录==========进入详情过车记录方法============>>" + cmd);
            //设备主动推送的过车记录信息
           // parsingDetails(str,imgUrl,cameraNumber);

        }else if("".equals(cmd)){
            //其他
            System.out.println("返回的信息：指令======================>>" + cmd);
        }else{
            System.out.println("返回的信息：指令======================>>" + cmd);
        }
    }

    /**
     * @Description 用来处理图片
     * @author yanzengbao
     * @date 2019/12/20 下午2:11
     */
    public static Map onIVSResultRecv(byte[] newData, byte[] data, int len) throws Exception {
        //返回信息的map集合
        Map map = new HashMap();
        //格式化以后json字符串
        String jsonStr = "";
        //存储图片的url
        String imgUrl = "";

        //接收到识别结果的处理
        if (data[0] == 'I' && data[1] == 'R') {
//            parseBinIVSResult(data, len);
            System.out.println("暂时用不到的二进制的结构体处理");
        } else {
            //未中文乱码处理的json字符串截取
            int pos = 0;
            while (true) {
                if (data[pos] == 0) { break;}
                pos++;
            }
            //中文乱码处理以后的json字符串截取
            int pos1 = 0;
            while (true) {
                if (newData[pos1] == 0) { break;}
                pos1++;
            }
            //中文乱码处理好以后的json字符串
            jsonStr = new String(newData, 0, pos1);
            // 此处改为通过json来解析车牌识别结果,来获取车牌图片的大小
            int nImgSize = len - pos - 1;
            // 保存图片
            imgUrl = saveImage(data, pos + 1, nImgSize);
        }
        map.put("jsonStr",jsonStr);
        map.put("imgUrl",imgUrl);
        return map;
    }

    /**
     * @Description 保存图片的方法
     * @author yanzengbao
     * @date 2019/12/23 上午11:09
     */
    public static String saveImage(byte[] buff, int pos, int len) {
        //String format = DateUtil.format(new Date(), "yyyyMMdd");
        String pathCarImg = "";
        //文件目录的全路径
        String imgPath = "E:/home/img/";
        try {
            File file = new File(imgPath);
            if (!file.exists() && !file.isDirectory()) {
                boolean isMk = file.mkdirs();
                System.out.println("目录创建状态：" + isMk);
            }
            //图片的全路径
            pathCarImg = imgPath + "pathCarImg_" + System.currentTimeMillis() + ".jpg";
            FileOutputStream outputStream = new FileOutputStream(pathCarImg);
            DataOutputStream out = new DataOutputStream(outputStream);
            //写入
            out.write(buff, pos, len);
            out.close();
        } catch (IOException io) {
            System.out.println(io.getMessage());
            System.out.println("IO异常，保存图片失败！图片路径：" + imgPath);
        }
        return pathCarImg;
    }


    /**
     * 注意：1、json字符串最外层是key-value类型，
     *      2、json字符串里的value不能是普通类型的数组，比如[val1,val2,val3...]，数组，里面的元素必须是key-value形式的。
     *
     *          //依赖包
     *      *       <dependency>
     *      *           <groupId>com.alibaba</groupId>
     *      *           <artifactId>fastjson</artifactId>
     *      *           <version>1.2.59</version>
     *      *       </dependency>
     *
     *
     * @Description json字符串转Map
     * @author yanzengbao
     * @date 2019/12/17 下午5:42
     */
    public static Map<String, Object> parseJSON2Map(JSONObject json) {
        Map<String, Object> map = new HashMap<String, Object>();
        // 最外层解析
        for (Object k : json.keySet()) {
            Object v = json.get(k);
            // 如果内层还是json数组的话，继续解析
            if (v instanceof JSONArray) {
                List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
                Iterator<Object> it = ((JSONArray) v).iterator();  //迭代器不用考虑长度为0的情况
                while (it.hasNext()) {
                    JSONObject json2 = (JSONObject) it.next();
                    list.add(parseJSON2Map(json2));
                }
                map.put(k.toString(), list);
            } else if (v instanceof JSONObject) {
                // 如果内层是json对象的话，继续解析
                map.put(k.toString(), parseJSON2Map((JSONObject) v));
            } else {
                // 如果内层是普通对象的话，直接放入map中
                map.put(k.toString(), v);
            }
        }
        return map;
    }
    /**
     * 生成协议完整byte数组
     */
    public static byte[] getCmd(String cmd){
        return addBytes(getHeader(cmd),cmd.getBytes());
    }
    /**
     * 合并两个byte数组
     */
    static byte[] addBytes(byte[] data1, byte[] data2) {
        byte[] data3 = new byte[data1.length + data2.length];
        System.arraycopy(data1, 0, data3, 0, data1.length);
        System.arraycopy(data2, 0, data3, data1.length, data2.length);
        return data3;
    }

    /**
     * 获取协议头信息
     */
    static byte[] getHeader(String cmd){
        int len =  cmd.getBytes().length;
        byte[] header = {'V','Z',0,0,0,0,0,0};
        header[4] += (byte) ((len >>24) & 0xFF);
        header[5] += (byte) ((len >>16) & 0xFF);
        header[6] += (byte) ((len >>8) & 0xFF);
        header[7] += (byte) (len & 0xFF);
        return header;
    }
}

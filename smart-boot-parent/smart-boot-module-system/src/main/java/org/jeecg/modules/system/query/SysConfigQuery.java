package org.jeecg.modules.system.query;

import lombok.Data;

import java.io.Serializable;

@Data
public class SysConfigQuery implements Serializable {
    /**
     * 标识
     */
    private String tabFlag;
    /**
     * 配置分类数据状态
     */
    private Integer tabStatus;
    /**
     * 状态
     */
    private Integer status;
}
package org.jeecg.modules.parking.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.daqing.entity.DongGuanStaticSentryBoxInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.modules.parking.entity.CarSentryBox;
import org.jeecg.modules.parking.service.ICarSentryBoxService;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: car_sentry_box
 * @Author: jeecg-boot
 * @Date:   2022-05-05
 * @Version: V1.0
 */
@Api(tags="car_sentry_box")
@RestController
@RequestMapping("/parking/carSentryBox")
@Slf4j
public class CarSentryBoxController extends JeecgController<CarSentryBox, ICarSentryBoxService> {
	@Autowired
	private ICarSentryBoxService carSentryBoxService;
	 @Autowired
	 private ISysDepartService sysDepartService;
	/**
	 * 分页列表查询
	 *
	 * @param carSentryBox
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "car_sentry_box-分页列表查询")
	@ApiOperation(value="car_sentry_box-分页列表查询", notes="car_sentry_box-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarSentryBox carSentryBox,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carSentryBox.setCompanyId(sysDepart.getId());
			}else{
				carSentryBox.setCompanyId("0");
			}
		}
		LambdaQueryWrapper<CarSentryBox> wrapper = Wrappers.lambdaQuery();
		if(StringUtils.isNotBlank(carSentryBox.getSentryBoxName())) {
			wrapper.and(c->c.like(CarSentryBox::getSentryBoxName, carSentryBox.getSentryBoxName()));
		}
		Page<CarSentryBox> page = new Page<CarSentryBox>(pageNo, pageSize);
		IPage<CarSentryBox> pageList = carSentryBoxService.pageList(carSentryBox,page, wrapper);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carSentryBox
	 * @return
	 */
	@AutoLog(value = "car_sentry_box-添加")
	@ApiOperation(value="car_sentry_box-添加", notes="car_sentry_box-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarSentryBox carSentryBox) {
		carSentryBoxService.add(carSentryBox);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param carSentryBox
	 * @return
	 */
	@AutoLog(value = "car_sentry_box-编辑")
	@ApiOperation(value="car_sentry_box-编辑", notes="car_sentry_box-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarSentryBox carSentryBox) {
		carSentryBoxService.updateById(carSentryBox);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_sentry_box-通过id删除")
	@ApiOperation(value="car_sentry_box-通过id删除", notes="car_sentry_box-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carSentryBoxService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "car_sentry_box-批量删除")
	@ApiOperation(value="car_sentry_box-批量删除", notes="car_sentry_box-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carSentryBoxService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_sentry_box-通过id查询")
	@ApiOperation(value="car_sentry_box-通过id查询", notes="car_sentry_box-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarSentryBox carSentryBox = carSentryBoxService.getById(id);
		if(carSentryBox==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carSentryBox);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carSentryBox
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarSentryBox carSentryBox) {
        return super.exportXls(request, carSentryBox, CarSentryBox.class, "car_sentry_box");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarSentryBox.class);
    }


	 /**
	  * 岗亭基础数据上传
	  * @param data
	  * @return
	  */
	 @PostMapping("/parkingLotInformationUpload")
	 public Result<?> dgSentryBoxInfo(@RequestBody DongGuanStaticSentryBoxInfo data){
		 return carSentryBoxService.dgSentryBoxInfo(data);
	 }
}

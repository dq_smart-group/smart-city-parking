package org.jeecg.modules.utils;

import com.xkcoding.http.util.StringUtil;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.constant.CacheConstant;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.system.util.SysConfigUtil;

@Slf4j
public class RedisGetUtil {

    /**
     * 获取当前环境
     * @param redisUtil
     * @return
     */
    public static String getIsEnvironment(RedisUtil redisUtil){
        Object o = redisUtil.get(CacheConstant.IS_ENVIRONMENT);
        if(o != null){
            return o.toString();
        }
        String is_environment = SysConfigUtil.getVal("is_environment");
        if(StringUtil.isNotEmpty(is_environment)){
            redisUtil.set(CacheConstant.IS_ENVIRONMENT,is_environment);
            return is_environment;
        }
        log.error("getIsEnvironment获取当前环境为空");
        return "";
    }


    /**
     * 获取packing请求路径
     * @param redisUtil
     */
    public static String getParkingPath(RedisUtil redisUtil){
        Object path = redisUtil.get(CacheConstant.PARKING_PATH);
        if(path != null){
            return path.toString();
        }
        String is_environment = getIsEnvironment(redisUtil);
        //开发环境
        if("1".equals(is_environment)){
            String dev_parking_path = SysConfigUtil.getVal("dev_parking_path");
            if(StringUtil.isNotEmpty(dev_parking_path)){
                redisUtil.set(CacheConstant.PARKING_PATH,dev_parking_path);
                return dev_parking_path;
            }
            log.error("getParkingPath获取packing请求路径为空");
            return "";
        }
        //生产环境
        String prod_parking_path = SysConfigUtil.getVal("prod_parking_path");
        if(StringUtil.isNotEmpty(prod_parking_path)){
            redisUtil.set(CacheConstant.PARKING_PATH,prod_parking_path);
            return prod_parking_path;
        }
        log.error("getParkingPath获取packing请求路径为空");
        return "";
    }



    /**
     * 获取Smoke请求路径
     * @param redisUtil
     */
    public static String getSmokePath(RedisUtil redisUtil){
        Object path = redisUtil.get(CacheConstant.SMOKE_PATH);
        if(path != null){
            return path.toString();
        }
        //获取当前项目环境
        String is_environment = getIsEnvironment(redisUtil);
        //开发环境
        if("1".equals(is_environment)){
            String dev_smoke_path = SysConfigUtil.getVal("dev_smoke_path");
            if(StringUtil.isNotEmpty(dev_smoke_path)){
                redisUtil.set(CacheConstant.SMOKE_PATH,dev_smoke_path);
                return dev_smoke_path;
            }
            log.error("getSmokePath获取Smoke请求路径为空");
            return "";
        }
        //生产环境
        String prod_smoke_path = SysConfigUtil.getVal("prod_smoke_path");
        if(StringUtil.isNotEmpty(prod_smoke_path)){
            redisUtil.set(CacheConstant.SMOKE_PATH,prod_smoke_path);
            return prod_smoke_path;
        }
        log.error("getSmokePath获取Smoke请求路径为空");
        return "";

    }


    /**
     * 获取Pay请求路径
     * @param redisUtil
     */
    public static String getPayPath(RedisUtil redisUtil){
        Object path = redisUtil.get(CacheConstant.PAY_PATH);
        if(path != null){
            return path.toString();
        }
        //获取当前项目环境
        String is_environment = getIsEnvironment(redisUtil);
        //开发环境
        if("1".equals(is_environment)){
            String dev_pay_path = SysConfigUtil.getVal("dev_pay_path");
            if(StringUtil.isNotEmpty(dev_pay_path)){
                redisUtil.set(CacheConstant.PAY_PATH,dev_pay_path);
                return dev_pay_path;
            }
            log.error("getPayPath获取Pay请求路径为空");
            return "";
        }
        //生产环境
        String prod_pay_path = SysConfigUtil.getVal("prod_pay_path");
        if(StringUtil.isNotEmpty(prod_pay_path)){
            redisUtil.set(CacheConstant.PAY_PATH,prod_pay_path);
            return prod_pay_path;
        }
        log.error("getPayPath获取Pay请求路径为空");
        return "";
    }


    /**
     * 获取City请求路径
     * @param redisUtil
     */
    public static String getCityPath(RedisUtil redisUtil){
        Object path = redisUtil.get(CacheConstant.CITY_PATH);
        if(path != null){
            return path.toString();
        }
        //获取当前项目环境
        String is_environment = getIsEnvironment(redisUtil);
        //开发环境
        if("1".equals(is_environment)){
            String dev_city_path = SysConfigUtil.getVal("dev_city_path");
            if(StringUtil.isNotEmpty(dev_city_path)){
                redisUtil.set(CacheConstant.CITY_PATH,dev_city_path);
                return dev_city_path;
            }
            log.error("getCityPath获取City请求路径为空");
            return "";
        }
        //生产环境
        String prod_city_path = SysConfigUtil.getVal("prod_city_path");
        if(StringUtil.isNotEmpty(prod_city_path)){
            redisUtil.set(CacheConstant.CITY_PATH,prod_city_path);
            return prod_city_path;
        }
        log.error("getCityPath获取City请求路径为空");
        return "";
    }

    /**
     * 获取mqtt请求路径
     * @param redisUtil
     */
    public static String getMqttPath(RedisUtil redisUtil){
        Object path = redisUtil.get(CacheConstant.MQTT_PATH);
        if(path != null){
            return path.toString();
        }
        //获取当前项目环境
        String is_environment = getIsEnvironment(redisUtil);
        //开发环境
        if("1".equals(is_environment)){
            String dev_mqtt_path = SysConfigUtil.getVal("dev_mqtt");
            if(StringUtil.isNotEmpty(dev_mqtt_path)){
                redisUtil.set(CacheConstant.MQTT_PATH,dev_mqtt_path);
                return dev_mqtt_path;
            }
            log.error("getMqttPath获取mqtt请求路径为空");
            return "";
        }
        //生产环境
        String prod_mqtt_path = SysConfigUtil.getVal("prod_mqtt");
        if(StringUtil.isNotEmpty(prod_mqtt_path)){
            redisUtil.set(CacheConstant.MQTT_PATH,prod_mqtt_path);
            return prod_mqtt_path;
        }
        log.error("getMqttPath获取mqtt请求路径为空");
        return "";
    }



    /**
     * 获取服务商应用ID
     * @param redisUtil
     * @return
     */
    public static String getWxAppid(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_APPID);
        if(object != null){
            return object.toString();
        }
        String wx_appid = SysConfigUtil.getVal("WX_APPID");
        if(StringUtil.isNotEmpty(wx_appid)){
            redisUtil.set(CacheConstant.WX_APPID,wx_appid);
            return wx_appid;
        }
        log.error("getWxAppid获取服务商应用ID为空");
        return "";
    }


    /**
     * 获取服务商户号
     * @param redisUtil
     * @return
     */
    public static String getWxMchid(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_MCHID);
        if(object != null){
            return object.toString();
        }
        String wx_mchid = SysConfigUtil.getVal("WX_MCHID");
        if(StringUtil.isNotEmpty(wx_mchid)){
            redisUtil.set(CacheConstant.WX_MCHID,wx_mchid);
            return wx_mchid;
        }
        log.error("getWxMchid获取服务商户号为空");
        return "";
    }


    /**
     * 获取wechatpay.pem证书内容
     * @param redisUtil
     * @return
     */
    public static String getWxWechatpaykey(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_WECHATPAYKEY);
        if(object != null){
            return object.toString();
        }
        String wx_wechatpaykey = SysConfigUtil.getVal("WX_WECHATPAYKEY");
        if(StringUtil.isNotEmpty(wx_wechatpaykey)){
            redisUtil.set(CacheConstant.WX_WECHATPAYKEY,wx_wechatpaykey);
            return wx_wechatpaykey;
        }
        log.error("getWxWechatpaykey获取wechatpay.pem证书内容为空");
        return "";
    }



    /**
     * 获取V3接口
     * @param redisUtil
     * @return
     */
    public static String getWxPrivatekey(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_PRIVATEKEY);
        if(object != null){
            return object.toString();
        }
        String wx_privatekey = SysConfigUtil.getVal("WX_PRIVATEKEY");
        if(StringUtil.isNotEmpty(wx_privatekey)){
            redisUtil.set(CacheConstant.WX_PRIVATEKEY,wx_privatekey);
            return wx_privatekey;
        }
        log.error("getWxPrivatekey获取V3接口为空");
        return "";
    }


    /**
     * 获取native支付请求链接
     * @param redisUtil
     * @return
     */
    public static String getWxNativev3API(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_NATIVEV3API);
        if(object != null){
            return object.toString();
        }
        String wx_nativev3API = SysConfigUtil.getVal("WX_NATIVEV3API");
        if(StringUtil.isNotEmpty(wx_nativev3API)){
            redisUtil.set(CacheConstant.WX_NATIVEV3API,wx_nativev3API);
            return wx_nativev3API;
        }
        log.error("getWxNativev3API获取native支付请求链接为空");
        return "";
    }

    /**
     * 获取异步回调通知路径
     * @param redisUtil
     * @return
     */
    public static String getWxNotifyuurl(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_NOTIFYUURL);
        if(object != null){
            return object.toString();
        }
        String wx_notifyuurl = SysConfigUtil.getVal("WX_NOTIFYUURL");
        if(StringUtil.isNotEmpty(wx_notifyuurl)){
            redisUtil.set(CacheConstant.WX_NOTIFYUURL,wx_notifyuurl);
            return wx_notifyuurl;
        }
        log.error("getWxNotifyuurl获取异步回调通知路径为空");
        return "";
    }

    /**
     * 获取api V3密钥
     * @param redisUtil
     * @return
     */
    public static String getWxApiv3KEY(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_APIV3KEY);
        if(object != null){
            return object.toString();
        }
        String wx_apiv3KEY = SysConfigUtil.getVal("WX_APIV3KEY");
        if(StringUtil.isNotEmpty(wx_apiv3KEY)){
            redisUtil.set(CacheConstant.WX_APIV3KEY,wx_apiv3KEY);
            return wx_apiv3KEY;
        }
        log.error("getWxApiv3KEY获取apiV3密钥为空");
        return "";
    }


    /**
     * 获取支付通知域名
     * @param redisUtil
     * @return
     */
    public static String getPayHost(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_PAY_HOST);
        if(object != null){
            return object.toString();
        }
        String wx_pay_host = SysConfigUtil.getVal("WX_PAY_HOST");
        if(StringUtil.isNotEmpty(wx_pay_host)){
            redisUtil.set(CacheConstant.WX_PAY_HOST,wx_pay_host);
            return wx_pay_host;
        }
        log.error("getPayHost获取支付通知域名为空");
        return "";
    }


    /**
     * 获取退款通知域名
     * @param redisUtil
     * @return
     */
    public static String getRefundHost(RedisUtil redisUtil){
        Object object = redisUtil.get(CacheConstant.WX_REFUND_HOST);
        if(object != null){
            return object.toString();
        }
        String wx_refund_host = SysConfigUtil.getVal("WX_REFUND_HOST");
        if(StringUtil.isNotEmpty(wx_refund_host)){
            redisUtil.set(CacheConstant.WX_REFUND_HOST,wx_refund_host);
            return wx_refund_host;
        }
        log.error("getRefundHost获取退款通知域名为空");
        return "";
    }




}

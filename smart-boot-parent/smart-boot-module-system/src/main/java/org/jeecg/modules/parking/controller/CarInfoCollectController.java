package org.jeecg.modules.parking.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import org.jeecg.modules.parking.mqtt.MqttPushClient;
import org.jeecg.modules.parking.query.CarInfoQuery;
import org.jeecg.modules.parking.service.ICarInfoCollectService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.parking.vo.CarInfoCollectVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 停车车辆信息收集表
 * @Author: jeecg-boot
 * @Date:   2021-11-24
 * @Version: V1.0
 */
@Api(tags="停车车辆信息收集表")
@RestController
@RequestMapping("/parking/carInfoCollect")
@Slf4j
public class CarInfoCollectController extends JeecgController<CarInfoCollect, ICarInfoCollectService> {
	@Autowired
	private ICarInfoCollectService carInfoCollectService;
	 @Autowired
	 private ISysDepartService sysDepartService;
	
	/**
	 * 分页列表查询
	 *
	 * @param carInfoCollectvo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "停车车辆信息收集表-分页列表查询")
	@ApiOperation(value="停车车辆信息收集表-分页列表查询", notes="停车车辆信息收集表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarInfoCollectVo carInfoCollectvo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!StringUtils.isEmpty(carInfoCollectvo.getCarStatuslists())){
			String[] split = carInfoCollectvo.getCarStatuslists().split(",");
			List<String> list = new ArrayList<>();
			for (String s : split) {
				list.add(s);
			}
			carInfoCollectvo.setCarStatuslist(list);
		}
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carInfoCollectvo.setCompanyId(sysDepart.getId());
			}else{
				carInfoCollectvo.setCompanyId("0");
			}
		}
		Page<CarInfoCollectVo> page = new Page<CarInfoCollectVo>(pageNo, pageSize);
		IPage<CarInfoCollectVo> pageList=carInfoCollectService.selectData(page,carInfoCollectvo);
		return Result.OK(pageList);
	}

	//统计金额
	 @GetMapping(value = "/moneyStatistics")
	 public Result<?> moneyStatistics(CarInfoCollectVo carInfoCollectvo){
		 if(!LoginUserUtil.isAdmin()){
			 SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			 if(sysDepart!=null){
				 carInfoCollectvo.setCompanyId(sysDepart.getId());
			 }else{
				 carInfoCollectvo.setCompanyId("0");
			 }
		 }
		 return carInfoCollectService.moneyStatistics(carInfoCollectvo);
	 }
	
	/**
	 *   添加
	 *
	 * @param carInfoCollect
	 * @return
	 */
	@AutoLog(value = "停车车辆信息收集表-添加")
	@ApiOperation(value="停车车辆信息收集表-添加", notes="停车车辆信息收集表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarInfoCollect carInfoCollect) {
		carInfoCollectService.save(carInfoCollect);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param carInfoCollect
	 * @return
	 */
	@AutoLog(value = "停车车辆信息收集表-编辑")
	@ApiOperation(value="停车车辆信息收集表-编辑", notes="停车车辆信息收集表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarInfoCollect carInfoCollect) {
		carInfoCollectService.updateById(carInfoCollect);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "停车车辆信息收集表-通过id删除")
	@ApiOperation(value="停车车辆信息收集表-通过id删除", notes="停车车辆信息收集表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carInfoCollectService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "停车车辆信息收集表-批量删除")
	@ApiOperation(value="停车车辆信息收集表-批量删除", notes="停车车辆信息收集表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carInfoCollectService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "停车车辆信息收集表-通过id查询")
	@ApiOperation(value="停车车辆信息收集表-通过id查询", notes="停车车辆信息收集表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarInfoCollect carInfoCollect = carInfoCollectService.getById(id);
		if(carInfoCollect==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carInfoCollect);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carInfoCollect
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarInfoCollect carInfoCollect) {
        return super.exportXls(request, carInfoCollect, CarInfoCollect.class, "停车车辆信息收集表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarInfoCollect.class);
    }


	 /**
	  * 免费放行和结算放行
	  * @return
	  */
	 @PostMapping(value = "/settlementRelease")
    public Result<?> settlementRelease(@RequestBody CarInfoQuery carInfoQuery){
    	return carInfoCollectService.settlementRelease(carInfoQuery);
	}

	 /**
	  * 根据园区Id查询此园区最近一次的出入场记录
	  *
	  * @param screenCode
	  * @return
	  */
	 @AutoLog(value = "停车车辆信息收集表-通过id查询")
	 @ApiOperation(value="停车车辆信息收集表-通过id查询", notes="停车车辆信息收集表-通过id查询")
	 @GetMapping(value = "/screenCarInfo")
	 public Result<?> screenCarInfo(@RequestParam(name="screenCode",required=true) String screenCode) {
	 	return carInfoCollectService.screenCarInfo(screenCode);
	 }


}

package org.jeecg.modules.system.service;

import org.jeecg.modules.system.query.SysConfigQuery;
import org.jeecg.modules.system.entity.SysConfig;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Description: 系统分类配置表
 * @Author: jeecg-boot
 * @Date:   2021-12-03
 * @Version: V1.0
 */
public interface ISysConfigService extends IService<SysConfig> {

    /**
     * 通过标识查询数据
     * @param query
     * @return
     */
    List<SysConfig> queryByFlag(SysConfigQuery query);

    /**
     * 保存
     * @param objectMap
     * @return
     */
    boolean save(Map<String, Object> objectMap);

    /**
     * 获取配置
     * @param key
     * @return
     */
    SysConfig getConfig(String key);
    /**
     * 通过标识获取值
     * @param key
     * @return
     */
    BigDecimal getBigDecimalVal(String key);
    /**
     * 通过标识获取值
     * @param key
     * @return
     */
    String getVal(String key);
    /**
     * 通过标识获取值
     * @param key
     * @return
     */
    String[] getValArray(String key);

    /**
     * 通过标识获取值
     * @param key
     * @return Boolean
     */
    Boolean getValBoolean(String key);
}

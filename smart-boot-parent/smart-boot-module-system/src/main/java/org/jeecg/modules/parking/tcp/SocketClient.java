package org.jeecg.modules.parking.tcp;

import java.io.IOException;
import java.net.Socket;

public class SocketClient {

    /**
     * 连接socket
     * @return
     */
    public static Socket open(String host){
        try {
            Socket socket = new Socket(host, 8131);
            socket.setSoTimeout(5*1000);
            return socket;
        }catch (IOException e){
            e.printStackTrace();
            System.err.println("连接设备tcp超时");
            return null;
        }
    }

    public static void close(Socket socket){
        try {
            socket.close();
        }catch (IOException e){
            e.printStackTrace();
            System.err.println("关闭连接设备失败");
        }
    }
}

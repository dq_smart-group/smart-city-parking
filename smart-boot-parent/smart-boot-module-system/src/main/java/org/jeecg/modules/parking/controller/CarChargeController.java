package org.jeecg.modules.parking.controller;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.parking.entity.CarCharge;
import org.jeecg.modules.parking.entity.CarShiftForm;
import org.jeecg.modules.parking.service.ICarChargeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.parking.vo.CameraDeviceChVo;
import org.jeecg.modules.parking.vo.CarChargeVo;
import org.jeecg.modules.parking.vo.CarInfoCollectVo;
import org.jeecg.modules.parking.vo.ChargeParkingVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.entity.SysUser;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 收费员表
 * @Author: jeecg-boot
 * @Date:   2022-03-25
 * @Version: V1.0
 */
@Api(tags="收费员表")
@RestController
@RequestMapping("/parking/carCharge")
@Slf4j
public class CarChargeController extends JeecgController<CarCharge, ICarChargeService> {
	@Autowired
	private ICarChargeService carChargeService;
	 @Autowired
	 private ISysDepartService sysDepartService;
	/**
	 * 分页列表查询
	 *
	 * @param carCharge
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "收费员表-分页列表查询")
	@ApiOperation(value="收费员表-分页列表查询", notes="收费员表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarCharge carCharge,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carCharge.setCompanyId(sysDepart.getId());
			}else{
				carCharge.setCompanyId("0");
			}
		}
		Page<CarChargeVo> page = new Page<CarChargeVo>(pageNo, pageSize);
		IPage<CarChargeVo> pageList = carChargeService.pageList(page, carCharge);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carCharge
	 * @return
	 */
	@AutoLog(value = "收费员表-添加")
	@ApiOperation(value="收费员表-添加", notes="收费员表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarCharge carCharge) {
		return carChargeService.saveCarCharge(carCharge);
	}
	
	/**
	 *  编辑
	 *
	 * @param carCharge
	 * @return
	 */
	@AutoLog(value = "收费员表-编辑")
	@ApiOperation(value="收费员表-编辑", notes="收费员表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarCharge carCharge) {

		return carChargeService.updateCarChargeById(carCharge);
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "收费员表-通过id删除")
	@ApiOperation(value="收费员表-通过id删除", notes="收费员表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carChargeService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "收费员表-批量删除")
	@ApiOperation(value="收费员表-批量删除", notes="收费员表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carChargeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "收费员表-通过id查询")
	@ApiOperation(value="收费员表-通过id查询", notes="收费员表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarCharge carCharge = carChargeService.getById(id);
		if(carCharge==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carCharge);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carCharge
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarCharge carCharge) {
        return super.exportXls(request, carCharge, CarCharge.class, "收费员表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarCharge.class);
    }

	 /**
	  * 查询出登录的收费员对应管理车场的所有设备
	  *
	  * @return
	  */
	 @GetMapping(value = "/getSheBeiByUserId")
	 public Result<?> getSheBeiByUserId(String deviceName) {
		 String loginUserId = LoginUserUtil.getLoginUserId();
		 List<CameraDeviceChVo> list = carChargeService.getSheBeiByUserId(loginUserId,deviceName);
		 if(list == null){
			 return Result.error("未查询到设备");
		 }
		 return Result.OK(list);
	 }


	 @GetMapping(value = "/getCarInfoDataBySerialno")
	 public Result<?> getCarInfoDataBySerialno(String serialno){
		 List<CarInfoCollectVo> list = carChargeService.getCarInfoDataBySerialno(serialno);
		 return Result.OK(list);
	 }

	 /**
	  * 根据登录的收费员返回车场信息
	  * @return
	  */
	 @GetMapping(value = "/getDataInfoByCharge")
	 public Result<?> getDataInfoByCharge(){
		 String loginUserId = LoginUserUtil.getLoginUserId();
		 ChargeParkingVo chargeParkingVo =  carChargeService.getDataInfoByCharge(loginUserId);
		 return Result.OK(chargeParkingVo);
	 }

	 /**
	  * 结算交班
	  * @return
	  */
	 @PostMapping("/shiftHandover")
	 public Result<?> shiftHandover(@RequestBody CarShiftForm carShiftForm){
		 String loginUserId = LoginUserUtil.getLoginUserId();
		 carShiftForm.setChargeId(loginUserId);
		 return carChargeService.shiftHandover(carShiftForm);
	 }


	 /**
	  * 查询出自己公司下的所有收费员
	  * @return
	  */
	 @GetMapping("/getSysUserByChange")
	 public List<SysUser> getSysUserByChange(){
		 return carChargeService.getSysUserByChange();
	 }
 }

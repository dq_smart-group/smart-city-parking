package org.jeecg.modules.system.service.impl;

import cn.hutool.core.lang.Assert;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import org.jeecg.modules.system.query.SysConfigQuery;
import org.jeecg.modules.system.entity.SysConfig;
import org.jeecg.modules.system.mapper.SysConfigMapper;
import org.jeecg.modules.system.service.ISysConfigService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * @Description: 系统分类配置表
 * @Author: jeecg-boot
 * @Date:   2021-12-03
 * @Version: V1.0
 */
@Service
public class SysConfigServiceImpl extends ServiceImpl<SysConfigMapper, SysConfig> implements ISysConfigService {
    @Override
    public List<SysConfig> queryByFlag(SysConfigQuery query) {
        return this.baseMapper.queryByFlag(query);
    }

    @Override
    @Transactional
    public boolean save(Map<String, Object> objectMap) {
        if(objectMap.size() != 0){
            UpdateWrapper<SysConfig> wrapper = null;
            SysConfig config = null;
            for(Map.Entry<String, Object> entry : objectMap.entrySet()){
                wrapper = new UpdateWrapper<>();
                wrapper.eq("menu_name",entry.getKey());
                config = new SysConfig();
                config.setDataValue(entry.getValue().toString());
                boolean success = this.update(config,wrapper);
                Assert.isTrue(success,"修改" + entry.getKey() +"失败");
            }
        }
        return true;
    }

    @Override
    public SysConfig getConfig(String key) {
        LambdaQueryWrapper<SysConfig> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysConfig::getMenuName,key);
        List<SysConfig> configs = this.baseMapper.selectList(wrapper);
        return configs.size() > 0 ? configs.get(0) : null;
    }

    @Override
    public BigDecimal getBigDecimalVal(String key) {
        String val = this.getVal(key);
        if(val != null){
            return new BigDecimal(val);
        }
        return null;
    }


    @Override
    public String getVal(String key) {
        SysConfig config = this.getConfig(key);
        return config != null ? config.getDataValue() : null;
    }

    @Override
    public String[] getValArray(String key) {
        String val = this.getVal(key);
        String[] arr = val != null ? val.split("\n") : null;
        return arr;
    }

    @Override
    public Boolean getValBoolean(String key) {
        String val = this.getVal(key);
        Boolean b = val != null ? Boolean.valueOf(val) : null;
        return b;
    }
}

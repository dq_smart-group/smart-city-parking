package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class LedQuery {
    //序列号
    private String serialno;
    //下载文本内容
    private String text;
    //第几行
    private String hang;
    //文字显示方式
    private String type;
    //颜色
    private String color;
}

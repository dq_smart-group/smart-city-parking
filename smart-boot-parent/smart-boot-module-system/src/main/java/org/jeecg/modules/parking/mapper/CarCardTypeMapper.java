package org.jeecg.modules.parking.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.lettuce.core.dynamic.annotation.Param;
import org.jeecg.modules.parking.entity.CarCardType;
import org.jeecg.modules.parking.entity.Depart;
import org.jeecg.modules.parking.query.CarCardTypeQuery;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;

import java.util.List;

/**
 * @Description: 车牌类型
 * @Author: jeecg-boot
 * @Date:   2021-12-09
 * @Version: V1.0
 */
public interface CarCardTypeMapper extends BaseMapper<CarCardType> {

    List<Depart> getDepartlist();

    List<Depart>getDepartByUserId(String loginUserId);

    List<CarCardType> pageList(Page<CarCardType> page, @Param("q") CarCardTypeQuery q);

    //根据公司id查询对应的车牌类型信息
    //List<CarCardType> getCardTypeByUserId(@Param("companyId") String companyId);

    //获取登陆人对应的公司下的车场下的车牌类型,是管理员就获取全部
    List<ParkingLotAndCamVo> getCardTypeByparkingLotId(String parkingLotId);


    List<CarCardType> getCardTypeByParkingId(@Param("parkingId") String parkingId);
}

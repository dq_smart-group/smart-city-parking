package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.parking.entity.CarDeviceGroup;
import org.jeecg.modules.parking.mapper.CarDeviceGroupMapper;
import org.jeecg.modules.parking.query.CarDeviceGroupQuery;
import org.jeecg.modules.parking.service.ICarDeviceGroupService;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.vo.CarDeviceGroupVo;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 设备组表
 * @Author: jeecg-boot
 * @Date:   2022-04-02
 * @Version: V1.0
 */
@Service
public class CarDeviceGroupServiceImpl extends ServiceImpl<CarDeviceGroupMapper, CarDeviceGroup> implements ICarDeviceGroupService {

    @Autowired
    private IParkingLotService parkingLotService;
    @Autowired
    private ISysDepartService sysDepartService;
    @Autowired
    private RedisUtil redisUtil;
    @Override
    public IPage<CarDeviceGroupVo> pageList(Page<CarDeviceGroupVo> page, CarDeviceGroupQuery carDeviceGroup) {
        List<CarDeviceGroupVo> list = this.baseMapper.pageList(page,carDeviceGroup);
        return page.setRecords(list);
    }

    /**
     * 获取登录人下的设备组 级联
     * @return
     */
    @Override
    public List<ParkingLotAndCamVo> selectByCompanyId() {
        String companyId = null;
        if(!LoginUserUtil.isAdmin()){
            companyId = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode()).getId();
            if(StringUtils.isEmpty(companyId)){
                companyId = "0";
            }
        }
        List<ParkingLotAndCamVo> list = null;
        if(companyId == null){
            list = (List<ParkingLotAndCamVo>)redisUtil.get(ParkingRedisConstant.PARKING_DEVICE_GROUP + "admin");
        }else{
            list = (List<ParkingLotAndCamVo>)redisUtil.get(ParkingRedisConstant.PARKING_DEVICE_GROUP + companyId);
        }
        if(list!=null){
            if(list.size()>0){
                return list;
            }
        }
        return selectDeviceGroupByCompanyId(companyId);
    }

    /**
     * 加载级联设备组的信息到redis中
     */
    public void saveDeviceGroupInRedis(){
        List<SysDepart> sysDeparts = sysDepartService.queryDept();
        for (SysDepart sysDepart : sysDeparts) {
            List<ParkingLotAndCamVo> parkingLotAndCamVos = selectDeviceGroupByCompanyId(sysDepart.getId());
            if(parkingLotAndCamVos.size()>0){
                redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_GROUP + sysDepart.getId(),parkingLotAndCamVos);
            }
        }
        //管理员
        List<ParkingLotAndCamVo> parkingLotAndCamVos = selectDeviceGroupByCompanyId(null);
        redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_GROUP + "admin",parkingLotAndCamVos);
    }

    /**
     * 查询设备组根据公司id
     */
    public List<ParkingLotAndCamVo> selectDeviceGroupByCompanyId(String companyId){
        //获取到登录人的级联车场
        List<ParkingLotAndCamVo> parkingLotByCompanyId = parkingLotService.getSentryBoxLotByUserId(companyId);
        List<ParkingLotAndCamVo> list = new ArrayList<>();
        if(parkingLotByCompanyId.size()>0){
            //公司
            for (ParkingLotAndCamVo parkingLotAndCamVo : parkingLotByCompanyId) {
                List<ParkingLotAndCamVo> children = parkingLotAndCamVo.getChildren();
                List<ParkingLotAndCamVo> lotAndCamVoArrayList = new ArrayList<>();
                if(children.size()>0){
                    //车场
                    for (ParkingLotAndCamVo child : children) {
                        //岗亭
                        List<ParkingLotAndCamVo> lotAndCamVos = child.getChildren();
                        List<ParkingLotAndCamVo> parkingLotAndCamVos = new ArrayList<>();
                        if(lotAndCamVos.size()>0){
                            for (ParkingLotAndCamVo lotAndCamVo : lotAndCamVos) {
                                //获取到设备组
                                List<ParkingLotAndCamVo> byParkingLotId = this.baseMapper.getByParkingLotId(lotAndCamVo.getValue());
                                if(byParkingLotId.size()>0){
                                    lotAndCamVo.setChildren(byParkingLotId);
                                    parkingLotAndCamVos.add(lotAndCamVo);
                                }
                            }
                        }
                        child.setChildren(parkingLotAndCamVos);
                        if(parkingLotAndCamVos.size()>0){
                            lotAndCamVoArrayList.add(child);
                        }
                    }

                }
                if(lotAndCamVoArrayList.size()>0){
                    parkingLotAndCamVo.setChildren(lotAndCamVoArrayList);
                    list.add(parkingLotAndCamVo);
                }
            }
        }
        return list;
    }

    private Integer getLength(List<ParkingLotAndCamVo> parkingLotAndCamVo) {

        return 0;
    }

    /**
     * 根据车场id获取设备组
     * @return
     */
    @Override
    public List<CarDeviceGroup> getCameraDanDeviceGroup(String parkingLotId) {
        return this.baseMapper.getCameraDanDeviceGroup(parkingLotId);
    }

    /**
     * 更新redis数据
     */
    @Override
    public void updateRedisDeviceGroup() {
        SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
        List<ParkingLotAndCamVo> parkingLotAndCamVos = selectDeviceGroupByCompanyId(sysDepart.getId());
        redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_GROUP+sysDepart.getId(),parkingLotAndCamVos);
        //管理员
        List<ParkingLotAndCamVo> parkingLotAndCamVoss = selectDeviceGroupByCompanyId(null);
        redisUtil.set(ParkingRedisConstant.PARKING_DEVICE_GROUP + "admin",parkingLotAndCamVoss);
    }
}

package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.CarCardType;
import org.jeecg.modules.parking.mapper.CarCardTypeMapper;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class CarCardTypeQuery extends CarCardType {
    /**创建开始时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    /**创建结束时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;
    //登陆人id
    private String userId;
}

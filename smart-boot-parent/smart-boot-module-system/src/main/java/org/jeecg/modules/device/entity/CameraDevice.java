package org.jeecg.modules.device.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecg.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * @Description: 停车场摄像机设备信息表
 * @Author: jeecg-boot
 * @Date:   2021-11-25
 * @Version: V1.0
 */
@Data
@TableName("camera_device")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="camera_device对象", description="停车场摄像机设备信息表")
public class CameraDevice implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
    @Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
    @Excel(name = "创建时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
    /**车场id*/
    @Excel(name = "车场id", width = 15)
    @ApiModelProperty(value = "车场id")
    private java.lang.String parkingLotId;
    /**车场id*/
    @Excel(name = "岗亭id", width = 15)
    @ApiModelProperty(value = "岗亭id")
    private String sentryBoxId;
	/**设备名称*/
	@Excel(name = "设备名称", width = 15)
    @ApiModelProperty(value = "设备名称")
    private java.lang.String deviceName;
	/**设备序列号*/
	@Excel(name = "设备序列号", width = 15)
    @ApiModelProperty(value = "设备序列号")
    private java.lang.String serialno;
	/**设备ip地址*/
	@Excel(name = "设备ip地址", width = 15)
    @ApiModelProperty(value = "设备ip地址")
    private java.lang.String ipaddr;
	/**出入口设备 ，0：入口，1：出口*/
	@Excel(name = "出入口设备 ，0：入口，1：出口", width = 15)
    @Dict(dicCode = "sx_device")
    @ApiModelProperty(value = "出入口设备 ，0：入口，1：出口")
    private java.lang.String deviceUse;
	/**设备状态，0：在线，1：离线*/
	@Excel(name = "设备状态，0：在线，1：离线", width = 15)
    @Dict(dicCode = "sx_isoffline")
    @ApiModelProperty(value = "设备状态，0：在线，1：离线")
    private java.lang.String isoffline;


    private String companyId;

    /**
     * 车辆入场
     * 0：车辆入场
     * 1：临时车禁止入场
     * 2：所有车确认入场
     * 3：临时车确认入场
     */
    private String vehicleEntry;

    /**车场标识符*/
    @Excel(name = "经度", width = 15)
    @ApiModelProperty(value = "车场标识符")
    private java.lang.String longiTude;
    /**车场标识符*/
    @Excel(name = "纬度", width = 15)
    @ApiModelProperty(value = "车场标识符")
    private java.lang.String latiTude;
    /**车场标识符*/
    @Excel(name = "地址（省市区镇村……）", width = 35)
    @ApiModelProperty(value = "地址（省市区镇村……）")
    private java.lang.String address;
    /**车场标识符*/
    @Excel(name = "详细地址（输入到门牌号……）", width = 35)
    @ApiModelProperty(value = "详细地址（输入到门牌号……）")
    private java.lang.String detailedAddress;

    /**园区编码**/
    @ApiModelProperty(value = "园区编码")
    private java.lang.String screenCode;

    /**
     * 设备组id
     */
    private String deviceGroupId;

    /**
     * 设备类型(0:主设备，1：辅设备，2：补拍设备)
     */
    private String repairFlag;

    /**
     * led屏型号
     */
    private String model;

    /**
     * 播放云视频编号
     */
    private String nvrNumber;
}

package org.jeecg.modules.authentication.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapBuild {


    private static Map<String,Object> hashMapBuild = new HashMap<>();

    private static TreeMap<String,Object> treeMapBuild = new TreeMap<String,Object>();

    private MapBuild(){

    }

    public static Map<String,Object> hashMapBuild(){
        hashMapBuild.clear();
        return hashMapBuild;
    }


    public static TreeMap<String,Object> treeMapBuild(){
        treeMapBuild.clear();
        return treeMapBuild;
    }

    public static void clear(){
        hashMapBuild.clear();
        treeMapBuild().clear();
    }



}

package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.parking.entity.CarCharge;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.entity.CarShiftForm;
import org.jeecg.modules.parking.vo.CameraDeviceChVo;
import org.jeecg.modules.parking.vo.CarChargeVo;
import org.jeecg.modules.parking.vo.CarInfoCollectVo;
import org.jeecg.modules.parking.vo.ChargeParkingVo;
import org.jeecg.modules.system.entity.SysUser;

import java.util.List;

/**
 * @Description: 收费员表
 * @Author: jeecg-boot
 * @Date:   2022-03-25
 * @Version: V1.0
 */
public interface ICarChargeService extends IService<CarCharge> {

    IPage<CarChargeVo> pageList(Page<CarChargeVo> page, CarCharge carCharge);

    /**
     * 将数据存入到redis中
     * @param redisUtil
     * @return
     */
    List<CarChargeVo> setListToRedis(RedisUtil redisUtil);

    /**
     * 查询出登录的收费员对应管理车场的所有设备
     *
     * @return
     */
    List<CameraDeviceChVo> getSheBeiByUserId(String loginUserId,String deviceName);

    List<CarInfoCollectVo> getCarInfoDataBySerialno(String serialno);

    /**
     * 根据登录的收费员返回车场信息
     * @return
     */
    ChargeParkingVo getDataInfoByCharge(String loginUserId);

    /**
     *  编辑
     *
     * @param carCharge
     * @return
     */
    Result<?> updateCarChargeById(CarCharge carCharge);

    Result<?> saveCarCharge(CarCharge carCharge);

    /**
     * 判断登录的是不是收费员
     * @param id
     * @return
     */
    Boolean isCharge(String id);

    /**
     * 结算交班
     * @return
     */
    Result<?> shiftHandover(CarShiftForm carShiftForm);


    /**
     * 查询出自己公司下所有的收费员
     * @return
     */
    List<SysUser> getSysUserByChange();

}

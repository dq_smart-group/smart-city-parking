package org.jeecg.modules.parking.query;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CarInfoQuery {
    private String id;
    private String serialno;
    /**
     * 预收金额
     */
    private BigDecimal amountReceivedAdvance;
    /**
     * 本次金额
     */
    private BigDecimal currentAmount;
    /**
     * 减免金额
     */
    private BigDecimal deductionAmount;
}

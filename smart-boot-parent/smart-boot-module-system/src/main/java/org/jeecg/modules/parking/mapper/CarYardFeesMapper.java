package org.jeecg.modules.parking.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarYardFees;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: car_yard_fees
 * @Author: jeecg-boot
 * @Date:   2022-03-29
 * @Version: V1.0
 */
public interface CarYardFeesMapper extends BaseMapper<CarYardFees> {

    /**
     * 根据信息id和车场id查找
     * @param infoId
     * @return
     */
    CarYardFees getDataByInfoId(@Param("infoId") String infoId,@Param("parkingLotId") String parkingLotId);
}

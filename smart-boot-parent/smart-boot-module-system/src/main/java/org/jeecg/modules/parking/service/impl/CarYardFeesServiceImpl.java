package org.jeecg.modules.parking.service.impl;

import org.jeecg.modules.parking.entity.CarYardFees;
import org.jeecg.modules.parking.mapper.CarYardFeesMapper;
import org.jeecg.modules.parking.service.ICarYardFeesService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: car_yard_fees
 * @Author: jeecg-boot
 * @Date:   2022-03-29
 * @Version: V1.0
 */
@Service
public class CarYardFeesServiceImpl extends ServiceImpl<CarYardFeesMapper, CarYardFees> implements ICarYardFeesService {

    /**
     * 根据信息id和车场id查找
     * @param infoId
     * @return
     */
    @Override
    public CarYardFees getDataByInfoId(String infoId,String parkingLotId) {
        return this.baseMapper.getDataByInfoId(infoId,parkingLotId);
    }
}

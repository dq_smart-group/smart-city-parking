package org.jeecg.modules.parking.query;

import lombok.Data;

import java.util.Date;

@Data
public class MerchantAmountQuery {
    /**
     * 车场id
     */
    private String parkingLotId;
    /**
     * 车牌类型id
     */
    private String typeId;
    /**
     * 入场时间
     */
    private Date comeInDate;
    /**
     * 商家减免分钟数
     */
    private Integer minutes;
}

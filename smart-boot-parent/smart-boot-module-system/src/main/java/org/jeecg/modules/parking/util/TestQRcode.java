package org.jeecg.modules.parking.util;

import cn.hutool.core.codec.Base64;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import org.jeecg.common.util.PicUtils;
import org.jeecg.modules.parking.api.classHexstringToByteUtil;
import org.jeecg.modules.parking.mqtt.MqttPushClient;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import java.util.Hashtable;

public class TestQRcode {

    private static final int BLACK = 0xFF000000;
    private static final int WHITE = 0xFFFFFFFF;
    private static final int margin = 0;
    private static final int LogoPart = 4;

    /**
     * 生成二维码矩阵信息
     * @param content 二维码图片内容
     * @param width 二维码图片宽度
     * @param height 二维码图片高度
     */
    public static BitMatrix setBitMatrix(String content, int width, int height){
        Hashtable<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8"); // 指定编码方式,防止中文乱码
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.H); // 指定纠错等级
        hints.put(EncodeHintType.MARGIN, margin); // 指定二维码四周白色区域大小
        BitMatrix bitMatrix = null;
        try {
            bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return bitMatrix;
    }

    /**
     * 将二维码图片输出
     * @param matrix 二维码矩阵信息
     * @param format 图片格式
     * @param outStream 输出流
     * @param logoPath logo图片路径
     */
    public static void writeToFile(BitMatrix matrix, String format, OutputStream outStream, String logoPath) throws IOException {
        BufferedImage image = toBufferedImage(matrix);
        // 加入LOGO水印效果
        if (StringUtils.isNotBlank(logoPath)) {
            image = addLogo(image, logoPath);
        }
        ImageIO.write(image, format, outStream);
    }

    /**
     * 生成二维码图片
     * @param matrix 二维码矩阵信息
     */
    public static BufferedImage toBufferedImage(BitMatrix matrix) {
        int width = matrix.getWidth();
        int height = matrix.getHeight();
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
            }
        }
        return image;
    }

    /**
     * 在二维码图片中添加logo图片
     * @param image 二维码图片
     * @param logoPath logo图片路径
     */
    public static BufferedImage addLogo(BufferedImage image, String logoPath) throws IOException {
        Graphics2D g = image.createGraphics();
        BufferedImage logoImage = ImageIO.read(new File(logoPath));
        // 计算logo图片大小,可适应长方形图片,根据较短边生成正方形
        int width = image.getWidth() < image.getHeight() ? image.getWidth() / LogoPart : image.getHeight() / LogoPart;
        int height = width;
        // 计算logo图片放置位置
        int x = (image.getWidth() - width) / 2;
        int y = (image.getHeight() - height) / 2;
        // 在二维码图片上绘制logo图片
        g.drawImage(logoImage, x, y, width, height, null);
        // 绘制logo边框,可选
//        g.drawRoundRect(x, y, logoImage.getWidth(), logoImage.getHeight(), 10, 10);
        g.setStroke(new BasicStroke(2)); // 画笔粗细
        g.setColor(Color.WHITE); // 边框颜色
        g.drawRect(x, y, width, height); // 矩形边框
        logoImage.flush();
        g.dispose();
        return image;
    }

    /**
     * 为图片添加文字
     * @param pressText 文字
     * @param newImage 带文字的图片
     * @param targetImage 需要添加文字的图片
     * @param fontStyle 字体风格
     * @param color 字体颜色
     * @param fontSize 字体大小
     * @param width 图片宽度
     * @param height 图片高度
     */
    public static void pressText(String pressText, String newImage, String targetImage, int fontStyle, Color color, int fontSize, int width, int height) {
        // 计算文字开始的位置
        // x开始的位置：（图片宽度-字体大小*字的个数）/2
        int startX = (width-(fontSize*pressText.length()))/2;
        // y开始的位置：图片高度-（图片高度-图片宽度）/2
        int startY = height-(height-width)/2 + fontSize;
        try {
            File file = new File(targetImage);
            BufferedImage src = ImageIO.read(file);
            int imageW = src.getWidth(null);
            int imageH = src.getHeight(null);
            BufferedImage image = new BufferedImage(imageW, imageH, BufferedImage.TYPE_INT_RGB);
            Graphics g = image.createGraphics();
            g.drawImage(src, 0, 0, imageW, imageH, null);
            g.setColor(color);
            g.setFont(new Font(null, fontStyle, fontSize));
            g.drawString(pressText, startX, startY);
            g.dispose();
            FileOutputStream out = new FileOutputStream(newImage);
            ImageIO.write(image, "png", out);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    /**
     *
     * @param content
     */

    /**
     * 生成45*45二维码图片
     * @param content  二维码文本内容
     * @param chePai   车牌
     *  @param topic   发布主题
     *  @param cont   二维码下面的文字内容
     * @param flag 是否开启播报
     */
    public static void shenPic(String content,String chePai,String topic,String cont,Boolean flag){
        String format = "png";
        int width = 45;
        int height = 45;
        //生成二维码矩形信息
        BitMatrix bitMatrix = setBitMatrix(content, width, height);
        bitMatrix = deleteWhite(bitMatrix);
        // 可通过输出流输出到页面,也可直接保存到文件
        OutputStream outStream = null;
        String path = "D:/img/"+chePai+".bmp";
        try {

            outStream = new FileOutputStream(new File(path));
            writeToFile(bitMatrix, format, outStream, null);
            outStream.close();
            //调用转单位图
            ImageToHex.PiczhuangDan(chePai,topic,cont,flag);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /**
     * 强制将白边去掉
     * @explain
     *  虽然生成二维码时，已经将margin的值设为了0，但是在实际生成二维码时有时候还是会生成白色的边框，边框的宽度为10px；
     *  白边的生成还与设定的二维码的宽、高及二维码内容的多少（内容越多，生成的二维码越密集）有关；
     *  因为是在生成二维码之后，才将白边裁掉，所以裁剪后的二维码（实际二维码的宽、高）肯定不是你想要的尺寸，只能自己一点点试喽！
     * @param matrix
     * @return 裁剪后的二维码（实际二维码的大小）
     */
    private static BitMatrix deleteWhite(BitMatrix matrix) {
        int[] rec = matrix.getEnclosingRectangle();
        int resWidth = rec[2] + 0;
        int resHeight = rec[3] + 0;

        BitMatrix resMatrix = new BitMatrix(resWidth, resHeight);
        resMatrix.clear();
        for (int i = 0; i < resWidth; i++) {
            for (int j = 0; j < resHeight; j++) {
                if (matrix.get(i + rec[0], j + rec[1]))
                    resMatrix.set(i, j);
            }
        }

        int width = resMatrix.getWidth();
        int height = resMatrix.getHeight();
        BufferedImage image = new BufferedImage(width, height,
                BufferedImage.TYPE_INT_RGB);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                image.setRGB(x, y, resMatrix.get(x, y) ? 0 : 255);// 0-黑色;255-白色
            }
        }

        return resMatrix;
    }

    public static void main(String[] args) {

        //调用生成二维码和LED屏显示
        //shenPic("weixin://wxpay/bizpayurl?pr=RVEozZQzz","gkjdfsjkljkl");


      /*  String content = "https://tool.oschina.net/hexconverthukjhjkhkjhkhjkhjkh";
        String logoPath = "C:/logo.png";
        String format = "jpg";
        int width = 45;
        int height = 45;
        //生成二维码矩形信息
        BitMatrix bitMatrix = setBitMatrix(content, width, height);
        // 可通过输出流输出到页面,也可直接保存到文件
        OutputStream outStream = null;
        String path = "D:/img/jam.bmp";
        try {

            outStream = new FileOutputStream(new File(path));

            writeToFile(bitMatrix, format, outStream, null);
            outStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }
}
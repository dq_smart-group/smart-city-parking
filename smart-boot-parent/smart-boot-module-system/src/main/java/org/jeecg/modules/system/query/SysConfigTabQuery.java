package org.jeecg.modules.system.query;

import java.io.Serializable;

public class SysConfigTabQuery implements Serializable {

    /**
     * 配置分类名称
     */
    private String title;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 类型
     */
    private Integer type;

}
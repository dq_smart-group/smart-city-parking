package org.jeecg.modules.parking.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.lettuce.core.dynamic.annotation.Param;
import org.jeecg.modules.parking.entity.ParkingLot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.query.ParkingLotQuery;
import org.jeecg.modules.parking.vo.ParkingLotVo;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.parking.vo.ParkingManagementVo;

import java.util.List;

/**
 * @Description: 车场表
 * @Author: jeecg-boot
 * @Date:   2021-12-07
 * @Version: V1.0
 */
public interface ParkingLotMapper extends BaseMapper<ParkingLot> {

    List<ParkingLotVo> pageList(Page<ParkingLotVo> page, @Param("q") ParkingLotQuery q);

    List<ParkingLotAndCamVo> getParkingLotByCompanyId(@Param("CompanyId") String CompanyId);

    List<ParkingManagementVo> getParkingManagementByCompanyId(@Param("CompanyId") String CompanyId);

    List<ParkingLot> getList();

    /**
     * 根据车场标识符查询车场
     * @param identifier
     * @return
     */
    ParkingLot getParkingLotByIdentifier(@Param("q") String identifier);
}

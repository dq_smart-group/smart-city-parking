package org.jeecg.modules.parking.query;

import lombok.Data;

@Data
public class CarBrandQuery {
    //车辆品牌
    private String brand;
    //车辆年份
    private String year;
    //车辆类型
    private String type;
}

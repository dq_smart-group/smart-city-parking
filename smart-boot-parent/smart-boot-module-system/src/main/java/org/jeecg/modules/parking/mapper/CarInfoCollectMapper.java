package org.jeecg.modules.parking.mapper;

import java.util.Date;
import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarInfoCollect;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.entity.CarShiftForm;
import org.jeecg.modules.parking.vo.CarInfoCollectVo;
import org.jeecg.modules.parking.vo.MoneyStatisticsVo;

/**
 * @Description: 停车车辆信息收集表
 * @Author: jeecg-boot
 * @Date:   2021-11-24
 * @Version: V1.0
 */
public interface CarInfoCollectMapper extends BaseMapper<CarInfoCollect> {
    List<CarInfoCollectVo> selectData(Page page, @Param("q")CarInfoCollectVo carInfoCollectVo);

    /**
     * 获取小于strDate的数据的数量
     * @param strDate
     * @return
     */
    int getListCountBeforeDay(@Param("strDate") String strDate);


    /**
     * 判断是否存在该表
     * @param tableName
     * @return
     */
    List<String> hasTable(@Param("tableName") String tableName);

    /**
     * 查询小于strDate时间的数据
     * @param strDate
     * @return
     */
    List<CarInfoCollect> getListBeforeDay(@Param("strDate")String strDate);

    /**
     * 根据时间删除表数据
     * @param strDate
     * @return
     */
    boolean deleteToBeforeDay(@Param("strDate") String strDate);

    Integer getCountByloginTime(@Param("loginTime") Date loginTime,@Param("list") List<String> list);

    List<CarInfoCollect> getoutCountByloginTime(@Param("loginTime")Date loginTime,@Param("list") List<String> list);

    List<CarInfoCollect> getImgIsNull();

    /**
     * 查询园区下最近一条出入场记录
     * @param list
     * @return
     */
    CarInfoCollect screenCarInfo(@Param("list")List<String> list);

    //统计金额
    MoneyStatisticsVo moneyStatistics(@Param("q") CarInfoCollectVo carInfoCollectvo);

    //统计一下这段时间内现金收款：已收金额和减免金额
    MoneyStatisticsVo moneyStatisticss(@Param("q") CarShiftForm dataByUserId);
}

package org.jeecg.modules.parking.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.aspect.annotation.AutoLog;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.parking.entity.CarRenewalHistory;
import org.jeecg.modules.parking.service.ICarRenewalHistoryService;
import org.jeecg.modules.parking.vo.CarRenewalHistoryVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;

 /**
 * @Description: car_renewal_history
 * @Author: jeecg-boot
 * @Date:   2022-04-16
 * @Version: V1.0
 */
@Api(tags="car_renewal_history")
@RestController
@RequestMapping("/parking/carRenewalHistory")
@Slf4j
public class CarRenewalHistoryController extends JeecgController<CarRenewalHistory, ICarRenewalHistoryService> {
	@Autowired
	private ICarRenewalHistoryService carRenewalHistoryService;
	
	/**
	 * 分页列表查询
	 *
	 * @param carRenewalHistoryVo
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "car_renewal_history-分页列表查询")
	@ApiOperation(value="car_renewal_history-分页列表查询", notes="car_renewal_history-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarRenewalHistoryVo carRenewalHistoryVo,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		Page<CarRenewalHistoryVo> page = new Page<CarRenewalHistoryVo>(pageNo, pageSize);
		IPage<CarRenewalHistoryVo> pageList = carRenewalHistoryService.pageList(page,carRenewalHistoryVo);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carRenewalHistory
	 * @return
	 */
	@AutoLog(value = "car_renewal_history-添加")
	@ApiOperation(value="car_renewal_history-添加", notes="car_renewal_history-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarRenewalHistory carRenewalHistory) {
		carRenewalHistoryService.save(carRenewalHistory);
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param carRenewalHistory
	 * @return
	 */
	@AutoLog(value = "car_renewal_history-编辑")
	@ApiOperation(value="car_renewal_history-编辑", notes="car_renewal_history-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarRenewalHistory carRenewalHistory) {
		carRenewalHistoryService.updateById(carRenewalHistory);
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_renewal_history-通过id删除")
	@ApiOperation(value="car_renewal_history-通过id删除", notes="car_renewal_history-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carRenewalHistoryService.removeById(id);
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "car_renewal_history-批量删除")
	@ApiOperation(value="car_renewal_history-批量删除", notes="car_renewal_history-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carRenewalHistoryService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "car_renewal_history-通过id查询")
	@ApiOperation(value="car_renewal_history-通过id查询", notes="car_renewal_history-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarRenewalHistory carRenewalHistory = carRenewalHistoryService.getById(id);
		if(carRenewalHistory==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carRenewalHistory);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carRenewalHistory
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarRenewalHistory carRenewalHistory) {
        return super.exportXls(request, carRenewalHistory, CarRenewalHistory.class, "car_renewal_history");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarRenewalHistory.class);
    }

}

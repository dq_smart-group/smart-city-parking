package org.jeecg.modules.parking.vo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
public class CameraDeviceChVo {
    private String id;
    private String devicename;
    private String rtsp;
    private String serialno;
    private String deviceUse;
    private String license;
    /**
     * 车辆类型
     */
    private String type;
    /**进场时间*/
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date comeInTime;

    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date outTime;
    private String imgPath;
    private List<CarInfoCollectVo> dataVoList;
    private String carInfoId;
}

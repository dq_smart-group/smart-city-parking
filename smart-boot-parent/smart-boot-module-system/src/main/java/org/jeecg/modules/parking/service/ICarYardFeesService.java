package org.jeecg.modules.parking.service;

import org.jeecg.modules.parking.entity.CarYardFees;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: car_yard_fees
 * @Author: jeecg-boot
 * @Date:   2022-03-29
 * @Version: V1.0
 */
public interface ICarYardFeesService extends IService<CarYardFees> {

    /**
     * 根据信息id和车场id查找
     * @param infoId
     * @return
     */
    CarYardFees getDataByInfoId(String infoId,String parkingLotId);
}

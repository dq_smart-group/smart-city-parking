package org.jeecg.modules.parking.query;

import lombok.Data;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
public class ParkingLotQuery extends ParkingLot {
    //公司名称
    private String departName;

    /**创建开始时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date beginOrderTime;

    /**创建结束时间*/
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endOrderTime;
}

package org.jeecg.modules.parking.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.oConvertUtils;
import org.jeecg.modules.parking.entity.CarDeviceGroup;
import org.jeecg.modules.parking.query.CarDeviceGroupQuery;
import org.jeecg.modules.parking.service.ICarDeviceGroupService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.parking.vo.CarDeviceGroupVo;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 设备组表
 * @Author: jeecg-boot
 * @Date:   2022-04-02
 * @Version: V1.0
 */
@Api(tags="设备组表")
@RestController
@RequestMapping("/parking/carDeviceGroup")
@Slf4j
public class CarDeviceGroupController extends JeecgController<CarDeviceGroup, ICarDeviceGroupService> {
	@Autowired
	private ICarDeviceGroupService carDeviceGroupService;

	 @Autowired
	 private ISysDepartService sysDepartService;
	 @Autowired
	 private RedisUtil redisUtil;
	
	/**
	 * 分页列表查询
	 *
	 * @param carDeviceGroup
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "设备组表-分页列表查询")
	@ApiOperation(value="设备组表-分页列表查询", notes="设备组表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(CarDeviceGroupQuery carDeviceGroup,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			if(sysDepart!=null){
				carDeviceGroup.setCompanyId(sysDepart.getId());
			}else{
				carDeviceGroup.setCompanyId("0");
			}

		}
		Page<CarDeviceGroupVo> page = new Page<CarDeviceGroupVo>(pageNo, pageSize);
		IPage<CarDeviceGroupVo> pageList = carDeviceGroupService.pageList(page, carDeviceGroup);
		return Result.OK(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param carDeviceGroup
	 * @return
	 */
	@AutoLog(value = "设备组表-添加")
	@ApiOperation(value="设备组表-添加", notes="设备组表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CarDeviceGroup carDeviceGroup) {
		LambdaQueryWrapper<CarDeviceGroup> wrapper = new LambdaQueryWrapper<>();
		wrapper.eq(CarDeviceGroup::getGroupName,carDeviceGroup.getGroupName());
		wrapper.eq(CarDeviceGroup::getParkingLotId,carDeviceGroup.getParkingLotId());
		List<CarDeviceGroup> list = carDeviceGroupService.list(wrapper);
		if(list.size()>0){
			return Result.error("该车场下有重复组名，请检查");
		}

		carDeviceGroupService.save(carDeviceGroup);
		carDeviceGroupService.updateRedisDeviceGroup();
		return Result.OK("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param carDeviceGroup
	 * @return
	 */
	@AutoLog(value = "设备组表-编辑")
	@ApiOperation(value="设备组表-编辑", notes="设备组表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CarDeviceGroup carDeviceGroup) {
		LambdaQueryWrapper<CarDeviceGroup> wrapper = new LambdaQueryWrapper<>();
		wrapper.eq(CarDeviceGroup::getGroupName,carDeviceGroup.getGroupName());
		wrapper.eq(CarDeviceGroup::getParkingLotId,carDeviceGroup.getParkingLotId());
		wrapper.ne(CarDeviceGroup::getId,carDeviceGroup.getId());
		List<CarDeviceGroup> list = carDeviceGroupService.list(wrapper);
		if(list.size()>0){
			return Result.error("该车场下有重复组名，请检查");
		}

		carDeviceGroupService.updateById(carDeviceGroup);
		carDeviceGroupService.updateRedisDeviceGroup();
		return Result.OK("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "设备组表-通过id删除")
	@ApiOperation(value="设备组表-通过id删除", notes="设备组表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		carDeviceGroupService.removeById(id);
		carDeviceGroupService.updateRedisDeviceGroup();
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "设备组表-批量删除")
	@ApiOperation(value="设备组表-批量删除", notes="设备组表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.carDeviceGroupService.removeByIds(Arrays.asList(ids.split(",")));
		carDeviceGroupService.updateRedisDeviceGroup();
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "设备组表-通过id查询")
	@ApiOperation(value="设备组表-通过id查询", notes="设备组表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CarDeviceGroup carDeviceGroup = carDeviceGroupService.getById(id);
		if(carDeviceGroup==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(carDeviceGroup);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param carDeviceGroup
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CarDeviceGroup carDeviceGroup) {
        return super.exportXls(request, carDeviceGroup, CarDeviceGroup.class, "设备组表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CarDeviceGroup.class);
    }


	 /**
	  * 获取登录人下的设备组 级联
	  * @return
	  */
	 @GetMapping(value = "/getCameraDeviceGroup")
	 public List<ParkingLotAndCamVo> getCameraDeviceGroup(){
		 return carDeviceGroupService.selectByCompanyId();
	 }


	 /**
	  * 根据车场id获取设备组
	  * @return
	  */
	 @GetMapping(value = "/getCameraDanDeviceGroup")
	 public List<CarDeviceGroup> getCameraDanDeviceGroup(String parkingLotId){
		 return carDeviceGroupService.getCameraDanDeviceGroup(parkingLotId);

	 }

}

package org.jeecg.modules.parking.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.jeecg.modules.parking.entity.CarTopUp;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

@Data
public class CarTopUpVo extends CarTopUp {
    /**账号*/
    @Excel(name = "账号", width = 15)
    private java.lang.String account;
    /**盐值*/
    @Excel(name = "盐值", width = 15)
    private java.lang.String salt;
    /**名称*/
    @Excel(name = "名称", width = 15)
    private java.lang.String name;
    /**密码*/
    @Excel(name = "密码", width = 15)
    private java.lang.String password;
    /**电话*/
    @Excel(name = "电话", width = 15)
    private java.lang.String phone;
    /**地址*/
    @Excel(name = "地址", width = 15)
    private java.lang.String address;
    /**开始时间*/
    @Excel(name = "开始时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private java.util.Date startTime;
    /**结束时间*/
    @Excel(name = "结束时间", width = 15, format = "yyyy-MM-dd")
    @JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private java.util.Date endTime;
    /**优惠类型--1：固定扣款；2：折扣率*/
    @Excel(name = "优惠类型--1：固定扣款；2：折扣率", width = 15)
    private java.lang.String preferentialType;
    /**单价（1元/分钟）*/
    @Excel(name = "单价（1元/分钟）", width = 15)
    private java.math.BigDecimal unitPrice;
    /**状态--1：启用；0：禁止*/
    @Excel(name = "状态--1：启用；0：禁止", width = 15)
    private java.lang.Integer state;
    /**签名（key）*/
    @Excel(name = "签名（key）", width = 15)
    private java.lang.String signature;
    /**说明*/
    @Excel(name = "说明", width = 15)
    private java.lang.String remark;
    /**折扣率*/
    @Excel(name = "折扣率", width = 15)
    private java.math.BigDecimal discountRate;
    /**剩余分钟数*/
    @Excel(name = "剩余分钟数", width = 15)
    private java.lang.Integer minutesRemaining;
    /**单次减免分钟数*/
    @Excel(name = "单次减免分钟数", width = 15)
    private java.lang.Integer singleReduction;
    /**二维码*/
    @Excel(name = "二维码", width = 15)
    private java.lang.String quickMark;
    /**余额*/
    @Excel(name = "余额", width = 15)
    private java.math.BigDecimal balance;

}

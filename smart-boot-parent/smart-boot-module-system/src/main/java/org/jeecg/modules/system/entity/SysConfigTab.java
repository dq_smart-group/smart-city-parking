package org.jeecg.modules.system.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 配置分类表
 * @Author: jeecg-boot
 * @Date:   2021-12-03
 * @Version: V1.0
 */
@Data
@TableName("sys_config_tab")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="sys_config_tab对象", description="配置分类表")
public class SysConfigTab implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ASSIGN_ID)
    private java.lang.String id;
	/**创建人*/
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.util.Date createTime;
	/**更新人*/
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private java.util.Date updateTime;

    private java.lang.String sysOrgCode;
	/**配置分类名称*/
	@Excel(name = "配置分类名称", width = 15)
    private java.lang.String title;
	/**标识*/
	@Excel(name = "标识", width = 15)
    private java.lang.String flag;
	/**状态*/
	@Excel(name = "状态", width = 15)
    @Dict(dicCode = "config_tab_status")
    private java.lang.Integer status;
	/**类型*/
	@Excel(name = "类型", width = 15)
    @Dict(dicCode = "config_tab_type")
    private java.lang.Integer type;
	/**当前数据是否允许删除（1可删除，0不能删除）*/
	@Excel(name = "当前数据是否允许删除（1可删除，0不能删除）", width = 15)
    @Dict(dicCode = "del_permission")
    private java.lang.Integer delPermission;
}

package org.jeecg.modules.parking.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarTopUp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: car_top_up
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
public interface CarTopUpMapper extends BaseMapper<CarTopUp> {

}

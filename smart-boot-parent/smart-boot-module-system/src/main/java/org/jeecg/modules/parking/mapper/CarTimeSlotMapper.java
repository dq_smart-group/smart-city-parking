package org.jeecg.modules.parking.mapper;

import java.util.List;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarTimeSlot;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.query.CarTimeSlotQuery;
import org.jeecg.modules.parking.vo.CarTimeSlotVo;

/**
 * @Description: 时间段管理表
 * @Author: jeecg-boot
 * @Date:   2022-01-23
 * @Version: V1.0
 */
public interface CarTimeSlotMapper extends BaseMapper<CarTimeSlot> {

    List<CarTimeSlotVo> pageList(Page<CarTimeSlotVo> page,@Param("q") CarTimeSlotQuery q);
}

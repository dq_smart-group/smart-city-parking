package org.jeecg.modules.parking.mapper;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarWhiteList;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.jeecg.modules.parking.query.CarWhiteListQuery;
import org.jeecg.modules.parking.vo.CarWhiteListVo;

import java.util.List;

/**
 * @Description: 车牌白名单表
 * @Author: jeecg-boot
 * @Date:   2021-12-02
 * @Version: V1.0
 */
public interface CarWhiteListMapper extends BaseMapper<CarWhiteList> {

    List<CarWhiteListVo> selectListPage(Page page, @Param("q") CarWhiteListQuery carWhiteListQuery);

    CarWhiteList getByChePai(@Param("chepai") String chepai);

    List<CarWhiteListVo> selectListByOpenid(@Param("openid") String openid);

    String getPhone(@Param("openid") String openid);

    List<CarWhiteList> detectionWhitelistExpired(@Param("date") String date);
}

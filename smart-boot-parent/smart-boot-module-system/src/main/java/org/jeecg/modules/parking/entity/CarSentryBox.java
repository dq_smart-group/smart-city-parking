package org.jeecg.modules.parking.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: car_sentry_box
 * @Author: jeecg-boot
 * @Date:   2022-05-05
 * @Version: V1.0
 */
@Data
@TableName("car_sentry_box")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="car_sentry_box对象", description="car_sentry_box")
public class CarSentryBox implements Serializable {
    private static final long serialVersionUID = 1L;

	/**岗亭id*/
	@TableId(type = IdType.ASSIGN_ID)
    @ApiModelProperty(value = "岗亭id")
    private java.lang.String id;
    /**公司id*/
    @Excel(name = "公司id", width = 15)
    @ApiModelProperty(value = "公司id")
    private java.lang.String companyId;
	/**车场id*/
	@Excel(name = "车场id", width = 15)
    @ApiModelProperty(value = "车场id")
    private java.lang.String parkingLotId;
	/**岗亭类型： 1- 单进单出 2- 单进 3- 单出 4- 同进同出*/
	@Excel(name = "岗亭类型： 1- 单进单出 2- 单进 3- 单出 4- 同进同出", width = 15)
    @ApiModelProperty(value = "岗亭类型： 1- 单进单出 2- 单进 3- 单出 4- 同进同出")
    private java.lang.Integer sentryBoxType;
	/**岗亭名称*/
	@Excel(name = "岗亭名称", width = 15)
    @ApiModelProperty(value = "岗亭名称")
    private java.lang.String sentryBoxName;
	/**上传静态交通：0-否 1-是*/
	@Excel(name = "上传静态交通：0-否 1-是", width = 15)
    @ApiModelProperty(value = "上传静态交通：0-否 1-是")
    private Boolean isStaticUp;
	/**上传静态交通时间*/
	@Excel(name = "上传静态交通时间", width = 15)
    @ApiModelProperty(value = "上传静态交通时间")
    private Date staticUpTime;
	/**备注*/
	@Excel(name = "备注", width = 15)
    @ApiModelProperty(value = "备注")
    private java.lang.String remark;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;


    /**
     * 所属车场
     */
    @TableField(exist = false)
    private java.lang.String parkingLotName;
}

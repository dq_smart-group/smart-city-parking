package org.jeecg.modules.parking.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.jeecg.modules.parking.entity.CarRechargeRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.modules.parking.vo.CarRechargeRecordVo;

/**
 * @Description: car_recharge_record
 * @Author: jeecg-boot
 * @Date:   2022-03-23
 * @Version: V1.0
 */
public interface ICarRechargeRecordService extends IService<CarRechargeRecord> {

    IPage<CarRechargeRecordVo> pageList(Page<CarRechargeRecord> page, QueryWrapper<CarRechargeRecordVo> queryWrapper, CarRechargeRecordVo carRechargeRecord);
}

package org.jeecg.modules.device.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.lang.Assert;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.device.entity.CameraDevice;
import org.jeecg.modules.device.query.CameraDeviceQuery;
import org.jeecg.modules.device.service.ICameraDeviceService;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecg.modules.device.vo.CameraDeviceVo;
import org.jeecg.common.system.base.controller.JeecgController;
import org.jeecg.modules.parking.entity.ParkingLot;
import org.jeecg.modules.parking.mqtt.MqttPushClient;
import org.jeecg.modules.parking.query.LedQuery;
import org.jeecg.modules.parking.service.IParkingLotService;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 停车场摄像机设备信息表
 * @Author: jeecg-boot
 * @Date:   2021-11-25
 * @Version: V1.0
 */
@Api(tags="停车场摄像机设备信息表")
@RestController
@RequestMapping("/device/cameraDevice")
@Slf4j
public class CameraDeviceController extends JeecgController<CameraDevice, ICameraDeviceService> {
	@Autowired
	private ICameraDeviceService cameraDeviceService;

	@Autowired
	private ISysDepartService sysDepartService;

	@Autowired
	private IParkingLotService parkingLotService;

	@Autowired
	private RedisUtil redisUtil;
	
	/**
	 * 分页列表查询
	 *
	 * @param cameraDeviceQuery
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "停车场摄像机设备信息表-分页列表查询")
	@ApiOperation(value="停车场摄像机设备信息表-分页列表查询", notes="停车场摄像机设备信息表-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList( CameraDeviceQuery cameraDeviceQuery,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		if(!LoginUserUtil.isAdmin()){
			SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
			cameraDeviceQuery.setCompanyId(sysDepart.getId());
		}
		Page<CameraDeviceVo> page = new Page<CameraDeviceVo>(pageNo, pageSize);
		IPage<CameraDeviceVo> pageList = cameraDeviceService.listPage(page, cameraDeviceQuery);
		return Result.OK(pageList);
	}



	/**
	 *   添加
	 *
	 * @param cameraDevice
	 * @return
	 */
	@AutoLog(value = "停车场摄像机设备信息表-添加")
	@ApiOperation(value="停车场摄像机设备信息表-添加", notes="停车场摄像机设备信息表-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody CameraDevice cameraDevice) {
		return cameraDeviceService.saveCameraDevice(cameraDevice);
	}
	
	/**
	 *  编辑
	 *
	 * @param cameraDevice
	 * @return
	 */
	@AutoLog(value = "停车场摄像机设备信息表-编辑")
	@ApiOperation(value="停车场摄像机设备信息表-编辑", notes="停车场摄像机设备信息表-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody CameraDevice cameraDevice) {

		return cameraDeviceService.updateByCameraDevice(cameraDevice);
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "停车场摄像机设备信息表-通过id删除")
	@ApiOperation(value="停车场摄像机设备信息表-通过id删除", notes="停车场摄像机设备信息表-通过id删除")
	@DeleteMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		CameraDevice byId = cameraDeviceService.getById(id);
		redisUtil.del(ParkingRedisConstant.PARKING_DEVICE+byId.getSerialno());
		cameraDeviceService.removeById(id);
		cameraDeviceService.setDataInRedis(byId.getDeviceGroupId());
		cameraDeviceService.updateRedisDeviceGroup();
		return Result.OK("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "停车场摄像机设备信息表-批量删除")
	@ApiOperation(value="停车场摄像机设备信息表-批量删除", notes="停车场摄像机设备信息表-批量删除")
	@DeleteMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		String[] split = ids.split(",");
		for (String s : split) {
			CameraDevice byId = cameraDeviceService.getById(s);
			cameraDeviceService.setDataInRedis(byId.getDeviceGroupId());
			redisUtil.del(ParkingRedisConstant.PARKING_DEVICE+byId.getSerialno());
		}
		this.cameraDeviceService.removeByIds(Arrays.asList(ids.split(",")));
		cameraDeviceService.updateRedisDeviceGroup();
		return Result.OK("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "停车场摄像机设备信息表-通过id查询")
	@ApiOperation(value="停车场摄像机设备信息表-通过id查询", notes="停车场摄像机设备信息表-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		CameraDevice cameraDevice = cameraDeviceService.getById(id);
		if(cameraDevice==null) {
			return Result.error("未找到对应数据");
		}
		return Result.OK(cameraDevice);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param cameraDevice
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, CameraDevice cameraDevice) {
        return super.exportXls(request, cameraDevice, CameraDevice.class, "停车场摄像机设备信息表");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, CameraDevice.class);
    }


	 /**
	  * 获取登录人下的设备
	  *
	  * @return
	  */
	 @GetMapping(value = "/getSheBei")
	 public List<CameraDevice> getSheBei() {
	 	String companyId = null;
	 	if(!LoginUserUtil.isAdmin()){
			companyId = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode()).getId();
		    if(StringUtils.isEmpty(companyId)){
				companyId = "0";
			}
	 	}
		 return cameraDeviceService.selectByOrgCode(companyId);
	 }


	 /**
	  * 获取登录人下的设备 级联
	  * @return
	  */
	 @GetMapping(value = "/getCameraDevice")
	 public List<ParkingLotAndCamVo> getCameraDevice(){
		 return cameraDeviceService.selectByCompanyId();
	 }


	 /**
	  * 开闸
	  *
	  * @param cameraDevice
	  * @return
	  */
	 @AutoLog(value = "开闸操作")
	 @ApiOperation(value="开闸", notes="开闸")
	 @PostMapping(value = "/openGate")
	 public Result<?> openGate(@RequestBody CameraDeviceVo cameraDevice) throws Exception{

		 String serialno = cameraDevice.getSerialno();
//		 String whyOpenGate = cameraDevice.getWhyOpenGate();
		 Assert.isTrue(StringUtils.isNotEmpty(serialno),"参数异常");
//		 Assert.isTrue(StringUtils.isNotEmpty(whyOpenGate),"开闸原因不能为空");

		 MqttPushClient.publishOpen(MqttPushClient.getTopic(serialno));
		 return Result.OK("开闸成功");
	 }

	 /**
	  * 开闸
	  *
	  * @param cameraDevice
	  * @return
	  */
	 @AutoLog(value = "关闸操作")
	 @ApiOperation(value="关闸", notes="关闸")
	 @PostMapping(value = "/closeGate")
	 public Result<?> closeGate(@RequestBody CameraDeviceVo cameraDevice) throws Exception{

		 String serialno = cameraDevice.getSerialno();
		 Assert.isTrue(StringUtils.isNotEmpty(serialno),"参数异常");
		 MqttPushClient.publishClose(MqttPushClient.getTopic(cameraDevice.getSerialno()));
		 return Result.OK("关闸成功");
	 }



	 /**
	  * 重拍成功
	  * @param serialno
	  * @return
	  * @throws Exception
	  */
	 @AutoLog(value = "重拍操作")
	 @GetMapping("/manualCapture")
	 public Result<?> manualCapture(String serialno)throws Exception{
		 Assert.isTrue(StringUtils.isNotEmpty(serialno),"参数异常");
		 MqttPushClient.manualCapture(serialno);
		 return Result.OK("重拍成功");
	 }

	 /**
	  * 修改设备ip
	  * @param cameraDevice
	  * @return
	  */
	 @PostMapping("/modifyIP")
	 public Result<?> modifyIP(@RequestBody CameraDevice cameraDevice){
		 return cameraDeviceService.modifyIP(cameraDevice);
	 }


	 /**
	  * 同步时间
	  * @return
	  */
	 @GetMapping("/synchronizationTime")
	 public Result<?> synchronizationTime(String serialno){
		 Assert.isTrue(StringUtils.isNotEmpty(serialno),"参数异常");
		 MqttPushClient.synchronizationTime(serialno);
		 return Result.OK();
	 }


	 /**
	  * 定时检查设备状态：在线、离线
	  * @return
	  */
	 @GetMapping("checkTheEquipmentStatus")
	 public Result checkTheEquipmentStatus(){
		 cameraDeviceService.checkTheEquipmentStatus();
		 return Result.OK();
	 }

}

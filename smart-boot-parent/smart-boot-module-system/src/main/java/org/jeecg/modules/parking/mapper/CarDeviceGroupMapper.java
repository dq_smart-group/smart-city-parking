package org.jeecg.modules.parking.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.parking.entity.CarDeviceGroup;
import org.jeecg.modules.parking.query.CarDeviceGroupQuery;
import org.jeecg.modules.parking.vo.CarDeviceGroupVo;
import org.jeecg.modules.parking.vo.ParkingLotAndCamVo;

import java.util.List;

/**
 * @Description: 设备组表
 * @Author: jeecg-boot
 * @Date:   2022-04-02
 * @Version: V1.0
 */
public interface CarDeviceGroupMapper extends BaseMapper<CarDeviceGroup> {

    List<CarDeviceGroupVo> pageList(Page<CarDeviceGroupVo> page,@Param("q") CarDeviceGroupQuery q);

    List<ParkingLotAndCamVo> getByParkingLotId(@Param("id") String id);

    /**
     * 根据车场id获取设备组
     * @return
     */
    List<CarDeviceGroup> getCameraDanDeviceGroup(@Param("parkingLotId") String parkingLotId);

}

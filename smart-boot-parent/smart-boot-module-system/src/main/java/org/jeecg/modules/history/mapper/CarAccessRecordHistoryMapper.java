package org.jeecg.modules.history.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.jeecg.modules.history.entity.CarAccessRecordHistory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: car_access_record_history_1
 * @Author: jeecg-boot
 * @Date:   2022-03-01
 * @Version: V1.0
 */
public interface CarAccessRecordHistoryMapper extends BaseMapper<CarAccessRecordHistory> {

    /**
     * 获取表格数据数量
     * @param tableName
     * @return
     */
    int getListCount(@Param("tableName") String tableName);

    /**
     * 创建表
     * @param tableName
     */
    void createHistoryTable(@Param("tableName")String tableName);

    /**
     * 保存数据到历史
     * @param list
     * @return
     */
//    int saveDataToHistory(@Param("list") List<CarAccessRecord> list ,@Param("tableName") String tableName);
}

package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang.StringUtils;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.constant.ParkingRedisConstant;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.modules.parking.entity.CarTimeSlot;
import org.jeecg.modules.parking.mapper.CarTimeSlotMapper;
import org.jeecg.modules.parking.query.CarTimeSlotQuery;
import org.jeecg.modules.parking.service.ICarTimeSlotService;
import org.jeecg.modules.parking.vo.CarTimeSlotVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 时间段管理表
 * @Author: jeecg-boot
 * @Date:   2022-01-23
 * @Version: V1.0
 */
@Service
public class CarTimeSlotServiceImpl extends ServiceImpl<CarTimeSlotMapper, CarTimeSlot> implements ICarTimeSlotService {

    @Autowired
    private RedisUtil redisUtil;
    @Override
    public IPage<CarTimeSlotVo> pageList(Page<CarTimeSlotVo> page, CarTimeSlotQuery carTimeSlotQuery) {

        List<CarTimeSlotVo> list = this.baseMapper.pageList(page,carTimeSlotQuery);
        if(list.size()>0){
            for (CarTimeSlotVo carTimeSlotVo : list) {
                String executionTime = carTimeSlotVo.getExecutionTime();
                if(StringUtils.isNotEmpty(executionTime)){
                    String[] split = executionTime.split(",");
                    if(split.length>0){
                        List<String> list1 = new ArrayList<>();
                        for (String s : split) {
                            list1.add(s);
                        }
                        carTimeSlotVo.setExecutionTimes(list1);
                    }
                }
            }
        }
        return page.setRecords(list);
    }

    /**
     * 添加
     * @param carTimeSlot
     */
    @Override
    public Result saveCarTimeSlot(CarTimeSlot carTimeSlot) {
        String executionTime = carTimeSlot.getExecutionTime();
        if(StringUtils.isNotEmpty(executionTime)){
            String[] split = executionTime.split(",");
            if(split.length>0){
                for (String s : split) {
                    LambdaQueryWrapper<CarTimeSlot> wrapper = new LambdaQueryWrapper<>();
                    wrapper.eq(CarTimeSlot::getDeviceId,carTimeSlot.getDeviceId());
                    wrapper.eq(CarTimeSlot::getType,carTimeSlot.getType());
                    wrapper.last("and find_in_set("+s+",execution_time)");
                    List<CarTimeSlot> carTimeSlots = this.baseMapper.selectList(wrapper);
                    if(carTimeSlots.size()>0){
                        return Result.error("该设备下的车辆类型已设置有执行时间，请检查");
                    }
                }
                this.baseMapper.insert(carTimeSlot);
                for (String s : split) {
                    redisUtil.set(ParkingRedisConstant.PARKING_TIME_SLOT+carTimeSlot.getDeviceId()+":"+carTimeSlot.getType()+":"+s,carTimeSlot);
                }
                return Result.OK("添加成功");
            }
            return Result.error("请选择执行时间，请检查");
        }
        return Result.error("请选择执行时间，请检查");
    }

    @Override
    public Result updateCarTimeSlotById(CarTimeSlot carTimeSlot) {
        String executionTime = carTimeSlot.getExecutionTime();
        if(StringUtils.isNotEmpty(executionTime)){
            String[] split = executionTime.split(",");
            if(split.length>0){
                for (String s : split) {
                    LambdaQueryWrapper<CarTimeSlot> wrapper = new LambdaQueryWrapper<>();
                    wrapper.eq(CarTimeSlot::getDeviceId,carTimeSlot.getDeviceId());
                    wrapper.eq(CarTimeSlot::getType,carTimeSlot.getType());
                    wrapper.ne(CarTimeSlot::getId,carTimeSlot.getId());
                    wrapper.last("and find_in_set("+s+",execution_time)");
                    List<CarTimeSlot> carTimeSlots = this.baseMapper.selectList(wrapper);
                    if(carTimeSlots.size()>0){
                        return Result.error("该设备下的车辆类型已设置有执行时间，请检查");
                    }
                }
                CarTimeSlot carTimeSlot1 = this.baseMapper.selectById(carTimeSlot.getId());
                String[] split1 = carTimeSlot1.getExecutionTime().split(",");
                for (String s : split1) {
                    redisUtil.del(ParkingRedisConstant.PARKING_TIME_SLOT+carTimeSlot1.getDeviceId()+":"+carTimeSlot1.getType()+":"+s);
                }
                this.baseMapper.updateById(carTimeSlot);
                for (String s : split) {
                    redisUtil.set(ParkingRedisConstant.PARKING_TIME_SLOT+carTimeSlot.getDeviceId()+":"+carTimeSlot.getType()+":"+s,carTimeSlot);
                }
                return Result.OK("修改成功");
            }
            return Result.error("请选择执行时间，请检查");
        }
        return Result.error("请选择执行时间，请检查");
    }

    /**
     * 根据设备id和车辆类型和星期几查找时间段管理
     * @param cameraId 设备id
     * @param type 车辆类型
     * @param day 星期几
     * @return
     */
    @Override
    public List<CarTimeSlot> getCarTimeSlotSByCameraIdAndType(String cameraId, String type,Integer day) {
        LambdaQueryWrapper<CarTimeSlot> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(CarTimeSlot::getDeviceId,cameraId);
        wrapper.in(CarTimeSlot::getType,type,"0");
        wrapper.last("and find_in_set("+day+",execution_time)");
        List<CarTimeSlot> carTimeSlots = this.baseMapper.selectList(wrapper);
        return carTimeSlots;
    }

    @Override
    public List<CarTimeSlot> getList() {
        return this.baseMapper.selectList(null);
    }

}

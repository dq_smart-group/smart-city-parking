package org.jeecg.modules.parking.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Assert;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.jeecg.common.system.util.LoginUserUtil;
import org.jeecg.modules.parking.entity.CarCommercial;
import org.jeecg.modules.parking.entity.CarRechargeRecord;
import org.jeecg.modules.parking.entity.CarTopUp;
import org.jeecg.modules.parking.mapper.CarTopUpMapper;
import org.jeecg.modules.parking.service.ICarTopUpService;
import org.jeecg.modules.parking.vo.CarTopUpVo;
import org.jeecg.modules.parking.vo.TopUpVo;
import org.jeecg.modules.system.entity.SysDepart;
import org.jeecg.modules.system.service.ISysDepartService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Description: car_top_up
 * @Author: jeecg-boot
 * @Date:   2022-03-08
 * @Version: V1.0
 */
@Service
public class CarTopUpServiceImpl extends ServiceImpl<CarTopUpMapper, CarTopUp> implements ICarTopUpService {

    @Autowired
    private CarCommercialServiceImpl commercialService;
    @Autowired
    CarRechargeRecordServiceImpl rechargeRecordService;
    @Autowired
    private ISysDepartService sysDepartService;

    @Override
    public IPage<CarTopUpVo> pageVo(Page<CarTopUp> page, QueryWrapper<CarTopUpVo> queryWrapper, CarTopUpVo carTopUp) {
        QueryWrapper<CarTopUp> carWrapper = new QueryWrapper<>();
        BeanUtils.copyProperties(queryWrapper,carWrapper);
        IPage<CarTopUp> carTopUpPage = this.page(page, carWrapper);
        List<CarTopUp> records = carTopUpPage.getRecords();
        IPage<CarTopUpVo> carTopUpVoPage = new Page<>();
        BeanUtils.copyProperties(carTopUpPage,carTopUpVoPage);
        if(records.size()>0) {
            //避免重复请求数据库，一次性查询出商户信息
            List<String> ids = records.stream().map(i -> i.getCommercialTenantId()).collect(Collectors.toList());
            List<CarCommercial> carCommercials;
            //筛选公司商户
            if(!LoginUserUtil.isAdmin()) {
                SysDepart sysDepart = sysDepartService.queryCompByOrgCode(LoginUserUtil.getOrgCode());
                if (sysDepart != null) {
                    carCommercials = commercialService.listByIds(ids).stream().filter(i -> i.getCompanyId().equals(sysDepart.getId())).collect(Collectors.toList());
                } else {
                    carCommercials = commercialService.listByIds(ids).stream().filter(i -> i.getCompanyId().equals("0")).collect(Collectors.toList());
                }
            }else {
                carCommercials = commercialService.listByIds(ids);
            }

            //筛选数据
            List<CarCommercial> commercials = carCommercials.stream().filter(i ->
                            null != carTopUp.getPhone() ? carTopUp.getPhone().equals(i.getPhone()):true).collect(Collectors.toList());
            //转换为map，便于匹配数据
            Map<String, CarTopUp> collectMap = records.stream().collect(Collectors.toMap(CarTopUp::getCommercialTenantId, i -> i));
            if(collectMap.size()>0) {
                //聚合数据
                List<CarTopUpVo> topUpVos = commercials.stream().map(i -> {
                    CarTopUpVo carTopUpVo = new CarTopUpVo();
                    BeanUtils.copyProperties(i, carTopUpVo);
                    carTopUpVo.setCommercialTenantId(i.getId());
                    BeanUtils.copyProperties(collectMap.get(i.getId()), carTopUpVo);
                    return carTopUpVo;
                }).collect(Collectors.toList());
                carTopUpVoPage.setRecords(topUpVos);
                carTopUpVoPage.setTotal(topUpVos.size());
            }else {
                carTopUpVoPage = new Page<>();
            }
        }
        return carTopUpVoPage;
    }

    @Override
    public void edit(TopUpVo topUpVo) {
        CarCommercial commercial = commercialService.getById(topUpVo.getId());
        //判断状态是否禁止
        Assert.isTrue(commercial.getState()==1,"该商户已禁用");
        //判断充值还是退费
        if(topUpVo.getRechargeStatus()){
            //充值
            //添加充值记录
            CarRechargeRecord carRechargeRecord = new CarRechargeRecord();
            //判断是余额充值还是分钟充值
            if(topUpVo.getRechargeType() == 1){
                //余额充值
                commercial.setBalance(commercial.getBalance().add(topUpVo.getRechargeValue()));
                carRechargeRecord.setRechargeAmount(topUpVo.getRechargeValue());
                carRechargeRecord.setRechargeMinute(0);
            }
            else {
                //分钟充值
                commercial.setMinutesRemaining(commercial.getMinutesRemaining()+topUpVo.getRechargeValue().intValue());
                carRechargeRecord.setRechargeMinute(topUpVo.getRechargeValue().intValue());
                carRechargeRecord.setRechargeAmount(BigDecimal.valueOf(0));
            }
            CarTopUp carTopUp = this.getOne(Wrappers.<CarTopUp>lambdaQuery().eq(CarTopUp::getCommercialTenantId, topUpVo.getId()));
            carTopUp.setLastTopUp(topUpVo.getRechargeValue().intValue());
            carTopUp.setTopUpTime(new Date());
            updateById(carTopUp);
            carRechargeRecord.setBalance(commercial.getBalance());
            carRechargeRecord.setCommercialTenantId(commercial.getId());
            carRechargeRecord.setPayTime(new Date());
            rechargeRecordService.save(carRechargeRecord);
        }else {
            //退费
            commercial.setBalance(commercial.getBalance().subtract(topUpVo.getRefundAmount()));
        }
        commercialService.updateById(commercial);
    }
}

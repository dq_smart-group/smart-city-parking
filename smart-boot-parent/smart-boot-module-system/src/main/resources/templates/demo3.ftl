<!DOCTYPE html>
<html><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>商户支付</title>

    <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <link href="/smart-city/css/bootstrap.min.css" rel="stylesheet">
    <link href="/smart-city/css/font-awesome.min.css" rel="stylesheet">
    <link href="/smart-city/css/animate.css" rel="stylesheet">
    <link href="/smart-city/css/style.min.css" rel="stylesheet">
    <link href="/smart-city/css/layer.css" rel="stylesheet">
    <link href="/smart-city/css/carpicker.css" rel="stylesheet">

    <script src="/smart-city/js/jquery.min.js"></script>
    <script src="/smart-city/js/jquery.common.js"></script>
    <script src="/smart-city/js/jquery.tmpl.min.js"></script>
    <script src="/smart-city/js/ttyUtil.js"></script>
    <script src="/smart-city/js/carpicker.js"></script>
    <script src="/smart-city/js/layer.min.js"></script>

    <style>
        /* layout */
        body {
            text-align: center;
        }

        .title {
            min-width: 100px;
            min-height: 50px;
        }

        .text {
            width: 100%;
            margin: 60px auto;
            text-align: center;
        }
        .car_number {
            display: inline-block;
            padding: 5px;
            width: 20%;
            line-height: 15px;
            font-size: 14px;
            background: #ff0000;
            color: #fff;
        }

        .car_context {
            display: inline-block;
            margin-top: 50px;
            padding: 5px;
            width: 80%;
            font-size: 24px;
            color: #CD2626;
        }

        .sub_title {
            padding: 10px;
            font-size: 18px;
            color: #fff;
            background: #1E90FF;
            padding: 20px 0px;
            margin: 0;
        }
    </style>
    <link rel="stylesheet" href="/smart-city/css/layer.css" id="layui_layer_skinlayercss" style="">
</head>
<body>
<div class="title">
    <div class="sub_title">智能城市停车</div>
</div>
<div class="row">
    <div class="col-sm-12">
        <#if code=="200">
            <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5 style="width: 100%;margin: auto;text-align: center;color: #ea394c">商户名称：${commercial.name}</h5>
            </div>
            <div class="ibox-content" id="searchForm">
                <div class="row">
                    <div class="col-sm-3">
                        <div class="input-group">
                            <input type="hidden" name="commercialID" id="commercialID" value="${commercial.id}">
                            <input type="hidden" name="state" id="state" value="${state}">
                            <input type="hidden" name="parkingLotId" id="parkingLotId" value="${commercial.parkingLotId}">
                            <input type="text" id="carNumber" name="carNumber" onkeyup="this.value=this.value.toUpperCase()" placeholder="请输入车牌号" class="input-sm form-control required" data-valid="isNonEmpty||isCarNo" data-error="不能为空||车牌号不正确"> <span class="input-group-btn">
                                    <button id="btn_search" type="button" class="btn btn-sm btn-primary"> 搜索</button> </span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- start -->
            <div class="wrapper wrapper-content animated fadeInRight">
                <div class="row col-sm-12" id="data-view"></div>
                <script id="data-tmpl" type="text/x-jquery-tmpl">
				                   <div class="row col-sm-12">
	                               <div class="contact-box no-padding">
									<div>
			                            <div class="text-center">
											<img alt="image" class="text-center m-t-xs img-responsive" style="height:160px; width:100%; padding:0 5px;" src="{{= comeImagePath}}">
			                            </div>
			                        </div>
			                       		 <div class="ibox-content profile-content" style="padding:0px 5px;">
											<div class="row">
													<div class="col-sm-7 text-left" style="padding-top:5px;"><strong>入场时间：{{= comeInTime}}</strong></div>
													<div class="col-sm-5 text-left">
														<div><strong>车库名称：{{= parkingName}}</strong></div>
														<div><strong>车牌号码：{{= license}}</strong></div>
														<div><strong>减免分钟：{{= singleReduction}}分钟</strong></div>
														<div><strong>有效时至：{{= validTime}}</strong></div>
													</div>
                                                    <input type="hidden" name="singleReduction" id="singleReduction" value="{{= singleReduction}}">
												<div class="col-sm-7 text-right" style="padding-bottom:5px; padding-right:20px">
													<button class="btn btn-xs btn-primary btn_pay" type="button" data-id="{{= id}}">确定支付</button>
												</div>
											</div>
										 </div>
									</div>
	                         	</div>
                </script>
            </div>
            <!-- end -->
        </div>
            <#else>
            <div class="text">
                <div class="text">
                    <div class="car_number">发生错误</div>
                </div>
                <div class="text">
                    <div class="car_context">${msg}</div>
                </div>
            </div>
        </#if>
    </div>
</div>

</body>
</html>
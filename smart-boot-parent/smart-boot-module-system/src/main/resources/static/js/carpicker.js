/** 
 *
 * var picker = new CarnoPicker(
 *      target: '#targetID',           目标元素ID（如：文本框，该值为城市名称）
 * );
 *
 * picker.init();
 */

var CarnoPicker = function (target) {

    this.template = $('<div id="laycarno_box" class="laycarno_box" style="position: absolute;"><ul class="laycarno_province" id="laycarno_province"><li>京</li><li>津</li><li>沪</li><li>冀</li><li>豫</li><li>云</li><li>辽</li><li>黑</li>'
        + '<li>湘</li><li>皖</li><li>鲁</li><li>新</li><li>苏</li><li>浙</li><li>赣</li><li>鄂</li><li>桂</li><li>甘</li><li>晋</li><li>蒙</li><li>陕</li><li>吉</li><li>闽</li><li>贵</li><li>粤</li><li>川</li><li>青</li><li>藏</li>'
        + '<li>琼</li><li>宁</li><li>渝</li></ul><ul class="laycarno_char" id="laycarno_char" style="display: none;"><li>A</li><li>B</li><li>C</li><li>D</li><li>E</li><li>F</li><li>G</li><li>H</li><li>I</li><li>J</li><li>K</li>'
        + '<li>L</li><li>M</li><li>N</li><li>O</li><li>P</li><li>Q</li><li>R</li><li>S</li><li>T</li><li>U</li><li>V</li><li>W</li><li>X</li><li>Y</li><li>Z</li></ul></div>');

    this.province = $('#laycarno_province', this.template);
    this.char = $('#laycarno_char', this.template);
    this.target = $(target);
};

CarnoPicker.prototype = {
    init: function () {
        var that = this;

        $(window).click(function (event) { 
            that.template.remove();
        });
         
        that.targetEvent();
    },
     
    provinceEvent: function () {
        var that = this;

        that.province.off('click').on('click', 'li', function (event) {
            event.preventDefault();
            event.stopPropagation();
              
            $(this).parent().hide();
            $(this).parent().next().show();

            that.target.val($(this).html());
            
            return false;
        });
    },

    charEvent: function () {
        var that = this;

        that.char.off('click').on('click', 'li', function (event) {
            event.preventDefault();
            event.stopPropagation();
            
            that.target.val(that.target.val() + $(this).html()).focus();
             
            that.template.remove();

            return false;
        });
         
    },
     
    targetEvent: function () {
        var that = this;
        
        that.target.off('click').click(function (event) {
            event.preventDefault();
            event.stopPropagation();

            that.province.show();
            that.char.hide();
            that.provinceEvent();
            that.charEvent();

            var _this = $(this);
            var offset = _this.offset();
            var top = offset.top + _this.outerHeight();

            that.template.css({
                'left': offset.left,
                'top': top
            });

            $('body').append(that.template);

            return false;

        }).blur(function (event) {
             
            $(this).val($(this).val().toUpperCase());
        });

    }
};



var pager = {
    init: function () {
        this.bindEvent();
    },
    bindEvent: function () {
        //查询
        $("#btn_search").click(function () {
            var param = $("#searchForm").formToJSON(true, function (data) {
                return data;
            });
            $.ajax({
                type: 'post',
                url: '/smart-city/parking/pay/listCar',
                dataType: 'json',
                data: param,
                success: function (data) {
                    if (data.code === 200) {
                        $("#data-view").html($('#data-tmpl').tmpl(data.result));
                        var _url = '/smart-city/parking/pay/payCar';

                        //支付
                        $(".btn_pay").click(function () {
                            //获取表格选择行
                            var infoCollectId = this.attributes["data-id"].nodeValue;
                            var param = {};
                            param.infoCollectId = infoCollectId;
                            param.commercialID = $("#commercialID").val();
                            param.parkingLotId = $("#parkingLotId").val();
                            param.singleReduction = $("#singleReduction").val();
                            param.state = $("#state").val();
                            layer.confirm('确认减免？', {
                                btn: ['确定', '取消'], icon: 3
                            }, function () {
                                $.ajax({
                                    type: 'post',
                                    url: _url,
                                    dataType: 'json',
                                    data: param,
                                    success: function (json) {
                                        if (json.status) {
                                            layer.msg(json.msg, {icon: 1, time: 2000}, function () {
                                                window.location.href = "./pay/merchant/wapPay.jsp";
                                            });
                                        } else {
                                            layer.msg(json.msg, {icon: 1, time: 2000}, function () {
                                            });
                                        }
                                    },
                                    error: function () {
                                        layer.msg('系统错误', {icon: 2, shift: 6, btn: ['确定']});
                                    }
                                });

                            }, function () {
                            });
                        });
                    } else {
                        layer.msg(data.message, {icon: 2, shift: 1, btn: ['确定']});
                    }
                },
                error: function (err) {
                    layer.msg('系统错误', {icon: 2, shift: 6, btn: ['确定']});
                }
            });
        });
    }
};

window.onload = function () {
    var cnPicker = new CarnoPicker('#carNumber');
    pager.init();
    cnPicker.init();
};
var tty = tty || {};


/**
 * 格式化小数
 */
tty.formatNumber = function formatNumber(number, precision) {
	if (number != null) {
		var f = parseFloat(number);
        if (isNaN(f)) {
            return;
        }
		var b = 1;
	    if (isNaN(number)) return number;
	    if (number < 0) b = -1;
	    var multiplier = Math.pow(10, precision);
	    return Math.round(Math.abs(number) * multiplier) / multiplier * b;
	}
}

/**
 * 格式化日期 yyyy-mm-dd
 */
tty.formatdate = function formatdate(val) {
	if (val != null) {
		var now = new Date(val);
		var M = (now.getMonth() + 1);
		var D = now.getDate();
		var clock = '';
		if (M < 10)
			clock += '0';
		clock += M + '-';
		if (D < 10)
			clock += '0';
		clock += D;

		return now.getFullYear() + '-' + clock;
	}
}

/**
 * 格式化日期 yyyy-mm-dd
 */
tty.date = function formatterdate(val) {
	if (val != null) {
		var now = new Date(val);
		var M = (now.getMonth() + 1);
		var D = now.getDate();
		var clock = '';
		if (M < 10)
			clock += '0';
		clock += M + '-';
		if (D < 10)
			clock += '0';
		clock += D;

		return now.getFullYear() + '-' + clock;
	}
}

/**
 * 格式化日期时间
 */
tty.dateTime = function formatterdate(val) {
	if (val != null) {
		var now = new Date(val);
		var M = (now.getMonth() + 1);
		var D = now.getDate();
		var hh = now.getHours();
		var mm = now.getMinutes();
		var ss = now.getTime() % 60000;
		ss = (ss - (ss % 1000)) / 1000;
		var clock = '';
		if (M < 10)
			clock += '0';
		clock += M + '-';
		if (D < 10)
			clock += '0';
		clock += D + ' ';

		if (hh < 10)
			clock += '0';
		clock += hh + ':';
		if (mm < 10)
			clock += '0';
		clock += mm + ':';
		if (ss < 10)
			clock += '0';
		clock += ss;

		return now.getFullYear() + '-' + clock;
	}
}

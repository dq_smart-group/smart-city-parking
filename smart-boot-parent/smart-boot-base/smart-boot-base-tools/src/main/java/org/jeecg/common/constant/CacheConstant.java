package org.jeecg.common.constant;

/**
 * @author: huangxutao
 * @date: 2019-06-14
 * @description: 缓存常量
 */
public interface CacheConstant {

	/**
	 * 字典信息缓存（含禁用的字典项）
	 */
    public static final String SYS_DICT_CACHE = "sys:cache:dict";

	/**
	 * 字典信息缓存 status为有效的
	 */
	public static final String SYS_ENABLE_DICT_CACHE = "sys:cache:dictEnable";
	/**
	 * 表字典信息缓存
	 */
    public static final String SYS_DICT_TABLE_CACHE = "sys:cache:dictTable";
	public static final String SYS_DICT_TABLE_BY_KEYS_CACHE = SYS_DICT_TABLE_CACHE + "ByKeys";

	/**
	 * 数据权限配置缓存
	 */
    public static final String SYS_DATA_PERMISSIONS_CACHE = "sys:cache:permission:datarules";

	/**
	 * 缓存用户信息
	 */
	public static final String SYS_USERS_CACHE = "sys:cache:user";

	/**
	 * 全部部门信息缓存
	 */
	public static final String SYS_DEPARTS_CACHE = "sys:cache:depart:alldata";


	/**
	 * 全部部门ids缓存
	 */
	public static final String SYS_DEPART_IDS_CACHE = "sys:cache:depart:allids";


	/**
	 * 测试缓存key
	 */
	public static final String TEST_DEMO_CACHE = "test:demo";

	/**
	 * 字典信息缓存
	 */
	public static final String SYS_DYNAMICDB_CACHE = "sys:cache:dbconnect:dynamic:";

	/**
	 * gateway路由缓存
	 */
	public static final String GATEWAY_ROUTES = "sys:cache:cloud:gateway_routes";


	/**
	 * gateway路由 reload key
	 */
	public static final String ROUTE_JVM_RELOAD_TOPIC = "gateway_jvm_route_reload_topic";

	/**
	 * TODO 冗余代码 待删除
	 *插件商城排行榜
	 */
	public static final String PLUGIN_MALL_RANKING = "pluginMall::rankingList";
	/**
	 * TODO 冗余代码 待删除
	 *插件商城排行榜
	 */
	public static final String PLUGIN_MALL_PAGE_LIST = "pluginMall::queryPageList";


	/**
	 * online列表页配置信息缓存key
	 */
	public static final String ONLINE_LIST = "sys:cache:online:list";

	/**
	 * online表单页配置信息缓存key
	 */
	public static final String ONLINE_FORM = "sys:cache:online:form";

	/**
	 * online报表
	 */
	public static final String ONLINE_RP = "sys:cache:online:rp";

	/**
	 * online图表
	 */
	public static final String ONLINE_GRAPH = "sys:cache:online:graph";


	/*********************************************************************************************************/

	/**
	 * 系统配置
	 */
	public static final String SYS_CONFIG = "sys:config:";


	/**
	 * 项目请求路径配置
	 */
	public static final String PROJECT_REQUEST_PATH_CONFIG = "projectRequestPathConfig:";

	/**
	 * 当前配置环境
	 */
	public static final String IS_ENVIRONMENT = SYS_CONFIG+PROJECT_REQUEST_PATH_CONFIG+"is_environment";

	/**
	 * parking请求路由
	 */
	public static final String PARKING_PATH = SYS_CONFIG+PROJECT_REQUEST_PATH_CONFIG+"parking_path";

	/**
	 * smoke请求路由
	 */
	public static final String SMOKE_PATH = SYS_CONFIG+PROJECT_REQUEST_PATH_CONFIG+"smoke_path";

	/**
	 * pay请求路由
	 */
	public static final String PAY_PATH = SYS_CONFIG+PROJECT_REQUEST_PATH_CONFIG+"pay_path";

	/**
	 * city请求路由
	 */
	public static final String CITY_PATH = SYS_CONFIG+PROJECT_REQUEST_PATH_CONFIG+"city_path";
	/**
	 * mqtt请求路由
	 */
	public static final String MQTT_PATH = SYS_CONFIG+PROJECT_REQUEST_PATH_CONFIG+"mqtt_path";


	/*********************************************************************************************************/

	/**
	 * 微信支付配置
	 */
	public static final String WX_PAY_CONFIG = "wxPayConfig:";

	/**
	 * 服务商应用ID
	 */
	public static final String WX_APPID = SYS_CONFIG+WX_PAY_CONFIG+"WX_APPID";

	/**
	 * 服务商户号
	 */
	public static final String WX_MCHID = SYS_CONFIG+WX_PAY_CONFIG+"WX_MCHID";

	/**
	 * wechatpay.pem证书内容
	 */
	public static final String WX_WECHATPAYKEY = SYS_CONFIG+WX_PAY_CONFIG+"WX_WECHATPAYKEY";

	/**
	 * V3接口
	 */
	public static final String WX_PRIVATEKEY = SYS_CONFIG+WX_PAY_CONFIG+"WX_PRIVATEKEY";

	/**
	 * native支付请求链接
	 */
	public static final String WX_NATIVEV3API = SYS_CONFIG+WX_PAY_CONFIG+"WX_NATIVEV3API";

	/**
	 * 异步回调通知路径
	 */
	public static final String WX_NOTIFYUURL = SYS_CONFIG+WX_PAY_CONFIG+"WX_NOTIFYUURL";

	/**
	 * api V3密钥
	 */
	public static final String WX_APIV3KEY = SYS_CONFIG+WX_PAY_CONFIG+"WX_APIV3KEY";


	/**
	 * 微信支付通知请求域名
	 */
	public static final String  WX_PAY_HOST = SYS_CONFIG+WX_PAY_CONFIG+"WX_PAY_HOST";

	/**
	 * 微信退款通知请求域名
	 */
	public static final String  WX_REFUND_HOST = SYS_CONFIG+WX_PAY_CONFIG+"WX_REFUND_HOST";

	/*********************************************************************************************************/




	/*********************************************************************************************************/

	/**
	 * 东莞静态交通配置
	 */
	public static final String DONG_GUAN_STATIC_TRAFFIC = "dongGuanStaticTraffic:";

	/**
	 * 停车场基础信息接口
	 */
	public static final String DONG_GUAN_PARKING_BASIS_INFO = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_PARKING_BASIS_INFO";

	/**
	 * 停车场通道（岗亭）基础信息接口
	 */
	public static final String DONG_GUAN_PARKING_CHANNEL_BASIS_INFO = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_PARKING_CHANNEL_BASIS_INFO";

	/**
	 * 停车场车辆进出场信息接口
	 */
	public static final String DONG_GUAN_PARKING_IN_OUT_BASIS_INFO = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_PARKING_IN_OUT_BASIS_INFO";

	/**
	 * 创建图片文件接口
	 */
	public static final String DONG_GUAN_CREATE_IMAGE_FILE = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_CREATE_IMAGE_FILE";

	/**
	 * 上传进出场车牌图片流接口
	 */
	public static final String DONG_GUAN_IN_OUT_PLATE_NUMBER_IMAGE_STREAM = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_IN_OUT_PLATE_NUMBER_IMAGE_STREAM";

	/**
	 * 路段基础信息接口
	 */
	public static final String DONG_GUAN_ROAD_BASIS_INFO = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_ROAD_BASIS_INFO";

	/**
	 * 道路泊位车辆进出信息接口
	 */
	public static final String DONG_GUAN_ROAD_IN_OUT_INFO = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_ROAD_IN_OUT_INFO";

	/**
	 * 剩余泊位同步接口(必须传)
	 */
	public static final String DONG_GUAN_SURPLUS_SYNC = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_SURPLUS_SYNC";

	/**
	 * 系统心跳检测接口
	 */
	public static final String DOMG_GUAN_SYSTEM_HEART_DANCE = DONG_GUAN_STATIC_TRAFFIC + "DOMG_GUAN_SYSTEM_HEART_DANCE";

	/**
	 * 测试环境请求域名
	 */
	public static final String DONG_GUAN_DEV_REQUEST_HOST = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_DEV_REQUEST_HOST";

	/**
	 * 生产环境请求域名
	 */
	public static final String DONG_GUAN_PROD_REQUEST_HOST = DONG_GUAN_STATIC_TRAFFIC +"DONG_GUAN_PROD_REQUEST_HOST";

	/**
	 * 当前环境
	 */
	public static final String DONG_GUAN_IS_ENVIRONMENT = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_IS_ENVIRONMENT";


	/**
	 * 业主id
	 */
	public static final String DONG_GUAN_COM_ID = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_COM_ID";

	/**
	 * 调用者id
	 */
	public static final String DONG_GUAN_APP_ID = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_APP_ID";

	/**
	 * requestKey
	 */
	public static final String DONG_GUAN_REQUEST_KEY = DONG_GUAN_STATIC_TRAFFIC + "DONG_GUAN_REQUEST_KEY";


}

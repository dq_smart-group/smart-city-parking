package org.jeecg.common.constant;

public interface ConfigConstant {

    /**
     * 基础配置
     */
    String SYS_CONFIG = "sys-config";
    /**
     * 公众号配置
     */
    String WEIXIN_MP = "weixin-mp";
    /**
     * 公众号支付配置
     */
    String WEIXIN_PAY = "weixin-pay";
    /**
     * 联通接口配置
     */
    String UNICOM_CONFIG = "unicom_config";

    /** 公众号配置 begin*/

    /**
     * 公众号名称
     */
    String WX_NAME = "wx_name";
    /**
     * 微信号
     */
    String WX_NUMBER = "wx_number";
    /**
     * wx_id
     */
    String WX_ID = "wx_id";
    /**
     * wx_appid
     */
    String WX_APPID = "wx_appid";
    /**
     * AppSecret
     */
    String WX_APP_SECRET = "wx_app_secret";
    /**
     * 微信验证TOKEN
     */
    String WX_TOKEN = "wx_token";
    /**
     * 消息加密方式
     */
    String WX_ENCODE = "wx_encode";
    /**
     * EncodingAESKey
     */
    String WX_ENCODINGAESKEY = "wx_encodingaeskey";
    /**
     *  公众号二维码
     */
    String WX_QRCODE = "wx_qrcode";
    /**
     * 公众号类型
     */
    String WX_TYPE = "wx_type";
    /**
     * wx_api
     */
    String WX_API = "wx_api";
    /**
     * 公众号logo
     */
    String WX_AVATAR = "wx_avatar";

    /** 公众号配置 end*/

    /** 微信支付 begin */
    /**
     * AppID
     */
    String PAY_WX_APPID = "pay_wx_appid";
    /**
     * AppSecret
     */
    String PAY_WX_APPSECRET = "pay_wx_appsecret";
    /**
     * MchId
     */
    String PAY_WX_MCHID = "pay_wx_mchid";
    /**
     * 微信支付证书
     */
    String PAY_WX_CLIENT_CERT = "pay_wx_client_cert";
    /**
     * 支付秘钥MchKey
     */
    String PAY_WX_MCHKEY = "pay_wx_mchkey";
    /**
     * 是否开启微信支付
     */
    String PAY_WX_OPEN = "pay_wx_open";
    /**
     * 支付路径
     */
    String PAY_DIR = "pay_dir";
    /** 微信支付 end */

    /**
     * 代理
     */
    String BASE_PROXY = "base_proxy";
}

package org.jeecg.enums;

public enum SysConfigStatusEnum {

    XS(1,"显示"),

    YC(0,"隐藏")
    ;

    private Integer value;

    private String text;

    SysConfigStatusEnum(Integer val, String txt){
        this.value = val;
        this.text = txt;
    }

    public Integer getValue() {
        return value;
    }

    public String getText() {
        return text;
    }
}


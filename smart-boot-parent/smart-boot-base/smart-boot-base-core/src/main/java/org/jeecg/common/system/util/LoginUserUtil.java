package org.jeecg.common.system.util;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.jeecg.common.constant.CommonConstant;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.oConvertUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @author ：
 * Created by 16:31 on 2019/7/6.
 */
public class LoginUserUtil {


    /**
     * 获取当前登陆用户对象
     * @return
     */
    public static LoginUser getLoginUser(){
        Object obj = SecurityUtils.getSubject().getPrincipal();
        if(obj == null){
            throw new AuthenticationException("获取用户信息失败，用户未登录");
        }
        LoginUser sysUser = (LoginUser) obj;
        return sysUser;
    }

    /**
     * 获取登录人一级部门编码
     * @return
     */
    public static String getOrgCode(){
        //获取部门编码
        String orgCode = getLoginUser().getOrgCode();
        if(orgCode.length()>3){
            orgCode = orgCode.substring(0,3);
        }
        return orgCode;
    }

    /**
     * 用户主键
     * @return
     */
    public static String getLoginUserId(){
        return getLoginUser().getId();
    }

    /**
     * 获取用户名称
     * @return
     */
    public static String getUserName(){
        return getLoginUser().getUsername();
    }

    /**
     * 是否为超级管理员
     * @return
     */
    public static Boolean isAdmin(){
        Integer grade = getLoginUser().getUserFlag();
        return Integer.valueOf(1).equals(grade);
    }

//    /**
//     * 验证登录返回username
//     * @param request
//     * @param redisUtil
//     * @return
//     */
//    public static String checkOutUerName(HttpServletRequest request, RedisUtil redisUtil){
//        String token = JwtUtil.getToken(request);
//        String cacheToken = String.valueOf(redisUtil.get(CommonConstant.PREFIX_USER_TOKEN + token));
//        if (oConvertUtils.isEmpty(cacheToken)) {
//            return null;
//        }
//        return JwtUtil.getUsername(token);
//    }
}
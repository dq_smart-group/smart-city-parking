package org.jeecg.config.authentication;

import io.micrometer.core.instrument.util.JsonUtils;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.util.RedisUtil;
import org.jeecg.common.util.oConvertUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AccessInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 如果不是映射到方法直接通过
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }

        //获取方法中的注解,看是否有该注解,用于防止接口被恶意调用多次
        AccessLimit accessLimit = ((HandlerMethod) handler).getMethodAnnotation(AccessLimit.class);
        if(null == accessLimit){
            return true;
        }
        int seconds = accessLimit.seconds();
        int maxCount = accessLimit.maxCount();

        //获取用户账号
        String userName = JwtUtil.getUserNameByToken(request);
        if (oConvertUtils.isEmpty(userName)) {
            return false;
        }

        String key ="blacklist:user:"+userName;
        Object count =  redisUtil.get(key);
        if (null == count) {
            redisUtil.set(key, "1",seconds);
            return true;
        }
        long expire = redisUtil.getExpire(key);

        if (Integer.parseInt(count.toString()) < maxCount) {
            count = String.valueOf(Integer.parseInt(count.toString())+1);
            redisUtil.set(key, count,expire);
            return true;
        }

        if (Integer.parseInt(count.toString()) >= maxCount) {
//            //response 返回 json 请求过于频繁请稍后再试
            response.setStatus(401);
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json; charset=utf-8");
            response.getWriter().write(JsonUtils.prettyPrint("操作频繁，请稍后重试"));
            return false;
        }
        return true;
    }

}

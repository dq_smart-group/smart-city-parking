package org.jeecg.config.authentication.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class MapBuild {


    private static Map<String,String> hashMapBuild = new HashMap<>();

    private static TreeMap<String,String> treeMapBuild = new TreeMap<String,String>();

    private MapBuild(){

    }

    public static Map<String,String> hashMapBuild(){
        hashMapBuild.clear();
        return hashMapBuild;
    }


    public static TreeMap<String,String> treeMapBuild(){
        treeMapBuild.clear();
        return treeMapBuild;
    }

    public static void clear(){
        hashMapBuild.clear();
        treeMapBuild().clear();
    }



}

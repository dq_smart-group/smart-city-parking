package org.jeecg.config.authentication;


import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * 定义接口恶意请求多次注解
 */
@Retention(RUNTIME)//表示它在运行时
@Target(METHOD) //表示它只能放在方法上
@SuppressWarnings("all")
public @interface AccessLimit {

    int seconds();//规定几秒

    int maxCount();//最大请求数

}

package org.jeecg.config.authentication.quartz;

import lombok.extern.slf4j.Slf4j;
import org.jeecg.config.authentication.utils.SmokeAuthentication;
import org.jeecg.config.sign.util.BodyReaderHttpServletRequestWrapper;
import org.jeecg.config.sign.util.HttpUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.SortedMap;

@Slf4j
public class OpenQuartzApiHandlerInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.info("request URI = " + request.getRequestURI());
        HttpServletRequest requestWrapper = new BodyReaderHttpServletRequestWrapper(request);
        //获取全部参数(包括URL和body上的)
        SortedMap<String, String> allParams = HttpUtils.getAllParams(requestWrapper);
        //将参数set进request中
        request.setAttribute("allParams",allParams.get("parameter"));
        return SmokeAuthentication.ygSmokeAuthentication(allParams);
//        return true;
    }

}

package org.jeecg.config.sign.interceptor;

import org.jeecg.config.authentication.AccessInterceptor;
import org.jeecg.config.authentication.quartz.OpenQuartzApiHandlerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 签名 拦截器配置
 */
@Configuration
public class SignAuthConfiguration implements WebMvcConfigurer {
    public static String[] urlList = new String[] {"/sys/dict/getDictItems/*", "/sys/dict/loadDict/*",
            "/sys/dict/loadDictOrderByValue/*", "/sys/dict/loadDictItem/*", "/sys/dict/loadTreeData",
            "/sys/api/queryTableDictItemsByCode", "/sys/api/queryFilterTableDictInfo", "/sys/api/queryTableDictByKeys",
            "/sys/api/translateDictFromTable", "/sys/api/translateDictFromTableByKeys"};


    //定时任务请求
    public static String[] quartzApi = new String[]{"/quartz/openapi/**"};


    @Bean
    public SignAuthInterceptor signAuthInterceptor() {
        return new SignAuthInterceptor();
    }
    @Bean
    public AccessInterceptor accessInterceptor() {
        return new AccessInterceptor();
    }

    @Bean
    public OpenQuartzApiHandlerInterceptor openQuartzApiHandlerInterceptor(){
        return new OpenQuartzApiHandlerInterceptor();
    }


    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(signAuthInterceptor()).addPathPatterns(urlList);
        //排除swagger拦截
        registry.addInterceptor(accessInterceptor())
                .excludePathPatterns("/swagger-resources/**", "/webjars/**", "/v2/**", "/swagger-ui.html/**");
//        registry.addInterceptor(openQuartzApiHandlerInterceptor()).addPathPatterns(quartzApi);
    }
}

package org.jeecg.config.authentication.utils;

import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;

import java.util.*;

public class SignUtil {

    /**
     * HashMap转TreeMap
     * @param map
     * @return
     */
    public static TreeMap<String, String> mapToTreeMap(Map<String,String> map){
        TreeMap<String, String> mapBuild = MapBuild.treeMapBuild();
        for (String key : map.keySet()) {
            mapBuild.put(key,map.get(key));
        }
        return mapBuild;
    }


    /**
     * HashMap转TreeMap
     * @param treeMap map
     * @return
     */
    public static Map<String ,String> threeMapToMap(TreeMap<String, String> treeMap){
        Map<String, String> mapBuild = MapBuild.hashMapBuild();
        for (String key : treeMap.keySet()) {
            mapBuild.put(key,treeMap.get(key));
        }
        return mapBuild;
    }


    /**
     * 签名
     * @param mapParam
     * @return
     */
    public static String sign(TreeMap<String, String> mapParam , String accessKey , String accessSecret) {
        String paramStr = getMapToString(mapParam);
        String signParam = paramStr + accessKey + accessSecret + paramStr;
        return SecureUtil.sha1(signParam).toUpperCase();
    }

    /**
     * Map转String
     *
     * @param map
     * @return
     */
    public static String getMapToString(Map<String, String> map) {
        Set<String> keySet = map.keySet();
        //将set集合转换为数组
        String[] keyArray = keySet.toArray(new String[keySet.size()]);
        //给数组排序(升序)
        Arrays.sort(keyArray);
        //因为String拼接效率会很低的，所以转用StringBuilder
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keyArray.length; i++) {
            // 参数值为空，则不参与签名 这个方法trim()是去空格
                sb.append(keyArray[i]).append("=").append(String.valueOf(map.get(keyArray[i])).trim());
        }
        return sb.toString();
    }


    /**
     * 是否为json
     * @param str
     * @return
     */
    public static boolean isJSON(String str) {
        boolean result = false;
        try {
            Object obj = JSON.parse(str);
            result = true;
        } catch (Exception e) {
            result = false;
        }
        return result;
    }

    /**
     * 获取请求参数
     * @param param
     * @return
     */
    public static Map<String, String> getUrlParams(String param) {
        Map<String, String> map = new HashMap<>(6);
        String[] params = param.split("&");
        for (int i = 0; i < params.length; i++) {
            String[] p = params[i].split("=");
            if (p.length == 2) {
                map.put(p[0], p[1]);
            } else if (p.length == 1){
                map.put(p[0],"");
            }
        }
        return map;
    }


}
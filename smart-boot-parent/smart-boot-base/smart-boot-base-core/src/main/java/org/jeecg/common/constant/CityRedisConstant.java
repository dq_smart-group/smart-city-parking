package org.jeecg.common.constant;

public interface CityRedisConstant {

    /**
     * 缓存第一级别键
     */
    public static final String CITY_REDIS = "sys:city:";


    /**
     * 公司信息
     */
    public static final String SYS_DEPART = CITY_REDIS + "depart:";


//    /**
//     * 应用信息
//     */
//    public static final String SYS_CITY_APP = CITY_REDIS + "app:";
//
//
//    /**
//     * 角色信息
//     */
//    public static final String SYS_CITY_ROLE = CITY_REDIS + "role:";

}

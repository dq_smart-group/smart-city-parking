package org.jeecg.common.constant;

public interface ParkingRedisConstant {

    public static final String PARKING = "parking:";

    /**
     * 白名单
     */
    public static final String PARKING_WHITE_LIST = "parking:white:";

    /**
     * 收费标准
     */
    public static final String PARKING_STANDARD = "parking:standard:";
    /**
     * led显示屏信息
     */
    public static final String PARKING_LED = "parking:led:";
    /**
     * 时间段管理信息
     */
    public static final String PARKING_TIME_SLOT = "parking:timeslot:";
    /**
     * 车场信息
     */
    public static final String PARKING_LOT = "parking:lot:";
    /**
     * 车辆类型信息
     */
    public static final String PARKING_TYPE = "parking:type:";
    /**
     * 设备信息
     */
    public static final String PARKING_DEVICE = "parking:device:";

    /**
     * 车牌信息
     */
    public static final String LICENSE_PLATE = "parking:licenseplate:";
    /**
     * 无牌进场时临时存放数据
     */
    public static final String PARKING_UNLICENSED = "parking:unlicensed:";


    /**
     * 抓拍设备
     */
    public static final String MAKE_FLAG = "parking:devicezhuapai:";

    /**
     * 收费员信息
     */
    public static final String CAR_CHARGE = "parking:carCharge:";


    /**
     * 储存图片的键
     */
    public static final String PARKING_IMG = "parking:img:";


    /**
     *
     */
    public static final String PARKING_HTTPIMG = "parking:httpimg:";


    /**
     * 储存级联设备组
     */
    public static final String PARKING_DEVICE_GROUP = "parking:device:group:";

    /**
     * 储存级联设备
     */
    public static final String PARKING_DEVICE_EQUIPMENT = "parking:device:equipment:";
    /**
     * 储存公司信息
     */
    public static final String SYS_DEPART = "sys:depart:";

    /**
     * 储存设备心跳
     */
    public static final String PARKING_DEVICE_HEARTBEAT = "parking:device:heartbeat:";

}

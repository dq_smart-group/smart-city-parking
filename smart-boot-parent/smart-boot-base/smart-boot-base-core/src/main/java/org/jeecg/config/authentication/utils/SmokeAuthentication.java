package org.jeecg.config.authentication.utils;

import cn.hutool.core.lang.Assert;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.TreeMap;

@Slf4j
public class SmokeAuthentication {


    @Value("${jeecg-quartz-authentication}")
    private static String quartzAuthentication;

    private static String authenticationKey = "sign";

    public static Boolean ygSmokeAuthentication(Map<String,String> map){
        try {
            if(map.size() == 0){
                Assert.isTrue(false,"参数异常");
            }
            String paramSign = map.get(authenticationKey);
            if(StringUtils.isEmpty(paramSign)){
                Assert.isTrue(false,"参数异常");
            }
            //移除掉
            map.remove(authenticationKey);

            TreeMap<String, String> treeMap = SignUtil.mapToTreeMap(map);
            String sign = SignUtil.sign(treeMap, quartzAuthentication, quartzAuthentication);
            //判断是否相等
            if(!paramSign.equals(sign)){
                Assert.isTrue(false,"非法调用");
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;
    }
}
